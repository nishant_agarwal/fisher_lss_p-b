!-----------------------------------------------------------------------------
! Module to obtain the power spectrum for different values of nrun
!-----------------------------------------------------------------------------

MODULE pk_nrun
  
  IMPLICIT none
  
  DOUBLE PRECISION, dimension(:), allocatable :: xlin,ylinm,ylinp,y2linm,y2linp
  DOUBLE PRECISION, dimension(:,:), allocatable :: ynlm,ynlp,ynlsignm,ynlsignp,y2nlm,y2nlp
  INTEGER :: jlo,ndata
  
contains

!-----------------------------------------------------------------------------
  
SUBROUTINE open_pk_nrun(filename_m,filename_p,n,D)

  IMPLICIT none

  DOUBLE PRECISION, intent(IN) :: D
  INTEGER, intent(IN) :: n
  DOUBLE PRECISION, dimension(:), allocatable :: klin,pklinm,pklinp
  DOUBLE PRECISION, dimension(:,:), allocatable :: pknlm,pknlp
  DOUBLE PRECISION :: dummy
  INTEGER :: i,j
  CHARACTER(len=128) :: filename_m,filename_p

  ndata = n
  ALLOCATE (xlin(ndata),ylinm(ndata),ylinp(ndata),y2linm(ndata),y2linp(ndata))
  ALLOCATE (ynlm(28,ndata),ynlp(28,ndata),ynlsignm(28,ndata),ynlsignp(28,ndata)&
       ,y2nlm(28,ndata),y2nlp(28,ndata))
  ALLOCATE (klin(ndata),pklinm(ndata),pklinp(ndata),pknlm(28,ndata),pknlp(28,ndata))

  open(32,file=filename_m,status='old')
  print*, 'Read in '//trim(filename_m)
  do i = 1, ndata
     read(32,*) klin(i), pklinm(i), pknlm(1:28,i) ! k and P(k), linear and nonlinear
  enddo
  close(32)

  open(33,file=filename_p,status='old')
  print*, 'Read in '//trim(filename_p)
  do i = 1, ndata
     read(33,*) klin(i), pklinp(i), pknlp(1:28,i) ! k and P(k), linear and nonlinear
  enddo
  close(33)
  
  xlin = dlog(klin)
  ylinm = dlog((D**2.d0)*pklinm)
  ylinp = dlog((D**2.d0)*pklinp)
  do i = 1, ndata
     ynlm(1:5,i) = dlog((D**2.d0)*abs(pknlm(1:5,i)))
     ynlm(6:28,i) = dlog((D**4.d0)*abs(pknlm(6:28,i)))
     ! store the sign but do the interpolation in log of the absolute value
     ynlsignm(1:28,i) = pknlm(1:28,i)/(abs(pknlm(1:28,i)))
     ! I4(k) has positive and negative values, so do linear interpolation
     ynlm(4,i) = (D**2.d0)*pknlm(4,i)
     ynlsignm(4,i) = 1.d0
     ynlp(1:5,i) = dlog((D**2.d0)*abs(pknlp(1:5,i)))
     ynlp(6:28,i) = dlog((D**4.d0)*abs(pknlp(6:28,i)))
     ! store the sign but do the interpolation in log of the absolute value
     ynlsignp(1:28,i) = pknlp(1:28,i)/(abs(pknlp(1:28,i)))
     ! I4(k) has positive and negative values, so do linear interpolation
     ynlp(4,i) = (D**2.d0)*pknlp(4,i)
     ynlsignp(4,i) = 1.d0
  enddo
  
  DEALLOCATE (klin,pklinm,pklinp,pknlm,pknlp)
  CALL spline(xlin,ylinm,ndata,1.d30,1.d30,y2linm)
  do i = 1, 28
     CALL spline(xlin,ynlm(i,:),ndata,1.d30,1.d30,y2nlm(i,:))
  enddo
  CALL spline(xlin,ylinp,ndata,1.d30,1.d30,y2linp)
  do i = 1, 28
     CALL spline(xlin,ynlp(i,:),ndata,1.d30,1.d30,y2nlp(i,:))
  enddo
  return
  
END SUBROUTINE open_pk_nrun

!-----------------------------------------------------------------------------

SUBROUTINE close_pk_nrun

  DEALLOCATE (xlin,ylinm,ylinp,y2linm,y2linp)
  DEALLOCATE (ynlm,ynlp,ynlsignm,ynlsignp,y2nlm,y2nlp)
  return

END SUBROUTINE close_pk_nrun

!-----------------------------------------------------------------------------

DOUBLE PRECISION FUNCTION pklin_nrunm(klin)
  IMPLICIT none
  DOUBLE PRECISION :: a,b,h,x,y,klin
  x = dlog(klin)
  CALL hunt(xlin,ndata,x,jlo)
  h=xlin(jlo+1)-xlin(jlo)
  a=(xlin(jlo+1)-x)/h
  b=(x-xlin(jlo))/h
  y=a*ylinm(jlo)+b*ylinm(jlo+1)+((a**3-a)*y2linm(jlo)+(b**3-b)*y2linm(jlo+1))*(h**2)/6.
  pklin_nrunm = dexp(y)
  return
END FUNCTION pklin_nrunm

!-----------------------------------------------------------------------------

DOUBLE PRECISION FUNCTION pklin_nrunp(klin)
  IMPLICIT none
  DOUBLE PRECISION :: a,b,h,x,y,klin
  x = dlog(klin)
  CALL hunt(xlin,ndata,x,jlo)
  h=xlin(jlo+1)-xlin(jlo)
  a=(xlin(jlo+1)-x)/h
  b=(x-xlin(jlo))/h
  y=a*ylinp(jlo)+b*ylinp(jlo+1)+((a**3-a)*y2linp(jlo)+(b**3-b)*y2linp(jlo+1))*(h**2)/6.
  pklin_nrunp = dexp(y)
  return
END FUNCTION pklin_nrunp

!-----------------------------------------------------------------------------

DOUBLE PRECISION FUNCTION pknl_nrunm(inl,klin)
  IMPLICIT none
  INTEGER :: inl
  DOUBLE PRECISION :: a,b,h,x,y,klin
  x = dlog(klin)
  CALL hunt(xlin,ndata,x,jlo)
  h=xlin(jlo+1)-xlin(jlo)
  a=(xlin(jlo+1)-x)/h
  b=(x-xlin(jlo))/h
  y=a*ynlm(inl,jlo)+b*ynlm(inl,jlo+1)+((a**3-a)*y2nlm(inl,jlo)+(b**3-b)*y2nlm(inl,jlo+1))*(h**2)/6.
  if (inl.eq.4) then
     pknl_nrunm = ynlsignm(inl,jlo)*y
  else
     pknl_nrunm = ynlsignm(inl,jlo)*dexp(y)
  endif
  return
END FUNCTION pknl_nrunm

!-----------------------------------------------------------------------------

DOUBLE PRECISION FUNCTION pknl_nrunp(inl,klin)
  IMPLICIT none
  INTEGER :: inl
  DOUBLE PRECISION :: a,b,h,x,y,klin
  x = dlog(klin)
  CALL hunt(xlin,ndata,x,jlo)
  h=xlin(jlo+1)-xlin(jlo)
  a=(xlin(jlo+1)-x)/h
  b=(x-xlin(jlo))/h
  y=a*ynlp(inl,jlo)+b*ynlp(inl,jlo+1)+((a**3-a)*y2nlp(inl,jlo)+(b**3-b)*y2nlp(inl,jlo+1))*(h**2)/6.
  if (inl.eq.4) then
     pknl_nrunp = ynlsignp(inl,jlo)*y
  else
     pknl_nrunp = ynlsignp(inl,jlo)*dexp(y)
  endif
  return
END FUNCTION pknl_nrunp

!-----------------------------------------------------------------------------

END MODULE pk_nrun


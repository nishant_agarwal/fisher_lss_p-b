!-----------------------------------------------------------------------------
! Program to calculate projected errors on ln(Da), ln(H), Omega_c0, f, ns, and nrun
!    using the redshift-space 1-loop galaxy power spectrum and tree-level bispectrum

! Ref 1. N. Agarwal, D. Jeong, V. Desjacques, and F. Schmidt (arXiv:2007.04340)
! Ref 2. D. Jeong, V. Desjacques, and F. Schmidt (arXiv:1806.04015)
! Based on earlier codes available under 'List of Routines' on E. Komatsu's webpage
!    Also see M. Shoji, D. Jeong, and E. Komatsu (arXiv:0805.4238)

! Last modified by N. Agarwal on 06/28/2020
!-----------------------------------------------------------------------------

PROGRAM fisher
  
  USE cosmo
  USE growth
  USE angular_distance
  USE pk_linear
  USE pk_all
  USE pk22
  USE pk13
  USE pk_oc0
  USE pk_ns
  USE pk_nrun
  USE bk
  
  IMPLICIT none

  ! Constants
  
  double precision :: h = 0.6778d0
  integer :: npara = 28 ! # of parameters
  
  ! Arrays
  
  double precision, dimension(:), allocatable :: der,Jvector
  double precision :: shiftspk(6),shiftsbk(6)
  double precision, dimension(:,:), allocatable :: cov,fis,dummy,pkarray
  double precision, dimension(:), allocatable :: klist
  double precision, dimension(28) :: pknlcomps,pknlcompsoc0m,pknlcompsoc0p,pknlcompsnsm,pknlcompsnsp, &
       pknlcompsnrunm,pknlcompsnrunp,neffnlcomps
  double precision :: Imp(0:8,0:6),Impoc0m(0:8,0:6),Impoc0p(0:8,0:6),Impnsm(0:8,0:6),Impnsp(0:8,0:6), &
       Impnrunm(0:8,0:6),Impnrunp(0:8,0:6),neffImp(0:8,0:6)
  double precision :: Anmp(0:4,0:8,0:6),Anmpoc0m(0:4,0:8,0:6),Anmpoc0p(0:4,0:8,0:6), &
       dAnmpdf(0:4,0:8,0:6),dAnmpdb1(0:4,0:8,0:6), &
       dAnmpdbeta(0:4,0:8,0:6),dAnmpdb2(0:4,0:8,0:6),dAnmpdbK2(0:4,0:8,0:6), &
       dAnmpdbPi2P(0:4,0:8,0:6),dAnmpdbK2P(0:4,0:8,0:6),dAnmpdbdeta(0:4,0:8,0:6), &
       dAnmpdbeta2(0:4,0:8,0:6)
  double precision :: calIn(1:5),calInoc0m(1:5),calInoc0p(1:5),calInnsm(1:5),calInnsp(1:5), &
       calInnrunm(1:5),calInnrunp(1:5),neffIn(1:5)
  double precision :: C13ln(0:3,1:5),C13lnoc0m(0:3,1:5),C13lnoc0p(0:3,1:5), &
       dC13lndf(0:3,1:5),dC13lndb1(0:3,1:5), &
       dC13lndbK2(0:3,1:5),dC13lndbtd(0:3,1:5),dC13lndbeta(0:3,1:5), &
       dC13lndbPi2P(0:3,1:5),dC13lndbK2P(0:3,1:5),dC13lndbdeta(0:3,1:5), &
       dC13lndbeta2(0:3,1:5),dC13lndbdPi2P(0:3,1:5),dC13lndbetaPi2P(0:3,1:5), &
       dC13lndbKPi2P(0:3,1:5),dC13lndbPi3P(0:3,1:5)
  double precision :: LegP(0:6),dLegPdmu2(0:6)
  integer, dimension(:), allocatable :: imarg

  ! Functions
  
  double precision :: g,dgdlna,da,one_over_h,rombint
  external :: ds8dlnk,ds12dlnk

  ! Variables

  double precision :: ob0h2,oc0h2,on0h2,oc0m,oc0p,nsm,nsp,nrunm,nrunp,sigma8,rstar
  double precision :: kmaxpk,kmaxbk,k0,k,kF,k1,k2,k3,dlnk,dk1,dk2,dk3,k1p,k2p,k3p
  double precision :: z,zin,Vsurvey,ngal,D,Doc0m,Doc0p,fz,fzoc0m,fzoc0p,factor,wkmu,sB,sBpi
  double precision :: mu,mu1,mu2,mu3,dmu,phi,dphi,alpha
  double precision :: b1,beta,bd2d,betav1,betav2,Pe1,Pe2,Pe3 ! l+hd bias parameters
  double precision :: b2,bK2,btd,bPi2P,bK2P,bdeta,beta2,bdPi2P,betaPi2P,bKPi2P, &
       bPi3P ! 22+13 bias parameters
  double precision :: Be,Pe4,Pe5 ! bispectrum parameters
  double precision :: betatrue,btdtrue ! true bias parameters
  double precision :: pkL,pkLoc0m,pkLoc0p,pkLnsm,pkLnsp,pkLnrunm,pkLnrunp, &
       pkL1,pkL1oc0m,pkL1oc0p,pkL1nsm,pkL1nsp,pkL1nrunm,pkL1nrunp, &
       pkL2,pkL2oc0m,pkL2oc0p,pkL2nsm,pkL2nsp,pkL2nrunm,pkL2nrunp, &
       pkL3,pkL3oc0m,pkL3oc0p,pkL3nsm,pkL3nsp,pkL3nrunm,pkL3nrunp, &
       pknlc,pk,pkoc0m,pkoc0p,pknsm,pknsp,pknrunm,pknrunp,pk1,pk2,pk3, &
       pkmonoL,pkmononl,pkquadL,pkquadnl
  double precision :: neffL,neff,nkL1,nkL2,nkL3,dlnpkdmu2,dlnpkdf,dlnpkdb1, &
       dlnpkdbd2d,dlnpkdbetav1,dlnpkdPe1,dlnpkdPe2,dlnpkdPe3,dlnpkdb2,dlnpkdbK2, &
       dlnpkdbtd,dlnpkdbeta,dlnpkdbetav2,dlnpkdbPi2P,dlnpkdbK2P,dlnpkdbdeta, &
       dlnpkdbeta2,dlnpkdbdPi2P,dlnpkdbetaPi2P,dlnpkdbKPi2P,dlnpkdbPi3P
  double precision :: kk1,kk2,kk3,dkk,bkmtree,bkmnl
  double precision :: bkfull,bkfulloc0m,bkfulloc0p,bkfullnsm,bkfullnsp, &
       bkfullnrunm,bkfullnrunp,dbkdk1,dbkdk2,dbkdk3,dbkdmu1,dbkdmu2,dbkdmu3, &
       dbkdf,dbkdb1,dbkdbeta,dbkdb2,dbkdbK2,dbkdbdeta,dbkdbeta2,dbkdbK2P, &
       dbkdbPi2P,dbkdBe,dbkdPe4,dbkdPe5
  integer :: indx,nmarg,ifile,n_linear,n,ik,ik1,ik2,ik3,imu,imu1,imu2,imu3,inl, &
       i,j,il,in,im,ip
  character(len=256) :: filename_linear,filename,filename_oc0m,filename_oc0p,filename_nsm, &
       filename_nsp,filename_nrunm,filename_nrunp
  character(len=1) :: charfisher3

!---------------------------------

  ! Do Fisher only over 3 (instead of 6) cosmological parameters?
  
  charfisher3 = 'n'
  
  ! Specify cosmological parameters for computing growth factor and angular diameter distance

  w = -1.d0
  ob0h2 = 0.022307d0
  oc0h2 = 0.11865d0
  on0h2 = 0.000638d0

  ! Choose kmax values
  
  kmaxpk = 0.35d0
  kmaxbk = 0.1d0
  
  ! Choose options for power spectrum and bispectrum below

  print*, ''
  print*, 'Select what observables and parameters to use:'
  print*, '(1) The nonlinear galaxy power spectrum, ignoring selection effects'
  print*, '(2) Include selection effects in (1) [Except for bPi3P]'
  print*, '(3) Include the galaxy bispectrum in (1)'
  print*, '(4) Include selection effects in (3) [Except for bPi3P]'
  
  read*, indx
  
  ALLOCATE (der(npara),fis(npara,npara),Jvector(npara))
  
  ! Choose the number of parameters to marginalize over (in addition to 6 or 3 cosmological parameters)
  
  if (indx.eq.1) then
     nmarg = 9 ! b1, bd2d, betav1, Pe1, Pe2, Pe3, b2, bK2, btd
  elseif (indx.eq.2) then
     nmarg = 18 ! (1) + beta, betav2, bPi2P, bK2P, bdeta, beta2, bdPi2P, betaPi2P, bKPi2P [No bPi3P]
  elseif (indx.eq.3) then
     nmarg = 12 ! (1) + Be + Pe4 + Pe5
  elseif (indx.eq.4) then
     nmarg = 21 ! (2) + Be + Pe4 + Pe5
  endif
  
  ALLOCATE (imarg(nmarg))
  print*, ''
  print*, 'Marginalizing over the following parameters (in addition to 6 or 3 cosmological parameters):'
  print*, '(1) b1: b_1, linear bias'
  print*, '(2) bd2d: b_{\nabla^2\delta}, higher-derivative bias'
  print*, '(3) betav1: \beta_{\nabla^2v}, velocity bias'
  print*, '(4) Pe1: P_{\epsilon}^{(0)}, leading stochastic term'
  print*, '(5) Pe2: P_{\epsilon}^{(2)}, higher-derivative stochastic term'
  print*, '(6) Pe3: P_{\epsilon\varepsilon\eta}^{(2)}, FoG stochastic term'
  print*, '(7) b2: b_2, second-order bias'
  print*, '(8) bK2: b_{K^2}, tidal field square'
  print*, '(9) btd: b_{td}, bias with time derivative'
  
  if ((indx.eq.2).or.(indx.eq.4)) then ! S for selection parameter
     print*, '(10) S: beta: b_{\eta}'
     print*, '(11) S: betav2: \beta_{\partial_{\parallel}^2v}'
     print*, '(12) S: bPi2P: b_{\Pi^{[2]}_{\parallel}}'
     print*, '(13) S: bK2P: b_{(KK)_{\parallel}}'
     print*, '(14) S: bdeta: b_{\delta\eta}'
     print*, '(15) S: beta2: b_{\eta^2}'
     print*, '(16) S: bdPi2P: b_{\delta\Pi_{\parallel}^{[2]}}'
     print*, '(17) S: betaPi2P: b_{\eta\Pi_{\parallel}^{[2]}}'
     print*, '(18) S: bKPi2P: b_{(\Pi^{[2]}K)_{\parallel}}'
     print*, '(19) S: bPi3P: b_{\Pi_{\parallel}^{[3]}} [REMOVED]' 
  endif

  if (indx.eq.3) then
     print*, '(10) Be: B_{\epsilon}^{(0)}, bispectrum stochastic term'
     print*, '(11) Pe4: P_{\epsilon\epsilon\delta}'
     print*, '(12) Pe5: P_{\epsilon\epsilon\eta}'
  endif

  if (indx.eq.4) then
     print*, '(20) Be: B_{\epsilon}^{(0)}, bispectrum stochastic term'
     print*, '(21) Pe4: P_{\epsilon\epsilon\delta}'
     print*, '(22) Pe5: P_{\epsilon\epsilon\eta}'
  endif

  do i = 1, nmarg
     imarg(i) = i
  enddo

  if (indx.eq.4) then
     imarg(19) = 20
     imarg(20) = 21
     imarg(21) = 22
  endif
  
  ! Choose survey specific parameters

  print*, ''
  print*, 'Select survey parameters to use'
  print*, '(1) Euclid'
  print*, '(2) Roman Space Telescope'
  print*, '(3) HETDEX'
  
  read*, ifile
  print*, ''
  
  if (ifile.eq.1) then
     filename = 'number_euclid.txt'
  elseif (ifile.eq.2) then
     filename = 'number_roman.txt'
  elseif (ifile.eq.3) then
     filename = 'number_hetdex.txt'
  endif

  open(1,file=filename,status='old')
  read(1,*) z, Vsurvey, ngal, b1
  close(1)
  Vsurvey = (1.d9)*Vsurvey
  ngal = (1.d-4)*ngal

  ! Set up growth factor and angular diameter distance
  ! Also set up derivatives with oc0, ns, and nrun

  om0 = (ob0h2 + 0.975d0*oc0h2 + on0h2)/(h**(2.d0))
  ode0 = 1.d0 - om0
  CALL setup_growth
  oc0m = om0
  Doc0m = g(z)/g(0.d0)*(1.d0+0.d0)/(1.d0+z)
  fzoc0m = 1.d0 + dgdlna(z)/g(z)
  
  om0 = (ob0h2 + 1.025d0*oc0h2 + on0h2)/(h**(2.d0))
  ode0 = 1.d0 - om0
  CALL setup_growth
  oc0p = om0
  Doc0p = g(z)/g(0.d0)*(1.d0+0.d0)/(1.d0+z)
  fzoc0p = 1.d0 + dgdlna(z)/g(z)

  om0 = (ob0h2 + oc0h2 + on0h2)/(h**(2.d0))
  ode0 = 1.d0 - om0 ! = 0.69179d0
  CALL setup_growth
  D = g(z)/g(0.d0)*(1.d0+0.d0)/(1.d0+z)
  fz = 1.d0 + dgdlna(z)/g(z)
  CALL setup_da

  nsm = 0.985d0*0.9672d0
  nsp = 1.015d0*0.9672d0
  nrunm = -0.01d0
  nrunp = 0.01d0
  
  ! Read in linear and nonlinear P(k)

  filename_linear = 'planck_base_plikHM_TTTEEE_lowTEB_lensing_post_BAO_H070p6_JLA_matterpower_sigma8=0.8166.dat'
  n_linear = 693 ! # of lines in the file
  filename = 'pk1loop_comps.dat'
  filename_oc0m = 'pk1loop_comps_oc0m.dat'
  filename_oc0p = 'pk1loop_comps_oc0p.dat'
  filename_nsm = 'pk1loop_comps_nsm.dat'
  filename_nsp = 'pk1loop_comps_nsp.dat'
  filename_nrunm = 'pk1loop_comps_nrunm.dat'
  filename_nrunp = 'pk1loop_comps_nrunp.dat'
  n = 500 ! # of lines in the file
  zin = 0.d0 ! redshift of the input power spectrum
  
  ALLOCATE (klist(n),dummy(29,n),pkarray(n,2001)) ! in pkarray, mu = -1 to 1 with dmu = 0.001
  open(2,file=filename,status='old')
  do i = 1, n
     read(2,*) klist(i), dummy(1:29,i)
  enddo
  close(2)

  CALL open_pk_linear(filename_linear,n_linear,D)
  CALL open_pk(filename,n,D)
  CALL open_pk_oc0(filename_oc0m,filename_oc0p,n,Doc0m,Doc0p)
  CALL open_pk_ns(filename_nsm,filename_nsp,n,D)
  CALL open_pk_nrun(filename_nrunm,filename_nrunp,n,D)

  ! Choose bias parameter values (b1 is chosen in the survey's text file)
  
  rstar = 1d0
  bd2d = -1d0
  betav1 = 1d0
  Pe1 = 1d0/ngal
  Pe2 = 0d0
  Pe3 = 0d0
  b2 = -0.69d0
  bK2 = -0.14d0
  btd = (23d0/42d0)*(b1-1d0)
  Be = (1d0/ngal)**2d0
  Pe4 = 2d0*b1/ngal
  Pe5 = 0d0

  beta = -1d0
  betav2 = 0d0
  bPi2P = 0d0
  bK2P = 0d0
  bdeta = -b1
  beta2 = 1d0
  bdPi2P = 0d0
  betaPi2P = 0d0
  bKPi2P = 0d0
  bPi3P = 0d0

  btdtrue = 0.99d0*btd ! 1% shift
  betatrue = 0.99d0*beta ! 1% shift
  
  ! Set up nonlinear coefficients and their derivatives

  CALL calculate_Anmp(fzoc0m,b1,b2,bK2,beta,bPi2P,bK2P,bdeta,beta2,Anmpoc0m)
  CALL calculate_Anmp(fzoc0p,b1,b2,bK2,beta,bPi2P,bK2P,bdeta,beta2,Anmpoc0p)
  CALL calculate_Anmp(fz,b1,b2,bK2,beta,bPi2P,bK2P,bdeta,beta2,Anmp)

  CALL calculate_dAnmpdf(fz,b1,b2,bK2,beta,bPi2P,bK2P,bdeta,beta2,dAnmpdf)
  CALL calculate_dAnmpdb1(fz,b1,b2,bK2,beta,bPi2P,bK2P,bdeta,beta2,dAnmpdb1)
  CALL calculate_dAnmpdb2(fz,b1,b2,bK2,beta,bPi2P,bK2P,bdeta,beta2,dAnmpdb2)
  CALL calculate_dAnmpdbK2(fz,b1,b2,bK2,beta,bPi2P,bK2P,bdeta,beta2,dAnmpdbK2)
  CALL calculate_dAnmpdbeta(fz,b1,b2,bK2,beta,bPi2P,bK2P,bdeta,beta2,dAnmpdbeta)
  CALL calculate_dAnmpdbPi2P(fz,b1,b2,bK2,beta,bPi2P,bK2P,bdeta,beta2,dAnmpdbPi2P)
  CALL calculate_dAnmpdbK2P(fz,b1,b2,bK2,beta,bPi2P,bK2P,bdeta,beta2,dAnmpdbK2P)
  CALL calculate_dAnmpdbdeta(fz,b1,b2,bK2,beta,bPi2P,bK2P,bdeta,beta2,dAnmpdbdeta)
  CALL calculate_dAnmpdbeta2(fz,b1,b2,bK2,beta,bPi2P,bK2P,bdeta,beta2,dAnmpdbeta2)

  CALL calculate_C13ln(fzoc0m,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P,C13lnoc0m)
  CALL calculate_C13ln(fzoc0p,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P,C13lnoc0p)
  CALL calculate_C13ln(fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P,C13ln)

  CALL calculate_dC13lndf(fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P,dC13lndf)
  CALL calculate_dC13lndb1(fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P,dC13lndb1)
  CALL calculate_dC13lndbK2(fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P,dC13lndbK2)
  CALL calculate_dC13lndbtd(fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P,dC13lndbtd)
  CALL calculate_dC13lndbeta(fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P,dC13lndbeta)
  CALL calculate_dC13lndbPi2P(fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P,dC13lndbPi2P)
  CALL calculate_dC13lndbK2P(fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P,dC13lndbK2P)
  CALL calculate_dC13lndbdeta(fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P,dC13lndbdeta)
  CALL calculate_dC13lndbeta2(fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P,dC13lndbeta2)
  CALL calculate_dC13lndbdPi2P(fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P,dC13lndbdPi2P)
  CALL calculate_dC13lndbetaPi2P(fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P,dC13lndbetaPi2P)
  CALL calculate_dC13lndbKPi2P(fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P,dC13lndbKPi2P)
  CALL calculate_dC13lndbPi3P(fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P,dC13lndbPi3P)

  ! Compute the Fisher matrix

  print*, ''
  print*, 'Computing the Fisher matrix ...'
  
  der = 0d0
  fis = 0d0
  Jvector = 0d0

  dmu = 1d-4    
  ik = 1
  k0 = klist(ik)
  k = 0d0
  do while (k <= kmaxpk)

     ik = ik+1
     k = klist(ik)
     pkL = pklin(k)
     neffL = dlnpklindlnk(k)
     pkLoc0m = pklin_oc0m(k)
     pkLoc0p = pklin_oc0p(k)
     pkLnsm = pklin_nsm(k)
     pkLnsp = pklin_nsp(k)
     pkLnrunm = pklin_nrunm(k)
     pkLnrunp = pklin_nrunp(k)
     do inl = 1, 28
        pknlcomps(inl) = pknl(inl,k)
        neffnlcomps(inl) = dlnpknldlnk(inl,k)
        pknlcompsoc0m(inl) = pknl_oc0m(inl,k)
        pknlcompsoc0p(inl) = pknl_oc0p(inl,k)
        pknlcompsnsm(inl) = pknl_nsm(inl,k)
        pknlcompsnsp(inl) = pknl_nsp(inl,k)
        pknlcompsnrunm(inl) = pknl_nrunm(inl,k)
        pknlcompsnrunp(inl) = pknl_nrunp(inl,k)
     enddo

     CALL assign_Imp(pknlcompsoc0m,neffnlcomps,Impoc0m,neffImp)
     CALL assign_In(pknlcompsoc0m,neffnlcomps,calInoc0m,neffIn)

     CALL assign_Imp(pknlcompsoc0p,neffnlcomps,Impoc0p,neffImp)
     CALL assign_In(pknlcompsoc0p,neffnlcomps,calInoc0p,neffIn)

     CALL assign_Imp(pknlcompsnsm,neffnlcomps,Impnsm,neffImp)
     CALL assign_In(pknlcompsnsm,neffnlcomps,calInnsm,neffIn)

     CALL assign_Imp(pknlcompsnsp,neffnlcomps,Impnsp,neffImp)
     CALL assign_In(pknlcompsnsp,neffnlcomps,calInnsp,neffIn)

     CALL assign_Imp(pknlcompsnrunm,neffnlcomps,Impnrunm,neffImp)
     CALL assign_In(pknlcompsnrunm,neffnlcomps,calInnrunm,neffIn)

     CALL assign_Imp(pknlcompsnrunp,neffnlcomps,Impnrunp,neffImp)
     CALL assign_In(pknlcompsnrunp,neffnlcomps,calInnrunp,neffIn)
     
     CALL assign_Imp(pknlcomps,neffnlcomps,Imp,neffImp)
     CALL assign_In(pknlcomps,neffnlcomps,calIn,neffIn)

     pkmonoL = 0d0
     pkmononl = 0d0
     pkquadnl = 0d0
     dlnk = dlog(k)-dlog(k0)   
     factor = k**3d0*dlnk/(2d0*3.1415926535d0**2d0) ! h^3 Mpc^-3
     mu = 0d0

     do while (mu<=1d0)
           
        mu2 = mu*mu
        LegP = 0d0
        dLegPdmu2 = 0d0
        LegP(0) = 1d0
        LegP(2) = 0.5d0*(3d0*mu2-1d0)
        dLegPdmu2(2) = 0.5d0*(3d0)
        LegP(4) = (1d0/8d0)*(35d0*mu2**2d0-30d0*mu2+3d0)
        dLegPdmu2(4) = (1d0/8d0)*(35d0*2d0*mu2-30d0)
        LegP(6) = (1d0/16d0)*(231d0*mu2**3d0-315d0*mu2**2d0+105d0*mu2-5d0)
        dLegPdmu2(6) = (1d0/16d0)*(231d0*3d0*mu2**2d0-315d0*2d0*mu2+105d0)
        pk = (b1-beta*mu2*fz)**2d0*pkL+Pe1 &
             -2d0*(b1*bd2d-beta*mu2*fz*(bd2d+b1*betav1+b1*betav2*mu2) &
             +(beta*mu2*fz)**2d0*(betav1+betav2*mu2))*k**2d0*pkL &
             +k**2d0*Pe2+beta*mu2*k**2d0*Pe3 &
             +sum((/(sum((/(sum((/(Anmp(in,im,ip)*Imp(im,ip)*mu2**in &
             ,ip=0,6)/)),im=0,8)/)),in=0,4)/)) &
             +2d0*sum((/(sum((/(C13ln(il,in)*calIn(in)*LegP(2*il) &
             ,in=1,5)/)),il=0,3)/))*pkL
        pkoc0m = (b1-beta*mu2*fzoc0m)**2d0*pkLoc0m+Pe1 &
             -2d0*(b1*bd2d-beta*mu2*fzoc0m*(bd2d+b1*betav1+b1*betav2*mu2) &
             +(beta*mu2*fzoc0m)**2d0*(betav1+betav2*mu2))*k**2d0*pkLoc0m &
             +k**2d0*Pe2+beta*mu2*k**2d0*Pe3 &
             +sum((/(sum((/(sum((/(Anmpoc0m(in,im,ip)*Impoc0m(im,ip)*mu2**in &
             ,ip=0,6)/)),im=0,8)/)),in=0,4)/)) &
             +2d0*sum((/(sum((/(C13lnoc0m(il,in)*calInoc0m(in)*LegP(2*il) &
             ,in=1,5)/)),il=0,3)/))*pkLoc0m
        pkoc0p = (b1-beta*mu2*fzoc0p)**2d0*pkLoc0p+Pe1 &
             -2d0*(b1*bd2d-beta*mu2*fzoc0p*(bd2d+b1*betav1+b1*betav2*mu2) &
             +(beta*mu2*fzoc0p)**2d0*(betav1+betav2*mu2))*k**2d0*pkLoc0p &
             +k**2d0*Pe2+beta*mu2*k**2d0*Pe3 &
             +sum((/(sum((/(sum((/(Anmpoc0p(in,im,ip)*Impoc0p(im,ip)*mu2**in &
             ,ip=0,6)/)),im=0,8)/)),in=0,4)/)) &
             +2d0*sum((/(sum((/(C13lnoc0p(il,in)*calInoc0p(in)*LegP(2*il) &
             ,in=1,5)/)),il=0,3)/))*pkLoc0p
        pknsm = (b1-beta*mu2*fz)**2d0*pkLnsm+Pe1 &
             -2d0*(b1*bd2d-beta*mu2*fz*(bd2d+b1*betav1+b1*betav2*mu2) &
             +(beta*mu2*fz)**2d0*(betav1+betav2*mu2))*k**2d0*pkLnsm &
             +k**2d0*Pe2+beta*mu2*k**2d0*Pe3 &
             +sum((/(sum((/(sum((/(Anmp(in,im,ip)*Impnsm(im,ip)*mu2**in &
             ,ip=0,6)/)),im=0,8)/)),in=0,4)/)) &
             +2d0*sum((/(sum((/(C13ln(il,in)*calInnsm(in)*LegP(2*il) &
             ,in=1,5)/)),il=0,3)/))*pkLnsm
        pknsp = (b1-beta*mu2*fz)**2d0*pkLnsp+Pe1 &
             -2d0*(b1*bd2d-beta*mu2*fz*(bd2d+b1*betav1+b1*betav2*mu2) &
             +(beta*mu2*fz)**2d0*(betav1+betav2*mu2))*k**2d0*pkLnsp &
             +k**2d0*Pe2+beta*mu2*k**2d0*Pe3 &
             +sum((/(sum((/(sum((/(Anmp(in,im,ip)*Impnsp(im,ip)*mu2**in &
             ,ip=0,6)/)),im=0,8)/)),in=0,4)/)) &
             +2d0*sum((/(sum((/(C13ln(il,in)*calInnsp(in)*LegP(2*il) &
             ,in=1,5)/)),il=0,3)/))*pkLnsp
        pknrunm = (b1-beta*mu2*fz)**2d0*pkLnrunm+Pe1 &
             -2d0*(b1*bd2d-beta*mu2*fz*(bd2d+b1*betav1+b1*betav2*mu2) &
             +(beta*mu2*fz)**2d0*(betav1+betav2*mu2))*k**2d0*pkLnrunm &
             +k**2d0*Pe2+beta*mu2*k**2d0*Pe3 &
             +sum((/(sum((/(sum((/(Anmp(in,im,ip)*Impnrunm(im,ip)*mu2**in &
             ,ip=0,6)/)),im=0,8)/)),in=0,4)/)) &
             +2d0*sum((/(sum((/(C13ln(il,in)*calInnrunm(in)*LegP(2*il) &
             ,in=1,5)/)),il=0,3)/))*pkLnrunm
        pknrunp = (b1-beta*mu2*fz)**2d0*pkLnrunp+Pe1 &
             -2d0*(b1*bd2d-beta*mu2*fz*(bd2d+b1*betav1+b1*betav2*mu2) &
             +(beta*mu2*fz)**2d0*(betav1+betav2*mu2))*k**2d0*pkLnrunp &
             +k**2d0*Pe2+beta*mu2*k**2d0*Pe3 &
             +sum((/(sum((/(sum((/(Anmp(in,im,ip)*Impnrunp(im,ip)*mu2**in &
             ,ip=0,6)/)),im=0,8)/)),in=0,4)/)) &
             +2d0*sum((/(sum((/(C13ln(il,in)*calInnrunp(in)*LegP(2*il) &
             ,in=1,5)/)),il=0,3)/))*pkLnrunp
        neff = (1d0/pk)*((b1-beta*mu2*fz)**2d0*pkL*neffL &
             -2d0*(b1*bd2d-beta*mu2*fz*(bd2d+b1*betav1+b1*betav2*mu2) &
             +(beta*mu2*fz)**2d0*(betav1+betav2*mu2))*k**2d0*pkL*(2d0+neffL) &
             +2d0*k**2d0*Pe2+2d0*beta*mu2*k**2d0*Pe3 &
             +sum((/(sum((/(sum((/(Anmp(in,im,ip)*Imp(im,ip)*neffImp(im,ip) &
             *mu2**in,ip=0,6)/)),im=0,8)/)),in=0,4)/)) &
             +2d0*sum((/(sum((/(C13ln(il,in)*calIn(in)*LegP(2*il) &
             ,in=1,5)/)),il=0,3)/))*pkL*neffL &
             +2d0*sum((/(sum((/(C13ln(il,in)*calIn(in)*neffIn(in)*LegP(2*il) &
             ,in=1,5)/)),il=0,3)/))*pkL)
        ! The range of `in' in Imp is from 1 to 4 below,
        ! and that of `il' in calIn is from 1 to 3 (this one is not as crucial)
        dlnpkdmu2 = (1d0/pk)*(2d0*(b1-beta*mu2*fz)*(-beta*fz)*pkL &
             -2d0*(-beta*fz*(bd2d+b1*betav1+2d0*b1*betav2*mu2) &
             +mu2*(beta*fz)**2d0*(2d0*betav1+3d0*betav2*mu2))*k**2d0*pkL &
             +beta*k**2d0*Pe3 &
             +sum((/(sum((/(sum((/(Anmp(in,im,ip)*Imp(im,ip)*in*mu2**(in-1) &
             ,ip=0,6)/)),im=0,8)/)),in=1,4)/)) &
             +2d0*sum((/(sum((/(C13ln(il,in)*calIn(in)*dLegPdmu2(2*il) &
             ,in=1,5)/)),il=1,3)/))*pkL)
        dlnpkdf = (1d0/pk)*(2d0*(b1-beta*mu2*fz)*(-beta*mu2)*pkL &
             -2d0*(-beta*mu2*(bd2d+b1*betav1+b1*betav2*mu2) &
             +2d0*fz*(beta*mu2)**2d0*(betav1+betav2*mu2))*k**2d0*pkL &
             +sum((/(sum((/(sum((/(dAnmpdf(in,im,ip)*Imp(im,ip)*mu2**in &
             ,ip=0,6)/)),im=0,8)/)),in=0,4)/)) &
             +2d0*sum((/(sum((/(dC13lndf(il,in)*calIn(in)*LegP(2*il) &
             ,in=1,5)/)),il=0,3)/))*pkL)
        dlnpkdb1 = (1d0/pk)*(2d0*(b1-beta*mu2*fz)*pkL &
             -2d0*(bd2d-beta*mu2*fz*(betav1+betav2*mu2))*k**2d0*pkL &
             +sum((/(sum((/(sum((/(dAnmpdb1(in,im,ip)*Imp(im,ip)*mu2**in &
             ,ip=0,6)/)),im=0,8)/)),in=0,4)/)) &
             +2d0*sum((/(sum((/(dC13lndb1(il,in)*calIn(in)*LegP(2*il) &
             ,in=1,5)/)),il=0,3)/))*pkL)
        dlnpkdbd2d = (1d0/pk)*(-2d0*(b1-beta*mu2*fz)*k**2d0*pkL)
        dlnpkdbetav1 = (1d0/pk)*(-2d0*(-beta*mu2*fz*b1 &
             +(beta*mu2*fz)**2d0)*k**2d0*pkL)
        dlnpkdPe1 = (1d0/pk)
        dlnpkdPe2 = (1d0/pk)*k**2d0
        dlnpkdPe3 = (1d0/pk)*(beta*mu2*k**2d0)
        dlnpkdb2 = (1d0/pk)*(sum((/(sum((/(sum((/(dAnmpdb2(in,im,ip)*Imp(im,ip) &
             *mu2**in,ip=0,6)/)),im=0,8)/)),in=0,4)/)))
        dlnpkdbK2 = (1d0/pk)*(sum((/(sum((/(sum((/(dAnmpdbK2(in,im,ip)*Imp(im,ip) &
             *mu2**in,ip=0,6)/)),im=0,8)/)),in=0,4)/)) &
             +2d0*sum((/(sum((/(dC13lndbK2(il,in)*calIn(in)*LegP(2*il) &
             ,in=1,5)/)),il=0,3)/))*pkL)
        dlnpkdbtd = (1d0/pk)*(2d0*sum((/(sum((/(dC13lndbtd(il,in)*calIn(in)*LegP(2*il) &
             ,in=1,5)/)),il=0,3)/))*pkL)
        dlnpkdbeta = (1d0/pk)*(2d0*(b1-beta*mu2*fz)*(-mu2*fz)*pkL &
             -2d0*(-mu2*fz*(bd2d+b1*betav1+b1*betav2*mu2) &
             +2d0*beta*(mu2*fz)**2d0*(betav1+betav2*mu2))*k**2d0*pkL &
             +mu2*k**2d0*Pe3 &
             +sum((/(sum((/(sum((/(dAnmpdbeta(in,im,ip)*Imp(im,ip)*mu2**in &
             ,ip=0,6)/)),im=0,8)/)),in=0,4)/)) &
             +2d0*sum((/(sum((/(dC13lndbeta(il,in)*calIn(in)*LegP(2*il) &
             ,in=1,5)/)),il=0,3)/))*pkL)
        dlnpkdbetav2 = (1d0/pk)*(-2d0*(-beta*mu2*fz*(b1*mu2) &
             +(beta*mu2*fz)**2d0*(mu2))*k**2d0*pkL)
        dlnpkdbPi2P = (1d0/pk)*(sum((/(sum((/(sum((/(dAnmpdbPi2P(in,im,ip)*Imp(im,ip) &
             *mu2**in,ip=0,6)/)),im=0,8)/)),in=0,4)/)) &
             +2d0*sum((/(sum((/(dC13lndbPi2P(il,in)*calIn(in)*LegP(2*il) &
             ,in=1,5)/)),il=0,3)/))*pkL)
        dlnpkdbK2P = (1d0/pk)*(sum((/(sum((/(sum((/(dAnmpdbK2P(in,im,ip)*Imp(im,ip) &
             *mu2**in,ip=0,6)/)),im=0,8)/)),in=0,4)/)) &
             +2d0*sum((/(sum((/(dC13lndbK2P(il,in)*calIn(in)*LegP(2*il) &
             ,in=1,5)/)),il=0,3)/))*pkL)
        dlnpkdbdeta = (1d0/pk)*(sum((/(sum((/(sum((/(dAnmpdbdeta(in,im,ip)*Imp(im,ip) &
             *mu2**in,ip=0,6)/)),im=0,8)/)),in=0,4)/)) &
             +2d0*sum((/(sum((/(dC13lndbdeta(il,in)*calIn(in)*LegP(2*il) &
             ,in=1,5)/)),il=0,3)/))*pkL)
        dlnpkdbeta2 = (1d0/pk)*(sum((/(sum((/(sum((/(dAnmpdbeta2(in,im,ip)*Imp(im,ip) &
             *mu2**in,ip=0,6)/)),im=0,8)/)),in=0,4)/)) &
             +2d0*sum((/(sum((/(dC13lndbeta2(il,in)*calIn(in)*LegP(2*il) &
             ,in=1,5)/)),il=0,3)/))*pkL)
        dlnpkdbdPi2P = (1d0/pk)*(2d0*sum((/(sum((/(dC13lndbdPi2P(il,in)*calIn(in)*LegP(2*il) &
             ,in=1,5)/)),il=0,3)/))*pkL)
        dlnpkdbetaPi2P = (1d0/pk)*(2d0*sum((/(sum((/(dC13lndbetaPi2P(il,in)*calIn(in)*LegP(2*il) &
             ,in=1,5)/)),il=0,3)/))*pkL)
        dlnpkdbKPi2P = (1d0/pk)*(2d0*sum((/(sum((/(dC13lndbKPi2P(il,in)*calIn(in)*LegP(2*il) &
             ,in=1,5)/)),il=0,3)/))*pkL)
        dlnpkdbPi3P = (1d0/pk)*(2d0*sum((/(sum((/(dC13lndbPi3P(il,in)*calIn(in)*LegP(2*il) &
             ,in=1,5)/)),il=0,3)/))*pkL)
        
        wkmu=0.5d0*Vsurvey
        der(1) = neff*(1d0-mu2)-2d0*mu2*(1d0-mu2)*dlnpkdmu2 ! dlnPkdlnDa
        der(2) = neff*(-mu2)-2d0*mu2*(1d0-mu2)*dlnpkdmu2 ! dlnPkdlnH
        der(3) = (1d0/pk)*(pkoc0p-pkoc0m)/(oc0p-oc0m) ! dlnPkdOmegac0
        der(4) = dlnpkdf ! dlnPkdf
        der(5) = (1d0/pk)*(pknsp-pknsm)/(nsp-nsm) ! dlnPkdns
        der(6) = (1d0/pk)*(pknrunp-pknrunm)/(nrunp-nrunm) ! dlnPkdnrun
        der(7) = dlnpkdb1
        der(8) = dlnpkdbd2d
        der(9) = dlnpkdbetav1
        der(10) = dlnpkdPe1
        der(11) = dlnpkdPe2
        der(12) = dlnpkdPe3
        der(13) = dlnpkdb2
        der(14) = dlnpkdbK2
        der(15) = dlnpkdbtd
        if ((indx.eq.2).or.(indx.eq.4)) then
           der(16) = dlnpkdbeta
           der(17) = dlnpkdbetav2
           der(18) = dlnpkdbPi2P
           der(19) = dlnpkdbK2P
           der(20) = dlnpkdbdeta
           der(21) = dlnpkdbeta2
           der(22) = dlnpkdbdPi2P
           der(23) = dlnpkdbetaPi2P
           der(24) = dlnpkdbKPi2P
           der(25) = dlnpkdbPi3P
        endif
        
        if (k <= kmaxpk) then
           do i = 1, npara
              fis(i,:) = fis(i,:)+factor*wkmu*der(i)*der(:)*dmu
              Jvector(i) = Jvector(i)+factor*wkmu*der(i)*(dlnpkdbeta*(beta-betatrue))*dmu
           enddo
        endif
        mu=mu+dmu

     enddo
     k0 = k   
  enddo
  
  if ((indx .eq. 3) .or. (indx .eq. 4)) then

  der = 0d0
  dmu = 1d-3
  ik = 1
  k = klist(ik)
  do while (k <= kmaxbk)
     imu = 1
     mu = -1d0    
     do while (mu <= 1d0)
        pkarray(ik,imu) = pkinbk(k,mu,&
             fz,b1,beta,bd2d,betav1,betav2,Pe1,Pe2,Pe3,Anmp,C13ln)
        imu = imu + 1
        mu = mu + dmu
     enddo
     ik = ik + 1
     k = klist(ik)
  enddo

  dmu = 5d-2
  dphi = 5d-2     
  ik1 = 1
  do while (klist(ik1+1) <= kmaxbk)
     ik1 = ik1 + 1
     k1 = klist(ik1)
     dk1 = k1-klist(ik1-1)
     ik2 = 1
     print*, 'At bispectrum wavenumber k1 = ', k1
     do while (klist(ik2+1) <= k1)
        ik2 = ik2 + 1
        k2 = klist(ik2)
        dk2 = k2-klist(ik2-1)
        ik3 = 1
        do while (klist(ik3+1) <= k2)
           ik3 = ik3 + 1
           k3 = klist(ik3)
           dk3 = k3-klist(ik3-1)
           if (((k1+k2).ge.k3).and.((k2+k3).ge.k1).and.((k3+k1).ge.k2)) then
              sB = 1d0
              if ((ik1.eq.ik2) .and. (ik2.eq.ik3) .and. (ik3.eq.ik1)) then
                 sB = 6d0
              else if ((ik1.eq.ik2) .or. (ik2.eq.ik3) .or. (ik3.eq.ik1)) then
                 sB = 2d0
              endif
              sBpi = 2d0*3.1415926535d0
              if ((abs((k1+k2-k3)).lt.1d-8) .or. (abs((k2+k3-k1)).lt.1d-8) &
                   .or. (abs((k3+k1-k2)).lt.1d-8)) then
                 sBpi = sBpi/2d0
              endif
              if ((k3**2d0-k1**2d0-k2**2d0)/(2d0*k1*k2) .le. -1d0) then
                 alpha = acos(-1d0)
              else if ((k3**2d0-k1**2d0-k2**2d0)/(2d0*k1*k2) .ge. 1d0) then
                 alpha = acos(1d0)
              else
                 alpha = acos((k3**2d0-k1**2d0-k2**2d0)/(2d0*k1*k2))
              endif
              pkL1 = pklin(k1)
              pkL2 = pklin(k2)
              pkL3 = pklin(k3)
              pkL1oc0m = pklin_oc0m(k1)
              pkL2oc0m = pklin_oc0m(k2)
              pkL3oc0m = pklin_oc0m(k3)
              pkL1oc0p = pklin_oc0p(k1)
              pkL2oc0p = pklin_oc0p(k2)
              pkL3oc0p = pklin_oc0p(k3)
              pkL1nsm = pklin_nsm(k1)
              pkL2nsm = pklin_nsm(k2)
              pkL3nsm = pklin_nsm(k3)
              pkL1nsp = pklin_nsp(k1)
              pkL2nsp = pklin_nsp(k2)
              pkL3nsp = pklin_nsp(k3)
              pkL1nrunm = pklin_nrunm(k1)
              pkL2nrunm = pklin_nrunm(k2)
              pkL3nrunm = pklin_nrunm(k3)
              pkL1nrunp = pklin_nrunp(k1)
              pkL2nrunp = pklin_nrunp(k2)
              pkL3nrunp = pklin_nrunp(k3)
              nkL1 = dlnpklindlnk(k1)
              nkL2 = dlnpklindlnk(k2)
              nkL3 = dlnpklindlnk(k3)
              mu = 0d0
              do while (mu <= 1d0)
                 phi = 0d0
                 do while (phi <= 2d0*3.1415926535d0)
                                       
                    k1p = k1*cos(phi)*mu
                    k2p = k2*cos(alpha+phi)*mu
                    k3p = -k1p-k2p
                    mu1 = k1p/k1
                    mu2 = k2p/k2
                    mu3 = k3p/k3
                    
                    bkfull = bktreeperm(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +bktreeperm(k2,mu2,pkL2,k3,mu3,pkL3,k1,mu1,pkL1,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +bktreeperm(k3,mu3,pkL3,k1,mu1,pkL1,k2,mu2,pkL2,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)
                    
                    bkfulloc0m = bktreeperm(k1,mu1,pkL1oc0m,k2,mu2,pkL2oc0m,k3,mu3,pkL3oc0m,&
                         fzoc0m,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +bktreeperm(k2,mu2,pkL2oc0m,k3,mu3,pkL3oc0m,k1,mu1,pkL1oc0m,&
                         fzoc0m,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +bktreeperm(k3,mu3,pkL3oc0m,k1,mu1,pkL1oc0m,k2,mu2,pkL2oc0m,&
                         fzoc0m,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)
                    
                    bkfulloc0p = bktreeperm(k1,mu1,pkL1oc0p,k2,mu2,pkL2oc0p,k3,mu3,pkL3oc0p,&
                         fzoc0p,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +bktreeperm(k2,mu2,pkL2oc0p,k3,mu3,pkL3oc0p,k1,mu1,pkL1oc0p,&
                         fzoc0p,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +bktreeperm(k3,mu3,pkL3oc0p,k1,mu1,pkL1oc0p,k2,mu2,pkL2oc0p,&
                         fzoc0p,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)
                    
                    bkfullnsm = bktreeperm(k1,mu1,pkL1nsm,k2,mu2,pkL2nsm,k3,mu3,pkL3nsm,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +bktreeperm(k2,mu2,pkL2nsm,k3,mu3,pkL3nsm,k1,mu1,pkL1nsm,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +bktreeperm(k3,mu3,pkL3nsm,k1,mu1,pkL1nsm,k2,mu2,pkL2nsm,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)
                    
                    bkfullnsp = bktreeperm(k1,mu1,pkL1nsp,k2,mu2,pkL2nsp,k3,mu3,pkL3nsp,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +bktreeperm(k2,mu2,pkL2nsp,k3,mu3,pkL3nsp,k1,mu1,pkL1nsp,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +bktreeperm(k3,mu3,pkL3nsp,k1,mu1,pkL1nsp,k2,mu2,pkL2nsp,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)
                    
                    bkfullnrunm = bktreeperm(k1,mu1,pkL1nrunm,k2,mu2,pkL2nrunm,k3,mu3,pkL3nrunm,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +bktreeperm(k2,mu2,pkL2nrunm,k3,mu3,pkL3nrunm,k1,mu1,pkL1nrunm,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +bktreeperm(k3,mu3,pkL3nrunm,k1,mu1,pkL1nrunm,k2,mu2,pkL2nrunm,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)
                    
                    bkfullnrunp = bktreeperm(k1,mu1,pkL1nrunp,k2,mu2,pkL2nrunp,k3,mu3,pkL3nrunp,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +bktreeperm(k2,mu2,pkL2nrunp,k3,mu3,pkL3nrunp,k1,mu1,pkL1nrunp,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +bktreeperm(k3,mu3,pkL3nrunp,k1,mu1,pkL1nrunp,k2,mu2,pkL2nrunp,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)

                    ! Be careful in permutations for the next six derivatives
                    
                    dbkdk1 = dbktreepermdk1(k1,mu1,pkL1,nkL1,k2,mu2,pkL2,nkL2,k3,mu3,pkL3,nkL3,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdk3(k2,mu2,pkL2,nkL2,k3,mu3,pkL3,nkL3,k1,mu1,pkL1,nkL1,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdk2(k3,mu3,pkL3,nkL3,k1,mu1,pkL1,nkL1,k2,mu2,pkL2,nkL2,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)
                    
                    dbkdk2 = dbktreepermdk2(k1,mu1,pkL1,nkL1,k2,mu2,pkL2,nkL2,k3,mu3,pkL3,nkL3,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdk1(k2,mu2,pkL2,nkL2,k3,mu3,pkL3,nkL3,k1,mu1,pkL1,nkL1,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdk3(k3,mu3,pkL3,nkL3,k1,mu1,pkL1,nkL1,k2,mu2,pkL2,nkL2,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)
                    
                    dbkdk3 = dbktreepermdk3(k1,mu1,pkL1,nkL1,k2,mu2,pkL2,nkL2,k3,mu3,pkL3,nkL3,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdk2(k2,mu2,pkL2,nkL2,k3,mu3,pkL3,nkL3,k1,mu1,pkL1,nkL1,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdk1(k3,mu3,pkL3,nkL3,k1,mu1,pkL1,nkL1,k2,mu2,pkL2,nkL2,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)
                    
                    dbkdmu1 = dbktreepermdmu1(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdmu3(k2,mu2,pkL2,k3,mu3,pkL3,k1,mu1,pkL1,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdmu2(k3,mu3,pkL3,k1,mu1,pkL1,k2,mu2,pkL2,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)
                    
                    dbkdmu2 = dbktreepermdmu2(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdmu1(k2,mu2,pkL2,k3,mu3,pkL3,k1,mu1,pkL1,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdmu3(k3,mu3,pkL3,k1,mu1,pkL1,k2,mu2,pkL2,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)
                    
                    dbkdmu3 = dbktreepermdmu3(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdmu2(k2,mu2,pkL2,k3,mu3,pkL3,k1,mu1,pkL1,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdmu1(k3,mu3,pkL3,k1,mu1,pkL1,k2,mu2,pkL2,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)
                    
                    dbkdf = dbktreepermdf(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdf(k2,mu2,pkL2,k3,mu3,pkL3,k1,mu1,pkL1,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdf(k3,mu3,pkL3,k1,mu1,pkL1,k2,mu2,pkL2,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)
                    
                    dbkdb1 = dbktreepermdb1(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdb1(k2,mu2,pkL2,k3,mu3,pkL3,k1,mu1,pkL1,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdb1(k3,mu3,pkL3,k1,mu1,pkL1,k2,mu2,pkL2,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)
                    
                    dbkdb2 = dbktreepermdb2(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdb2(k2,mu2,pkL2,k3,mu3,pkL3,k1,mu1,pkL1,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdb2(k3,mu3,pkL3,k1,mu1,pkL1,k2,mu2,pkL2,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)
                    
                    dbkdbK2 = dbktreepermdbK2(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdbK2(k2,mu2,pkL2,k3,mu3,pkL3,k1,mu1,pkL1,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdbK2(k3,mu3,pkL3,k1,mu1,pkL1,k2,mu2,pkL2,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)
                    
                    dbkdBe = dbktreepermdBe(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdBe(k2,mu2,pkL2,k3,mu3,pkL3,k1,mu1,pkL1,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdBe(k3,mu3,pkL3,k1,mu1,pkL1,k2,mu2,pkL2,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)
                    
                    dbkdPe4 = dbktreepermdPe4(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdPe4(k2,mu2,pkL2,k3,mu3,pkL3,k1,mu1,pkL1,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdPe4(k3,mu3,pkL3,k1,mu1,pkL1,k2,mu2,pkL2,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)
                    
                    dbkdPe5 = dbktreepermdPe5(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdPe5(k2,mu2,pkL2,k3,mu3,pkL3,k1,mu1,pkL1,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdPe5(k3,mu3,pkL3,k1,mu1,pkL1,k2,mu2,pkL2,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)
                    
                    dbkdbeta = dbktreepermdbeta(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdbeta(k2,mu2,pkL2,k3,mu3,pkL3,k1,mu1,pkL1,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdbeta(k3,mu3,pkL3,k1,mu1,pkL1,k2,mu2,pkL2,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)
                    
                    dbkdbdeta = dbktreepermdbdeta(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdbdeta(k2,mu2,pkL2,k3,mu3,pkL3,k1,mu1,pkL1,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdbdeta(k3,mu3,pkL3,k1,mu1,pkL1,k2,mu2,pkL2,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)
                    
                    dbkdbeta2 = dbktreepermdbeta2(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdbeta2(k2,mu2,pkL2,k3,mu3,pkL3,k1,mu1,pkL1,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdbeta2(k3,mu3,pkL3,k1,mu1,pkL1,k2,mu2,pkL2,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)
                    
                    dbkdbK2P = dbktreepermdbK2P(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdbK2P(k2,mu2,pkL2,k3,mu3,pkL3,k1,mu1,pkL1,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdbK2P(k3,mu3,pkL3,k1,mu1,pkL1,k2,mu2,pkL2,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)
                    
                    dbkdbPi2P = dbktreepermdbPi2P(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdbPi2P(k2,mu2,pkL2,k3,mu3,pkL3,k1,mu1,pkL1,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)&
                         +dbktreepermdbPi2P(k3,mu3,pkL3,k1,mu1,pkL1,k2,mu2,pkL2,&
                         fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)

                    imu1 = floor(mu1/(1d-3))+1001
                    imu2 = floor(mu2/(1d-3))+1001
                    imu3 = floor(mu3/(1d-3))+1001
                    pk1 = pkarray(ik1,imu1)
                    pk2 = pkarray(ik2,imu2)
                    pk3 = pkarray(ik3,imu3)

                    kF = 2d0*3.1415926535d0/(Vsurvey**(1d0/3))
                    factor = 12d0*sBpi*k1*dk1*k2*dk2*k3*dk3/(kF**(6d0))
                    factor = factor*1d0/(sB*Vsurvey*pk1*pk2*pk3)
                    if (mu .eq. 0d0) factor = factor*0.5d0
                    
                    der(1) = k1*dbkdk1*(1d0-mu1**2.)-dbkdmu1*mu1*(1d0-mu1**2.) &
                         +k2*dbkdk2*(1d0-mu2**2.)-dbkdmu2*mu2*(1d0-mu2**2.) &
                         +k3*dbkdk3*(1d0-mu3**2.)-dbkdmu3*mu3*(1d0-mu3**2.) ! dBkdlnDa
                    der(2) = k1*dbkdk1*(-mu1**2.)-dbkdmu1*mu1*(1d0-mu1**2.) &
                         +k2*dbkdk2*(-mu2**2.)-dbkdmu2*mu2*(1d0-mu2**2.) &
                         +k3*dbkdk3*(-mu3**2.)-dbkdmu3*mu3*(1d0-mu3**2.) ! dBkdlnH
                    der(3) = (bkfulloc0p-bkfulloc0m)/(oc0p-oc0m) ! dBkdOmegac0
                    der(4) = dbkdf !dBkdf
                    der(5) = (bkfullnsp-bkfullnsm)/(nsp-nsm) ! dBkdns
                    der(6) = (bkfullnrunp-bkfullnrunm)/(nrunp-nrunm) ! dBkdnrun
                    der(7) = dbkdb1
                    der(13) = dbkdb2
                    der(14) = dbkdbK2
                    if (indx .eq. 3) then
                       der(16) = dbkdBe
                       der(17) = dbkdPe4
                       der(18) = dbkdPe5
                    endif
                    if (indx .eq. 4) then
                       der(16) = dbkdbeta
                       der(18) = dbkdbPi2P
                       der(19) = dbkdbK2P
                       der(20) = dbkdbdeta
                       der(21) = dbkdbeta2
                       der(26) = dbkdBe
                       der(27) = dbkdPe4
                       der(28) = dbkdPe5
                    endif
                    
                    do i = 1, npara
                       fis(i,:) = fis(i,:)+factor*der(i)*der(:)*dmu*dphi
                       Jvector(i) = Jvector(i)-factor*der(i)*(dbkdbeta*(beta-betatrue))*dmu*dphi
                    enddo

                    phi = phi + dphi
                 enddo
                 imu = imu + 1
                 mu = mu + dmu
              enddo
           endif
        enddo
     enddo
  enddo
  
  endif

  ! Print survey parameters

  print*, ''
  print'(1A5,1F5.3)', 'z = ', z
  print'(1A8,1F8.5,1A11)', 'Vsur = ', Vsurvey/(1.d9), ' h^-3 Gpc^3'
  print'(1A8,1F12.8,1A11)','ngal = ', ngal, ' h^3 Mpc^-3'

  ! Print cosmological parameters at z = z_survey
  
  print*, 'D(z) at z = ', z, ' relative to that at z = 0, is: ', g(z)/g(0.d0)*(1.d0+0.d0)/(1.d0+z)
  print*, 'f = ', 1d0+dgdlna(z)/g(z)
  print*, 'Angular diameter distance, D_A(z) = ', da(z), ' h^-1 Mpc'
  print*, 'Hubble parameter, H(z) = ', 1.d0/(one_over_h(z))*100, ' h'
  print*, 'sigma_8  = ', sqrt(rombint(ds8dlnk,dlog(1d-3),dlog(100d0),1d-7))
  print*, 'sigma_12 = ', sqrt(rombint(ds12dlnk,dlog(1d-3),dlog(100d0),1d-7))

  ! Print kmax values used

  print*, ''
  print*, 'kmaxpk = ', kmaxpk, ' h Mpc^-1'
  if ((indx .eq. 3) .or. (indx .eq. 4)) then
     print*, 'kmaxbk = ', kmaxbk, ' h Mpc^-1'
  endif
  print*, ''
  
  ! Print galaxy distribution parameters used

  print*, 'b1       = ', b1
  print*, 'bd2d     = ', bd2d
  print*, 'betav1   = ', betav1
  print*, 'Pe1      = ', Pe1
  print*, 'Pe2      = ', Pe2
  print*, 'Pe3      = ', Pe3
  print*, 'b2       = ', b2
  print*, 'bK2      = ', bK2
  print*, 'btd      = ', btd
  print*, 'Be       = ', Be
  print*, 'Pe4      = ', Pe4
  print*, 'Pe5      = ', Pe5
  
  print*, 'beta     = ', beta
  print*, 'betav2   = ', betav2
  print*, 'bPi2P    = ', bPi2P
  print*, 'bK2P     = ', bK2P
  print*, 'bdeta    = ', bdeta
  print*, 'beta2    = ', beta2
  print*, 'bdPi2P   = ', bdPi2P
  print*, 'betaPi2P = ', betaPi2P
  print*, 'bKPi2P   = ', bKPi2P
  print*, 'bPi3P    = ', bPi3P

  if (charfisher3 .eq. 'y') then
     CALL report_result_3params(indx,imarg,nmarg,npara,fis,Jvector,Pe1)
  else
     CALL report_result(indx,imarg,nmarg,npara,fis,Jvector,Pe1)
  endif

  CALL close_pk_linear
  CALL close_pk
  CALL close_pk_oc0
  CALL close_pk_ns
  CALL close_pk_nrun
  
  DEALLOCATE (der,fis,Jvector,imarg,klist,dummy)
  
END PROGRAM fisher

! -----------------------------------------------------------------

SUBROUTINE report_result(indx,imarg,nmarg,npara,fis,Jvector,Pe1)
  
  IMPLICIT none
  integer, intent(IN) :: indx,imarg(nmarg),nmarg,npara
  double precision, intent(IN) :: fis(npara,npara),Jvector(npara),Pe1
  double precision, allocatable, dimension(:) :: Jvectorp,deltatheta
  double precision, allocatable, dimension(:,:) :: cov,covstore
  integer, allocatable, dimension(:) :: work
  integer :: i,j,k
  
  ALLOCATE (cov(6+nmarg,6+nmarg),covstore(6+nmarg,6+nmarg),work(6+nmarg),&
       Jvectorp(6+nmarg),deltatheta(6+nmarg))
  cov(1:6,1:6) = fis(1:6,1:6)
  do i = 1, nmarg
     cov(1:6,6+i) = fis(1:6,6+imarg(i))
     cov(6+i,1:6) = cov(1:6,6+i)
     do j = i, nmarg
        cov(6+i,6+j) = fis(6+imarg(i),6+imarg(j))
        cov(6+j,6+i) = cov(6+i,6+j)
     enddo
  enddo

  open(51,file='fmatrix.dat',action='write') ! Write to file the fisher matrix
  do i = 1, 6+nmarg
     covstore(i,1:6+nmarg) = cov(i,1:6+nmarg)
     if (indx.eq.1) then
        write(51,'(15E30.15)') cov(i,1:6+nmarg)
     else if (indx.eq.2) then
        write(51,'(24E30.15)') cov(i,1:6+nmarg)
     else if (indx.eq.3) then
        write(51,'(18E30.15)') cov(i,1:6+nmarg)
     else if (indx.eq.4) then
        write(51,'(27E30.15)') cov(i,1:6+nmarg)
     endif
  enddo
  close(51)

  Jvectorp(1:6) = Jvector(1:6)
  do i = 1, nmarg
     Jvectorp(6+i) = Jvector(6+imarg(i))
  enddo

  CALL DVERT(cov,6+nmarg,6+nmarg,work)

  print*, ''
  print*, '1\sigma errors on cosmological parameters:'
  print*, ''
  print'(1A16,1F12.7)', ' Err[lnDa](%)  = ', sqrt(cov(1,1))*1d2
  print'(1A16,1F12.7)', ' Err[lnH](%)   = ', sqrt(cov(2,2))*1d2
  print'(1A16,1F12.7)', ' Err[Omega_c0] = ', sqrt(cov(3,3))
  print'(1A16,1F12.7)', ' Err[f]        = ', sqrt(cov(4,4))
  print'(1A16,1F12.7)', ' Err[ns]       = ', sqrt(cov(5,5))
  print'(1A16,1F12.7)', ' Err[nrun]     = ', sqrt(cov(6,6))

  deltatheta = 0d0
  do i = 1, 6+nmarg
     do j = 1, 6+nmarg
        deltatheta(i) = deltatheta(i)+cov(i,j)*Jvectorp(j)
     enddo
  enddo

  open(40,file='shifts_beta.dat',action='write') ! Write to file the shifts in all parameters when betatrue != beta
  do i = 1, 6+nmarg
     write(40,'(1E15.6)') deltatheta(i)/(sqrt(cov(i,i)))
  enddo
  close(40)
  
  print*, ''
  print*, '1\sigma errors on other parameters:'
  print*, ''

  open(50,file='rmatrix.dat',action='write') ! Write to file the full correlation matrix r(a,b)
  if (indx.eq.1) then
     write(50,'(16A10)') '', 'lnDa', 'lnH', 'Omega_c0', 'f', 'ns', 'nrun', 'b1', 'bd2d', 'betav1', 'Pe1', 'Pe2', 'Pe3', &
          'b2', 'bK2', 'btd'
  else if (indx.eq.2) then
     write(50,'(25A10)') '', 'lnDa', 'lnH', 'Omega_c0', 'f', 'ns', 'nrun', 'b1', 'bd2d', 'betav1', 'Pe1', 'Pe2', 'Pe3', &
          'b2', 'bK2', 'btd', 'beta', 'betav2', 'bPi2P', 'bK2P', 'bdeta', 'beta2', 'bdPi2P', 'betaPi2P', &
          'bKPi2P'
  else if (indx.eq.3) then
     write(50,'(19A10)') '', 'lnDa', 'lnH', 'Omega_c0', 'f', 'ns', 'nrun', 'b1', 'bd2d', 'betav1', 'Pe1', 'Pe2', 'Pe3', &
          'b2', 'bK2', 'btd', 'Be', 'Pe4', 'Pe5'
  else if (indx.eq.4) then
     write(50,'(28A10)') '', 'lnDa', 'lnH', 'Omega_c0', 'f', 'ns', 'nrun', 'b1', 'bd2d', 'betav1', 'Pe1', 'Pe2', 'Pe3', &
          'b2', 'bK2', 'btd', 'beta', 'betav2', 'bPi2P', 'bK2P', 'bdeta', 'beta2', 'bdPi2P', 'betaPi2P', &
          'bKPi2P', 'Be', 'Pe4', 'Pe5'
  endif
  write(50,'(1A10,28F10.5)') 'lnDa', (cov(1,j)/(sqrt(cov(1,1)*cov(j,j))),j=1,6+nmarg)
  write(50,'(1A10,28F10.5)') 'lnH', (cov(2,j)/(sqrt(cov(2,2)*cov(j,j))),j=1,6+nmarg)
  write(50,'(1A10,28F10.5)') 'Omega_c0', (cov(3,j)/(sqrt(cov(3,3)*cov(j,j))),j=1,6+nmarg)
  write(50,'(1A10,28F10.5)') 'f', (cov(4,j)/(sqrt(cov(4,4)*cov(j,j))),j=1,6+nmarg)
  write(50,'(1A10,28F10.5)') 'ns', (cov(5,j)/(sqrt(cov(5,5)*cov(j,j))),j=1,6+nmarg)
  write(50,'(1A10,28F10.5)') 'nrun', (cov(6,j)/(sqrt(cov(6,6)*cov(j,j))),j=1,6+nmarg)

  do i = 1, nmarg
     if (imarg(i).eq.1) then
        print'(1A22,1F12.7)', 'Err[b1]            = ', sqrt(cov(6+i,6+i))
        write(50,'(1A10,28F10.5)') 'b1', (cov(6+i,j)/(sqrt(cov(6+i,6+i)*cov(j,j))),j=1,6+nmarg)
     elseif (imarg(i).eq.2) then
        print'(1A22,1F12.7)', 'Err[bd2d]          = ', sqrt(cov(6+i,6+i))
        write(50,'(1A10,28F10.5)') 'bd2d', (cov(6+i,j)/(sqrt(cov(6+i,6+i)*cov(j,j))),j=1,6+nmarg)
     elseif (imarg(i).eq.3) then
        print'(1A22,1F12.7)', 'Err[betav1]        = ', sqrt(cov(6+i,6+i))
        write(50,'(1A10,28F10.5)') 'betav1', (cov(6+i,j)/(sqrt(cov(6+i,6+i)*cov(j,j))),j=1,6+nmarg)
     elseif (imarg(i).eq.4) then
        print'(1A22,1F12.7)', 'Err[Pe1]/Pe1       = ', sqrt(cov(6+i,6+i))/Pe1
        write(50,'(1A10,28F10.5)') 'Pe1', (cov(6+i,j)/(sqrt(cov(6+i,6+i)*cov(j,j))),j=1,6+nmarg)
     elseif (imarg(i).eq.5) then
        print'(1A22,1F12.7)', 'Err[Pe2]/Pe1^(5/3) = ', sqrt(cov(6+i,6+i))/(Pe1**(5d0/3d0))
        write(50,'(1A10,28F10.5)') 'Pe2', (cov(6+i,j)/(sqrt(cov(6+i,6+i)*cov(j,j))),j=1,6+nmarg)
     elseif (imarg(i).eq.6) then
        print'(1A22,1F12.7)', 'Err[Pe3]/Pe1^(5/3) = ', sqrt(cov(6+i,6+i))/(Pe1**(5d0/3d0))
        write(50,'(1A10,28F10.5)') 'Pe3', (cov(6+i,j)/(sqrt(cov(6+i,6+i)*cov(j,j))),j=1,6+nmarg)
     elseif (imarg(i).eq.7) then
        print'(1A22,1F12.7)', 'Err[b2]            = ', sqrt(cov(6+i,6+i))
        write(50,'(1A10,28F10.5)') 'b2', (cov(6+i,j)/(sqrt(cov(6+i,6+i)*cov(j,j))),j=1,6+nmarg)
     elseif (imarg(i).eq.8) then
        print'(1A22,1F12.7)', 'Err[bK2]           = ', sqrt(cov(6+i,6+i))
        write(50,'(1A10,28F10.5)') 'bK2', (cov(6+i,j)/(sqrt(cov(6+i,6+i)*cov(j,j))),j=1,6+nmarg)
     elseif (imarg(i).eq.9) then
        print'(1A22,1F12.7)', 'Err[btd]           = ', sqrt(cov(6+i,6+i))
        write(50,'(1A10,28F10.5)') 'btd', (cov(6+i,j)/(sqrt(cov(6+i,6+i)*cov(j,j))),j=1,6+nmarg)
     endif

     if (indx.eq.3) then
        if (imarg(i).eq.10) then
           print'(1A22,1F12.7)', 'Err[Be]/Pe1^2      = ', sqrt(cov(6+i,6+i))/(Pe1**2d0)
           write(50,'(1A10,28F10.5)') 'Be', (cov(6+i,j)/(sqrt(cov(6+i,6+i)*cov(j,j))),j=1,6+nmarg)
        elseif (imarg(i).eq.11) then
           print'(1A22,1F12.7)', 'Err[Pe4]/Pe1       = ', sqrt(cov(6+i,6+i))/Pe1
           write(50,'(1A10,28F10.5)') 'Pe4', (cov(6+i,j)/(sqrt(cov(6+i,6+i)*cov(j,j))),j=1,6+nmarg)
        elseif (imarg(i).eq.12) then
           print'(1A22,1F12.7)', 'Err[Pe5]/Pe1       = ', sqrt(cov(6+i,6+i))/Pe1
           write(50,'(1A10,28F10.5)') 'Pe5', (cov(6+i,j)/(sqrt(cov(6+i,6+i)*cov(j,j))),j=1,6+nmarg)
        endif
     endif
     
     if ((indx.eq.2).or.(indx.eq.4)) then
        if (imarg(i).eq.10) then
           print'(1A22,1F12.7)', 'Err[beta]          = ', sqrt(cov(6+i,6+i))
           write(50,'(1A10,28F10.5)') 'beta', (cov(6+i,j)/(sqrt(cov(6+i,6+i)*cov(j,j))),j=1,6+nmarg)
        elseif (imarg(i).eq.11) then
           print'(1A22,1F12.7)', 'Err[betav2]        = ', sqrt(cov(6+i,6+i))
           write(50,'(1A10,28F10.5)') 'betav2', (cov(6+i,j)/(sqrt(cov(6+i,6+i)*cov(j,j))),j=1,6+nmarg)
        elseif (imarg(i).eq.12) then
           print'(1A22,1F12.7)', 'Err[bPi2P]         = ', sqrt(cov(6+i,6+i))
           write(50,'(1A10,28F10.5)') 'bPi2P', (cov(6+i,j)/(sqrt(cov(6+i,6+i)*cov(j,j))),j=1,6+nmarg)
        elseif (imarg(i).eq.13) then
           print'(1A22,1F12.7)', 'Err[bK2P]          = ', sqrt(cov(6+i,6+i))
           write(50,'(1A10,28F10.5)') 'bK2P', (cov(6+i,j)/(sqrt(cov(6+i,6+i)*cov(j,j))),j=1,6+nmarg)
        elseif (imarg(i).eq.14) then
           print'(1A22,1F12.7)', 'Err[bdeta]         = ', sqrt(cov(6+i,6+i))
           write(50,'(1A10,28F10.5)') 'bdeta', (cov(6+i,j)/(sqrt(cov(6+i,6+i)*cov(j,j))),j=1,6+nmarg)
        elseif (imarg(i).eq.15) then
           print'(1A22,1F12.7)', 'Err[beta2]         = ', sqrt(cov(6+i,6+i))
           write(50,'(1A10,28F10.5)') 'beta2', (cov(6+i,j)/(sqrt(cov(6+i,6+i)*cov(j,j))),j=1,6+nmarg)
        elseif (imarg(i).eq.16) then
           print'(1A22,1F12.7)', 'Err[bdPi2P]        = ', sqrt(cov(6+i,6+i))
           write(50,'(1A10,28F10.5)') 'bdPi2P', (cov(6+i,j)/(sqrt(cov(6+i,6+i)*cov(j,j))),j=1,6+nmarg)
        elseif (imarg(i).eq.17) then
           print'(1A22,1F12.7)', 'Err[betaPi2P]      = ', sqrt(cov(6+i,6+i))
           write(50,'(1A10,28F10.5)') 'betaPi2P', (cov(6+i,j)/(sqrt(cov(6+i,6+i)*cov(j,j))),j=1,6+nmarg)
        elseif (imarg(i).eq.18) then
           print'(1A22,1F12.7)', 'Err[bKPi2P]        = ', sqrt(cov(6+i,6+i))
           write(50,'(1A10,28F10.5)') 'bKPi2P', (cov(6+i,j)/(sqrt(cov(6+i,6+i)*cov(j,j))),j=1,6+nmarg)
        elseif (imarg(i).eq.19) then
           print'(1A22,1F12.7)', 'Err[bPi3P]         = ', sqrt(cov(6+i,6+i))
           write(50,'(1A10,28F10.5)') 'bPi3P', (cov(6+i,j)/(sqrt(cov(6+i,6+i)*cov(j,j))),j=1,6+nmarg)
        endif
     endif

     if (indx.eq.4) then
        if (imarg(i).eq.20) then
           print'(1A22,1F12.7)', 'Err[Be]/Pe1^2      = ', sqrt(cov(6+i,6+i))/(Pe1**2d0)
           write(50,'(1A10,28F10.5)') 'Be', (cov(6+i,j)/(sqrt(cov(6+i,6+i)*cov(j,j))),j=1,6+nmarg)
        elseif (imarg(i).eq.21) then
           print'(1A22,1F12.7)', 'Err[Pe4]/Pe1       = ', sqrt(cov(6+i,6+i))/Pe1
           write(50,'(1A10,28F10.5)') 'Pe4', (cov(6+i,j)/(sqrt(cov(6+i,6+i)*cov(j,j))),j=1,6+nmarg)
        elseif (imarg(i).eq.22) then
           print'(1A22,1F12.7)', 'Err[Pe5]/Pe1       = ', sqrt(cov(6+i,6+i))/Pe1
           write(50,'(1A10,28F10.5)') 'Pe5', (cov(6+i,j)/(sqrt(cov(6+i,6+i)*cov(j,j))),j=1,6+nmarg)
        endif
     endif
     
  enddo
  
  close(50)
    
  DEALLOCATE(cov,covstore,work,Jvectorp,deltatheta)
  return
  
END SUBROUTINE report_result

! -----------------------------------------------------------------

SUBROUTINE report_result_3params(indx,imarg,nmarg,npara,fis,Jvector,Pe1)
  
  IMPLICIT none
  integer, intent(IN) :: indx,imarg(nmarg),nmarg,npara
  double precision, intent(IN) :: fis(npara,npara),Jvector(npara),Pe1
  double precision, allocatable, dimension(:) :: Jvectorp,deltatheta
  double precision, allocatable, dimension(:,:) :: cov,covstore
  integer, allocatable, dimension(:) :: work
  integer :: i,j,k
  
  ALLOCATE (cov(3+nmarg,3+nmarg),covstore(3+nmarg,3+nmarg),work(3+nmarg),&
       Jvectorp(3+nmarg),deltatheta(3+nmarg))
  cov(1:2,1:2) = fis(1:2,1:2)
  cov(1,3) = fis(1,4)
  cov(2,3) = fis(2,4)
  cov(3,1) = cov(1,3)
  cov(3,2) = cov(2,3)
  cov(3,3) = fis(4,4)
  do i = 1, nmarg
     cov(1:2,3+i) = fis(1:2,6+imarg(i))
     cov(3,3+i) = fis(4,6+imarg(i))
     cov(3+i,1:3) = cov(1:3,3+i)
     do j = i, nmarg
        cov(3+i,3+j) = fis(6+imarg(i),6+imarg(j))
        cov(3+j,3+i) = cov(3+i,3+j)
     enddo
  enddo

  open(51,file='fmatrix.dat',action='write') ! Write to file the fisher matrix
  do i = 1, 3+nmarg
     covstore(i,1:3+nmarg) = cov(i,1:3+nmarg)
     if (indx.eq.1) then
        write(51,'(12E30.15)') cov(i,1:3+nmarg)
     else if (indx.eq.2) then
        write(51,'(21E30.15)') cov(i,1:3+nmarg)
     else if (indx.eq.3) then
        write(51,'(15E30.15)') cov(i,1:3+nmarg)
     else if (indx.eq.4) then
        write(51,'(24E30.15)') cov(i,1:3+nmarg)
     endif
  enddo
  close(51)

  Jvectorp(1:2) = Jvector(1:2)
  Jvectorp(3) = Jvector(4)
  do i = 1, nmarg
     Jvectorp(3+i) = Jvector(6+imarg(i))
  enddo

  CALL DVERT(cov,3+nmarg,3+nmarg,work)

  print*, ''
  print*, '1\sigma errors on cosmological parameters:'
  print*, ''
  print'(1A16,1F12.7)', ' Err[lnDa](%)  = ', sqrt(cov(1,1))*1d2
  print'(1A16,1F12.7)', ' Err[lnH](%)   = ', sqrt(cov(2,2))*1d2
  print'(1A16,1F12.7)', ' Err[f]        = ', sqrt(cov(3,3))

  deltatheta = 0d0
  do i = 1, 3+nmarg
     do j = 1, 3+nmarg
        deltatheta(i) = deltatheta(i)+cov(i,j)*Jvectorp(j)
     enddo
  enddo

  open(40,file='shifts_beta.dat',action='write') ! Write to file the shifts in all parameters when betatrue != beta
  do i = 1, 3+nmarg
     write(40,'(1E15.6)') deltatheta(i)/(sqrt(cov(i,i)))
  enddo
  close(40)
  
  print*, ''
  print*, '1\sigma errors on other parameters:'
  print*, ''

  open(50,file='rmatrix.dat',action='write') ! Write to file the full correlation matrix r(a,b)
  if (indx.eq.1) then
     write(50,'(13A10)') '', 'lnDa', 'lnH', 'f', 'b1', 'bd2d', 'betav1', 'Pe1', 'Pe2', 'Pe3', &
          'b2', 'bK2', 'btd'
  else if (indx.eq.2) then
     write(50,'(22A10)') '', 'lnDa', 'lnH', 'f', 'b1', 'bd2d', 'betav1', 'Pe1', 'Pe2', 'Pe3', &
          'b2', 'bK2', 'btd', 'beta', 'betav2', 'bPi2P', 'bK2P', 'bdeta', 'beta2', 'bdPi2P', 'betaPi2P', &
          'bKPi2P'
  else if (indx.eq.3) then
     write(50,'(16A10)') '', 'lnDa', 'lnH', 'f', 'b1', 'bd2d', 'betav1', 'Pe1', 'Pe2', 'Pe3', &
          'b2', 'bK2', 'btd', 'Be', 'Pe4', 'Pe5'
  else if (indx.eq.4) then
     write(50,'(25A10)') '', 'lnDa', 'lnH', 'f', 'b1', 'bd2d', 'betav1', 'Pe1', 'Pe2', 'Pe3', &
          'b2', 'bK2', 'btd', 'beta', 'betav2', 'bPi2P', 'bK2P', 'bdeta', 'beta2', 'bdPi2P', 'betaPi2P', &
          'bKPi2P', 'Be', 'Pe4', 'Pe5'
  endif
  write(50,'(1A10,25F10.5)') 'lnDa', (cov(1,j)/(sqrt(cov(1,1)*cov(j,j))),j=1,3+nmarg)
  write(50,'(1A10,25F10.5)') 'lnH', (cov(2,j)/(sqrt(cov(2,2)*cov(j,j))),j=1,3+nmarg)
  write(50,'(1A10,25F10.5)') 'f', (cov(3,j)/(sqrt(cov(3,3)*cov(j,j))),j=1,3+nmarg)

  do i = 1, nmarg
     if (imarg(i).eq.1) then
        print'(1A22,1F12.7)', 'Err[b1]            = ', sqrt(cov(3+i,3+i))
        write(50,'(1A10,25F10.5)') 'b1', (cov(3+i,j)/(sqrt(cov(3+i,3+i)*cov(j,j))),j=1,3+nmarg)
     elseif (imarg(i).eq.2) then
        print'(1A22,1F12.7)', 'Err[bd2d]          = ', sqrt(cov(3+i,3+i))
        write(50,'(1A10,25F10.5)') 'bd2d', (cov(3+i,j)/(sqrt(cov(3+i,3+i)*cov(j,j))),j=1,3+nmarg)
     elseif (imarg(i).eq.3) then
        print'(1A22,1F12.7)', 'Err[betav1]        = ', sqrt(cov(3+i,3+i))
        write(50,'(1A10,25F10.5)') 'betav1', (cov(3+i,j)/(sqrt(cov(3+i,3+i)*cov(j,j))),j=1,3+nmarg)
     elseif (imarg(i).eq.4) then
        print'(1A22,1F12.7)', 'Err[Pe1]/Pe1       = ', sqrt(cov(3+i,3+i))/Pe1
        write(50,'(1A10,25F10.5)') 'Pe1', (cov(3+i,j)/(sqrt(cov(3+i,3+i)*cov(j,j))),j=1,3+nmarg)
     elseif (imarg(i).eq.5) then
        print'(1A22,1F12.7)', 'Err[Pe2]/Pe1^(5/3) = ', sqrt(cov(3+i,3+i))/(Pe1**(5d0/3d0))
        write(50,'(1A10,25F10.5)') 'Pe2', (cov(3+i,j)/(sqrt(cov(3+i,3+i)*cov(j,j))),j=1,3+nmarg)
     elseif (imarg(i).eq.6) then
        print'(1A22,1F12.7)', 'Err[Pe3]/Pe1^(5/3) = ', sqrt(cov(3+i,3+i))/(Pe1**(5d0/3d0))
        write(50,'(1A10,25F10.5)') 'Pe3', (cov(3+i,j)/(sqrt(cov(3+i,3+i)*cov(j,j))),j=1,3+nmarg)
     elseif (imarg(i).eq.7) then
        print'(1A22,1F12.7)', 'Err[b2]            = ', sqrt(cov(3+i,3+i))
        write(50,'(1A10,25F10.5)') 'b2', (cov(3+i,j)/(sqrt(cov(3+i,3+i)*cov(j,j))),j=1,3+nmarg)
     elseif (imarg(i).eq.8) then
        print'(1A22,1F12.7)', 'Err[bK2]           = ', sqrt(cov(3+i,3+i))
        write(50,'(1A10,25F10.5)') 'bK2', (cov(3+i,j)/(sqrt(cov(3+i,3+i)*cov(j,j))),j=1,3+nmarg)
     elseif (imarg(i).eq.9) then
        print'(1A22,1F12.7)', 'Err[btd]           = ', sqrt(cov(3+i,3+i))
        write(50,'(1A10,25F10.5)') 'btd', (cov(3+i,j)/(sqrt(cov(3+i,3+i)*cov(j,j))),j=1,3+nmarg)
     endif

     if (indx.eq.3) then
        if (imarg(i).eq.10) then
           print'(1A22,1F12.7)', 'Err[Be]/Pe1^2      = ', sqrt(cov(3+i,3+i))/(Pe1**2d0)
           write(50,'(1A10,25F10.5)') 'Be', (cov(3+i,j)/(sqrt(cov(3+i,3+i)*cov(j,j))),j=1,3+nmarg)
        elseif (imarg(i).eq.11) then
           print'(1A22,1F12.7)', 'Err[Pe4]/Pe1       = ', sqrt(cov(3+i,3+i))/Pe1
           write(50,'(1A10,25F10.5)') 'Pe4', (cov(3+i,j)/(sqrt(cov(3+i,3+i)*cov(j,j))),j=1,3+nmarg)
        elseif (imarg(i).eq.12) then
           print'(1A22,1F12.7)', 'Err[Pe5]/Pe1       = ', sqrt(cov(3+i,3+i))/Pe1
           write(50,'(1A10,25F10.5)') 'Pe5', (cov(3+i,j)/(sqrt(cov(3+i,3+i)*cov(j,j))),j=1,3+nmarg)
        endif
     endif
     
     if ((indx.eq.2).or.(indx.eq.4)) then
        if (imarg(i).eq.10) then
           print'(1A22,1F12.7)', 'Err[beta]          = ', sqrt(cov(3+i,3+i))
           write(50,'(1A10,25F10.5)') 'beta', (cov(3+i,j)/(sqrt(cov(3+i,3+i)*cov(j,j))),j=1,3+nmarg)
        elseif (imarg(i).eq.11) then
           print'(1A22,1F12.7)', 'Err[betav2]        = ', sqrt(cov(3+i,3+i))
           write(50,'(1A10,25F10.5)') 'betav2', (cov(3+i,j)/(sqrt(cov(3+i,3+i)*cov(j,j))),j=1,3+nmarg)
        elseif (imarg(i).eq.12) then
           print'(1A22,1F12.7)', 'Err[bPi2P]         = ', sqrt(cov(3+i,3+i))
           write(50,'(1A10,25F10.5)') 'bPi2P', (cov(3+i,j)/(sqrt(cov(3+i,3+i)*cov(j,j))),j=1,3+nmarg)
        elseif (imarg(i).eq.13) then
           print'(1A22,1F12.7)', 'Err[bK2P]          = ', sqrt(cov(3+i,3+i))
           write(50,'(1A10,25F10.5)') 'bK2P', (cov(3+i,j)/(sqrt(cov(3+i,3+i)*cov(j,j))),j=1,3+nmarg)
        elseif (imarg(i).eq.14) then
           print'(1A22,1F12.7)', 'Err[bdeta]         = ', sqrt(cov(3+i,3+i))
           write(50,'(1A10,25F10.5)') 'bdeta', (cov(3+i,j)/(sqrt(cov(3+i,3+i)*cov(j,j))),j=1,3+nmarg)
        elseif (imarg(i).eq.15) then
           print'(1A22,1F12.7)', 'Err[beta2]         = ', sqrt(cov(3+i,3+i))
           write(50,'(1A10,25F10.5)') 'beta2', (cov(3+i,j)/(sqrt(cov(3+i,3+i)*cov(j,j))),j=1,3+nmarg)
        elseif (imarg(i).eq.16) then
           print'(1A22,1F12.7)', 'Err[bdPi2P]        = ', sqrt(cov(3+i,3+i))
           write(50,'(1A10,25F10.5)') 'bdPi2P', (cov(3+i,j)/(sqrt(cov(3+i,3+i)*cov(j,j))),j=1,3+nmarg)
        elseif (imarg(i).eq.17) then
           print'(1A22,1F12.7)', 'Err[betaPi2P]      = ', sqrt(cov(3+i,3+i))
           write(50,'(1A10,25F10.5)') 'betaPi2P', (cov(3+i,j)/(sqrt(cov(3+i,3+i)*cov(j,j))),j=1,3+nmarg)
        elseif (imarg(i).eq.18) then
           print'(1A22,1F12.7)', 'Err[bKPi2P]        = ', sqrt(cov(3+i,3+i))
           write(50,'(1A10,25F10.5)') 'bKPi2P', (cov(3+i,j)/(sqrt(cov(3+i,3+i)*cov(j,j))),j=1,3+nmarg)
        elseif (imarg(i).eq.19) then
           print'(1A22,1F12.7)', 'Err[bPi3P]         = ', sqrt(cov(3+i,3+i))
           write(50,'(1A10,25F10.5)') 'bPi3P', (cov(3+i,j)/(sqrt(cov(3+i,3+i)*cov(j,j))),j=1,3+nmarg)
        endif
     endif

     if (indx.eq.4) then
        if (imarg(i).eq.20) then
           print'(1A22,1F12.7)', 'Err[Be]/Pe1^2      = ', sqrt(cov(3+i,3+i))/(Pe1**2d0)
           write(50,'(1A10,25F10.5)') 'Be', (cov(3+i,j)/(sqrt(cov(3+i,3+i)*cov(j,j))),j=1,3+nmarg)
        elseif (imarg(i).eq.21) then
           print'(1A22,1F12.7)', 'Err[Pe4]/Pe1       = ', sqrt(cov(3+i,3+i))/Pe1
           write(50,'(1A10,25F10.5)') 'Pe4', (cov(3+i,j)/(sqrt(cov(3+i,3+i)*cov(j,j))),j=1,3+nmarg)
        elseif (imarg(i).eq.22) then
           print'(1A22,1F12.7)', 'Err[Pe5]/Pe1       = ', sqrt(cov(3+i,3+i))/Pe1
           write(50,'(1A10,25F10.5)') 'Pe5', (cov(3+i,j)/(sqrt(cov(3+i,3+i)*cov(j,j))),j=1,3+nmarg)
        endif
     endif
     
  enddo
  
  close(50)
    
  DEALLOCATE(cov,covstore,work,Jvectorp,deltatheta)
  return
  
END SUBROUTINE report_result_3params

! -----------------------------------------------------------------

DOUBLE PRECISION FUNCTION ds8dlnk(lnk)

  USE pk_linear
  IMPLICIT none
  DOUBLE PRECISION :: lnk,k,j1,r8
  DOUBLE PRECISION :: pi = 3.14159265d0

  k = dexp(lnk)
  r8 = 8d0
  j1 = (sin(k*r8)/(k*r8)-cos(k*r8))/(k*r8)
  ds8dlnk = (1d0/(2*pi**2d0))*k**3d0*pklinp(k)
  ds8dlnk = ds8dlnk*(3d0/(k*r8)*j1)**2d0

  return
  
END FUNCTION ds8dlnk

! -----------------------------------------------------------------

DOUBLE PRECISION FUNCTION ds12dlnk(lnk)

  USE pk_linear
  IMPLICIT none
  DOUBLE PRECISION :: lnk,k,j1,r12
  DOUBLE PRECISION :: pi = 3.14159265d0

  k = dexp(lnk)
  r12 = 8.1336d0
  j1 = (sin(k*r12)/(k*r12)-cos(k*r12))/(k*r12)
  ds12dlnk = (1d0/(2*pi**2d0))*k**3d0*pklinp(k)
  ds12dlnk = ds12dlnk*(3d0/(k*r12)*j1)**2d0

  return
  
END FUNCTION ds12dlnk

! -----------------------------------------------------------------

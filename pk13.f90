!-----------------------------------------------------------------------------
! Module to calculate all coefficients C13(ln) and derivatives for pk13
!-----------------------------------------------------------------------------

MODULE pk13
  
  IMPLICIT none
  
contains

!-----------------------------------------------------------------------------
  
SUBROUTINE assign_In(pknlcomps,neffnlcomps,calIn,neffIn)

  IMPLICIT NONE

  double precision, dimension(28), intent(in) :: pknlcomps,neffnlcomps
  double precision, intent(out) :: calIn(1:5),neffIn(1:5)
  integer :: i

  calIn = 0d0
  neffIn = 0d0

  do i = 1, 5
     calIn(i) = pknlcomps(i)
     neffIn(i) = neffnlcomps(i)
  enddo

END SUBROUTINE assign_In
    
!-----------------------------------------------------------------------------
  
SUBROUTINE calculate_C13ln(fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
     betaPi2P,bKPi2P,bPi3P,C13ln)

  IMPLICIT none

  DOUBLE PRECISION, intent(IN) :: fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P
  DOUBLE PRECISION, intent(OUT) :: C13ln(0:3,1:5)

  C13ln = 0d0

  C13ln(0,1) = (2*b1**2)/21. + (10*b1*bK2)/7. + (10*b1*bK2P)/21. + (5*b1*bKPi2&
  &P)/21. + (4*b1*bPi2P)/63. + (2*b1*bPi3P)/63. + (4*b1*btd)/7. - (8*b1*beta*f&
  &z)/63. - (2*b1*betaPi2P*fz)/21. - (10*beta*bK2*fz)/21. - (4*beta*bK2P*fz)/2&
  &1. - (2*beta*bKPi2P*fz)/21. + (4*beta*bPi2P*fz)/105. + (3*beta*bPi3P*fz)/70&
  &. - (4*beta*btd*fz)/21. + (2*beta**2*fz**2)/35. + (4*b1*beta2*fz**2)/35. + &
  &(2*beta*betaPi2P*fz**2)/49. - (beta*bPi2P*fz**2)/49. + (6*beta**2*fz**3)/24&
  &5. - (12*beta*beta2*fz**3)/245.

  C13ln(0,2) = b1**2/14. - (15*b1*bK2)/7. - (5*b1*bK2P)/7. - (5*b1*bKPi2P)/14.&
  & - (2*b1*bPi2P)/21. - (b1*bPi3P)/21. - (6*b1*btd)/7. + (b1**2*fz)/7. + (b1*&
  &beta*fz)/21. + (b1*betaPi2P*fz)/7. + (5*beta*bK2*fz)/7. + (2*beta*bK2P*fz)/&
  &7. + (beta*bKPi2P*fz)/7. - (2*beta*bPi2P*fz)/35. - (9*beta*bPi3P*fz)/140. +&
  & (2*beta*btd*fz)/7. - (6*b1*beta*fz**2)/35. - (3*beta**2*fz**2)/70. - (6*b1&
  &*beta2*fz**2)/35. - (3*beta*betaPi2P*fz**2)/49. + (3*beta*bPi2P*fz**2)/98. &
  &+ (6*beta**2*fz**3)/245. + (18*beta*beta2*fz**3)/245.

  C13ln(0,3) = -b1**2/6. + (5*b1*bK2)/7. + (5*b1*bK2P)/21. + (5*b1*bKPi2P)/42.&
  & + (2*b1*bPi2P)/63. + (b1*bPi3P)/63. + (2*b1*btd)/7. - (b1**2*fz)/7. + (5*b&
  &1*beta*fz)/63. + (4*bdPi2P*beta*fz)/21. - (b1*betaPi2P*fz)/21. - (5*beta*bK&
  &2*fz)/21. - (2*beta*bK2P*fz)/63. - (beta*bKPi2P*fz)/63. + (6*beta*bPi2P*fz)&
  &/35. + (61*beta*bPi3P*fz)/420. - (2*beta*btd*fz)/21. + (2*b1*beta*fz**2)/35&
  &. - (4*bdeta*beta*fz**2)/35. - (beta**2*fz**2)/70. + (2*b1*beta2*fz**2)/35.&
  & - (3*beta*betaPi2P*fz**2)/49. - (5*beta*bPi2P*fz**2)/98. + (12*beta**2*fz*&
  &*3)/245. + (18*beta*beta2*fz**3)/245.

  C13ln(0,4) = (-4*bdPi2P*beta*fz)/21. - (4*beta*bK2P*fz)/63. - (2*beta*bKPi2P&
  &*fz)/63. - (16*beta*bPi2P*fz)/105. - (13*beta*bPi3P*fz)/105. + (4*b1*beta*f&
  &z**2)/35. + (4*bdeta*beta*fz**2)/35. + (4*beta*betaPi2P*fz**2)/49. + (2*bet&
  &a*bPi2P*fz**2)/49. - (24*beta**2*fz**3)/245. - (24*beta*beta2*fz**3)/245.

  C13ln(1,1) = (5*b1*bK2P)/21. + (5*b1*bKPi2P)/42. - (4*b1*bPi2P)/9. - (101*b1&
  &*bPi3P)/252. - (16*b1*beta*fz)/63. - (10*b1*betaPi2P*fz)/147. - (20*beta*bK&
  &2*fz)/21. - (65*beta*bK2P*fz)/147. - (65*beta*bKPi2P*fz)/294. + (15*b1*bPi2&
  &P*fz)/98. + (4*beta*bPi2P*fz)/21. + (37*beta*bPi3P*fz)/196. - (8*beta*btd*f&
  &z)/21. - (9*b1*beta*fz**2)/49. + (8*beta**2*fz**2)/49. + (4*b1*beta2*fz**2)&
  &/49. + (10*beta*betaPi2P*fz**2)/147. - (5*beta*bPi2P*fz**2)/42. + (beta**2*&
  &fz**3)/7. - (4*beta*beta2*fz**3)/49.

  C13ln(1,2) = (-5*b1*bK2P)/14. - (5*b1*bKPi2P)/28. + (2*b1*bPi2P)/3. + (101*b&
  &1*bPi3P)/168. + (2*b1**2*fz)/7. + (2*b1*beta*fz)/21. + (5*b1*betaPi2P*fz)/4&
  &9. + (10*beta*bK2*fz)/7. + (65*beta*bK2P*fz)/98. + (65*beta*bKPi2P*fz)/196.&
  & - (45*b1*bPi2P*fz)/196. - (2*beta*bPi2P*fz)/7. - (111*beta*bPi3P*fz)/392. &
  &+ (4*beta*btd*fz)/7. - (3*b1*beta*fz**2)/14. - (6*beta**2*fz**2)/49. - (6*b&
  &1*beta2*fz**2)/49. - (5*beta*betaPi2P*fz**2)/49. + (5*beta*bPi2P*fz**2)/28.&
  & - (beta**2*fz**3)/98. + (6*beta*beta2*fz**3)/49.

  C13ln(1,3) = (-10*b1*bdPi2P)/7. - (5*b1*bK2P)/14. - (5*b1*bKPi2P)/28. - (86*&
  &b1*bPi2P)/63. - (569*b1*bPi3P)/504. + (4*b1**2*fz)/7. + (6*b1*bdeta*fz)/7. &
  &+ (10*b1*beta*fz)/63. + (110*bdPi2P*beta*fz)/147. + (85*b1*betaPi2P*fz)/147&
  &. - (10*beta*bK2*fz)/21. + (25*beta*bK2P*fz)/882. + (25*beta*bKPi2P*fz)/176&
  &4. + (75*b1*bPi2P*fz)/196. + (34*beta*bPi2P*fz)/49. + (683*beta*bPi3P*fz)/1&
  &176. - (4*beta*btd*fz)/21. - (11*b1*beta*fz**2)/14. - (22*bdeta*beta*fz**2)&
  &/49. - (2*beta**2*fz**2)/49. - (34*b1*beta2*fz**2)/49. - (25*beta*betaPi2P*&
  &fz**2)/147. - (5*beta*bPi2P*fz**2)/196. + (11*beta**2*fz**3)/98. + (10*beta&
  &*beta2*fz**3)/49.

  C13ln(1,4) = (10*b1*bdPi2P)/7. + (10*b1*bK2P)/21. + (5*b1*bKPi2P)/21. + (8*b&
  &1*bPi2P)/7. + (13*b1*bPi3P)/14. - (6*b1**2*fz)/7. - (6*b1*bdeta*fz)/7. - (1&
  &10*bdPi2P*beta*fz)/147. - (30*b1*betaPi2P*fz)/49. - (110*beta*bK2P*fz)/441.&
  & - (55*beta*bKPi2P*fz)/441. - (15*b1*bPi2P*fz)/49. - (88*beta*bPi2P*fz)/147&
  &. - (143*beta*bPi3P*fz)/294. + (58*b1*beta*fz**2)/49. + (22*bdeta*beta*fz**&
  &2)/49. + (36*b1*beta2*fz**2)/49. - (10*beta*betaPi2P*fz**2)/147. - (15*beta&
  &*bPi2P*fz**2)/49. + (4*beta**2*fz**3)/49. + (4*beta*beta2*fz**3)/49.

  C13ln(1,5) = (40*beta*betaPi2P*fz**2)/147. + (40*beta*bPi2P*fz**2)/147. - (1&
  &6*beta**2*fz**3)/49. - (16*beta*beta2*fz**3)/49.

  C13ln(2,1) = (8*b1*betaPi2P*fz)/49. - (4*beta*bK2P*fz)/49. - (2*beta*bKPi2P*&
  &fz)/49. + (10*b1*bPi2P*fz)/49. + (16*beta*bPi2P*fz)/105. + (101*beta*bPi3P*&
  &fz)/735. - (12*b1*beta*fz**2)/49. + (16*beta**2*fz**2)/245. - (48*b1*beta2*&
  &fz**2)/245. - (32*beta*betaPi2P*fz**2)/539. - (12*beta*bPi2P*fz**2)/77. + (&
  &72*beta**2*fz**3)/385. + (192*beta*beta2*fz**3)/2695.

  C13ln(2,2) = (-12*b1*betaPi2P*fz)/49. + (6*beta*bK2P*fz)/49. + (3*beta*bKPi2&
  &P*fz)/49. - (15*b1*bPi2P*fz)/49. - (8*beta*bPi2P*fz)/35. - (101*beta*bPi3P*&
  &fz)/490. + (6*b1*beta*fz**2)/35. - (12*beta**2*fz**2)/245. + (72*b1*beta2*f&
  &z**2)/245. + (48*beta*betaPi2P*fz**2)/539. + (18*beta*bPi2P*fz**2)/77. - (3&
  &6*beta**2*fz**3)/245. - (288*beta*beta2*fz**3)/2695.

  C13ln(2,3) = (24*bdPi2P*beta*fz)/49. - (26*b1*betaPi2P*fz)/49. + (6*beta*bK2&
  &P*fz)/49. + (3*beta*bKPi2P*fz)/49. - (45*b1*bPi2P*fz)/49. + (344*beta*bPi2P&
  &*fz)/735. + (569*beta*bPi3P*fz)/1470. + (18*b1*beta*fz**2)/35. - (72*bdeta*&
  &beta*fz**2)/245. - (4*beta**2*fz**2)/245. + (156*b1*beta2*fz**2)/245. + (38&
  &*beta*betaPi2P*fz**2)/539. + (180*beta*bPi2P*fz**2)/539. - (432*beta**2*fz*&
  &*3)/2695. - (228*beta*beta2*fz**3)/2695.

  C13ln(2,4) = (-24*bdPi2P*beta*fz)/49. + (100*b1*betaPi2P*fz)/49. - (8*beta*b&
  &K2P*fz)/49. - (4*beta*bKPi2P*fz)/49. + (120*b1*bPi2P*fz)/49. - (96*beta*bPi&
  &2P*fz)/245. - (78*beta*bPi3P*fz)/245. - (528*b1*beta*fz**2)/245. + (72*bdet&
  &a*beta*fz**2)/245. - (120*b1*beta2*fz**2)/49. - (444*beta*betaPi2P*fz**2)/5&
  &39. - (612*beta*bPi2P*fz**2)/539. + (2664*beta**2*fz**3)/2695. + (2664*beta&
  &*beta2*fz**3)/2695.

  C13ln(2,5) = (-10*b1*betaPi2P*fz)/7. - (10*b1*bPi2P*fz)/7. + (12*b1*beta*fz*&
  &*2)/7. + (12*b1*beta2*fz**2)/7. + (390*beta*betaPi2P*fz**2)/539. + (390*bet&
  &a*bPi2P*fz**2)/539. - (468*beta**2*fz**3)/539. - (468*beta*beta2*fz**3)/539&
  &.

  C13ln(3,1) = (-80*beta*betaPi2P*fz**2)/1617. - (100*beta*bPi2P*fz**2)/1617. &
  &+ (40*beta**2*fz**3)/539. + (32*beta*beta2*fz**3)/539.

  C13ln(3,2) = (40*beta*betaPi2P*fz**2)/539. + (50*beta*bPi2P*fz**2)/539. - (4&
  &*beta**2*fz**3)/49. - (48*beta*beta2*fz**3)/539.

  C13ln(3,3) = (260*beta*betaPi2P*fz**2)/1617. + (150*beta*bPi2P*fz**2)/539. -&
  & (116*beta**2*fz**3)/539. - (104*beta*beta2*fz**3)/539.

  C13ln(3,4) = (-1000*beta*betaPi2P*fz**2)/1617. - (400*beta*bPi2P*fz**2)/539.&
  & + (400*beta**2*fz**3)/539. + (400*beta*beta2*fz**3)/539.

  C13ln(3,5) = (100*beta*betaPi2P*fz**2)/231. + (100*beta*bPi2P*fz**2)/231. - &
  &(40*beta**2*fz**3)/77. - (40*beta*beta2*fz**3)/77.
  
  return
  
END SUBROUTINE calculate_C13ln

!-----------------------------------------------------------------------------

SUBROUTINE calculate_dC13lndf(fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
     betaPi2P,bKPi2P,bPi3P,dC13lndf)

  IMPLICIT none

  DOUBLE PRECISION, intent(IN) :: fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P
  DOUBLE PRECISION, intent(OUT) :: dC13lndf(0:3,1:5)

  dC13lndf = 0d0

  dC13lndf(0,1) = (-8*b1*beta)/63. - (2*b1*betaPi2P)/21. - (10*beta*bK2)/21. -&
  & (4*beta*bK2P)/21. - (2*beta*bKPi2P)/21. + (4*beta*bPi2P)/105. + (3*beta*bP&
  &i3P)/70. - (4*beta*btd)/21. + (4*beta**2*fz)/35. + (8*b1*beta2*fz)/35. + (4&
  &*beta*betaPi2P*fz)/49. - (2*beta*bPi2P*fz)/49. + (18*beta**2*fz**2)/245. - &
  &(36*beta*beta2*fz**2)/245.

  dC13lndf(0,2) = b1**2/7. + (b1*beta)/21. + (b1*betaPi2P)/7. + (5*beta*bK2)/7&
  &. + (2*beta*bK2P)/7. + (beta*bKPi2P)/7. - (2*beta*bPi2P)/35. - (9*beta*bPi3&
  &P)/140. + (2*beta*btd)/7. - (12*b1*beta*fz)/35. - (3*beta**2*fz)/35. - (12*&
  &b1*beta2*fz)/35. - (6*beta*betaPi2P*fz)/49. + (3*beta*bPi2P*fz)/49. + (18*b&
  &eta**2*fz**2)/245. + (54*beta*beta2*fz**2)/245.

  dC13lndf(0,3) = -b1**2/7. + (5*b1*beta)/63. + (4*bdPi2P*beta)/21. - (b1*beta&
  &Pi2P)/21. - (5*beta*bK2)/21. - (2*beta*bK2P)/63. - (beta*bKPi2P)/63. + (6*b&
  &eta*bPi2P)/35. + (61*beta*bPi3P)/420. - (2*beta*btd)/21. + (4*b1*beta*fz)/3&
  &5. - (8*bdeta*beta*fz)/35. - (beta**2*fz)/35. + (4*b1*beta2*fz)/35. - (6*be&
  &ta*betaPi2P*fz)/49. - (5*beta*bPi2P*fz)/49. + (36*beta**2*fz**2)/245. + (54&
  &*beta*beta2*fz**2)/245.

  dC13lndf(0,4) = (-4*bdPi2P*beta)/21. - (4*beta*bK2P)/63. - (2*beta*bKPi2P)/6&
  &3. - (16*beta*bPi2P)/105. - (13*beta*bPi3P)/105. + (8*b1*beta*fz)/35. + (8*&
  &bdeta*beta*fz)/35. + (8*beta*betaPi2P*fz)/49. + (4*beta*bPi2P*fz)/49. - (72&
  &*beta**2*fz**2)/245. - (72*beta*beta2*fz**2)/245.

  dC13lndf(1,1) = (-16*b1*beta)/63. - (10*b1*betaPi2P)/147. - (20*beta*bK2)/21&
  &. - (65*beta*bK2P)/147. - (65*beta*bKPi2P)/294. + (15*b1*bPi2P)/98. + (4*be&
  &ta*bPi2P)/21. + (37*beta*bPi3P)/196. - (8*beta*btd)/21. - (18*b1*beta*fz)/4&
  &9. + (16*beta**2*fz)/49. + (8*b1*beta2*fz)/49. + (20*beta*betaPi2P*fz)/147.&
  & - (5*beta*bPi2P*fz)/21. + (3*beta**2*fz**2)/7. - (12*beta*beta2*fz**2)/49.

  dC13lndf(1,2) = (2*b1**2)/7. + (2*b1*beta)/21. + (5*b1*betaPi2P)/49. + (10*b&
  &eta*bK2)/7. + (65*beta*bK2P)/98. + (65*beta*bKPi2P)/196. - (45*b1*bPi2P)/19&
  &6. - (2*beta*bPi2P)/7. - (111*beta*bPi3P)/392. + (4*beta*btd)/7. - (3*b1*be&
  &ta*fz)/7. - (12*beta**2*fz)/49. - (12*b1*beta2*fz)/49. - (10*beta*betaPi2P*&
  &fz)/49. + (5*beta*bPi2P*fz)/14. - (3*beta**2*fz**2)/98. + (18*beta*beta2*fz&
  &**2)/49.

  dC13lndf(1,3) = (4*b1**2)/7. + (6*b1*bdeta)/7. + (10*b1*beta)/63. + (110*bdP&
  &i2P*beta)/147. + (85*b1*betaPi2P)/147. - (10*beta*bK2)/21. + (25*beta*bK2P)&
  &/882. + (25*beta*bKPi2P)/1764. + (75*b1*bPi2P)/196. + (34*beta*bPi2P)/49. +&
  & (683*beta*bPi3P)/1176. - (4*beta*btd)/21. - (11*b1*beta*fz)/7. - (44*bdeta&
  &*beta*fz)/49. - (4*beta**2*fz)/49. - (68*b1*beta2*fz)/49. - (50*beta*betaPi&
  &2P*fz)/147. - (5*beta*bPi2P*fz)/98. + (33*beta**2*fz**2)/98. + (30*beta*bet&
  &a2*fz**2)/49.

  dC13lndf(1,4) = (-6*b1**2)/7. - (6*b1*bdeta)/7. - (110*bdPi2P*beta)/147. - (&
  &30*b1*betaPi2P)/49. - (110*beta*bK2P)/441. - (55*beta*bKPi2P)/441. - (15*b1&
  &*bPi2P)/49. - (88*beta*bPi2P)/147. - (143*beta*bPi3P)/294. + (116*b1*beta*f&
  &z)/49. + (44*bdeta*beta*fz)/49. + (72*b1*beta2*fz)/49. - (20*beta*betaPi2P*&
  &fz)/147. - (30*beta*bPi2P*fz)/49. + (12*beta**2*fz**2)/49. + (12*beta*beta2&
  &*fz**2)/49.

  dC13lndf(1,5) = (80*beta*betaPi2P*fz)/147. + (80*beta*bPi2P*fz)/147. - (48*b&
  &eta**2*fz**2)/49. - (48*beta*beta2*fz**2)/49.

  dC13lndf(2,1) = (8*b1*betaPi2P)/49. - (4*beta*bK2P)/49. - (2*beta*bKPi2P)/49&
  &. + (10*b1*bPi2P)/49. + (16*beta*bPi2P)/105. + (101*beta*bPi3P)/735. - (24*&
  &b1*beta*fz)/49. + (32*beta**2*fz)/245. - (96*b1*beta2*fz)/245. - (64*beta*b&
  &etaPi2P*fz)/539. - (24*beta*bPi2P*fz)/77. + (216*beta**2*fz**2)/385. + (576&
  &*beta*beta2*fz**2)/2695.

  dC13lndf(2,2) = (-12*b1*betaPi2P)/49. + (6*beta*bK2P)/49. + (3*beta*bKPi2P)/&
  &49. - (15*b1*bPi2P)/49. - (8*beta*bPi2P)/35. - (101*beta*bPi3P)/490. + (12*&
  &b1*beta*fz)/35. - (24*beta**2*fz)/245. + (144*b1*beta2*fz)/245. + (96*beta*&
  &betaPi2P*fz)/539. + (36*beta*bPi2P*fz)/77. - (108*beta**2*fz**2)/245. - (86&
  &4*beta*beta2*fz**2)/2695.

  dC13lndf(2,3) = (24*bdPi2P*beta)/49. - (26*b1*betaPi2P)/49. + (6*beta*bK2P)/&
  &49. + (3*beta*bKPi2P)/49. - (45*b1*bPi2P)/49. + (344*beta*bPi2P)/735. + (56&
  &9*beta*bPi3P)/1470. + (36*b1*beta*fz)/35. - (144*bdeta*beta*fz)/245. - (8*b&
  &eta**2*fz)/245. + (312*b1*beta2*fz)/245. + (76*beta*betaPi2P*fz)/539. + (36&
  &0*beta*bPi2P*fz)/539. - (1296*beta**2*fz**2)/2695. - (684*beta*beta2*fz**2)&
  &/2695.

  dC13lndf(2,4) = (-24*bdPi2P*beta)/49. + (100*b1*betaPi2P)/49. - (8*beta*bK2P&
  &)/49. - (4*beta*bKPi2P)/49. + (120*b1*bPi2P)/49. - (96*beta*bPi2P)/245. - (&
  &78*beta*bPi3P)/245. - (1056*b1*beta*fz)/245. + (144*bdeta*beta*fz)/245. - (&
  &240*b1*beta2*fz)/49. - (888*beta*betaPi2P*fz)/539. - (1224*beta*bPi2P*fz)/5&
  &39. + (7992*beta**2*fz**2)/2695. + (7992*beta*beta2*fz**2)/2695.

  dC13lndf(2,5) = (-10*b1*betaPi2P)/7. - (10*b1*bPi2P)/7. + (24*b1*beta*fz)/7.&
  & + (24*b1*beta2*fz)/7. + (780*beta*betaPi2P*fz)/539. + (780*beta*bPi2P*fz)/&
  &539. - (1404*beta**2*fz**2)/539. - (1404*beta*beta2*fz**2)/539.

  dC13lndf(3,1) = (-160*beta*betaPi2P*fz)/1617. - (200*beta*bPi2P*fz)/1617. + &
  &(120*beta**2*fz**2)/539. + (96*beta*beta2*fz**2)/539.

  dC13lndf(3,2) = (80*beta*betaPi2P*fz)/539. + (100*beta*bPi2P*fz)/539. - (12*&
  &beta**2*fz**2)/49. - (144*beta*beta2*fz**2)/539.

  dC13lndf(3,3) = (520*beta*betaPi2P*fz)/1617. + (300*beta*bPi2P*fz)/539. - (3&
  &48*beta**2*fz**2)/539. - (312*beta*beta2*fz**2)/539.

  dC13lndf(3,4) = (-2000*beta*betaPi2P*fz)/1617. - (800*beta*bPi2P*fz)/539. + &
  &(1200*beta**2*fz**2)/539. + (1200*beta*beta2*fz**2)/539.

  dC13lndf(3,5) = (200*beta*betaPi2P*fz)/231. + (200*beta*bPi2P*fz)/231. - (12&
  &0*beta**2*fz**2)/77. - (120*beta*beta2*fz**2)/77.
  
  return
  
END SUBROUTINE calculate_dC13lndf

!-----------------------------------------------------------------------------

SUBROUTINE calculate_dC13lndb1(fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
     betaPi2P,bKPi2P,bPi3P,dC13lndb1)

  IMPLICIT none

  DOUBLE PRECISION, intent(IN) :: fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P
  DOUBLE PRECISION, intent(OUT) :: dC13lndb1(0:3,1:5)

  dC13lndb1 = 0d0

  dC13lndb1(0,1) = (4*b1)/21. + (10*bK2)/7. + (10*bK2P)/21. + (5*bKPi2P)/21. +&
  & (4*bPi2P)/63. + (2*bPi3P)/63. + (4*btd)/7. - (8*beta*fz)/63. - (2*betaPi2P&
  &*fz)/21. + (4*beta2*fz**2)/35.

  dC13lndb1(0,2) = b1/7. - (15*bK2)/7. - (5*bK2P)/7. - (5*bKPi2P)/14. - (2*bPi&
  &2P)/21. - bPi3P/21. - (6*btd)/7. + (2*b1*fz)/7. + (beta*fz)/21. + (betaPi2P&
  &*fz)/7. - (6*beta*fz**2)/35. - (6*beta2*fz**2)/35.

  dC13lndb1(0,3) = -b1/3. + (5*bK2)/7. + (5*bK2P)/21. + (5*bKPi2P)/42. + (2*bP&
  &i2P)/63. + bPi3P/63. + (2*btd)/7. - (2*b1*fz)/7. + (5*beta*fz)/63. - (betaP&
  &i2P*fz)/21. + (2*beta*fz**2)/35. + (2*beta2*fz**2)/35.

  dC13lndb1(0,4) = (4*beta*fz**2)/35.

  dC13lndb1(1,1) = (5*bK2P)/21. + (5*bKPi2P)/42. - (4*bPi2P)/9. - (101*bPi3P)/&
  &252. - (16*beta*fz)/63. - (10*betaPi2P*fz)/147. + (15*bPi2P*fz)/98. - (9*be&
  &ta*fz**2)/49. + (4*beta2*fz**2)/49.

  dC13lndb1(1,2) = (-5*bK2P)/14. - (5*bKPi2P)/28. + (2*bPi2P)/3. + (101*bPi3P)&
  &/168. + (4*b1*fz)/7. + (2*beta*fz)/21. + (5*betaPi2P*fz)/49. - (45*bPi2P*fz&
  &)/196. - (3*beta*fz**2)/14. - (6*beta2*fz**2)/49.

  dC13lndb1(1,3) = (-10*bdPi2P)/7. - (5*bK2P)/14. - (5*bKPi2P)/28. - (86*bPi2P&
  &)/63. - (569*bPi3P)/504. + (8*b1*fz)/7. + (6*bdeta*fz)/7. + (10*beta*fz)/63&
  &. + (85*betaPi2P*fz)/147. + (75*bPi2P*fz)/196. - (11*beta*fz**2)/14. - (34*&
  &beta2*fz**2)/49.

  dC13lndb1(1,4) = (10*bdPi2P)/7. + (10*bK2P)/21. + (5*bKPi2P)/21. + (8*bPi2P)&
  &/7. + (13*bPi3P)/14. - (12*b1*fz)/7. - (6*bdeta*fz)/7. - (30*betaPi2P*fz)/4&
  &9. - (15*bPi2P*fz)/49. + (58*beta*fz**2)/49. + (36*beta2*fz**2)/49.

  dC13lndb1(2,1) = (8*betaPi2P*fz)/49. + (10*bPi2P*fz)/49. - (12*beta*fz**2)/4&
  &9. - (48*beta2*fz**2)/245.

  dC13lndb1(2,2) = (-12*betaPi2P*fz)/49. - (15*bPi2P*fz)/49. + (6*beta*fz**2)/&
  &35. + (72*beta2*fz**2)/245.

  dC13lndb1(2,3) = (-26*betaPi2P*fz)/49. - (45*bPi2P*fz)/49. + (18*beta*fz**2)&
  &/35. + (156*beta2*fz**2)/245.

  dC13lndb1(2,4) = (100*betaPi2P*fz)/49. + (120*bPi2P*fz)/49. - (528*beta*fz**&
  &2)/245. - (120*beta2*fz**2)/49.

  dC13lndb1(2,5) = (-10*betaPi2P*fz)/7. - (10*bPi2P*fz)/7. + (12*beta*fz**2)/7&
  &. + (12*beta2*fz**2)/7.  

  return
  
END SUBROUTINE calculate_dC13lndb1

!-----------------------------------------------------------------------------

SUBROUTINE calculate_dC13lndbK2(fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
     betaPi2P,bKPi2P,bPi3P,dC13lndbK2)

  IMPLICIT none

  DOUBLE PRECISION, intent(IN) :: fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P
  DOUBLE PRECISION, intent(OUT) :: dC13lndbK2(0:3,1:5)

  dC13lndbK2 = 0d0

  dC13lndbK2(0,1) = (10*b1)/7. - (10*beta*fz)/21.

  dC13lndbK2(0,2) = (-15*b1)/7. + (5*beta*fz)/7.

  dC13lndbK2(0,3) = (5*b1)/7. - (5*beta*fz)/21.

  dC13lndbK2(1,1) = (-20*beta*fz)/21.

  dC13lndbK2(1,2) = (10*beta*fz)/7.

  dC13lndbK2(1,3) = (-10*beta*fz)/21.  

  return
  
END SUBROUTINE calculate_dC13lndbK2

!-----------------------------------------------------------------------------

SUBROUTINE calculate_dC13lndbtd(fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
     betaPi2P,bKPi2P,bPi3P,dC13lndbtd)

  IMPLICIT none

  DOUBLE PRECISION, intent(IN) :: fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P
  DOUBLE PRECISION, intent(OUT) :: dC13lndbtd(0:3,1:5)

  dC13lndbtd = 0d0

  dC13lndbtd(0,1) = (4*b1)/7. - (4*beta*fz)/21.

  dC13lndbtd(0,2) = (-6*b1)/7. + (2*beta*fz)/7.

  dC13lndbtd(0,3) = (2*b1)/7. - (2*beta*fz)/21.

  dC13lndbtd(1,1) = (-8*beta*fz)/21.

  dC13lndbtd(1,2) = (4*beta*fz)/7.

  dC13lndbtd(1,3) = (-4*beta*fz)/21.  

  return
  
END SUBROUTINE calculate_dC13lndbtd

!-----------------------------------------------------------------------------

SUBROUTINE calculate_dC13lndbeta(fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
     betaPi2P,bKPi2P,bPi3P,dC13lndbeta)

  IMPLICIT none

  DOUBLE PRECISION, intent(IN) :: fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P
  DOUBLE PRECISION, intent(OUT) :: dC13lndbeta(0:3,1:5)

  dC13lndbeta = 0d0

  dC13lndbeta(0,1) = (-8*b1*fz)/63. - (10*bK2*fz)/21. - (4*bK2P*fz)/21. - (2*b&
  &KPi2P*fz)/21. + (4*bPi2P*fz)/105. + (3*bPi3P*fz)/70. - (4*btd*fz)/21. + (4*&
  &beta*fz**2)/35. + (2*betaPi2P*fz**2)/49. - (bPi2P*fz**2)/49. + (12*beta*fz*&
  &*3)/245. - (12*beta2*fz**3)/245.

  dC13lndbeta(0,2) = (b1*fz)/21. + (5*bK2*fz)/7. + (2*bK2P*fz)/7. + (bKPi2P*fz&
  &)/7. - (2*bPi2P*fz)/35. - (9*bPi3P*fz)/140. + (2*btd*fz)/7. - (6*b1*fz**2)/&
  &35. - (3*beta*fz**2)/35. - (3*betaPi2P*fz**2)/49. + (3*bPi2P*fz**2)/98. + (&
  &12*beta*fz**3)/245. + (18*beta2*fz**3)/245.

  dC13lndbeta(0,3) = (5*b1*fz)/63. + (4*bdPi2P*fz)/21. - (5*bK2*fz)/21. - (2*b&
  &K2P*fz)/63. - (bKPi2P*fz)/63. + (6*bPi2P*fz)/35. + (61*bPi3P*fz)/420. - (2*&
  &btd*fz)/21. + (2*b1*fz**2)/35. - (4*bdeta*fz**2)/35. - (beta*fz**2)/35. - (&
  &3*betaPi2P*fz**2)/49. - (5*bPi2P*fz**2)/98. + (24*beta*fz**3)/245. + (18*be&
  &ta2*fz**3)/245.

  dC13lndbeta(0,4) = (-4*bdPi2P*fz)/21. - (4*bK2P*fz)/63. - (2*bKPi2P*fz)/63. &
  &- (16*bPi2P*fz)/105. - (13*bPi3P*fz)/105. + (4*b1*fz**2)/35. + (4*bdeta*fz*&
  &*2)/35. + (4*betaPi2P*fz**2)/49. + (2*bPi2P*fz**2)/49. - (48*beta*fz**3)/24&
  &5. - (24*beta2*fz**3)/245.

  dC13lndbeta(1,1) = (-16*b1*fz)/63. - (20*bK2*fz)/21. - (65*bK2P*fz)/147. - (&
  &65*bKPi2P*fz)/294. + (4*bPi2P*fz)/21. + (37*bPi3P*fz)/196. - (8*btd*fz)/21.&
  & - (9*b1*fz**2)/49. + (16*beta*fz**2)/49. + (10*betaPi2P*fz**2)/147. - (5*b&
  &Pi2P*fz**2)/42. + (2*beta*fz**3)/7. - (4*beta2*fz**3)/49.

  dC13lndbeta(1,2) = (2*b1*fz)/21. + (10*bK2*fz)/7. + (65*bK2P*fz)/98. + (65*b&
  &KPi2P*fz)/196. - (2*bPi2P*fz)/7. - (111*bPi3P*fz)/392. + (4*btd*fz)/7. - (3&
  &*b1*fz**2)/14. - (12*beta*fz**2)/49. - (5*betaPi2P*fz**2)/49. + (5*bPi2P*fz&
  &**2)/28. - (beta*fz**3)/49. + (6*beta2*fz**3)/49.

  dC13lndbeta(1,3) = (10*b1*fz)/63. + (110*bdPi2P*fz)/147. - (10*bK2*fz)/21. +&
  & (25*bK2P*fz)/882. + (25*bKPi2P*fz)/1764. + (34*bPi2P*fz)/49. + (683*bPi3P*&
  &fz)/1176. - (4*btd*fz)/21. - (11*b1*fz**2)/14. - (22*bdeta*fz**2)/49. - (4*&
  &beta*fz**2)/49. - (25*betaPi2P*fz**2)/147. - (5*bPi2P*fz**2)/196. + (11*bet&
  &a*fz**3)/49. + (10*beta2*fz**3)/49.

  dC13lndbeta(1,4) = (-110*bdPi2P*fz)/147. - (110*bK2P*fz)/441. - (55*bKPi2P*f&
  &z)/441. - (88*bPi2P*fz)/147. - (143*bPi3P*fz)/294. + (58*b1*fz**2)/49. + (2&
  &2*bdeta*fz**2)/49. - (10*betaPi2P*fz**2)/147. - (15*bPi2P*fz**2)/49. + (8*b&
  &eta*fz**3)/49. + (4*beta2*fz**3)/49.

  dC13lndbeta(1,5) = (40*betaPi2P*fz**2)/147. + (40*bPi2P*fz**2)/147. - (32*be&
  &ta*fz**3)/49. - (16*beta2*fz**3)/49.

  dC13lndbeta(2,1) = (-4*bK2P*fz)/49. - (2*bKPi2P*fz)/49. + (16*bPi2P*fz)/105.&
  & + (101*bPi3P*fz)/735. - (12*b1*fz**2)/49. + (32*beta*fz**2)/245. - (32*bet&
  &aPi2P*fz**2)/539. - (12*bPi2P*fz**2)/77. + (144*beta*fz**3)/385. + (192*bet&
  &a2*fz**3)/2695.

  dC13lndbeta(2,2) = (6*bK2P*fz)/49. + (3*bKPi2P*fz)/49. - (8*bPi2P*fz)/35. - &
  &(101*bPi3P*fz)/490. + (6*b1*fz**2)/35. - (24*beta*fz**2)/245. + (48*betaPi2&
  &P*fz**2)/539. + (18*bPi2P*fz**2)/77. - (72*beta*fz**3)/245. - (288*beta2*fz&
  &**3)/2695.

  dC13lndbeta(2,3) = (24*bdPi2P*fz)/49. + (6*bK2P*fz)/49. + (3*bKPi2P*fz)/49. &
  &+ (344*bPi2P*fz)/735. + (569*bPi3P*fz)/1470. + (18*b1*fz**2)/35. - (72*bdet&
  &a*fz**2)/245. - (8*beta*fz**2)/245. + (38*betaPi2P*fz**2)/539. + (180*bPi2P&
  &*fz**2)/539. - (864*beta*fz**3)/2695. - (228*beta2*fz**3)/2695.

  dC13lndbeta(2,4) = (-24*bdPi2P*fz)/49. - (8*bK2P*fz)/49. - (4*bKPi2P*fz)/49.&
  & - (96*bPi2P*fz)/245. - (78*bPi3P*fz)/245. - (528*b1*fz**2)/245. + (72*bdet&
  &a*fz**2)/245. - (444*betaPi2P*fz**2)/539. - (612*bPi2P*fz**2)/539. + (5328*&
  &beta*fz**3)/2695. + (2664*beta2*fz**3)/2695.

  dC13lndbeta(2,5) = (12*b1*fz**2)/7. + (390*betaPi2P*fz**2)/539. + (390*bPi2P&
  &*fz**2)/539. - (936*beta*fz**3)/539. - (468*beta2*fz**3)/539.

  dC13lndbeta(3,1) = (-80*betaPi2P*fz**2)/1617. - (100*bPi2P*fz**2)/1617. + (8&
  &0*beta*fz**3)/539. + (32*beta2*fz**3)/539.

  dC13lndbeta(3,2) = (40*betaPi2P*fz**2)/539. + (50*bPi2P*fz**2)/539. - (8*bet&
  &a*fz**3)/49. - (48*beta2*fz**3)/539.

  dC13lndbeta(3,3) = (260*betaPi2P*fz**2)/1617. + (150*bPi2P*fz**2)/539. - (23&
  &2*beta*fz**3)/539. - (104*beta2*fz**3)/539.

  dC13lndbeta(3,4) = (-1000*betaPi2P*fz**2)/1617. - (400*bPi2P*fz**2)/539. + (&
  &800*beta*fz**3)/539. + (400*beta2*fz**3)/539.

  dC13lndbeta(3,5) = (100*betaPi2P*fz**2)/231. + (100*bPi2P*fz**2)/231. - (80*&
  &beta*fz**3)/77. - (40*beta2*fz**3)/77.  

  return
  
END SUBROUTINE calculate_dC13lndbeta

!-----------------------------------------------------------------------------

SUBROUTINE calculate_dC13lndbPi2P(fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
     betaPi2P,bKPi2P,bPi3P,dC13lndbPi2P)

  IMPLICIT none

  DOUBLE PRECISION, intent(IN) :: fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P
  DOUBLE PRECISION, intent(OUT) :: dC13lndbPi2P(0:3,1:5)

  dC13lndbPi2P = 0d0

  dC13lndbPi2P(0,1) = (4*b1)/63. + (4*beta*fz)/105. - (beta*fz**2)/49.

  dC13lndbPi2P(0,2) = (-2*b1)/21. - (2*beta*fz)/35. + (3*beta*fz**2)/98.

  dC13lndbPi2P(0,3) = (2*b1)/63. + (6*beta*fz)/35. - (5*beta*fz**2)/98.

  dC13lndbPi2P(0,4) = (-16*beta*fz)/105. + (2*beta*fz**2)/49.

  dC13lndbPi2P(1,1) = (-4*b1)/9. + (15*b1*fz)/98. + (4*beta*fz)/21. - (5*beta*&
  &fz**2)/42.

  dC13lndbPi2P(1,2) = (2*b1)/3. - (45*b1*fz)/196. - (2*beta*fz)/7. + (5*beta*f&
  &z**2)/28.

  dC13lndbPi2P(1,3) = (-86*b1)/63. + (75*b1*fz)/196. + (34*beta*fz)/49. - (5*b&
  &eta*fz**2)/196.

  dC13lndbPi2P(1,4) = (8*b1)/7. - (15*b1*fz)/49. - (88*beta*fz)/147. - (15*bet&
  &a*fz**2)/49.

  dC13lndbPi2P(1,5) = (40*beta*fz**2)/147.

  dC13lndbPi2P(2,1) = (10*b1*fz)/49. + (16*beta*fz)/105. - (12*beta*fz**2)/77.

  dC13lndbPi2P(2,2) = (-15*b1*fz)/49. - (8*beta*fz)/35. + (18*beta*fz**2)/77.

  dC13lndbPi2P(2,3) = (-45*b1*fz)/49. + (344*beta*fz)/735. + (180*beta*fz**2)/&
  &539.

  dC13lndbPi2P(2,4) = (120*b1*fz)/49. - (96*beta*fz)/245. - (612*beta*fz**2)/5&
  &39.

  dC13lndbPi2P(2,5) = (-10*b1*fz)/7. + (390*beta*fz**2)/539.

  dC13lndbPi2P(3,1) = (-100*beta*fz**2)/1617.

  dC13lndbPi2P(3,2) = (50*beta*fz**2)/539.

  dC13lndbPi2P(3,3) = (150*beta*fz**2)/539.

  dC13lndbPi2P(3,4) = (-400*beta*fz**2)/539.

  dC13lndbPi2P(3,5) = (100*beta*fz**2)/231.

  return
  
END SUBROUTINE calculate_dC13lndbPi2P

!-----------------------------------------------------------------------------

SUBROUTINE calculate_dC13lndbK2P(fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
     betaPi2P,bKPi2P,bPi3P,dC13lndbK2P)

  IMPLICIT none

  DOUBLE PRECISION, intent(IN) :: fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P
  DOUBLE PRECISION, intent(OUT) :: dC13lndbK2P(0:3,1:5)

  dC13lndbK2P = 0d0

  dC13lndbK2P(0,1) = (10*b1)/21. - (4*beta*fz)/21.

  dC13lndbK2P(0,2) = (-5*b1)/7. + (2*beta*fz)/7.

  dC13lndbK2P(0,3) = (5*b1)/21. - (2*beta*fz)/63.

  dC13lndbK2P(0,4) = (-4*beta*fz)/63.

  dC13lndbK2P(1,1) = (5*b1)/21. - (65*beta*fz)/147.

  dC13lndbK2P(1,2) = (-5*b1)/14. + (65*beta*fz)/98.

  dC13lndbK2P(1,3) = (-5*b1)/14. + (25*beta*fz)/882.

  dC13lndbK2P(1,4) = (10*b1)/21. - (110*beta*fz)/441.

  dC13lndbK2P(2,1) = (-4*beta*fz)/49.

  dC13lndbK2P(2,2) = (6*beta*fz)/49.

  dC13lndbK2P(2,3) = (6*beta*fz)/49.

  dC13lndbK2P(2,4) = (-8*beta*fz)/49.  

  return
  
END SUBROUTINE calculate_dC13lndbK2P

!-----------------------------------------------------------------------------

SUBROUTINE calculate_dC13lndbdeta(fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
     betaPi2P,bKPi2P,bPi3P,dC13lndbdeta)

  IMPLICIT none

  DOUBLE PRECISION, intent(IN) :: fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P
  DOUBLE PRECISION, intent(OUT) :: dC13lndbdeta(0:3,1:5)

  dC13lndbdeta = 0d0

  dC13lndbdeta(0,3) = (-4*beta*fz**2)/35.

  dC13lndbdeta(0,4) = (4*beta*fz**2)/35.

  dC13lndbdeta(1,3) = (6*b1*fz)/7. - (22*beta*fz**2)/49.

  dC13lndbdeta(1,4) = (-6*b1*fz)/7. + (22*beta*fz**2)/49.

  dC13lndbdeta(2,3) = (-72*beta*fz**2)/245.

  dC13lndbdeta(2,4) = (72*beta*fz**2)/245.  

  return
  
END SUBROUTINE calculate_dC13lndbdeta

!-----------------------------------------------------------------------------

SUBROUTINE calculate_dC13lndbeta2(fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
     betaPi2P,bKPi2P,bPi3P,dC13lndbeta2)

  IMPLICIT none

  DOUBLE PRECISION, intent(IN) :: fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P
  DOUBLE PRECISION, intent(OUT) :: dC13lndbeta2(0:3,1:5)

  dC13lndbeta2 = 0d0

  dC13lndbeta2(0,1) = (4*b1*fz**2)/35. - (12*beta*fz**3)/245.

  dC13lndbeta2(0,2) = (-6*b1*fz**2)/35. + (18*beta*fz**3)/245.

  dC13lndbeta2(0,3) = (2*b1*fz**2)/35. + (18*beta*fz**3)/245.

  dC13lndbeta2(0,4) = (-24*beta*fz**3)/245.

  dC13lndbeta2(1,1) = (4*b1*fz**2)/49. - (4*beta*fz**3)/49.

  dC13lndbeta2(1,2) = (-6*b1*fz**2)/49. + (6*beta*fz**3)/49.

  dC13lndbeta2(1,3) = (-34*b1*fz**2)/49. + (10*beta*fz**3)/49.

  dC13lndbeta2(1,4) = (36*b1*fz**2)/49. + (4*beta*fz**3)/49.

  dC13lndbeta2(1,5) = (-16*beta*fz**3)/49.

  dC13lndbeta2(2,1) = (-48*b1*fz**2)/245. + (192*beta*fz**3)/2695.

  dC13lndbeta2(2,2) = (72*b1*fz**2)/245. - (288*beta*fz**3)/2695.

  dC13lndbeta2(2,3) = (156*b1*fz**2)/245. - (228*beta*fz**3)/2695.

  dC13lndbeta2(2,4) = (-120*b1*fz**2)/49. + (2664*beta*fz**3)/2695.

  dC13lndbeta2(2,5) = (12*b1*fz**2)/7. - (468*beta*fz**3)/539.

  dC13lndbeta2(3,1) = (32*beta*fz**3)/539.

  dC13lndbeta2(3,2) = (-48*beta*fz**3)/539.

  dC13lndbeta2(3,3) = (-104*beta*fz**3)/539.

  dC13lndbeta2(3,4) = (400*beta*fz**3)/539.

  dC13lndbeta2(3,5) = (-40*beta*fz**3)/77.  

  return
  
END SUBROUTINE calculate_dC13lndbeta2

!-----------------------------------------------------------------------------

SUBROUTINE calculate_dC13lndbdPi2P(fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
     betaPi2P,bKPi2P,bPi3P,dC13lndbdPi2P)

  IMPLICIT none

  DOUBLE PRECISION, intent(IN) :: fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P
  DOUBLE PRECISION, intent(OUT) :: dC13lndbdPi2P(0:3,1:5)

  dC13lndbdPi2P = 0d0

  dC13lndbdPi2P(0,3) = (4*beta*fz)/21.

  dC13lndbdPi2P(0,4) = (-4*beta*fz)/21.

  dC13lndbdPi2P(1,3) = (-10*b1)/7. + (110*beta*fz)/147.

  dC13lndbdPi2P(1,4) = (10*b1)/7. - (110*beta*fz)/147.

  dC13lndbdPi2P(2,3) = (24*beta*fz)/49.

  dC13lndbdPi2P(2,4) = (-24*beta*fz)/49.

  return
  
END SUBROUTINE calculate_dC13lndbdPi2P

!-----------------------------------------------------------------------------

SUBROUTINE calculate_dC13lndbetaPi2P(fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
     betaPi2P,bKPi2P,bPi3P,dC13lndbetaPi2P)

  IMPLICIT none

  DOUBLE PRECISION, intent(IN) :: fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P
  DOUBLE PRECISION, intent(OUT) :: dC13lndbetaPi2P(0:3,1:5)

  dC13lndbetaPi2P = 0d0

  dC13lndbetaPi2P(0,1) = (-2*b1*fz)/21. + (2*beta*fz**2)/49.

  dC13lndbetaPi2P(0,2) = (b1*fz)/7. - (3*beta*fz**2)/49.

  dC13lndbetaPi2P(0,3) = -(b1*fz)/21. - (3*beta*fz**2)/49.

  dC13lndbetaPi2P(0,4) = (4*beta*fz**2)/49.

  dC13lndbetaPi2P(1,1) = (-10*b1*fz)/147. + (10*beta*fz**2)/147.

  dC13lndbetaPi2P(1,2) = (5*b1*fz)/49. - (5*beta*fz**2)/49.

  dC13lndbetaPi2P(1,3) = (85*b1*fz)/147. - (25*beta*fz**2)/147.

  dC13lndbetaPi2P(1,4) = (-30*b1*fz)/49. - (10*beta*fz**2)/147.

  dC13lndbetaPi2P(1,5) = (40*beta*fz**2)/147.

  dC13lndbetaPi2P(2,1) = (8*b1*fz)/49. - (32*beta*fz**2)/539.

  dC13lndbetaPi2P(2,2) = (-12*b1*fz)/49. + (48*beta*fz**2)/539.

  dC13lndbetaPi2P(2,3) = (-26*b1*fz)/49. + (38*beta*fz**2)/539.

  dC13lndbetaPi2P(2,4) = (100*b1*fz)/49. - (444*beta*fz**2)/539.

  dC13lndbetaPi2P(2,5) = (-10*b1*fz)/7. + (390*beta*fz**2)/539.

  dC13lndbetaPi2P(3,1) = (-80*beta*fz**2)/1617.

  dC13lndbetaPi2P(3,2) = (40*beta*fz**2)/539.

  dC13lndbetaPi2P(3,3) = (260*beta*fz**2)/1617.

  dC13lndbetaPi2P(3,4) = (-1000*beta*fz**2)/1617.

  dC13lndbetaPi2P(3,5) = (100*beta*fz**2)/231.  

  return
  
END SUBROUTINE calculate_dC13lndbetaPi2P

!-----------------------------------------------------------------------------

SUBROUTINE calculate_dC13lndbKPi2P(fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
     betaPi2P,bKPi2P,bPi3P,dC13lndbKPi2P)

  IMPLICIT none

  DOUBLE PRECISION, intent(IN) :: fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P
  DOUBLE PRECISION, intent(OUT) :: dC13lndbKPi2P(0:3,1:5)

  dC13lndbKPi2P = 0d0

  dC13lndbKPi2P(0,1) = (5*b1)/21. - (2*beta*fz)/21.

  dC13lndbKPi2P(0,2) = (-5*b1)/14. + (beta*fz)/7.

  dC13lndbKPi2P(0,3) = (5*b1)/42. - (beta*fz)/63.

  dC13lndbKPi2P(0,4) = (-2*beta*fz)/63.

  dC13lndbKPi2P(1,1) = (5*b1)/42. - (65*beta*fz)/294.

  dC13lndbKPi2P(1,2) = (-5*b1)/28. + (65*beta*fz)/196.

  dC13lndbKPi2P(1,3) = (-5*b1)/28. + (25*beta*fz)/1764.

  dC13lndbKPi2P(1,4) = (5*b1)/21. - (55*beta*fz)/441.

  dC13lndbKPi2P(2,1) = (-2*beta*fz)/49.

  dC13lndbKPi2P(2,2) = (3*beta*fz)/49.

  dC13lndbKPi2P(2,3) = (3*beta*fz)/49.

  dC13lndbKPi2P(2,4) = (-4*beta*fz)/49.  

  return
  
END SUBROUTINE calculate_dC13lndbKPi2P

!-----------------------------------------------------------------------------

SUBROUTINE calculate_dC13lndbPi3P(fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
     betaPi2P,bKPi2P,bPi3P,dC13lndbPi3P)

  IMPLICIT none

  DOUBLE PRECISION, intent(IN) :: fz,b1,bK2,btd,beta,bPi2P,bK2P,bdeta,beta2,bdPi2P,&
       betaPi2P,bKPi2P,bPi3P
  DOUBLE PRECISION, intent(OUT) :: dC13lndbPi3P(0:3,1:5)

  dC13lndbPi3P = 0d0

  dC13lndbPi3P(0,1) = (2*b1)/63. + (3*beta*fz)/70.

  dC13lndbPi3P(0,2) = -b1/21. - (9*beta*fz)/140.

  dC13lndbPi3P(0,3) = b1/63. + (61*beta*fz)/420.

  dC13lndbPi3P(0,4) = (-13*beta*fz)/105.

  dC13lndbPi3P(1,1) = (-101*b1)/252. + (37*beta*fz)/196.

  dC13lndbPi3P(1,2) = (101*b1)/168. - (111*beta*fz)/392.

  dC13lndbPi3P(1,3) = (-569*b1)/504. + (683*beta*fz)/1176.

  dC13lndbPi3P(1,4) = (13*b1)/14. - (143*beta*fz)/294.

  dC13lndbPi3P(2,1) = (101*beta*fz)/735.

  dC13lndbPi3P(2,2) = (-101*beta*fz)/490.

  dC13lndbPi3P(2,3) = (569*beta*fz)/1470.

  dC13lndbPi3P(2,4) = (-78*beta*fz)/245.

  return
  
END SUBROUTINE calculate_dC13lndbPi3P

!-----------------------------------------------------------------------------

END MODULE pk13

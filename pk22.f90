!-----------------------------------------------------------------------------
! Module to calculate all coefficients A(nmp) and derivatives for pk22
!-----------------------------------------------------------------------------

MODULE pk22
  
  IMPLICIT none
  
contains

!-----------------------------------------------------------------------------
  
SUBROUTINE assign_Imp(pknlcomps,neffnlcomps,Imp,neffImp)

  IMPLICIT NONE

  double precision, dimension(28), intent(in) :: pknlcomps,neffnlcomps
  double precision, intent(out) :: Imp(0:8,0:6),neffImp(0:8,0:6)

  Imp = 0d0
  neffImp = 0d0

  Imp(0,0) = pknlcomps(6)
  Imp(0,2) = pknlcomps(7)
  Imp(0,4) = pknlcomps(8)
  Imp(0,6) = pknlcomps(9)
  Imp(1,1) = pknlcomps(10)
  Imp(1,3) = pknlcomps(11)
  Imp(1,5) = pknlcomps(12)
  Imp(2,0) = pknlcomps(13)
  Imp(2,2) = pknlcomps(14)
  Imp(2,4) = pknlcomps(15)
  Imp(2,6) = pknlcomps(16)
  Imp(3,1) = pknlcomps(17)
  Imp(3,3) = pknlcomps(18)
  Imp(3,5) = pknlcomps(19)
  Imp(4,2) = pknlcomps(20)
  Imp(4,4) = pknlcomps(21)
  Imp(4,6) = pknlcomps(22)
  Imp(5,3) = pknlcomps(23)
  Imp(5,5) = pknlcomps(24)
  Imp(6,4) = pknlcomps(25)
  Imp(6,6) = pknlcomps(26)
  Imp(7,5) = pknlcomps(27)
  Imp(8,6) = pknlcomps(28)

  neffImp(0,0) = neffnlcomps(6)
  neffImp(0,2) = neffnlcomps(7)
  neffImp(0,4) = neffnlcomps(8)
  neffImp(0,6) = neffnlcomps(9)
  neffImp(1,1) = neffnlcomps(10)
  neffImp(1,3) = neffnlcomps(11)
  neffImp(1,5) = neffnlcomps(12)
  neffImp(2,0) = neffnlcomps(13)
  neffImp(2,2) = neffnlcomps(14)
  neffImp(2,4) = neffnlcomps(15)
  neffImp(2,6) = neffnlcomps(16)
  neffImp(3,1) = neffnlcomps(17)
  neffImp(3,3) = neffnlcomps(18)
  neffImp(3,5) = neffnlcomps(19)
  neffImp(4,2) = neffnlcomps(20)
  neffImp(4,4) = neffnlcomps(21)
  neffImp(4,6) = neffnlcomps(22)
  neffImp(5,3) = neffnlcomps(23)
  neffImp(5,5) = neffnlcomps(24)
  neffImp(6,4) = neffnlcomps(25)
  neffImp(6,6) = neffnlcomps(26)
  neffImp(7,5) = neffnlcomps(27)
  neffImp(8,6) = neffnlcomps(28)

END SUBROUTINE assign_Imp  
    
!-----------------------------------------------------------------------------
  
SUBROUTINE calculate_Anmp(fz,b1,b2,bK2,beta,bPi2P,bK2P,bdeta,beta2,Anmp)

  IMPLICIT none

  DOUBLE PRECISION, intent(IN) :: fz,b1,beta,b2,bK2,bPi2P,bK2P,bdeta,beta2
  DOUBLE PRECISION, intent(OUT) :: Anmp(0:4,0:8,0:6)

  Anmp = 0d0

  Anmp(0,0,2) = (9*b1**2)/196. + (3*b1*b2)/14. + b2**2/4. - (b1*bK2)/7. - (b2*&
  &bK2)/3. + bK2**2/9. - (b1*bK2P)/42. - (b2*bK2P)/18. + (bK2*bK2P)/27. + (11*&
  &bK2P**2)/648. + (3*b1**2*fz)/28. + (b1*b2*fz)/4. - (3*b1*bdeta*fz)/28. - (b&
  &2*bdeta*fz)/4. - (b1*bK2*fz)/6. + (bdeta*bK2*fz)/6. - (5*b1*bK2P*fz)/72. + &
  &(5*bdeta*bK2P*fz)/72. + (3*b1**2*fz**2)/32. - (3*b1*bdeta*fz**2)/16. + (3*b&
  &deta**2*fz**2)/32.

  Anmp(0,0,4) = (3*b1*b2)/14. + b2**2/2. + (2*b1*bK2)/7. + (b2*bK2)/3. - (4*bK&
  &2**2)/9. + (5*b1*bK2P)/42. + (2*b2*bK2P)/9. - (7*bK2*bK2P)/27. - (19*bK2P**&
  &2)/324. + (3*b1*bPi2P)/14. + (b2*bPi2P)/2. - (bK2*bPi2P)/3. - (5*bK2P*bPi2P&
  &)/36. + (3*b1**2*fz)/14. + (3*b1*b2*fz)/4. - (3*b1*bdeta*fz)/14. - (3*b2*bd&
  &eta*fz)/4. + (b1*bK2P*fz)/24. - (bdeta*bK2P*fz)/24. + (3*b1*bPi2P*fz)/8. - &
  &(3*bdeta*bPi2P*fz)/8. + (3*b1**2*fz**2)/8. - (3*b1*bdeta*fz**2)/4. + (3*bde&
  &ta**2*fz**2)/8. + (9*b1*beta*fz**2)/56. + (3*b2*beta*fz**2)/8. + (9*b1*beta&
  &2*fz**2)/56. + (3*b2*beta2*fz**2)/8. - (beta*bK2*fz**2)/4. - (beta2*bK2*fz*&
  &*2)/4. - (beta*bK2P*fz**2)/8. - (beta2*bK2P*fz**2)/8. + (5*b1*beta*fz**3)/1&
  &6. - (5*bdeta*beta*fz**3)/16. + (5*b1*beta2*fz**3)/16. - (5*bdeta*beta2*fz*&
  &*3)/16.

  Anmp(0,0,6) = b2**2/4. + (2*b2*bK2)/3. + (4*bK2**2)/9. + (5*b2*bK2P)/18. + (&
  &10*bK2*bK2P)/27. + (59*bK2P**2)/648. + (b2*bPi2P)/2. + (2*bK2*bPi2P)/3. + (&
  &13*bK2P*bPi2P)/36. + (3*bPi2P**2)/8. + (b1*b2*fz)/2. - (b2*bdeta*fz)/2. + (&
  &2*b1*bK2*fz)/3. - (2*bdeta*bK2*fz)/3. + (13*b1*bK2P*fz)/36. - (13*bdeta*bK2&
  &P*fz)/36. + (3*b1*bPi2P*fz)/4. - (3*bdeta*bPi2P*fz)/4. + (3*b1**2*fz**2)/8.&
  & - (3*b1*bdeta*fz**2)/4. + (3*bdeta**2*fz**2)/8. + (3*b2*beta*fz**2)/8. + (&
  &3*b2*beta2*fz**2)/8. + (beta*bK2*fz**2)/2. + (beta2*bK2*fz**2)/2. + (7*beta&
  &*bK2P*fz**2)/24. + (7*beta2*bK2P*fz**2)/24. + (5*beta*bPi2P*fz**2)/8. + (5*&
  &beta2*bPi2P*fz**2)/8. + (5*b1*beta*fz**3)/8. - (5*bdeta*beta*fz**3)/8. + (5&
  &*b1*beta2*fz**3)/8. - (5*bdeta*beta2*fz**3)/8. + (35*beta**2*fz**4)/128. + &
  &(35*beta*beta2*fz**4)/64. + (35*beta2**2*fz**4)/128.

  Anmp(0,1,1) = (3*b1**2)/14. + (b1*b2)/2. - (b1*bK2)/3. - (b1*bK2P)/18. + (b1&
  &**2*fz)/4. - (b1*bdeta*fz)/4.

  Anmp(0,1,3) = (b1*b2)/14. - b2**2 + (2*b1*bK2)/21. - (2*b2*bK2)/3. + (8*bK2*&
  &*2)/9. + (b1*bK2P)/9. - (5*b2*bK2P)/18. + (11*bK2*bK2P)/27. + (23*bK2P**2)/&
  &324. + (2*b1*bPi2P)/7. - (b2*bPi2P)/2. + (bK2*bPi2P)/3. + (5*bK2P*bPi2P)/36&
  &. + (2*b1**2*fz)/7. - b1*b2*fz - (2*b1*bdeta*fz)/7. + b2*bdeta*fz - (b1*bK2&
  &*fz)/3. + (bdeta*bK2*fz)/3. - (7*b1*bK2P*fz)/72. + (7*bdeta*bK2P*fz)/72. - &
  &(3*b1*bPi2P*fz)/8. + (3*bdeta*bPi2P*fz)/8. - (3*b1**2*fz**2)/8. + (3*b1*bde&
  &ta*fz**2)/4. - (3*bdeta**2*fz**2)/8. + (3*b1*beta*fz**2)/8. + (3*b1*beta2*f&
  &z**2)/8.

  Anmp(0,1,5) = -b2**2 - (8*b2*bK2)/3. - (16*bK2**2)/9. - (17*b2*bK2P)/18. - (&
  &34*bK2*bK2P)/27. - (79*bK2P**2)/324. - (3*b2*bPi2P)/2. - 2*bK2*bPi2P - (5*b&
  &K2P*bPi2P)/6. - (3*bPi2P**2)/4. - (3*b1*b2*fz)/2. + (3*b2*bdeta*fz)/2. - 2*&
  &b1*bK2*fz + 2*bdeta*bK2*fz - (5*b1*bK2P*fz)/6. + (5*bdeta*bK2P*fz)/6. - (3*&
  &b1*bPi2P*fz)/2. + (3*bdeta*bPi2P*fz)/2. - (3*b1**2*fz**2)/4. + (3*b1*bdeta*&
  &fz**2)/2. - (3*bdeta**2*fz**2)/4. - (3*b2*beta*fz**2)/4. - (3*b2*beta2*fz**&
  &2)/4. - beta*bK2*fz**2 - beta2*bK2*fz**2 - (3*beta*bK2P*fz**2)/8. - (3*beta&
  &2*bK2P*fz**2)/8. - (5*beta*bPi2P*fz**2)/8. - (5*beta2*bPi2P*fz**2)/8. - (5*&
  &b1*beta*fz**3)/8. + (5*bdeta*beta*fz**3)/8. - (5*b1*beta2*fz**3)/8. + (5*bd&
  &eta*beta2*fz**3)/8.

  Anmp(0,2,0) = b1**2/4.

  Anmp(0,2,2) = (-15*b1**2)/49. - (12*b1*b2)/7. - (3*b1*bK2)/7. + b2*bK2 - (2*&
  &bK2**2)/3. - (5*b1*bK2P)/21. + (b2*bK2P)/6. - (2*bK2*bK2P)/9. - (5*bK2P**2)&
  &/108. - (b1*bPi2P)/2. - (27*b1**2*fz)/28. - (b1*b2*fz)/4. + (27*b1*bdeta*fz&
  &)/28. + (b2*bdeta*fz)/4. + (2*b1*bK2*fz)/3. - (2*bdeta*bK2*fz)/3. + (7*b1*b&
  &K2P*fz)/36. - (7*bdeta*bK2P*fz)/36. - (3*b1**2*fz**2)/16. + (3*b1*bdeta*fz*&
  &*2)/8. - (3*bdeta**2*fz**2)/16.

  Anmp(0,2,4) = (-5*b1*b2)/7. + b2**2 - (20*b1*bK2)/21. + (11*b2*bK2)/3. + (28&
  &*bK2**2)/9. - (59*b1*bK2P)/126. + (7*b2*bK2P)/9. + (52*bK2*bK2P)/27. + (215&
  &*bK2P**2)/648. - (13*b1*bPi2P)/14. + (b2*bPi2P)/2. + (8*bK2*bPi2P)/3. + (31&
  &*bK2P*bPi2P)/36. + (3*bPi2P**2)/8. - (13*b1**2*fz)/14. + (b1*b2*fz)/4. + (1&
  &3*b1*bdeta*fz)/14. - (b2*bdeta*fz)/4. + (7*b1*bK2*fz)/3. - (7*bdeta*bK2*fz)&
  &/3. + (5*b1*bK2P*fz)/9. - (5*bdeta*bK2P*fz)/9. - (3*b1**2*fz**2)/8. + (3*b1&
  &*bdeta*fz**2)/4. - (3*bdeta**2*fz**2)/8. - (6*b1*beta*fz**2)/7. - (3*b2*bet&
  &a*fz**2)/4. - (6*b1*beta2*fz**2)/7. - (3*b2*beta2*fz**2)/4. + (5*beta*bK2*f&
  &z**2)/4. + (5*beta2*bK2*fz**2)/4. + (11*beta*bK2P*fz**2)/24. + (11*beta2*bK&
  &2P*fz**2)/24. - (15*b1*beta*fz**3)/16. + (15*bdeta*beta*fz**3)/16. - (15*b1&
  &*beta2*fz**3)/16. + (15*bdeta*beta2*fz**3)/16.

  Anmp(0,2,6) = -(b2*bK2P)/6. - (2*bK2*bK2P)/9. - (13*bK2P**2)/108. - (b2*bPi2&
  &P)/2. - (2*bK2*bPi2P)/3. - (11*bK2P*bPi2P)/18. - (3*bPi2P**2)/4. - (b1*b2*f&
  &z)/2. + (b2*bdeta*fz)/2. - (2*b1*bK2*fz)/3. + (2*bdeta*bK2*fz)/3. - (11*b1*&
  &bK2P*fz)/18. + (11*bdeta*bK2P*fz)/18. - (3*b1*bPi2P*fz)/2. + (3*bdeta*bPi2P&
  &*fz)/2. - (3*b1**2*fz**2)/4. + (3*b1*bdeta*fz**2)/2. - (3*bdeta**2*fz**2)/4&
  &. - (3*b2*beta*fz**2)/4. - (3*b2*beta2*fz**2)/4. - beta*bK2*fz**2 - beta2*b&
  &K2*fz**2 - (19*beta*bK2P*fz**2)/24. - (19*beta2*bK2P*fz**2)/24. - (15*beta*&
  &bPi2P*fz**2)/8. - (15*beta2*bPi2P*fz**2)/8. - (15*b1*beta*fz**3)/8. + (15*b&
  &deta*beta*fz**3)/8. - (15*b1*beta2*fz**3)/8. + (15*bdeta*beta2*fz**3)/8. - &
  &(35*beta**2*fz**4)/32. - (35*beta*beta2*fz**4)/16. - (35*beta2**2*fz**4)/32&
  &.

  Anmp(0,3,1) = (-5*b1**2)/7. + b1*bK2 + (b1*bK2P)/6. - (b1**2*fz)/4. + (b1*bd&
  &eta*fz)/4.

  Anmp(0,3,3) = (10*b1*b2)/7. + (40*b1*bK2)/21. - 2*b2*bK2 - (8*bK2**2)/3. + (&
  &29*b1*bK2P)/63. - (b2*bK2P)/6. - (4*bK2*bK2P)/3. - (11*bK2P**2)/54. + (3*b1&
  &*bPi2P)/7. + (b2*bPi2P)/2. - (4*bK2*bPi2P)/3. - (7*bK2P*bPi2P)/18. + (3*b1*&
  &*2*fz)/7. + b1*b2*fz - (3*b1*bdeta*fz)/7. - b2*bdeta*fz - (2*b1*bK2*fz)/3. &
  &+ (2*bdeta*bK2*fz)/3. - (b1*bK2P*fz)/36. + (bdeta*bK2P*fz)/36. + (3*b1*bPi2&
  &P*fz)/4. - (3*bdeta*bPi2P*fz)/4. + (3*b1**2*fz**2)/4. - (3*b1*bdeta*fz**2)/&
  &2. + (3*bdeta**2*fz**2)/4. - (3*b1*beta*fz**2)/4. - (3*b1*beta2*fz**2)/4.

  Anmp(0,3,5) = (b2*bK2P)/2. + (2*bK2*bK2P)/3. + (5*bK2P**2)/18. + (3*b2*bPi2P&
  &)/2. + 2*bK2*bPi2P + (4*bK2P*bPi2P)/3. + (3*bPi2P**2)/2. + (3*b1*b2*fz)/2. &
  &- (3*b2*bdeta*fz)/2. + 2*b1*bK2*fz - 2*bdeta*bK2*fz + (4*b1*bK2P*fz)/3. - (&
  &4*bdeta*bK2P*fz)/3. + 3*b1*bPi2P*fz - 3*bdeta*bPi2P*fz + (3*b1**2*fz**2)/2.&
  & - 3*b1*bdeta*fz**2 + (3*bdeta**2*fz**2)/2. + (3*b2*beta*fz**2)/2. + (3*b2*&
  &beta2*fz**2)/2. + 2*beta*bK2*fz**2 + 2*beta2*bK2*fz**2 + (23*beta*bK2P*fz**&
  &2)/24. + (23*beta2*bK2P*fz**2)/24. + (15*beta*bPi2P*fz**2)/8. + (15*beta2*b&
  &Pi2P*fz**2)/8. + (15*b1*beta*fz**3)/8. - (15*bdeta*beta*fz**3)/8. + (15*b1*&
  &beta2*fz**3)/8. - (15*bdeta*beta2*fz**3)/8.

  Anmp(0,4,2) = (25*b1**2)/49. - (10*b1*bK2)/7. + bK2**2 - (b1*bK2P)/14. + (bK&
  &2*bK2P)/3. + bK2P**2/24. + (b1*bPi2P)/2. + (6*b1**2*fz)/7. - (6*b1*bdeta*fz&
  &)/7. - (b1*bK2*fz)/2. + (bdeta*bK2*fz)/2. - (b1*bK2P*fz)/8. + (bdeta*bK2P*f&
  &z)/8. + (3*b1**2*fz**2)/32. - (3*b1*bdeta*fz**2)/16. + (3*bdeta**2*fz**2)/3&
  &2.

  Anmp(0,4,4) = (5*b1*bK2P)/21. - (b2*bK2P)/3. - (7*bK2*bK2P)/9. - (13*bK2P**2&
  &)/54. + (5*b1*bPi2P)/7. - b2*bPi2P - (7*bK2*bPi2P)/3. - (35*bK2P*bPi2P)/36.&
  & - (3*bPi2P**2)/4. + (5*b1**2*fz)/7. - b1*b2*fz - (5*b1*bdeta*fz)/7. + b2*b&
  &deta*fz - (7*b1*bK2*fz)/3. + (7*bdeta*bK2*fz)/3. - (61*b1*bK2P*fz)/72. + (6&
  &1*bdeta*bK2P*fz)/72. - (9*b1*bPi2P*fz)/8. + (9*bdeta*bPi2P*fz)/8. - (3*b1**&
  &2*fz**2)/8. + (3*b1*bdeta*fz**2)/4. - (3*bdeta**2*fz**2)/8. + (69*b1*beta*f&
  &z**2)/56. + (3*b2*beta*fz**2)/8. + (69*b1*beta2*fz**2)/56. + (3*b2*beta2*fz&
  &**2)/8. - (7*beta*bK2*fz**2)/4. - (7*beta2*bK2*fz**2)/4. - (13*beta*bK2P*fz&
  &**2)/24. - (13*beta2*bK2P*fz**2)/24. + (15*b1*beta*fz**3)/16. - (15*bdeta*b&
  &eta*fz**3)/16. + (15*b1*beta2*fz**3)/16. - (15*bdeta*beta2*fz**3)/16.

  Anmp(0,4,6) = bK2P**2/24. + (bK2P*bPi2P)/4. + (3*bPi2P**2)/8. + (b1*bK2P*fz)&
  &/4. - (bdeta*bK2P*fz)/4. + (3*b1*bPi2P*fz)/4. - (3*bdeta*bPi2P*fz)/4. + (3*&
  &b1**2*fz**2)/8. - (3*b1*bdeta*fz**2)/4. + (3*bdeta**2*fz**2)/8. + (3*b2*bet&
  &a*fz**2)/8. + (3*b2*beta2*fz**2)/8. + (beta*bK2*fz**2)/2. + (beta2*bK2*fz**&
  &2)/2. + (17*beta*bK2P*fz**2)/24. + (17*beta2*bK2P*fz**2)/24. + (15*beta*bPi&
  &2P*fz**2)/8. + (15*beta2*bPi2P*fz**2)/8. + (15*b1*beta*fz**3)/8. - (15*bdet&
  &a*beta*fz**3)/8. + (15*b1*beta2*fz**3)/8. - (15*bdeta*beta2*fz**3)/8. + (10&
  &5*beta**2*fz**4)/64. + (105*beta*beta2*fz**4)/32. + (105*beta2**2*fz**4)/64&
  &.

  Anmp(0,5,3) = (-5*b1*bK2P)/21. + (bK2*bK2P)/3. + bK2P**2/12. - (5*b1*bPi2P)/&
  &7. + bK2*bPi2P + (bK2P*bPi2P)/4. - (5*b1**2*fz)/7. + (5*b1*bdeta*fz)/7. + b&
  &1*bK2*fz - bdeta*bK2*fz + (b1*bK2P*fz)/8. - (bdeta*bK2P*fz)/8. - (3*b1*bPi2&
  &P*fz)/8. + (3*bdeta*bPi2P*fz)/8. - (3*b1**2*fz**2)/8. + (3*b1*bdeta*fz**2)/&
  &4. - (3*bdeta**2*fz**2)/8. + (3*b1*beta*fz**2)/8. + (3*b1*beta2*fz**2)/8.

  Anmp(0,5,5) = -bK2P**2/12. - (bK2P*bPi2P)/2. - (3*bPi2P**2)/4. - (b1*bK2P*fz&
  &)/2. + (bdeta*bK2P*fz)/2. - (3*b1*bPi2P*fz)/2. + (3*bdeta*bPi2P*fz)/2. - (3&
  &*b1**2*fz**2)/4. + (3*b1*bdeta*fz**2)/2. - (3*bdeta**2*fz**2)/4. - (3*b2*be&
  &ta*fz**2)/4. - (3*b2*beta2*fz**2)/4. - beta*bK2*fz**2 - beta2*bK2*fz**2 - (&
  &19*beta*bK2P*fz**2)/24. - (19*beta2*bK2P*fz**2)/24. - (15*beta*bPi2P*fz**2)&
  &/8. - (15*beta2*bPi2P*fz**2)/8. - (15*b1*beta*fz**3)/8. + (15*bdeta*beta*fz&
  &**3)/8. - (15*b1*beta2*fz**3)/8. + (15*bdeta*beta2*fz**3)/8.

  Anmp(0,6,4) = bK2P**2/24. + (bK2P*bPi2P)/4. + (3*bPi2P**2)/8. + (b1*bK2P*fz)&
  &/4. - (bdeta*bK2P*fz)/4. + (3*b1*bPi2P*fz)/4. - (3*bdeta*bPi2P*fz)/4. + (3*&
  &b1**2*fz**2)/8. - (3*b1*bdeta*fz**2)/4. + (3*bdeta**2*fz**2)/8. - (15*b1*be&
  &ta*fz**2)/28. - (15*b1*beta2*fz**2)/28. + (3*beta*bK2*fz**2)/4. + (3*beta2*&
  &bK2*fz**2)/4. + (5*beta*bK2P*fz**2)/24. + (5*beta2*bK2P*fz**2)/24. - (5*b1*&
  &beta*fz**3)/16. + (5*bdeta*beta*fz**3)/16. - (5*b1*beta2*fz**3)/16. + (5*bd&
  &eta*beta2*fz**3)/16.

  Anmp(0,6,6) = (-5*beta*bK2P*fz**2)/24. - (5*beta2*bK2P*fz**2)/24. - (5*beta*&
  &bPi2P*fz**2)/8. - (5*beta2*bPi2P*fz**2)/8. - (5*b1*beta*fz**3)/8. + (5*bdet&
  &a*beta*fz**3)/8. - (5*b1*beta2*fz**3)/8. + (5*bdeta*beta2*fz**3)/8. - (35*b&
  &eta**2*fz**4)/32. - (35*beta*beta2*fz**4)/16. - (35*beta2**2*fz**4)/32.

  Anmp(0,7,5) = (5*beta*bK2P*fz**2)/24. + (5*beta2*bK2P*fz**2)/24. + (5*beta*b&
  &Pi2P*fz**2)/8. + (5*beta2*bPi2P*fz**2)/8. + (5*b1*beta*fz**3)/8. - (5*bdeta&
  &*beta*fz**3)/8. + (5*b1*beta2*fz**3)/8. - (5*bdeta*beta2*fz**3)/8.

  Anmp(0,8,6) = (35*beta**2*fz**4)/128. + (35*beta*beta2*fz**4)/64. + (35*beta&
  &2**2*fz**4)/128.

  Anmp(1,0,0) = (b1**2*fz**2)/8.

  Anmp(1,0,2) = -(b1*bK2P)/14. - (b2*bK2P)/6. + (bK2*bK2P)/9. - bK2P**2/108. +&
  & (15*b1*bPi2P)/49. + (5*b2*bPi2P)/7. - (10*bK2*bPi2P)/21. - (5*bK2P*bPi2P)/&
  &63. - (3*b1**2*fz)/28. - (b1*b2*fz)/4. - (3*b1*bdeta*fz)/28. - (b2*bdeta*fz&
  &)/4. + (3*b1*beta*fz)/98. + (b2*beta*fz)/14. + (b1*bK2*fz)/6. + (bdeta*bK2*&
  &fz)/6. - (beta*bK2*fz)/21. + (7*b1*bK2P*fz)/36. + (bdeta*bK2P*fz)/36. - (be&
  &ta*bK2P*fz)/126. + (6*b1*bPi2P*fz)/7. - (5*bdeta*bPi2P*fz)/14. + (5*b1**2*f&
  &z**2)/16. - (3*b1*bdeta*fz**2)/8. + (bdeta**2*fz**2)/16. + (5*b1*beta*fz**2&
  &)/14. + (3*b2*beta*fz**2)/4. - (bdeta*beta*fz**2)/28. + (3*b1*beta2*fz**2)/&
  &14. + (b2*beta2*fz**2)/2. - (beta*bK2*fz**2)/2. - (beta2*bK2*fz**2)/3. - (5&
  &*beta*bK2P*fz**2)/24. - (5*beta2*bK2P*fz**2)/36. + (21*b1*beta*fz**3)/16. -&
  & (9*bdeta*beta*fz**3)/16. + (9*b1*beta2*fz**3)/8. - (3*bdeta*beta2*fz**3)/8&
  &.

  Anmp(1,0,4) = -(b1*bK2P)/14. - (b2*bK2P)/3. - (bK2*bK2P)/9. + bK2P**2/27. - &
  &(3*b1*bPi2P)/14. + (3*b2*bPi2P)/14. + (9*bK2*bPi2P)/7. + (11*bK2P*bPi2P)/14&
  &. + (17*bPi2P**2)/14. - (3*b1**2*fz)/14. - (3*b1*b2*fz)/4. + (3*b1*bdeta*fz&
  &)/14. + (b2*bdeta*fz)/4. + (b2*beta*fz)/14. - (2*bdeta*bK2*fz)/3. + (2*beta&
  &*bK2*fz)/21. + (b1*bK2P*fz)/12. - (13*bdeta*bK2P*fz)/36. + (5*beta*bK2P*fz)&
  &/126. + (27*b1*bPi2P*fz)/28. - (41*bdeta*bPi2P*fz)/28. + (beta*bPi2P*fz)/14&
  &. - (b1**2*fz**2)/4. + (bdeta**2*fz**2)/4. - (b1*beta*fz**2)/4. - (bdeta*be&
  &ta*fz**2)/14. - (9*b1*beta2*fz**2)/28. - (b2*beta2*fz**2)/4. + (3*beta*bK2*&
  &fz**2)/2. + (7*beta2*bK2*fz**2)/6. + (5*beta*bK2P*fz**2)/4. + (77*beta2*bK2&
  &P*fz**2)/72. + (177*beta*bPi2P*fz**2)/56. + (39*beta2*bPi2P*fz**2)/14. + (2&
  &7*b1*beta*fz**3)/16. - (33*bdeta*beta*fz**3)/16. + (3*beta**2*fz**3)/56. + &
  &(21*b1*beta2*fz**3)/16. - (27*bdeta*beta2*fz**3)/16. + (3*beta*beta2*fz**3)&
  &/56. + (35*beta**2*fz**4)/16. + (65*beta*beta2*fz**4)/16. + (15*beta2**2*fz&
  &**4)/8.

  Anmp(1,0,6) = -(b2*bK2P)/6. - (2*bK2*bK2P)/9. - (13*bK2P**2)/108. - (b2*bPi2&
  &P)/2. - (2*bK2*bPi2P)/3. - (11*bK2P*bPi2P)/18. - (3*bPi2P**2)/4. - (b1*b2*f&
  &z)/2. + (b2*bdeta*fz)/2. - (2*b1*bK2*fz)/3. + (2*bdeta*bK2*fz)/3. - (11*b1*&
  &bK2P*fz)/18. + (11*bdeta*bK2P*fz)/18. - (3*b1*bPi2P*fz)/2. + (3*bdeta*bPi2P&
  &*fz)/2. - (3*b1**2*fz**2)/4. + (3*b1*bdeta*fz**2)/2. - (3*bdeta**2*fz**2)/4&
  &. - (3*b2*beta*fz**2)/4. - (3*b2*beta2*fz**2)/4. - beta*bK2*fz**2 - beta2*b&
  &K2*fz**2 - (19*beta*bK2P*fz**2)/24. - (19*beta2*bK2P*fz**2)/24. - (15*beta*&
  &bPi2P*fz**2)/8. - (15*beta2*bPi2P*fz**2)/8. - (15*b1*beta*fz**3)/8. + (15*b&
  &deta*beta*fz**3)/8. - (15*b1*beta2*fz**3)/8. + (15*bdeta*beta2*fz**3)/8. - &
  &(35*beta**2*fz**4)/32. - (35*beta*beta2*fz**4)/16. - (35*beta2**2*fz**4)/32&
  &.

  Anmp(1,1,1) = -(b1*bK2P)/6. + (5*b1*bPi2P)/7. - (13*b1**2*fz)/28. - (b1*b2*f&
  &z)/2. - (b1*bdeta*fz)/4. - (b1*beta*fz)/7. - (b2*beta*fz)/2. + (b1*bK2*fz)/&
  &3. + (beta*bK2*fz)/3. - (b1*bK2P*fz)/9. + (beta*bK2P*fz)/18. - (b1*bPi2P*fz&
  &)/2. - (5*b1**2*fz**2)/4. + (3*b1*bdeta*fz**2)/4. + (b1*beta*fz**2)/2. + (b&
  &deta*beta*fz**2)/4. + (b1*beta2*fz**2)/2.

  Anmp(1,1,3) = (-5*b1*bK2P)/21. + (b2*bK2P)/6. + (5*bK2*bK2P)/9. - bK2P**2/54&
  &. - (5*b1*bPi2P)/7. - (27*b2*bPi2P)/14. - (11*bK2*bPi2P)/7. - (7*bK2P*bPi2P&
  &)/6. - (12*bPi2P**2)/7. - (5*b1**2*fz)/7. - (b1*b2*fz)/2. + (5*b1*bdeta*fz)&
  &/7. + b2*bdeta*fz - (9*b2*beta*fz)/14. + (b1*bK2*fz)/3. + (bdeta*bK2*fz)/3.&
  & - (6*beta*bK2*fz)/7. - (49*b1*bK2P*fz)/36. + (29*bdeta*bK2P*fz)/36. - (bet&
  &a*bK2P*fz)/3. - (139*b1*bPi2P*fz)/28. + (83*bdeta*bPi2P*fz)/28. - (4*beta*b&
  &Pi2P*fz)/7. - (13*b1**2*fz**2)/4. + (9*b1*bdeta*fz**2)/2. - (5*bdeta**2*fz*&
  &*2)/4. - (73*b1*beta*fz**2)/28. - (9*b2*beta*fz**2)/2. + (4*bdeta*beta*fz**&
  &2)/7. - (57*b1*beta2*fz**2)/28. - 4*b2*beta2*fz**2 + (2*beta2*bK2*fz**2)/3.&
  & - (3*beta*bK2P*fz**2)/8. - (5*beta2*bK2P*fz**2)/36. - (21*beta*bPi2P*fz**2&
  &)/8. - (9*beta2*bPi2P*fz**2)/4. - (33*b1*beta*fz**3)/4. + (39*bdeta*beta*fz&
  &**3)/8. - (3*beta**2*fz**3)/8. - (63*b1*beta2*fz**3)/8. + (9*bdeta*beta2*fz&
  &**3)/2. - (3*beta*beta2*fz**3)/8.

  Anmp(1,1,5) = (b2*bK2P)/6. + (2*bK2*bK2P)/9. - (7*bK2P**2)/54. + (b2*bPi2P)/&
  &2. + (2*bK2*bPi2P)/3. - (8*bK2P*bPi2P)/9. - (3*bPi2P**2)/2. + (b1*b2*fz)/2.&
  & - (b2*bdeta*fz)/2. + (2*b1*bK2*fz)/3. - (2*bdeta*bK2*fz)/3. - (8*b1*bK2P*f&
  &z)/9. + (8*bdeta*bK2P*fz)/9. - 3*b1*bPi2P*fz + 3*bdeta*bPi2P*fz - (3*b1**2*&
  &fz**2)/2. + 3*b1*bdeta*fz**2 - (3*bdeta**2*fz**2)/2. - (3*b2*beta*fz**2)/2.&
  & - (3*b2*beta2*fz**2)/2. - 2*beta*bK2*fz**2 - 2*beta2*bK2*fz**2 - (83*beta*&
  &bK2P*fz**2)/24. - (83*beta2*bK2P*fz**2)/24. - (75*beta*bPi2P*fz**2)/8. - (7&
  &5*beta2*bPi2P*fz**2)/8. - (75*b1*beta*fz**3)/8. + (75*bdeta*beta*fz**3)/8. &
  &- (75*b1*beta2*fz**3)/8. + (75*bdeta*beta2*fz**3)/8. - (35*beta**2*fz**4)/4&
  &. - (35*beta*beta2*fz**4)/2. - (35*beta2**2*fz**4)/4.

  Anmp(1,2,0) = -(b1**2*fz)/2. - (b1*beta*fz)/2. - (b1**2*fz**2)/8.

  Anmp(1,2,2) = (2*b1*bK2P)/7. + (b2*bK2P)/2. - (2*bK2*bK2P)/3. - (137*b1*bPi2&
  &P)/98. + (2*b2*bPi2P)/7. + (26*bK2*bPi2P)/21. + (34*bK2P*bPi2P)/63. + bPi2P&
  &**2/2. + (17*b1**2*fz)/28. + (11*b1*b2*fz)/4. + (15*b1*bdeta*fz)/28. - (3*b&
  &2*bdeta*fz)/4. + (4*b1*beta*fz)/49. + (10*b2*beta*fz)/7. - (b1*bK2*fz)/3. +&
  & (25*beta*bK2*fz)/21. + (10*b1*bK2P*fz)/9. - (bdeta*bK2P*fz)/2. + (23*beta*&
  &bK2P*fz)/63. + (39*b1*bPi2P*fz)/14. - (11*bdeta*bPi2P*fz)/14. + (beta*bPi2P&
  &*fz)/2. + (33*b1**2*fz**2)/8. - (9*b1*bdeta*fz**2)/2. + (7*bdeta**2*fz**2)/&
  &8. - (26*b1*beta*fz**2)/7. - (3*b2*beta*fz**2)/4. - (19*bdeta*beta*fz**2)/2&
  &8. - (55*b1*beta2*fz**2)/14. - (b2*beta2*fz**2)/2. + 2*beta*bK2*fz**2 + (4*&
  &beta2*bK2*fz**2)/3. + (7*beta*bK2P*fz**2)/12. + (7*beta2*bK2P*fz**2)/18. - &
  &(21*b1*beta*fz**3)/8. + (9*bdeta*beta*fz**3)/8. - (9*b1*beta2*fz**3)/4. + (&
  &3*bdeta*beta2*fz**3)/4.

  Anmp(1,2,4) = (19*b1*bK2P)/42. + (4*b2*bK2P)/3. + (4*bK2*bK2P)/9. + (65*bK2P&
  &**2)/108. + (19*b1*bPi2P)/14. + (39*b2*bPi2P)/14. - (2*bK2*bPi2P)/7. + (37*&
  &bK2P*bPi2P)/14. + (93*bPi2P**2)/28. + (19*b1**2*fz)/14. + (17*b1*b2*fz)/4. &
  &- (19*b1*bdeta*fz)/14. - (13*b2*bdeta*fz)/4. + (3*b2*beta*fz)/7. + (5*b1*bK&
  &2*fz)/3. - (bdeta*bK2*fz)/3. + (4*beta*bK2*fz)/7. + (89*b1*bK2P*fz)/18. - (&
  &32*bdeta*bK2P*fz)/9. + (3*beta*bK2P*fz)/14. + (88*b1*bPi2P*fz)/7. - (127*bd&
  &eta*bPi2P*fz)/14. + (5*beta*bPi2P*fz)/14. + (37*b1**2*fz**2)/4. - 15*b1*bde&
  &ta*fz**2 + (23*bdeta**2*fz**2)/4. + (47*b1*beta*fz**2)/14. + (39*b2*beta*fz&
  &**2)/4. - (5*bdeta*beta*fz**2)/14. + 3*b1*beta2*fz**2 + 10*b2*beta2*fz**2 +&
  & (5*beta*bK2*fz**2)/2. + (17*beta2*bK2*fz**2)/6. + (37*beta*bK2P*fz**2)/24.&
  & + (133*beta2*bK2P*fz**2)/72. + (123*beta*bPi2P*fz**2)/28. + (36*beta2*bPi2&
  &P*fz**2)/7. + (231*b1*beta*fz**3)/16. - (159*bdeta*beta*fz**3)/16. + (3*bet&
  &a**2*fz**3)/14. + (243*b1*beta2*fz**3)/16. - (171*bdeta*beta2*fz**3)/16. + &
  &(3*beta*beta2*fz**3)/14. - (105*beta**2*fz**4)/16. - (195*beta*beta2*fz**4)&
  &/16. - (45*beta2**2*fz**4)/8.

  Anmp(1,2,6) = (b2*bK2P)/2. + (2*bK2*bK2P)/3. + (11*bK2P**2)/18. + (3*b2*bPi2&
  &P)/2. + 2*bK2*bPi2P + (10*bK2P*bPi2P)/3. + (9*bPi2P**2)/2. + (3*b1*b2*fz)/2&
  &. - (3*b2*bdeta*fz)/2. + 2*b1*bK2*fz - 2*bdeta*bK2*fz + (10*b1*bK2P*fz)/3. &
  &- (10*bdeta*bK2P*fz)/3. + 9*b1*bPi2P*fz - 9*bdeta*bPi2P*fz + (9*b1**2*fz**2&
  &)/2. - 9*b1*bdeta*fz**2 + (9*bdeta**2*fz**2)/2. + (9*b2*beta*fz**2)/2. + (9&
  &*b2*beta2*fz**2)/2. + 6*beta*bK2*fz**2 + 6*beta2*bK2*fz**2 + (53*beta*bK2P*&
  &fz**2)/8. + (53*beta2*bK2P*fz**2)/8. + (135*beta*bPi2P*fz**2)/8. + (135*bet&
  &a2*bPi2P*fz**2)/8. + (135*b1*beta*fz**3)/8. - (135*bdeta*beta*fz**3)/8. + (&
  &135*b1*beta2*fz**3)/8. - (135*bdeta*beta2*fz**3)/8. + (105*beta**2*fz**4)/8&
  &. + (105*beta*beta2*fz**4)/4. + (105*beta2**2*fz**4)/8.

  Anmp(1,3,1) = (b1*bK2P)/2. + (2*b1*bPi2P)/7. + (69*b1**2*fz)/28. - (3*b1*bde&
  &ta*fz)/4. + (8*b1*beta*fz)/7. - b1*bK2*fz - beta*bK2*fz - (beta*bK2P*fz)/6.&
  & + (b1*bPi2P*fz)/2. + (5*b1**2*fz**2)/4. - (3*b1*bdeta*fz**2)/4. - (b1*beta&
  &*fz**2)/2. - (bdeta*beta*fz**2)/4. - (b1*beta2*fz**2)/2.

  Anmp(1,3,3) = (11*b1*bK2P)/21. - (3*b2*bK2P)/2. - (4*bK2*bK2P)/3. - (4*bK2P*&
  &*2)/9. + (11*b1*bPi2P)/7. - (29*b2*bPi2P)/14. - (16*bK2*bPi2P)/21. - (62*bK&
  &2P*bPi2P)/63. - (4*bPi2P**2)/7. + (11*b1**2*fz)/7. - 5*b1*b2*fz - (11*b1*bd&
  &eta*fz)/7. + 3*b2*bdeta*fz - (6*b2*beta*fz)/7. - (14*b1*bK2*fz)/3. + 2*bdet&
  &a*bK2*fz - (8*beta*bK2*fz)/7. - (47*b1*bK2P*fz)/18. + (11*bdeta*bK2P*fz)/6.&
  & - (beta*bK2P*fz)/7. - (57*b1*bPi2P*fz)/14. + (43*bdeta*bPi2P*fz)/14. + (be&
  &ta*bPi2P*fz)/7. - (7*b1**2*fz**2)/2. + 6*b1*bdeta*fz**2 - (5*bdeta**2*fz**2&
  &)/2. + (143*b1*beta*fz**2)/14. + (9*b2*beta*fz**2)/2. - (bdeta*beta*fz**2)/&
  &7. + (141*b1*beta2*fz**2)/14. + 4*b2*beta2*fz**2 - 6*beta*bK2*fz**2 - (20*b&
  &eta2*bK2*fz**2)/3. - (beta*bK2P*fz**2)/4. - (11*beta2*bK2P*fz**2)/18. + (21&
  &*beta*bPi2P*fz**2)/4. + (9*beta2*bPi2P*fz**2)/2. + (33*b1*beta*fz**3)/2. - &
  &(39*bdeta*beta*fz**3)/4. + (3*beta**2*fz**3)/4. + (63*b1*beta2*fz**3)/4. - &
  &9*bdeta*beta2*fz**3 + (3*beta*beta2*fz**3)/4.

  Anmp(1,3,5) = (-3*b2*bK2P)/2. - 2*bK2*bK2P - bK2P**2 - (9*b2*bPi2P)/2. - 6*b&
  &K2*bPi2P - 5*bK2P*bPi2P - 6*bPi2P**2 - (9*b1*b2*fz)/2. + (9*b2*bdeta*fz)/2.&
  & - 6*b1*bK2*fz + 6*bdeta*bK2*fz - 5*b1*bK2P*fz + 5*bdeta*bK2P*fz - 12*b1*bP&
  &i2P*fz + 12*bdeta*bPi2P*fz - 6*b1**2*fz**2 + 12*b1*bdeta*fz**2 - 6*bdeta**2&
  &*fz**2 - 6*b2*beta*fz**2 - 6*b2*beta2*fz**2 - 8*beta*bK2*fz**2 - 8*beta2*bK&
  &2*fz**2 + (13*beta*bK2P*fz**2)/24. + (13*beta2*bK2P*fz**2)/24. + (45*beta*b&
  &Pi2P*fz**2)/8. + (45*beta2*bPi2P*fz**2)/8. + (45*b1*beta*fz**3)/8. - (45*bd&
  &eta*beta*fz**3)/8. + (45*b1*beta2*fz**3)/8. - (45*bdeta*beta2*fz**3)/8. + (&
  &105*beta**2*fz**4)/4. + (105*beta*beta2*fz**4)/2. + (105*beta2**2*fz**4)/4.

  Anmp(1,4,2) = (-17*b1*bK2P)/14. + bK2*bK2P + bK2P**2/12. - (187*b1*bPi2P)/98&
  &. + (4*bK2*bPi2P)/7. - (5*bK2P*bPi2P)/21. - bPi2P**2/2. - 4*b1**2*fz + (18*&
  &b1*bdeta*fz)/7. - (30*b1*beta*fz)/49. + (7*b1*bK2*fz)/2. - (3*bdeta*bK2*fz)&
  &/2. + (6*beta*bK2*fz)/7. - (3*b1*bK2P*fz)/4. + (bdeta*bK2P*fz)/4. - (beta*b&
  &K2P*fz)/42. - (51*b1*bPi2P*fz)/14. + (8*bdeta*bPi2P*fz)/7. - (beta*bPi2P*fz&
  &)/2. - (71*b1**2*fz**2)/16. + (39*b1*bdeta*fz**2)/8. - (15*bdeta**2*fz**2)/&
  &16. + (47*b1*beta*fz**2)/14. + (5*bdeta*beta*fz**2)/7. + (26*b1*beta2*fz**2&
  &)/7. - (3*beta*bK2*fz**2)/2. - beta2*bK2*fz**2 - (3*beta*bK2P*fz**2)/8. - (&
  &beta2*bK2P*fz**2)/4. + (21*b1*beta*fz**3)/16. - (9*bdeta*beta*fz**3)/16. + &
  &(9*b1*beta2*fz**3)/8. - (3*bdeta*beta2*fz**3)/8.

  Anmp(1,4,4) = (-5*b1*bK2P)/7. + b2*bK2P + (7*bK2*bK2P)/3. + (2*bK2P**2)/9. -&
  & (15*b1*bPi2P)/7. + 3*b2*bPi2P + 7*bK2*bPi2P + (17*bK2P*bPi2P)/42. - (11*bP&
  &i2P**2)/14. - (15*b1**2*fz)/7. + 3*b1*b2*fz + (15*b1*bdeta*fz)/7. - 3*b2*bd&
  &eta*fz + 7*b1*bK2*fz - 7*bdeta*bK2*fz - (13*b1*bK2P*fz)/12. + (bdeta*bK2P*f&
  &z)/12. - (beta*bK2P*fz)/7. - (169*b1*bPi2P*fz)/28. + (85*bdeta*bPi2P*fz)/28&
  &. - (3*beta*bPi2P*fz)/7. - (21*b1**2*fz**2)/4. + (15*b1*bdeta*fz**2)/2. - (&
  &9*bdeta**2*fz**2)/4. - (237*b1*beta*fz**2)/28. - (39*b2*beta*fz**2)/4. + (3&
  &*bdeta*beta*fz**2)/7. - (225*b1*beta2*fz**2)/28. - (39*b2*beta2*fz**2)/4. +&
  & (7*beta*bK2*fz**2)/2. + (7*beta2*bK2*fz**2)/2. - (14*beta*bK2P*fz**2)/3. -&
  & (115*beta2*bK2P*fz**2)/24. - (1023*beta*bPi2P*fz**2)/56. - (261*beta2*bPi2&
  &P*fz**2)/14. - (543*b1*beta*fz**3)/16. + (417*bdeta*beta*fz**3)/16. - (33*b&
  &eta**2*fz**3)/56. - (549*b1*beta2*fz**3)/16. + (423*bdeta*beta2*fz**3)/16. &
  &- (33*beta*beta2*fz**3)/56. + (105*beta**2*fz**4)/16. + (195*beta*beta2*fz*&
  &*4)/16. + (45*beta2**2*fz**4)/8.

  Anmp(1,4,6) = (-5*bK2P**2)/12. - (5*bK2P*bPi2P)/2. - (15*bPi2P**2)/4. - (5*b&
  &1*bK2P*fz)/2. + (5*bdeta*bK2P*fz)/2. - (15*b1*bPi2P*fz)/2. + (15*bdeta*bPi2&
  &P*fz)/2. - (15*b1**2*fz**2)/4. + (15*b1*bdeta*fz**2)/2. - (15*bdeta**2*fz**&
  &2)/4. - (15*b2*beta*fz**2)/4. - (15*b2*beta2*fz**2)/4. - 5*beta*bK2*fz**2 -&
  & 5*beta2*bK2*fz**2 - (245*beta*bK2P*fz**2)/24. - (245*beta2*bK2P*fz**2)/24.&
  & - (225*beta*bPi2P*fz**2)/8. - (225*beta2*bPi2P*fz**2)/8. - (225*b1*beta*fz&
  &**3)/8. + (225*bdeta*beta*fz**3)/8. - (225*b1*beta2*fz**3)/8. + (225*bdeta*&
  &beta2*fz**3)/8. - (525*beta**2*fz**4)/16. - (525*beta*beta2*fz**4)/8. - (52&
  &5*beta2**2*fz**4)/16.

  Anmp(1,5,3) = (5*b1*bK2P)/7. - bK2*bK2P + bK2P**2/6. + (15*b1*bPi2P)/7. - 3*&
  &bK2*bPi2P + (53*bK2P*bPi2P)/42. + (16*bPi2P**2)/7. + (15*b1**2*fz)/7. - (15&
  &*b1*bdeta*fz)/7. - 3*b1*bK2*fz + 3*bdeta*bK2*fz + (11*b1*bK2P*fz)/4. - (7*b&
  &deta*bK2P*fz)/4. + (beta*bK2P*fz)/7. + (253*b1*bPi2P*fz)/28. - (169*bdeta*b&
  &Pi2P*fz)/28. + (3*beta*bPi2P*fz)/7. + (27*b1**2*fz**2)/4. - (21*b1*bdeta*fz&
  &**2)/2. + (15*bdeta**2*fz**2)/4. - (213*b1*beta*fz**2)/28. - (3*bdeta*beta*&
  &fz**2)/7. - (225*b1*beta2*fz**2)/28. + 6*beta*bK2*fz**2 + 6*beta2*bK2*fz**2&
  & + (5*beta*bK2P*fz**2)/8. + (3*beta2*bK2P*fz**2)/4. - (21*beta*bPi2P*fz**2)&
  &/8. - (9*beta2*bPi2P*fz**2)/4. - (33*b1*beta*fz**3)/4. + (39*bdeta*beta*fz*&
  &*3)/8. - (3*beta**2*fz**3)/8. - (63*b1*beta2*fz**3)/8. + (9*bdeta*beta2*fz*&
  &*3)/2. - (3*beta*beta2*fz**3)/8.

  Anmp(1,5,5) = (5*bK2P**2)/6. + 5*bK2P*bPi2P + (15*bPi2P**2)/2. + 5*b1*bK2P*f&
  &z - 5*bdeta*bK2P*fz + 15*b1*bPi2P*fz - 15*bdeta*bPi2P*fz + (15*b1**2*fz**2)&
  &/2. - 15*b1*bdeta*fz**2 + (15*bdeta**2*fz**2)/2. + (15*b2*beta*fz**2)/2. + &
  &(15*b2*beta2*fz**2)/2. + 10*beta*bK2*fz**2 + 10*beta2*bK2*fz**2 + (175*beta&
  &*bK2P*fz**2)/24. + (175*beta2*bK2P*fz**2)/24. + (135*beta*bPi2P*fz**2)/8. +&
  & (135*beta2*bPi2P*fz**2)/8. + (135*b1*beta*fz**3)/8. - (135*bdeta*beta*fz**&
  &3)/8. + (135*b1*beta2*fz**3)/8. - (135*bdeta*beta2*fz**3)/8. - (105*beta**2&
  &*fz**4)/4. - (105*beta*beta2*fz**4)/2. - (105*beta2**2*fz**4)/4.

  Anmp(1,6,4) = (-5*bK2P**2)/12. - (5*bK2P*bPi2P)/2. - (15*bPi2P**2)/4. - (5*b&
  &1*bK2P*fz)/2. + (5*bdeta*bK2P*fz)/2. - (15*b1*bPi2P*fz)/2. + (15*bdeta*bPi2&
  &P*fz)/2. - (15*b1**2*fz**2)/4. + (15*b1*bdeta*fz**2)/2. - (15*bdeta**2*fz**&
  &2)/4. + (75*b1*beta*fz**2)/14. + (75*b1*beta2*fz**2)/14. - (15*beta*bK2*fz*&
  &*2)/2. - (15*beta2*bK2*fz**2)/2. + (15*beta*bK2P*fz**2)/8. + (15*beta2*bK2P&
  &*fz**2)/8. + (75*beta*bPi2P*fz**2)/7. + (75*beta2*bPi2P*fz**2)/7. + (285*b1&
  &*beta*fz**3)/16. - (225*bdeta*beta*fz**3)/16. + (9*beta**2*fz**3)/28. + (28&
  &5*b1*beta2*fz**3)/16. - (225*bdeta*beta2*fz**3)/16. + (9*beta*beta2*fz**3)/&
  &28. - (35*beta**2*fz**4)/16. - (65*beta*beta2*fz**4)/16. - (15*beta2**2*fz*&
  &*4)/8.

  Anmp(1,6,6) = (35*beta*bK2P*fz**2)/8. + (35*beta2*bK2P*fz**2)/8. + (105*beta&
  &*bPi2P*fz**2)/8. + (105*beta2*bPi2P*fz**2)/8. + (105*b1*beta*fz**3)/8. - (1&
  &05*bdeta*beta*fz**3)/8. + (105*b1*beta2*fz**3)/8. - (105*bdeta*beta2*fz**3)&
  &/8. + (245*beta**2*fz**4)/8. + (245*beta*beta2*fz**4)/4. + (245*beta2**2*fz&
  &**4)/8.

  Anmp(1,7,5) = (-35*beta*bK2P*fz**2)/8. - (35*beta2*bK2P*fz**2)/8. - (105*bet&
  &a*bPi2P*fz**2)/8. - (105*beta2*bPi2P*fz**2)/8. - (105*b1*beta*fz**3)/8. + (&
  &105*bdeta*beta*fz**3)/8. - (105*b1*beta2*fz**3)/8. + (105*bdeta*beta2*fz**3&
  &)/8. + (35*beta**2*fz**4)/4. + (35*beta*beta2*fz**4)/2. + (35*beta2**2*fz**&
  &4)/4.

  Anmp(1,8,6) = (-315*beta**2*fz**4)/32. - (315*beta*beta2*fz**4)/16. - (315*b&
  &eta2**2*fz**4)/32.

  Anmp(2,0,0) = -(b1**2*fz**2)/8. + (b1*beta*fz**3)/4.

  Anmp(2,0,2) = bK2P**2/24. - (5*bK2P*bPi2P)/21. + (25*bPi2P**2)/49. - (b1*bK2&
  &P*fz)/8. + (bdeta*bK2P*fz)/8. - (beta*bK2P*fz)/42. - (6*b1*bPi2P*fz)/7. - (&
  &5*bdeta*bPi2P*fz)/14. + (5*beta*bPi2P*fz)/49. - (13*b1**2*fz**2)/32. + (9*b&
  &1*bdeta*fz**2)/16. + (3*bdeta**2*fz**2)/32. - (5*b1*beta*fz**2)/14. - (3*b2&
  &*beta*fz**2)/4. - (bdeta*beta*fz**2)/28. + (beta**2*fz**2)/196. - (3*b1*bet&
  &a2*fz**2)/14. - (b2*beta2*fz**2)/2. + (beta*bK2*fz**2)/2. + (beta2*bK2*fz**&
  &2)/3. + (beta*bK2P*fz**2)/4. + (beta2*bK2P*fz**2)/18. + (11*beta*bPi2P*fz**&
  &2)/7. + (5*beta2*bPi2P*fz**2)/7. - (17*b1*beta*fz**3)/8. - (bdeta*beta*fz**&
  &3)/8. + (3*beta**2*fz**3)/28. - (9*b1*beta2*fz**3)/4. + (bdeta*beta2*fz**3)&
  &/4. + (beta*beta2*fz**3)/14. + (51*beta**2*fz**4)/32. + (15*beta*beta2*fz**&
  &4)/8. + (3*beta2**2*fz**4)/8.

  Anmp(2,0,4) = -bK2P**2/36. - (41*bK2P*bPi2P)/84. - (17*bPi2P**2)/14. - (b1*b&
  &K2P*fz)/8. + (7*bdeta*bK2P*fz)/24. - (beta*bK2P*fz)/42. - (75*b1*bPi2P*fz)/&
  &56. + (103*bdeta*bPi2P*fz)/56. - (beta*bPi2P*fz)/14. - (b1**2*fz**2)/8. + (&
  &3*b1*bdeta*fz**2)/4. - (5*bdeta**2*fz**2)/8. + (5*b1*beta*fz**2)/56. - (3*b&
  &2*beta*fz**2)/8. + (bdeta*beta*fz**2)/14. + (9*b1*beta2*fz**2)/56. - (b2*be&
  &ta2*fz**2)/8. - (5*beta*bK2*fz**2)/4. - (11*beta2*bK2*fz**2)/12. - (47*beta&
  &*bK2P*fz**2)/24. - (119*beta2*bK2P*fz**2)/72. - (177*beta*bPi2P*fz**2)/28. &
  &- (39*beta2*bPi2P*fz**2)/7. - (69*b1*beta*fz**3)/16. + (81*bdeta*beta*fz**3&
  &)/16. - (3*beta**2*fz**3)/28. - (57*b1*beta2*fz**3)/16. + (69*bdeta*beta2*f&
  &z**3)/16. - (3*beta*beta2*fz**3)/28. - (105*beta**2*fz**4)/16. - (195*beta*&
  &beta2*fz**4)/16. - (45*beta2**2*fz**4)/8.

  Anmp(2,0,6) = bK2P**2/24. + (bK2P*bPi2P)/4. + (3*bPi2P**2)/8. + (b1*bK2P*fz)&
  &/4. - (bdeta*bK2P*fz)/4. + (3*b1*bPi2P*fz)/4. - (3*bdeta*bPi2P*fz)/4. + (3*&
  &b1**2*fz**2)/8. - (3*b1*bdeta*fz**2)/4. + (3*bdeta**2*fz**2)/8. + (3*b2*bet&
  &a*fz**2)/8. + (3*b2*beta2*fz**2)/8. + (beta*bK2*fz**2)/2. + (beta2*bK2*fz**&
  &2)/2. + (17*beta*bK2P*fz**2)/24. + (17*beta2*bK2P*fz**2)/24. + (15*beta*bPi&
  &2P*fz**2)/8. + (15*beta2*bPi2P*fz**2)/8. + (15*b1*beta*fz**3)/8. - (15*bdet&
  &a*beta*fz**3)/8. + (15*b1*beta2*fz**3)/8. - (15*bdeta*beta2*fz**3)/8. + (10&
  &5*beta**2*fz**4)/64. + (105*beta*beta2*fz**4)/32. + (105*beta2**2*fz**4)/64&
  &.

  Anmp(2,1,1) = (b1*bK2P*fz)/3. + (beta*bK2P*fz)/6. - (3*b1*bPi2P*fz)/14. - (5&
  &*beta*bPi2P*fz)/7. + (5*b1**2*fz**2)/4. - (b1*bdeta*fz**2)/4. - (11*b1*beta&
  &*fz**2)/14. - (b2*beta*fz**2)/2. + (bdeta*beta*fz**2)/4. - (beta**2*fz**2)/&
  &14. - (b1*beta2*fz**2)/2. + (beta*bK2*fz**2)/3. - (beta*bK2P*fz**2)/9. - (b&
  &eta*bPi2P*fz**2)/2. - (7*b1*beta*fz**3)/2. + (3*bdeta*beta*fz**3)/4. - (3*b&
  &eta**2*fz**3)/4. - (3*b1*beta2*fz**3)/2. - (beta*beta2*fz**3)/2.

  Anmp(2,1,3) = (7*bK2P**2)/36. + (19*bK2P*bPi2P)/28. + (2*bPi2P**2)/7. + (43*&
  &b1*bK2P*fz)/24. - (19*bdeta*bK2P*fz)/24. + (beta*bK2P*fz)/7. + (219*b1*bPi2&
  &P*fz)/56. - (51*bdeta*bPi2P*fz)/56. + (3*beta*bPi2P*fz)/7. + (29*b1**2*fz**&
  &2)/8. - (17*b1*bdeta*fz**2)/4. + (5*bdeta**2*fz**2)/8. + (117*b1*beta*fz**2&
  &)/56. + 4*b2*beta*fz**2 - (3*bdeta*beta*fz**2)/7. + (93*b1*beta2*fz**2)/56.&
  & + 4*b2*beta2*fz**2 - (2*beta*bK2*fz**2)/3. - (2*beta2*bK2*fz**2)/3. - (13*&
  &beta*bK2P*fz**2)/36. + (7*beta2*bK2P*fz**2)/18. - (141*beta*bPi2P*fz**2)/28&
  &. - (39*beta2*bPi2P*fz**2)/14. + (21*b1*beta*fz**3)/2. - (3*bdeta*beta*fz**&
  &3)/4. + (9*beta**2*fz**3)/28. + (51*b1*beta2*fz**3)/4. - 3*bdeta*beta2*fz**&
  &3 + (9*beta*beta2*fz**3)/28. - (105*beta**2*fz**4)/8. - (165*beta*beta2*fz*&
  &*4)/8. - (15*beta2**2*fz**4)/2.

  Anmp(2,1,5) = bK2P**2/4. + (3*bK2P*bPi2P)/2. + (9*bPi2P**2)/4. + (3*b1*bK2P*&
  &fz)/2. - (3*bdeta*bK2P*fz)/2. + (9*b1*bPi2P*fz)/2. - (9*bdeta*bPi2P*fz)/2. &
  &+ (9*b1**2*fz**2)/4. - (9*b1*bdeta*fz**2)/2. + (9*bdeta**2*fz**2)/4. + (9*b&
  &2*beta*fz**2)/4. + (9*b2*beta2*fz**2)/4. + 3*beta*bK2*fz**2 + 3*beta2*bK2*f&
  &z**2 + (59*beta*bK2P*fz**2)/8. + (59*beta2*bK2P*fz**2)/8. + (165*beta*bPi2P&
  &*fz**2)/8. + (165*beta2*bPi2P*fz**2)/8. + (165*b1*beta*fz**3)/8. - (165*bde&
  &ta*beta*fz**3)/8. + (165*b1*beta2*fz**3)/8. - (165*bdeta*beta2*fz**3)/8. + &
  &(105*beta**2*fz**4)/4. + (105*beta*beta2*fz**4)/2. + (105*beta2**2*fz**4)/4&
  &.

  Anmp(2,2,0) = (3*b1**2*fz**2)/8. + (beta**2*fz**2)/4. - (b1*beta*fz**3)/4.

  Anmp(2,2,2) = -bK2P**2/4. + (2*bK2P*bPi2P)/7. - (9*bPi2P**2)/98. - (17*b1*bK&
  &2P*fz)/12. + (bdeta*bK2P*fz)/4. + (2*beta*bK2P*fz)/21. + (5*b1*bPi2P*fz)/14&
  &. - (3*bdeta*bPi2P*fz)/14. + (113*beta*bPi2P*fz)/98. - (47*b1**2*fz**2)/16.&
  & + (13*b1*bdeta*fz**2)/8. - (3*bdeta**2*fz**2)/16. + (39*b1*beta*fz**2)/7. &
  &+ (13*b2*beta*fz**2)/4. - (23*bdeta*beta*fz**2)/28. + (3*beta**2*fz**2)/49.&
  & + (61*b1*beta2*fz**2)/14. + (3*b2*beta2*fz**2)/2. - (5*beta*bK2*fz**2)/3. &
  &- 2*beta2*bK2*fz**2 + (14*beta*bK2P*fz**2)/9. + (2*beta2*bK2P*fz**2)/3. + (&
  &47*beta*bPi2P*fz**2)/14. + (11*beta2*bPi2P*fz**2)/7. + (85*b1*beta*fz**3)/4&
  &. - 7*bdeta*beta*fz**3 + (99*beta**2*fz**3)/28. + (33*b1*beta2*fz**3)/2. - &
  &4*bdeta*beta2*fz**3 + (47*beta*beta2*fz**3)/14. - (51*beta**2*fz**4)/16. - &
  &(15*beta*beta2*fz**4)/4. - (3*beta2**2*fz**4)/4.

  Anmp(2,2,4) = (-5*bK2P**2)/8. - (193*bK2P*bPi2P)/84. - (71*bPi2P**2)/56. - (&
  &9*b1*bK2P*fz)/2. + 3*bdeta*bK2P*fz - (beta*bK2P*fz)/14. - (64*b1*bPi2P*fz)/&
  &7. + (65*bdeta*bPi2P*fz)/14. - (3*beta*bPi2P*fz)/14. - (63*b1**2*fz**2)/8. &
  &+ (45*b1*bdeta*fz**2)/4. - (27*bdeta**2*fz**2)/8. - (33*b1*beta*fz**2)/14. &
  &- (15*b2*beta*fz**2)/2. + (3*bdeta*beta*fz**2)/14. - (15*b1*beta2*fz**2)/7.&
  & - (33*b2*beta2*fz**2)/4. - (7*beta*bK2*fz**2)/4. - (11*beta2*bK2*fz**2)/4.&
  & + (65*beta*bK2P*fz**2)/24. + (25*beta2*bK2P*fz**2)/24. + (33*beta*bPi2P*fz&
  &**2)/2. + 12*beta2*bPi2P*fz**2 - (81*b1*beta*fz**3)/16. - (111*bdeta*beta*f&
  &z**3)/16. - (153*b1*beta2*fz**3)/16. - (39*bdeta*beta2*fz**3)/16. + (945*be&
  &ta**2*fz**4)/16. + (1755*beta*beta2*fz**4)/16. + (405*beta2**2*fz**4)/8.

  Anmp(2,2,6) = (-5*bK2P**2)/12. - (5*bK2P*bPi2P)/2. - (15*bPi2P**2)/4. - (5*b&
  &1*bK2P*fz)/2. + (5*bdeta*bK2P*fz)/2. - (15*b1*bPi2P*fz)/2. + (15*bdeta*bPi2&
  &P*fz)/2. - (15*b1**2*fz**2)/4. + (15*b1*bdeta*fz**2)/2. - (15*bdeta**2*fz**&
  &2)/4. - (15*b2*beta*fz**2)/4. - (15*b2*beta2*fz**2)/4. - 5*beta*bK2*fz**2 -&
  & 5*beta2*bK2*fz**2 - (245*beta*bK2P*fz**2)/24. - (245*beta2*bK2P*fz**2)/24.&
  & - (225*beta*bPi2P*fz**2)/8. - (225*beta2*bPi2P*fz**2)/8. - (225*b1*beta*fz&
  &**3)/8. + (225*bdeta*beta*fz**3)/8. - (225*b1*beta2*fz**3)/8. + (225*bdeta*&
  &beta2*fz**3)/8. - (525*beta**2*fz**4)/16. - (525*beta*beta2*fz**4)/8. - (52&
  &5*beta2**2*fz**4)/16.

  Anmp(2,3,1) = (-2*b1*bK2P*fz)/3. - (beta*bK2P*fz)/2. - (11*b1*bPi2P*fz)/14. &
  &- (2*beta*bPi2P*fz)/7. - (11*b1**2*fz**2)/4. + (5*b1*bdeta*fz**2)/4. + (11*&
  &b1*beta*fz**2)/14. + (3*bdeta*beta*fz**2)/4. - (3*beta**2*fz**2)/7. + (3*b1&
  &*beta2*fz**2)/2. - beta*bK2*fz**2 + (beta*bPi2P*fz**2)/2. + (7*b1*beta*fz**&
  &3)/2. - (3*bdeta*beta*fz**3)/4. + (3*beta**2*fz**3)/4. + (3*b1*beta2*fz**3)&
  &/2. + (beta*beta2*fz**3)/2.

  Anmp(2,3,3) = bK2P**2/18. - (13*bK2P*bPi2P)/42. - (10*bPi2P**2)/7. - (b1*bK2&
  &P*fz)/4. - (bdeta*bK2P*fz)/12. - (5*beta*bK2P*fz)/7. - (75*b1*bPi2P*fz)/28.&
  & + (47*bdeta*bPi2P*fz)/28. - (15*beta*bPi2P*fz)/7. - (5*b1**2*fz**2)/4. + (&
  &3*b1*bdeta*fz**2)/2. - (bdeta**2*fz**2)/4. - (345*b1*beta*fz**2)/28. - (19*&
  &b2*beta*fz**2)/2. + (15*bdeta*beta*fz**2)/7. - (285*b1*beta2*fz**2)/28. - 8&
  &*b2*beta2*fz**2 + (10*beta*bK2*fz**2)/3. + (16*beta2*bK2*fz**2)/3. - (143*b&
  &eta*bK2P*fz**2)/18. - (64*beta2*bK2P*fz**2)/9. - (237*beta*bPi2P*fz**2)/14.&
  & - (108*beta2*bPi2P*fz**2)/7. - 63*b1*beta*fz**3 + (59*bdeta*beta*fz**3)/2.&
  & - (93*beta**2*fz**3)/14. - (123*b1*beta2*fz**3)/2. + 28*bdeta*beta2*fz**3 &
  &- (93*beta*beta2*fz**3)/14. + (105*beta**2*fz**4)/4. + (165*beta*beta2*fz**&
  &4)/4. + 15*beta2**2*fz**4

  Anmp(2,3,5) = (5*bK2P**2)/18. + (5*bK2P*bPi2P)/3. + (5*bPi2P**2)/2. + (5*b1*&
  &bK2P*fz)/3. - (5*bdeta*bK2P*fz)/3. + 5*b1*bPi2P*fz - 5*bdeta*bPi2P*fz + (5*&
  &b1**2*fz**2)/2. - 5*b1*bdeta*fz**2 + (5*bdeta**2*fz**2)/2. + (5*b2*beta*fz*&
  &*2)/2. + (5*b2*beta2*fz**2)/2. + (10*beta*bK2*fz**2)/3. + (10*beta2*bK2*fz*&
  &*2)/3. - (1085*beta*bK2P*fz**2)/72. - (1085*beta2*bK2P*fz**2)/72. - (375*be&
  &ta*bPi2P*fz**2)/8. - (375*beta2*bPi2P*fz**2)/8. - (375*b1*beta*fz**3)/8. + &
  &(375*bdeta*beta*fz**3)/8. - (375*b1*beta2*fz**3)/8. + (375*bdeta*beta2*fz**&
  &3)/8. - (525*beta**2*fz**4)/4. - (525*beta*beta2*fz**4)/2. - (525*beta2**2*&
  &fz**4)/4.

  Anmp(2,4,2) = (23*bK2P**2)/72. + (13*bK2P*bPi2P)/21. + (57*bPi2P**2)/98. + (&
  &77*b1*bK2P*fz)/24. - (25*bdeta*bK2P*fz)/24. + (13*beta*bK2P*fz)/14. + (11*b&
  &1*bPi2P*fz)/2. - (10*bdeta*bPi2P*fz)/7. + (171*beta*bPi2P*fz)/98. + (243*b1&
  &**2*fz**2)/32. - (115*b1*bdeta*fz**2)/16. + (35*bdeta**2*fz**2)/32. - (73*b&
  &1*beta*fz**2)/14. - (15*bdeta*beta*fz**2)/7. + (9*beta**2*fz**2)/49. - (50*&
  &b1*beta2*fz**2)/7. + (9*beta*bK2*fz**2)/2. + 3*beta2*bK2*fz**2 - (5*beta*bK&
  &2P*fz**2)/4. - (beta2*bK2P*fz**2)/2. - (69*beta*bPi2P*fz**2)/14. - (16*beta&
  &2*bPi2P*fz**2)/7. - (153*b1*beta*fz**3)/8. + (57*bdeta*beta*fz**3)/8. - (51&
  &*beta**2*fz**3)/14. - (57*b1*beta2*fz**3)/4. + (15*bdeta*beta2*fz**3)/4. - &
  &(24*beta*beta2*fz**3)/7. + (51*beta**2*fz**4)/32. + (15*beta*beta2*fz**4)/8&
  &. + (3*beta2**2*fz**4)/8.

  Anmp(2,4,4) = (5*bK2P**2)/6. + (325*bK2P*bPi2P)/84. + (115*bPi2P**2)/28. + (&
  &145*b1*bK2P*fz)/24. - (35*bdeta*bK2P*fz)/8. + (3*beta*bK2P*fz)/7. + (825*b1&
  &*bPi2P*fz)/56. - (545*bdeta*bPi2P*fz)/56. + (9*beta*bPi2P*fz)/7. + (85*b1**&
  &2*fz**2)/8. - (65*b1*bdeta*fz**2)/4. + (45*bdeta**2*fz**2)/8. + (477*b1*bet&
  &a*fz**2)/56. + (115*b2*beta*fz**2)/8. - (9*bdeta*beta*fz**2)/7. + (405*b1*b&
  &eta2*fz**2)/56. + (115*b2*beta2*fz**2)/8. + (35*beta*bK2*fz**2)/12. + (35*b&
  &eta2*bK2*fz**2)/12. + (1205*beta*bK2P*fz**2)/72. + (1295*beta2*bK2P*fz**2)/&
  &72. + (1035*beta*bPi2P*fz**2)/28. + (285*beta2*bPi2P*fz**2)/7. + (1305*b1*b&
  &eta*fz**3)/16. - (845*bdeta*beta*fz**3)/16. + (93*beta**2*fz**3)/28. + (136&
  &5*b1*beta2*fz**3)/16. - (905*bdeta*beta2*fz**3)/16. + (93*beta*beta2*fz**3)&
  &/28. - (1575*beta**2*fz**4)/16. - (2925*beta*beta2*fz**4)/16. - (675*beta2*&
  &*2*fz**4)/8.

  Anmp(2,4,6) = (35*bK2P**2)/72. + (35*bK2P*bPi2P)/12. + (35*bPi2P**2)/8. + (3&
  &5*b1*bK2P*fz)/12. - (35*bdeta*bK2P*fz)/12. + (35*b1*bPi2P*fz)/4. - (35*bdet&
  &a*bPi2P*fz)/4. + (35*b1**2*fz**2)/8. - (35*b1*bdeta*fz**2)/4. + (35*bdeta**&
  &2*fz**2)/8. + (35*b2*beta*fz**2)/8. + (35*b2*beta2*fz**2)/8. + (35*beta*bK2&
  &*fz**2)/6. + (35*beta2*bK2*fz**2)/6. + (1645*beta*bK2P*fz**2)/72. + (1645*b&
  &eta2*bK2P*fz**2)/72. + (525*beta*bPi2P*fz**2)/8. + (525*beta2*bPi2P*fz**2)/&
  &8. + (525*b1*beta*fz**3)/8. - (525*bdeta*beta*fz**3)/8. + (525*b1*beta2*fz*&
  &*3)/8. - (525*bdeta*beta2*fz**3)/8. + (3675*beta**2*fz**4)/32. + (3675*beta&
  &*beta2*fz**4)/16. + (3675*beta2**2*fz**4)/32.

  Anmp(2,5,3) = (-25*bK2P**2)/36. - (85*bK2P*bPi2P)/28. - (20*bPi2P**2)/7. - (&
  &125*b1*bK2P*fz)/24. + (85*bdeta*bK2P*fz)/24. - (3*beta*bK2P*fz)/7. - (685*b&
  &1*bPi2P*fz)/56. + (405*bdeta*bPi2P*fz)/56. - (9*beta*bPi2P*fz)/7. - (75*b1*&
  &*2*fz**2)/8. + (55*b1*bdeta*fz**2)/4. - (35*bdeta**2*fz**2)/8. + (573*b1*be&
  &ta*fz**2)/56. + (9*bdeta*beta*fz**2)/7. + (645*b1*beta2*fz**2)/56. - 10*bet&
  &a*bK2*fz**2 - 10*beta2*bK2*fz**2 + (85*beta*bK2P*fz**2)/12. + (35*beta2*bK2&
  &P*fz**2)/6. + (615*beta*bPi2P*fz**2)/28. + (255*beta2*bPi2P*fz**2)/14. + (1&
  &05*b1*beta*fz**3)/2. - (115*bdeta*beta*fz**3)/4. + (177*beta**2*fz**3)/28. &
  &+ (195*b1*beta2*fz**3)/4. - 25*bdeta*beta2*fz**3 + (177*beta*beta2*fz**3)/2&
  &8. - (105*beta**2*fz**4)/8. - (165*beta*beta2*fz**4)/8. - (15*beta2**2*fz**&
  &4)/2.

  Anmp(2,5,5) = (-35*bK2P**2)/36. - (35*bK2P*bPi2P)/6. - (35*bPi2P**2)/4. - (3&
  &5*b1*bK2P*fz)/6. + (35*bdeta*bK2P*fz)/6. - (35*b1*bPi2P*fz)/2. + (35*bdeta*&
  &bPi2P*fz)/2. - (35*b1**2*fz**2)/4. + (35*b1*bdeta*fz**2)/2. - (35*bdeta**2*&
  &fz**2)/4. - (35*b2*beta*fz**2)/4. - (35*b2*beta2*fz**2)/4. - (35*beta*bK2*f&
  &z**2)/3. - (35*beta2*bK2*fz**2)/3. - (455*beta*bK2P*fz**2)/72. - (455*beta2&
  &*bK2P*fz**2)/72. - (105*beta*bPi2P*fz**2)/8. - (105*beta2*bPi2P*fz**2)/8. -&
  & (105*b1*beta*fz**3)/8. + (105*bdeta*beta*fz**3)/8. - (105*b1*beta2*fz**3)/&
  &8. + (105*bdeta*beta2*fz**3)/8. + (735*beta**2*fz**4)/4. + (735*beta*beta2*&
  &fz**4)/2. + (735*beta2**2*fz**4)/4.

  Anmp(2,6,4) = (35*bK2P**2)/72. + (35*bK2P*bPi2P)/12. + (35*bPi2P**2)/8. + (3&
  &5*b1*bK2P*fz)/12. - (35*bdeta*bK2P*fz)/12. + (35*b1*bPi2P*fz)/4. - (35*bdet&
  &a*bPi2P*fz)/4. + (35*b1**2*fz**2)/8. - (35*b1*bdeta*fz**2)/4. + (35*bdeta**&
  &2*fz**2)/8. - (25*b1*beta*fz**2)/4. - (25*b1*beta2*fz**2)/4. + (35*beta*bK2&
  &*fz**2)/4. + (35*beta2*bK2*fz**2)/4. - (385*beta*bK2P*fz**2)/24. - (385*bet&
  &a2*bK2P*fz**2)/24. - (330*beta*bPi2P*fz**2)/7. - (330*beta2*bPi2P*fz**2)/7.&
  & - (1155*b1*beta*fz**3)/16. + (875*bdeta*beta*fz**3)/16. - (45*beta**2*fz**&
  &3)/14. - (1155*b1*beta2*fz**3)/16. + (875*bdeta*beta2*fz**3)/16. - (45*beta&
  &*beta2*fz**3)/14. + (735*beta**2*fz**4)/16. + (1365*beta*beta2*fz**4)/16. +&
  & (315*beta2**2*fz**4)/8.

  Anmp(2,6,6) = (-105*beta*bK2P*fz**2)/8. - (105*beta2*bK2P*fz**2)/8. - (315*b&
  &eta*bPi2P*fz**2)/8. - (315*beta2*bPi2P*fz**2)/8. - (315*b1*beta*fz**3)/8. +&
  & (315*bdeta*beta*fz**3)/8. - (315*b1*beta2*fz**3)/8. + (315*bdeta*beta2*fz*&
  &*3)/8. - (2205*beta**2*fz**4)/16. - (2205*beta*beta2*fz**4)/8. - (2205*beta&
  &2**2*fz**4)/16.

  Anmp(2,7,5) = (105*beta*bK2P*fz**2)/8. + (105*beta2*bK2P*fz**2)/8. + (315*be&
  &ta*bPi2P*fz**2)/8. + (315*beta2*bPi2P*fz**2)/8. + (315*b1*beta*fz**3)/8. - &
  &(315*bdeta*beta*fz**3)/8. + (315*b1*beta2*fz**3)/8. - (315*bdeta*beta2*fz**&
  &3)/8. - (315*beta**2*fz**4)/4. - (315*beta*beta2*fz**4)/2. - (315*beta2**2*&
  &fz**4)/4.

  Anmp(2,8,6) = (3465*beta**2*fz**4)/64. + (3465*beta*beta2*fz**4)/32. + (3465&
  &*beta2**2*fz**4)/64.

  Anmp(3,0,0) = -(b1*beta*fz**3)/4. + (beta**2*fz**4)/8.

  Anmp(3,0,2) = -(beta*bK2P*fz**2)/24. + (beta2*bK2P*fz**2)/12. - (11*beta*bPi&
  &2P*fz**2)/7. - (5*beta2*bPi2P*fz**2)/7. + (13*b1*beta*fz**3)/16. + (11*bdet&
  &a*beta*fz**3)/16. - (3*beta**2*fz**3)/28. + (9*b1*beta2*fz**3)/8. + (bdeta*&
  &beta2*fz**3)/8. - (beta*beta2*fz**3)/14. - (51*beta**2*fz**4)/16. - (15*bet&
  &a*beta2*fz**4)/4. - (3*beta2**2*fz**4)/4.

  Anmp(3,0,4) = (5*beta*bK2P*fz**2)/6. + (17*beta2*bK2P*fz**2)/24. + (177*beta&
  &*bPi2P*fz**2)/56. + (39*beta2*bPi2P*fz**2)/14. + (37*b1*beta*fz**3)/16. - (&
  &43*bdeta*beta*fz**3)/16. + (3*beta**2*fz**3)/56. + (31*b1*beta2*fz**3)/16. &
  &- (37*bdeta*beta2*fz**3)/16. + (3*beta*beta2*fz**3)/56. + (105*beta**2*fz**&
  &4)/16. + (195*beta*beta2*fz**4)/16. + (45*beta2**2*fz**4)/8.

  Anmp(3,0,6) = (-5*beta*bK2P*fz**2)/24. - (5*beta2*bK2P*fz**2)/24. - (5*beta*&
  &bPi2P*fz**2)/8. - (5*beta2*bPi2P*fz**2)/8. - (5*b1*beta*fz**3)/8. + (5*bdet&
  &a*beta*fz**3)/8. - (5*b1*beta2*fz**3)/8. + (5*bdeta*beta2*fz**3)/8. - (35*b&
  &eta**2*fz**4)/32. - (35*beta*beta2*fz**4)/16. - (35*beta2**2*fz**4)/32.

  Anmp(3,1,1) = (beta*bK2P*fz**2)/3. - (3*beta*bPi2P*fz**2)/14. + (7*b1*beta*f&
  &z**3)/2. - (bdeta*beta*fz**3)/4. + (19*beta**2*fz**3)/28. + (3*b1*beta2*fz*&
  &*3)/2. + (beta*beta2*fz**3)/2. - (9*beta**2*fz**4)/4. - (3*beta*beta2*fz**4&
  &)/2.

  Anmp(3,1,3) = (5*beta*bK2P*fz**2)/8. - (beta2*bK2P*fz**2)/4. + (429*beta*bPi&
  &2P*fz**2)/56. + (141*beta2*bPi2P*fz**2)/28. - (9*b1*beta*fz**3)/4. - (33*bd&
  &eta*beta*fz**3)/8. + (3*beta**2*fz**3)/56. - (39*b1*beta2*fz**3)/8. - (3*bd&
  &eta*beta2*fz**3)/2. + (3*beta*beta2*fz**3)/56. + (105*beta**2*fz**4)/4. + (&
  &165*beta*beta2*fz**4)/4. + 15*beta2**2*fz**4

  Anmp(3,1,5) = (-85*beta*bK2P*fz**2)/24. - (85*beta2*bK2P*fz**2)/24. - (85*be&
  &ta*bPi2P*fz**2)/8. - (85*beta2*bPi2P*fz**2)/8. - (85*b1*beta*fz**3)/8. + (8&
  &5*bdeta*beta*fz**3)/8. - (85*b1*beta2*fz**3)/8. + (85*bdeta*beta2*fz**3)/8.&
  & - (105*beta**2*fz**4)/4. - (105*beta*beta2*fz**4)/2. - (105*beta2**2*fz**4&
  &)/4.

  Anmp(3,2,0) = (3*b1*beta*fz**3)/4. + (beta**2*fz**3)/2. - (beta**2*fz**4)/8.

  Anmp(3,2,2) = (-9*beta*bK2P*fz**2)/4. - (3*beta2*bK2P*fz**2)/2. - (3*beta*bP&
  &i2P*fz**2)/14. - (beta2*bPi2P*fz**2)/7. - (141*b1*beta*fz**3)/8. + (27*bdet&
  &a*beta*fz**3)/8. - (93*beta**2*fz**3)/28. - (57*b1*beta2*fz**3)/4. + (9*bde&
  &ta*beta2*fz**3)/4. - (45*beta*beta2*fz**3)/14. + (153*beta**2*fz**4)/8. + (&
  &45*beta*beta2*fz**4)/2. + (9*beta2**2*fz**4)/2.

  Anmp(3,2,4) = (-35*beta*bK2P*fz**2)/8. - (25*beta2*bK2P*fz**2)/8. - (585*bet&
  &a*bPi2P*fz**2)/28. - (120*beta2*bPi2P*fz**2)/7. - (135*b1*beta*fz**3)/16. +&
  & (255*bdeta*beta*fz**3)/16. - (3*beta**2*fz**3)/14. - (75*b1*beta2*fz**3)/1&
  &6. + (195*bdeta*beta2*fz**3)/16. - (3*beta*beta2*fz**3)/14. - (1575*beta**2&
  &*fz**4)/16. - (2925*beta*beta2*fz**4)/16. - (675*beta2**2*fz**4)/8.

  Anmp(3,2,6) = (35*beta*bK2P*fz**2)/8. + (35*beta2*bK2P*fz**2)/8. + (105*beta&
  &*bPi2P*fz**2)/8. + (105*beta2*bPi2P*fz**2)/8. + (105*b1*beta*fz**3)/8. - (1&
  &05*bdeta*beta*fz**3)/8. + (105*b1*beta2*fz**3)/8. - (105*bdeta*beta2*fz**3)&
  &/8. + (245*beta**2*fz**4)/8. + (245*beta*beta2*fz**4)/4. + (245*beta2**2*fz&
  &**4)/8.

  Anmp(3,3,1) = (-2*beta*bK2P*fz**2)/3. - (11*beta*bPi2P*fz**2)/14. - (13*b1*b&
  &eta*fz**3)/2. + (5*bdeta*beta*fz**3)/4. - (75*beta**2*fz**3)/28. - (5*b1*be&
  &ta2*fz**3)/2. - (3*beta*beta2*fz**3)/2. + (9*beta**2*fz**4)/4. + (3*beta*be&
  &ta2*fz**4)/2.

  Anmp(3,3,3) = (85*beta*bK2P*fz**2)/12. + (15*beta2*bK2P*fz**2)/2. + (135*bet&
  &a*bPi2P*fz**2)/28. + (85*beta2*bPi2P*fz**2)/14. + (85*b1*beta*fz**3)/2. - (&
  &55*bdeta*beta*fz**3)/4. + (157*beta**2*fz**3)/28. + (175*b1*beta2*fz**3)/4.&
  & - 15*bdeta*beta2*fz**3 + (157*beta*beta2*fz**3)/28. - (175*beta**2*fz**4)/&
  &2. - (275*beta*beta2*fz**4)/2. - 50*beta2**2*fz**4

  Anmp(3,3,5) = (105*beta*bK2P*fz**2)/8. + (105*beta2*bK2P*fz**2)/8. + (315*be&
  &ta*bPi2P*fz**2)/8. + (315*beta2*bPi2P*fz**2)/8. + (315*b1*beta*fz**3)/8. - &
  &(315*bdeta*beta*fz**3)/8. + (315*b1*beta2*fz**3)/8. - (315*bdeta*beta2*fz**&
  &3)/8. + (735*beta**2*fz**4)/4. + (735*beta*beta2*fz**4)/2. + (735*beta2**2*&
  &fz**4)/4.

  Anmp(3,4,2) = (95*beta*bK2P*fz**2)/24. + (25*beta2*bK2P*fz**2)/12. + (95*bet&
  &a*bPi2P*fz**2)/14. + (20*beta2*bPi2P*fz**2)/7. + (405*b1*beta*fz**3)/16. - &
  &(145*bdeta*beta*fz**3)/16. + (97*beta**2*fz**3)/14. + (145*b1*beta2*fz**3)/&
  &8. - (35*bdeta*beta2*fz**3)/8. + (44*beta*beta2*fz**3)/7. - (255*beta**2*fz&
  &**4)/16. - (75*beta*beta2*fz**4)/4. - (15*beta2**2*fz**4)/4.

  Anmp(3,4,4) = (-35*beta*bK2P*fz**2)/4. - (245*beta2*bK2P*fz**2)/24. - (575*b&
  &eta*bPi2P*fz**2)/56. - (205*beta2*bPi2P*fz**2)/14. - (665*b1*beta*fz**3)/16&
  &. + (315*bdeta*beta*fz**3)/16. - (145*beta**2*fz**3)/56. - (735*b1*beta2*fz&
  &**3)/16. + (385*bdeta*beta2*fz**3)/16. - (145*beta*beta2*fz**3)/56. + (3675&
  &*beta**2*fz**4)/16. + (6825*beta*beta2*fz**4)/16. + (1575*beta2**2*fz**4)/8&
  &.

  Anmp(3,4,6) = (-105*beta*bK2P*fz**2)/8. - (105*beta2*bK2P*fz**2)/8. - (315*b&
  &eta*bPi2P*fz**2)/8. - (315*beta2*bPi2P*fz**2)/8. - (315*b1*beta*fz**3)/8. +&
  & (315*bdeta*beta*fz**3)/8. - (315*b1*beta2*fz**3)/8. + (315*bdeta*beta2*fz*&
  &*3)/8. - (2205*beta**2*fz**4)/16. - (2205*beta*beta2*fz**4)/8. - (2205*beta&
  &2**2*fz**4)/16.

  Anmp(3,5,3) = (-91*beta*bK2P*fz**2)/8. - (119*beta2*bK2P*fz**2)/12. - (1315*&
  &beta*bPi2P*fz**2)/56. - (535*beta2*bPi2P*fz**2)/28. - (217*b1*beta*fz**3)/4&
  &. + (231*bdeta*beta*fz**3)/8. - (485*beta**2*fz**3)/56. - (399*b1*beta2*fz*&
  &*3)/8. + (49*bdeta*beta2*fz**3)/2. - (485*beta*beta2*fz**3)/56. + (245*beta&
  &**2*fz**4)/4. + (385*beta*beta2*fz**4)/4. + 35*beta2**2*fz**4

  Anmp(3,5,5) = (-21*beta*bK2P*fz**2)/8. - (21*beta2*bK2P*fz**2)/8. - (63*beta&
  &*bPi2P*fz**2)/8. - (63*beta2*bPi2P*fz**2)/8. - (63*b1*beta*fz**3)/8. + (63*&
  &bdeta*beta*fz**3)/8. - (63*b1*beta2*fz**3)/8. + (63*bdeta*beta2*fz**3)/8. -&
  & (1323*beta**2*fz**4)/4. - (1323*beta*beta2*fz**4)/2. - (1323*beta2**2*fz**&
  &4)/4.

  Anmp(3,6,4) = (133*beta*bK2P*fz**2)/8. + (133*beta2*bK2P*fz**2)/8. + 41*beta&
  &*bPi2P*fz**2 + 41*beta2*bPi2P*fz**2 + (987*b1*beta*fz**3)/16. - (735*bdeta*&
  &beta*fz**3)/16. + (15*beta**2*fz**3)/4. + (987*b1*beta2*fz**3)/16. - (735*b&
  &deta*beta2*fz**3)/16. + (15*beta*beta2*fz**3)/4. - (2205*beta**2*fz**4)/16.&
  & - (4095*beta*beta2*fz**4)/16. - (945*beta2**2*fz**4)/8.

  Anmp(3,6,6) = (77*beta*bK2P*fz**2)/8. + (77*beta2*bK2P*fz**2)/8. + (231*beta&
  &*bPi2P*fz**2)/8. + (231*beta2*bPi2P*fz**2)/8. + (231*b1*beta*fz**3)/8. - (2&
  &31*bdeta*beta*fz**3)/8. + (231*b1*beta2*fz**3)/8. - (231*bdeta*beta2*fz**3)&
  &/8. + (1617*beta**2*fz**4)/8. + (1617*beta*beta2*fz**4)/4. + (1617*beta2**2&
  &*fz**4)/8.

  Anmp(3,7,5) = (-77*beta*bK2P*fz**2)/8. - (77*beta2*bK2P*fz**2)/8. - (231*bet&
  &a*bPi2P*fz**2)/8. - (231*beta2*bPi2P*fz**2)/8. - (231*b1*beta*fz**3)/8. + (&
  &231*bdeta*beta*fz**3)/8. - (231*b1*beta2*fz**3)/8. + (231*bdeta*beta2*fz**3&
  &)/8. + (693*beta**2*fz**4)/4. + (693*beta*beta2*fz**4)/2. + (693*beta2**2*f&
  &z**4)/4.

  Anmp(3,8,6) = (-3003*beta**2*fz**4)/32. - (3003*beta*beta2*fz**4)/16. - (300&
  &3*beta2**2*fz**4)/32.

  Anmp(4,0,0) = -(beta**2*fz**4)/8.

  Anmp(4,0,2) = (51*beta**2*fz**4)/32. + (15*beta*beta2*fz**4)/8. + (3*beta2**&
  &2*fz**4)/8.

  Anmp(4,0,4) = (-35*beta**2*fz**4)/16. - (65*beta*beta2*fz**4)/16. - (15*beta&
  &2**2*fz**4)/8.

  Anmp(4,0,6) = (35*beta**2*fz**4)/128. + (35*beta*beta2*fz**4)/64. + (35*beta&
  &2**2*fz**4)/128.

  Anmp(4,1,1) = (9*beta**2*fz**4)/4. + (3*beta*beta2*fz**4)/2.

  Anmp(4,1,3) = (-105*beta**2*fz**4)/8. - (165*beta*beta2*fz**4)/8. - (15*beta&
  &2**2*fz**4)/2.

  Anmp(4,1,5) = (35*beta**2*fz**4)/4. + (35*beta*beta2*fz**4)/2. + (35*beta2**&
  &2*fz**4)/4.

  Anmp(4,2,0) = (3*beta**2*fz**4)/8.

  Anmp(4,2,2) = (-255*beta**2*fz**4)/16. - (75*beta*beta2*fz**4)/4. - (15*beta&
  &2**2*fz**4)/4.

  Anmp(4,2,4) = (735*beta**2*fz**4)/16. + (1365*beta*beta2*fz**4)/16. + (315*b&
  &eta2**2*fz**4)/8.

  Anmp(4,2,6) = (-315*beta**2*fz**4)/32. - (315*beta*beta2*fz**4)/16. - (315*b&
  &eta2**2*fz**4)/32.

  Anmp(4,3,1) = (-15*beta**2*fz**4)/4. - (5*beta*beta2*fz**4)/2.

  Anmp(4,3,3) = (245*beta**2*fz**4)/4. + (385*beta*beta2*fz**4)/4. + 35*beta2*&
  &*2*fz**4

  Anmp(4,3,5) = (-315*beta**2*fz**4)/4. - (315*beta*beta2*fz**4)/2. - (315*bet&
  &a2**2*fz**4)/4.

  Anmp(4,4,2) = (595*beta**2*fz**4)/32. + (175*beta*beta2*fz**4)/8. + (35*beta&
  &2**2*fz**4)/8.

  Anmp(4,4,4) = (-2205*beta**2*fz**4)/16. - (4095*beta*beta2*fz**4)/16. - (945&
  &*beta2**2*fz**4)/8.

  Anmp(4,4,6) = (3465*beta**2*fz**4)/64. + (3465*beta*beta2*fz**4)/32. + (3465&
  &*beta2**2*fz**4)/64.

  Anmp(4,5,3) = (-441*beta**2*fz**4)/8. - (693*beta*beta2*fz**4)/8. - (63*beta&
  &2**2*fz**4)/2.

  Anmp(4,5,5) = (693*beta**2*fz**4)/4. + (693*beta*beta2*fz**4)/2. + (693*beta&
  &2**2*fz**4)/4.

  Anmp(4,6,4) = (1617*beta**2*fz**4)/16. + (3003*beta*beta2*fz**4)/16. + (693*&
  &beta2**2*fz**4)/8.

  Anmp(4,6,6) = (-3003*beta**2*fz**4)/32. - (3003*beta*beta2*fz**4)/16. - (300&
  &3*beta2**2*fz**4)/32.

  Anmp(4,7,5) = (-429*beta**2*fz**4)/4. - (429*beta*beta2*fz**4)/2. - (429*bet&
  &a2**2*fz**4)/4.

  Anmp(4,8,6) = (6435*beta**2*fz**4)/128. + (6435*beta*beta2*fz**4)/64. + (643&
  &5*beta2**2*fz**4)/128.
  
  return
  
END SUBROUTINE calculate_Anmp

!-----------------------------------------------------------------------------

SUBROUTINE calculate_dAnmpdf(fz,b1,b2,bK2,beta,bPi2P,bK2P,bdeta,beta2,dAnmpdf)

  IMPLICIT none

  DOUBLE PRECISION, intent(IN) :: fz,b1,beta,b2,bK2,bPi2P,bK2P,bdeta,beta2
  DOUBLE PRECISION, intent(OUT) :: dAnmpdf(0:4,0:8,0:6)

  dAnmpdf = 0d0

  dAnmpdf(0,0,2) = (3*b1**2)/28. + (b1*b2)/4. - (3*b1*bdeta)/28. - (b2*bdeta)/&
  &4. - (b1*bK2)/6. + (bdeta*bK2)/6. - (5*b1*bK2P)/72. + (5*bdeta*bK2P)/72. + &
  &(3*b1**2*fz)/16. - (3*b1*bdeta*fz)/8. + (3*bdeta**2*fz)/16.

  dAnmpdf(0,0,4) = (3*b1**2)/14. + (3*b1*b2)/4. - (3*b1*bdeta)/14. - (3*b2*bde&
  &ta)/4. + (b1*bK2P)/24. - (bdeta*bK2P)/24. + (3*b1*bPi2P)/8. - (3*bdeta*bPi2&
  &P)/8. + (3*b1**2*fz)/4. - (3*b1*bdeta*fz)/2. + (3*bdeta**2*fz)/4. + (9*b1*b&
  &eta*fz)/28. + (3*b2*beta*fz)/4. + (9*b1*beta2*fz)/28. + (3*b2*beta2*fz)/4. &
  &- (beta*bK2*fz)/2. - (beta2*bK2*fz)/2. - (beta*bK2P*fz)/4. - (beta2*bK2P*fz&
  &)/4. + (15*b1*beta*fz**2)/16. - (15*bdeta*beta*fz**2)/16. + (15*b1*beta2*fz&
  &**2)/16. - (15*bdeta*beta2*fz**2)/16.

  dAnmpdf(0,0,6) = (b1*b2)/2. - (b2*bdeta)/2. + (2*b1*bK2)/3. - (2*bdeta*bK2)/&
  &3. + (13*b1*bK2P)/36. - (13*bdeta*bK2P)/36. + (3*b1*bPi2P)/4. - (3*bdeta*bP&
  &i2P)/4. + (3*b1**2*fz)/4. - (3*b1*bdeta*fz)/2. + (3*bdeta**2*fz)/4. + (3*b2&
  &*beta*fz)/4. + (3*b2*beta2*fz)/4. + beta*bK2*fz + beta2*bK2*fz + (7*beta*bK&
  &2P*fz)/12. + (7*beta2*bK2P*fz)/12. + (5*beta*bPi2P*fz)/4. + (5*beta2*bPi2P*&
  &fz)/4. + (15*b1*beta*fz**2)/8. - (15*bdeta*beta*fz**2)/8. + (15*b1*beta2*fz&
  &**2)/8. - (15*bdeta*beta2*fz**2)/8. + (35*beta**2*fz**3)/32. + (35*beta*bet&
  &a2*fz**3)/16. + (35*beta2**2*fz**3)/32.

  dAnmpdf(0,1,1) = b1**2/4. - (b1*bdeta)/4.

  dAnmpdf(0,1,3) = (2*b1**2)/7. - b1*b2 - (2*b1*bdeta)/7. + b2*bdeta - (b1*bK2&
  &)/3. + (bdeta*bK2)/3. - (7*b1*bK2P)/72. + (7*bdeta*bK2P)/72. - (3*b1*bPi2P)&
  &/8. + (3*bdeta*bPi2P)/8. - (3*b1**2*fz)/4. + (3*b1*bdeta*fz)/2. - (3*bdeta*&
  &*2*fz)/4. + (3*b1*beta*fz)/4. + (3*b1*beta2*fz)/4.

  dAnmpdf(0,1,5) = (-3*b1*b2)/2. + (3*b2*bdeta)/2. - 2*b1*bK2 + 2*bdeta*bK2 - &
  &(5*b1*bK2P)/6. + (5*bdeta*bK2P)/6. - (3*b1*bPi2P)/2. + (3*bdeta*bPi2P)/2. -&
  & (3*b1**2*fz)/2. + 3*b1*bdeta*fz - (3*bdeta**2*fz)/2. - (3*b2*beta*fz)/2. -&
  & (3*b2*beta2*fz)/2. - 2*beta*bK2*fz - 2*beta2*bK2*fz - (3*beta*bK2P*fz)/4. &
  &- (3*beta2*bK2P*fz)/4. - (5*beta*bPi2P*fz)/4. - (5*beta2*bPi2P*fz)/4. - (15&
  &*b1*beta*fz**2)/8. + (15*bdeta*beta*fz**2)/8. - (15*b1*beta2*fz**2)/8. + (1&
  &5*bdeta*beta2*fz**2)/8.

  dAnmpdf(0,2,2) = (-27*b1**2)/28. - (b1*b2)/4. + (27*b1*bdeta)/28. + (b2*bdet&
  &a)/4. + (2*b1*bK2)/3. - (2*bdeta*bK2)/3. + (7*b1*bK2P)/36. - (7*bdeta*bK2P)&
  &/36. - (3*b1**2*fz)/8. + (3*b1*bdeta*fz)/4. - (3*bdeta**2*fz)/8.

  dAnmpdf(0,2,4) = (-13*b1**2)/14. + (b1*b2)/4. + (13*b1*bdeta)/14. - (b2*bdet&
  &a)/4. + (7*b1*bK2)/3. - (7*bdeta*bK2)/3. + (5*b1*bK2P)/9. - (5*bdeta*bK2P)/&
  &9. - (3*b1**2*fz)/4. + (3*b1*bdeta*fz)/2. - (3*bdeta**2*fz)/4. - (12*b1*bet&
  &a*fz)/7. - (3*b2*beta*fz)/2. - (12*b1*beta2*fz)/7. - (3*b2*beta2*fz)/2. + (&
  &5*beta*bK2*fz)/2. + (5*beta2*bK2*fz)/2. + (11*beta*bK2P*fz)/12. + (11*beta2&
  &*bK2P*fz)/12. - (45*b1*beta*fz**2)/16. + (45*bdeta*beta*fz**2)/16. - (45*b1&
  &*beta2*fz**2)/16. + (45*bdeta*beta2*fz**2)/16.

  dAnmpdf(0,2,6) = -(b1*b2)/2. + (b2*bdeta)/2. - (2*b1*bK2)/3. + (2*bdeta*bK2)&
  &/3. - (11*b1*bK2P)/18. + (11*bdeta*bK2P)/18. - (3*b1*bPi2P)/2. + (3*bdeta*b&
  &Pi2P)/2. - (3*b1**2*fz)/2. + 3*b1*bdeta*fz - (3*bdeta**2*fz)/2. - (3*b2*bet&
  &a*fz)/2. - (3*b2*beta2*fz)/2. - 2*beta*bK2*fz - 2*beta2*bK2*fz - (19*beta*b&
  &K2P*fz)/12. - (19*beta2*bK2P*fz)/12. - (15*beta*bPi2P*fz)/4. - (15*beta2*bP&
  &i2P*fz)/4. - (45*b1*beta*fz**2)/8. + (45*bdeta*beta*fz**2)/8. - (45*b1*beta&
  &2*fz**2)/8. + (45*bdeta*beta2*fz**2)/8. - (35*beta**2*fz**3)/8. - (35*beta*&
  &beta2*fz**3)/4. - (35*beta2**2*fz**3)/8.

  dAnmpdf(0,3,1) = -b1**2/4. + (b1*bdeta)/4.

  dAnmpdf(0,3,3) = (3*b1**2)/7. + b1*b2 - (3*b1*bdeta)/7. - b2*bdeta - (2*b1*b&
  &K2)/3. + (2*bdeta*bK2)/3. - (b1*bK2P)/36. + (bdeta*bK2P)/36. + (3*b1*bPi2P)&
  &/4. - (3*bdeta*bPi2P)/4. + (3*b1**2*fz)/2. - 3*b1*bdeta*fz + (3*bdeta**2*fz&
  &)/2. - (3*b1*beta*fz)/2. - (3*b1*beta2*fz)/2.

  dAnmpdf(0,3,5) = (3*b1*b2)/2. - (3*b2*bdeta)/2. + 2*b1*bK2 - 2*bdeta*bK2 + (&
  &4*b1*bK2P)/3. - (4*bdeta*bK2P)/3. + 3*b1*bPi2P - 3*bdeta*bPi2P + 3*b1**2*fz&
  & - 6*b1*bdeta*fz + 3*bdeta**2*fz + 3*b2*beta*fz + 3*b2*beta2*fz + 4*beta*bK&
  &2*fz + 4*beta2*bK2*fz + (23*beta*bK2P*fz)/12. + (23*beta2*bK2P*fz)/12. + (1&
  &5*beta*bPi2P*fz)/4. + (15*beta2*bPi2P*fz)/4. + (45*b1*beta*fz**2)/8. - (45*&
  &bdeta*beta*fz**2)/8. + (45*b1*beta2*fz**2)/8. - (45*bdeta*beta2*fz**2)/8.

  dAnmpdf(0,4,2) = (6*b1**2)/7. - (6*b1*bdeta)/7. - (b1*bK2)/2. + (bdeta*bK2)/&
  &2. - (b1*bK2P)/8. + (bdeta*bK2P)/8. + (3*b1**2*fz)/16. - (3*b1*bdeta*fz)/8.&
  & + (3*bdeta**2*fz)/16.

  dAnmpdf(0,4,4) = (5*b1**2)/7. - b1*b2 - (5*b1*bdeta)/7. + b2*bdeta - (7*b1*b&
  &K2)/3. + (7*bdeta*bK2)/3. - (61*b1*bK2P)/72. + (61*bdeta*bK2P)/72. - (9*b1*&
  &bPi2P)/8. + (9*bdeta*bPi2P)/8. - (3*b1**2*fz)/4. + (3*b1*bdeta*fz)/2. - (3*&
  &bdeta**2*fz)/4. + (69*b1*beta*fz)/28. + (3*b2*beta*fz)/4. + (69*b1*beta2*fz&
  &)/28. + (3*b2*beta2*fz)/4. - (7*beta*bK2*fz)/2. - (7*beta2*bK2*fz)/2. - (13&
  &*beta*bK2P*fz)/12. - (13*beta2*bK2P*fz)/12. + (45*b1*beta*fz**2)/16. - (45*&
  &bdeta*beta*fz**2)/16. + (45*b1*beta2*fz**2)/16. - (45*bdeta*beta2*fz**2)/16&
  &.

  dAnmpdf(0,4,6) = (b1*bK2P)/4. - (bdeta*bK2P)/4. + (3*b1*bPi2P)/4. - (3*bdeta&
  &*bPi2P)/4. + (3*b1**2*fz)/4. - (3*b1*bdeta*fz)/2. + (3*bdeta**2*fz)/4. + (3&
  &*b2*beta*fz)/4. + (3*b2*beta2*fz)/4. + beta*bK2*fz + beta2*bK2*fz + (17*bet&
  &a*bK2P*fz)/12. + (17*beta2*bK2P*fz)/12. + (15*beta*bPi2P*fz)/4. + (15*beta2&
  &*bPi2P*fz)/4. + (45*b1*beta*fz**2)/8. - (45*bdeta*beta*fz**2)/8. + (45*b1*b&
  &eta2*fz**2)/8. - (45*bdeta*beta2*fz**2)/8. + (105*beta**2*fz**3)/16. + (105&
  &*beta*beta2*fz**3)/8. + (105*beta2**2*fz**3)/16.

  dAnmpdf(0,5,3) = (-5*b1**2)/7. + (5*b1*bdeta)/7. + b1*bK2 - bdeta*bK2 + (b1*&
  &bK2P)/8. - (bdeta*bK2P)/8. - (3*b1*bPi2P)/8. + (3*bdeta*bPi2P)/8. - (3*b1**&
  &2*fz)/4. + (3*b1*bdeta*fz)/2. - (3*bdeta**2*fz)/4. + (3*b1*beta*fz)/4. + (3&
  &*b1*beta2*fz)/4.

  dAnmpdf(0,5,5) = -(b1*bK2P)/2. + (bdeta*bK2P)/2. - (3*b1*bPi2P)/2. + (3*bdet&
  &a*bPi2P)/2. - (3*b1**2*fz)/2. + 3*b1*bdeta*fz - (3*bdeta**2*fz)/2. - (3*b2*&
  &beta*fz)/2. - (3*b2*beta2*fz)/2. - 2*beta*bK2*fz - 2*beta2*bK2*fz - (19*bet&
  &a*bK2P*fz)/12. - (19*beta2*bK2P*fz)/12. - (15*beta*bPi2P*fz)/4. - (15*beta2&
  &*bPi2P*fz)/4. - (45*b1*beta*fz**2)/8. + (45*bdeta*beta*fz**2)/8. - (45*b1*b&
  &eta2*fz**2)/8. + (45*bdeta*beta2*fz**2)/8.

  dAnmpdf(0,6,4) = (b1*bK2P)/4. - (bdeta*bK2P)/4. + (3*b1*bPi2P)/4. - (3*bdeta&
  &*bPi2P)/4. + (3*b1**2*fz)/4. - (3*b1*bdeta*fz)/2. + (3*bdeta**2*fz)/4. - (1&
  &5*b1*beta*fz)/14. - (15*b1*beta2*fz)/14. + (3*beta*bK2*fz)/2. + (3*beta2*bK&
  &2*fz)/2. + (5*beta*bK2P*fz)/12. + (5*beta2*bK2P*fz)/12. - (15*b1*beta*fz**2&
  &)/16. + (15*bdeta*beta*fz**2)/16. - (15*b1*beta2*fz**2)/16. + (15*bdeta*bet&
  &a2*fz**2)/16.

  dAnmpdf(0,6,6) = (-5*beta*bK2P*fz)/12. - (5*beta2*bK2P*fz)/12. - (5*beta*bPi&
  &2P*fz)/4. - (5*beta2*bPi2P*fz)/4. - (15*b1*beta*fz**2)/8. + (15*bdeta*beta*&
  &fz**2)/8. - (15*b1*beta2*fz**2)/8. + (15*bdeta*beta2*fz**2)/8. - (35*beta**&
  &2*fz**3)/8. - (35*beta*beta2*fz**3)/4. - (35*beta2**2*fz**3)/8.

  dAnmpdf(0,7,5) = (5*beta*bK2P*fz)/12. + (5*beta2*bK2P*fz)/12. + (5*beta*bPi2&
  &P*fz)/4. + (5*beta2*bPi2P*fz)/4. + (15*b1*beta*fz**2)/8. - (15*bdeta*beta*f&
  &z**2)/8. + (15*b1*beta2*fz**2)/8. - (15*bdeta*beta2*fz**2)/8.

  dAnmpdf(0,8,6) = (35*beta**2*fz**3)/32. + (35*beta*beta2*fz**3)/16. + (35*be&
  &ta2**2*fz**3)/32.

  dAnmpdf(1,0,0) = (b1**2*fz)/4.

  dAnmpdf(1,0,2) = (-3*b1**2)/28. - (b1*b2)/4. - (3*b1*bdeta)/28. - (b2*bdeta)&
  &/4. + (3*b1*beta)/98. + (b2*beta)/14. + (b1*bK2)/6. + (bdeta*bK2)/6. - (bet&
  &a*bK2)/21. + (7*b1*bK2P)/36. + (bdeta*bK2P)/36. - (beta*bK2P)/126. + (6*b1*&
  &bPi2P)/7. - (5*bdeta*bPi2P)/14. + (5*b1**2*fz)/8. - (3*b1*bdeta*fz)/4. + (b&
  &deta**2*fz)/8. + (5*b1*beta*fz)/7. + (3*b2*beta*fz)/2. - (bdeta*beta*fz)/14&
  &. + (3*b1*beta2*fz)/7. + b2*beta2*fz - beta*bK2*fz - (2*beta2*bK2*fz)/3. - &
  &(5*beta*bK2P*fz)/12. - (5*beta2*bK2P*fz)/18. + (63*b1*beta*fz**2)/16. - (27&
  &*bdeta*beta*fz**2)/16. + (27*b1*beta2*fz**2)/8. - (9*bdeta*beta2*fz**2)/8.

  dAnmpdf(1,0,4) = (-3*b1**2)/14. - (3*b1*b2)/4. + (3*b1*bdeta)/14. + (b2*bdet&
  &a)/4. + (b2*beta)/14. - (2*bdeta*bK2)/3. + (2*beta*bK2)/21. + (b1*bK2P)/12.&
  & - (13*bdeta*bK2P)/36. + (5*beta*bK2P)/126. + (27*b1*bPi2P)/28. - (41*bdeta&
  &*bPi2P)/28. + (beta*bPi2P)/14. - (b1**2*fz)/2. + (bdeta**2*fz)/2. - (b1*bet&
  &a*fz)/2. - (bdeta*beta*fz)/7. - (9*b1*beta2*fz)/14. - (b2*beta2*fz)/2. + 3*&
  &beta*bK2*fz + (7*beta2*bK2*fz)/3. + (5*beta*bK2P*fz)/2. + (77*beta2*bK2P*fz&
  &)/36. + (177*beta*bPi2P*fz)/28. + (39*beta2*bPi2P*fz)/7. + (81*b1*beta*fz**&
  &2)/16. - (99*bdeta*beta*fz**2)/16. + (9*beta**2*fz**2)/56. + (63*b1*beta2*f&
  &z**2)/16. - (81*bdeta*beta2*fz**2)/16. + (9*beta*beta2*fz**2)/56. + (35*bet&
  &a**2*fz**3)/4. + (65*beta*beta2*fz**3)/4. + (15*beta2**2*fz**3)/2.

  dAnmpdf(1,0,6) = -(b1*b2)/2. + (b2*bdeta)/2. - (2*b1*bK2)/3. + (2*bdeta*bK2)&
  &/3. - (11*b1*bK2P)/18. + (11*bdeta*bK2P)/18. - (3*b1*bPi2P)/2. + (3*bdeta*b&
  &Pi2P)/2. - (3*b1**2*fz)/2. + 3*b1*bdeta*fz - (3*bdeta**2*fz)/2. - (3*b2*bet&
  &a*fz)/2. - (3*b2*beta2*fz)/2. - 2*beta*bK2*fz - 2*beta2*bK2*fz - (19*beta*b&
  &K2P*fz)/12. - (19*beta2*bK2P*fz)/12. - (15*beta*bPi2P*fz)/4. - (15*beta2*bP&
  &i2P*fz)/4. - (45*b1*beta*fz**2)/8. + (45*bdeta*beta*fz**2)/8. - (45*b1*beta&
  &2*fz**2)/8. + (45*bdeta*beta2*fz**2)/8. - (35*beta**2*fz**3)/8. - (35*beta*&
  &beta2*fz**3)/4. - (35*beta2**2*fz**3)/8.

  dAnmpdf(1,1,1) = (-13*b1**2)/28. - (b1*b2)/2. - (b1*bdeta)/4. - (b1*beta)/7.&
  & - (b2*beta)/2. + (b1*bK2)/3. + (beta*bK2)/3. - (b1*bK2P)/9. + (beta*bK2P)/&
  &18. - (b1*bPi2P)/2. - (5*b1**2*fz)/2. + (3*b1*bdeta*fz)/2. + b1*beta*fz + (&
  &bdeta*beta*fz)/2. + b1*beta2*fz

  dAnmpdf(1,1,3) = (-5*b1**2)/7. - (b1*b2)/2. + (5*b1*bdeta)/7. + b2*bdeta - (&
  &9*b2*beta)/14. + (b1*bK2)/3. + (bdeta*bK2)/3. - (6*beta*bK2)/7. - (49*b1*bK&
  &2P)/36. + (29*bdeta*bK2P)/36. - (beta*bK2P)/3. - (139*b1*bPi2P)/28. + (83*b&
  &deta*bPi2P)/28. - (4*beta*bPi2P)/7. - (13*b1**2*fz)/2. + 9*b1*bdeta*fz - (5&
  &*bdeta**2*fz)/2. - (73*b1*beta*fz)/14. - 9*b2*beta*fz + (8*bdeta*beta*fz)/7&
  &. - (57*b1*beta2*fz)/14. - 8*b2*beta2*fz + (4*beta2*bK2*fz)/3. - (3*beta*bK&
  &2P*fz)/4. - (5*beta2*bK2P*fz)/18. - (21*beta*bPi2P*fz)/4. - (9*beta2*bPi2P*&
  &fz)/2. - (99*b1*beta*fz**2)/4. + (117*bdeta*beta*fz**2)/8. - (9*beta**2*fz*&
  &*2)/8. - (189*b1*beta2*fz**2)/8. + (27*bdeta*beta2*fz**2)/2. - (9*beta*beta&
  &2*fz**2)/8.

  dAnmpdf(1,1,5) = (b1*b2)/2. - (b2*bdeta)/2. + (2*b1*bK2)/3. - (2*bdeta*bK2)/&
  &3. - (8*b1*bK2P)/9. + (8*bdeta*bK2P)/9. - 3*b1*bPi2P + 3*bdeta*bPi2P - 3*b1&
  &**2*fz + 6*b1*bdeta*fz - 3*bdeta**2*fz - 3*b2*beta*fz - 3*b2*beta2*fz - 4*b&
  &eta*bK2*fz - 4*beta2*bK2*fz - (83*beta*bK2P*fz)/12. - (83*beta2*bK2P*fz)/12&
  &. - (75*beta*bPi2P*fz)/4. - (75*beta2*bPi2P*fz)/4. - (225*b1*beta*fz**2)/8.&
  & + (225*bdeta*beta*fz**2)/8. - (225*b1*beta2*fz**2)/8. + (225*bdeta*beta2*f&
  &z**2)/8. - 35*beta**2*fz**3 - 70*beta*beta2*fz**3 - 35*beta2**2*fz**3

  dAnmpdf(1,2,0) = -b1**2/2. - (b1*beta)/2. - (b1**2*fz)/4.

  dAnmpdf(1,2,2) = (17*b1**2)/28. + (11*b1*b2)/4. + (15*b1*bdeta)/28. - (3*b2*&
  &bdeta)/4. + (4*b1*beta)/49. + (10*b2*beta)/7. - (b1*bK2)/3. + (25*beta*bK2)&
  &/21. + (10*b1*bK2P)/9. - (bdeta*bK2P)/2. + (23*beta*bK2P)/63. + (39*b1*bPi2&
  &P)/14. - (11*bdeta*bPi2P)/14. + (beta*bPi2P)/2. + (33*b1**2*fz)/4. - 9*b1*b&
  &deta*fz + (7*bdeta**2*fz)/4. - (52*b1*beta*fz)/7. - (3*b2*beta*fz)/2. - (19&
  &*bdeta*beta*fz)/14. - (55*b1*beta2*fz)/7. - b2*beta2*fz + 4*beta*bK2*fz + (&
  &8*beta2*bK2*fz)/3. + (7*beta*bK2P*fz)/6. + (7*beta2*bK2P*fz)/9. - (63*b1*be&
  &ta*fz**2)/8. + (27*bdeta*beta*fz**2)/8. - (27*b1*beta2*fz**2)/4. + (9*bdeta&
  &*beta2*fz**2)/4.

  dAnmpdf(1,2,4) = (19*b1**2)/14. + (17*b1*b2)/4. - (19*b1*bdeta)/14. - (13*b2&
  &*bdeta)/4. + (3*b2*beta)/7. + (5*b1*bK2)/3. - (bdeta*bK2)/3. + (4*beta*bK2)&
  &/7. + (89*b1*bK2P)/18. - (32*bdeta*bK2P)/9. + (3*beta*bK2P)/14. + (88*b1*bP&
  &i2P)/7. - (127*bdeta*bPi2P)/14. + (5*beta*bPi2P)/14. + (37*b1**2*fz)/2. - 3&
  &0*b1*bdeta*fz + (23*bdeta**2*fz)/2. + (47*b1*beta*fz)/7. + (39*b2*beta*fz)/&
  &2. - (5*bdeta*beta*fz)/7. + 6*b1*beta2*fz + 20*b2*beta2*fz + 5*beta*bK2*fz &
  &+ (17*beta2*bK2*fz)/3. + (37*beta*bK2P*fz)/12. + (133*beta2*bK2P*fz)/36. + &
  &(123*beta*bPi2P*fz)/14. + (72*beta2*bPi2P*fz)/7. + (693*b1*beta*fz**2)/16. &
  &- (477*bdeta*beta*fz**2)/16. + (9*beta**2*fz**2)/14. + (729*b1*beta2*fz**2)&
  &/16. - (513*bdeta*beta2*fz**2)/16. + (9*beta*beta2*fz**2)/14. - (105*beta**&
  &2*fz**3)/4. - (195*beta*beta2*fz**3)/4. - (45*beta2**2*fz**3)/2.

  dAnmpdf(1,2,6) = (3*b1*b2)/2. - (3*b2*bdeta)/2. + 2*b1*bK2 - 2*bdeta*bK2 + (&
  &10*b1*bK2P)/3. - (10*bdeta*bK2P)/3. + 9*b1*bPi2P - 9*bdeta*bPi2P + 9*b1**2*&
  &fz - 18*b1*bdeta*fz + 9*bdeta**2*fz + 9*b2*beta*fz + 9*b2*beta2*fz + 12*bet&
  &a*bK2*fz + 12*beta2*bK2*fz + (53*beta*bK2P*fz)/4. + (53*beta2*bK2P*fz)/4. +&
  & (135*beta*bPi2P*fz)/4. + (135*beta2*bPi2P*fz)/4. + (405*b1*beta*fz**2)/8. &
  &- (405*bdeta*beta*fz**2)/8. + (405*b1*beta2*fz**2)/8. - (405*bdeta*beta2*fz&
  &**2)/8. + (105*beta**2*fz**3)/2. + 105*beta*beta2*fz**3 + (105*beta2**2*fz*&
  &*3)/2.

  dAnmpdf(1,3,1) = (69*b1**2)/28. - (3*b1*bdeta)/4. + (8*b1*beta)/7. - b1*bK2 &
  &- beta*bK2 - (beta*bK2P)/6. + (b1*bPi2P)/2. + (5*b1**2*fz)/2. - (3*b1*bdeta&
  &*fz)/2. - b1*beta*fz - (bdeta*beta*fz)/2. - b1*beta2*fz

  dAnmpdf(1,3,3) = (11*b1**2)/7. - 5*b1*b2 - (11*b1*bdeta)/7. + 3*b2*bdeta - (&
  &6*b2*beta)/7. - (14*b1*bK2)/3. + 2*bdeta*bK2 - (8*beta*bK2)/7. - (47*b1*bK2&
  &P)/18. + (11*bdeta*bK2P)/6. - (beta*bK2P)/7. - (57*b1*bPi2P)/14. + (43*bdet&
  &a*bPi2P)/14. + (beta*bPi2P)/7. - 7*b1**2*fz + 12*b1*bdeta*fz - 5*bdeta**2*f&
  &z + (143*b1*beta*fz)/7. + 9*b2*beta*fz - (2*bdeta*beta*fz)/7. + (141*b1*bet&
  &a2*fz)/7. + 8*b2*beta2*fz - 12*beta*bK2*fz - (40*beta2*bK2*fz)/3. - (beta*b&
  &K2P*fz)/2. - (11*beta2*bK2P*fz)/9. + (21*beta*bPi2P*fz)/2. + 9*beta2*bPi2P*&
  &fz + (99*b1*beta*fz**2)/2. - (117*bdeta*beta*fz**2)/4. + (9*beta**2*fz**2)/&
  &4. + (189*b1*beta2*fz**2)/4. - 27*bdeta*beta2*fz**2 + (9*beta*beta2*fz**2)/&
  &4.

  dAnmpdf(1,3,5) = (-9*b1*b2)/2. + (9*b2*bdeta)/2. - 6*b1*bK2 + 6*bdeta*bK2 - &
  &5*b1*bK2P + 5*bdeta*bK2P - 12*b1*bPi2P + 12*bdeta*bPi2P - 12*b1**2*fz + 24*&
  &b1*bdeta*fz - 12*bdeta**2*fz - 12*b2*beta*fz - 12*b2*beta2*fz - 16*beta*bK2&
  &*fz - 16*beta2*bK2*fz + (13*beta*bK2P*fz)/12. + (13*beta2*bK2P*fz)/12. + (4&
  &5*beta*bPi2P*fz)/4. + (45*beta2*bPi2P*fz)/4. + (135*b1*beta*fz**2)/8. - (13&
  &5*bdeta*beta*fz**2)/8. + (135*b1*beta2*fz**2)/8. - (135*bdeta*beta2*fz**2)/&
  &8. + 105*beta**2*fz**3 + 210*beta*beta2*fz**3 + 105*beta2**2*fz**3

  dAnmpdf(1,4,2) = -4*b1**2 + (18*b1*bdeta)/7. - (30*b1*beta)/49. + (7*b1*bK2)&
  &/2. - (3*bdeta*bK2)/2. + (6*beta*bK2)/7. - (3*b1*bK2P)/4. + (bdeta*bK2P)/4.&
  & - (beta*bK2P)/42. - (51*b1*bPi2P)/14. + (8*bdeta*bPi2P)/7. - (beta*bPi2P)/&
  &2. - (71*b1**2*fz)/8. + (39*b1*bdeta*fz)/4. - (15*bdeta**2*fz)/8. + (47*b1*&
  &beta*fz)/7. + (10*bdeta*beta*fz)/7. + (52*b1*beta2*fz)/7. - 3*beta*bK2*fz -&
  & 2*beta2*bK2*fz - (3*beta*bK2P*fz)/4. - (beta2*bK2P*fz)/2. + (63*b1*beta*fz&
  &**2)/16. - (27*bdeta*beta*fz**2)/16. + (27*b1*beta2*fz**2)/8. - (9*bdeta*be&
  &ta2*fz**2)/8.

  dAnmpdf(1,4,4) = (-15*b1**2)/7. + 3*b1*b2 + (15*b1*bdeta)/7. - 3*b2*bdeta + &
  &7*b1*bK2 - 7*bdeta*bK2 - (13*b1*bK2P)/12. + (bdeta*bK2P)/12. - (beta*bK2P)/&
  &7. - (169*b1*bPi2P)/28. + (85*bdeta*bPi2P)/28. - (3*beta*bPi2P)/7. - (21*b1&
  &**2*fz)/2. + 15*b1*bdeta*fz - (9*bdeta**2*fz)/2. - (237*b1*beta*fz)/14. - (&
  &39*b2*beta*fz)/2. + (6*bdeta*beta*fz)/7. - (225*b1*beta2*fz)/14. - (39*b2*b&
  &eta2*fz)/2. + 7*beta*bK2*fz + 7*beta2*bK2*fz - (28*beta*bK2P*fz)/3. - (115*&
  &beta2*bK2P*fz)/12. - (1023*beta*bPi2P*fz)/28. - (261*beta2*bPi2P*fz)/7. - (&
  &1629*b1*beta*fz**2)/16. + (1251*bdeta*beta*fz**2)/16. - (99*beta**2*fz**2)/&
  &56. - (1647*b1*beta2*fz**2)/16. + (1269*bdeta*beta2*fz**2)/16. - (99*beta*b&
  &eta2*fz**2)/56. + (105*beta**2*fz**3)/4. + (195*beta*beta2*fz**3)/4. + (45*&
  &beta2**2*fz**3)/2.

  dAnmpdf(1,4,6) = (-5*b1*bK2P)/2. + (5*bdeta*bK2P)/2. - (15*b1*bPi2P)/2. + (1&
  &5*bdeta*bPi2P)/2. - (15*b1**2*fz)/2. + 15*b1*bdeta*fz - (15*bdeta**2*fz)/2.&
  & - (15*b2*beta*fz)/2. - (15*b2*beta2*fz)/2. - 10*beta*bK2*fz - 10*beta2*bK2&
  &*fz - (245*beta*bK2P*fz)/12. - (245*beta2*bK2P*fz)/12. - (225*beta*bPi2P*fz&
  &)/4. - (225*beta2*bPi2P*fz)/4. - (675*b1*beta*fz**2)/8. + (675*bdeta*beta*f&
  &z**2)/8. - (675*b1*beta2*fz**2)/8. + (675*bdeta*beta2*fz**2)/8. - (525*beta&
  &**2*fz**3)/4. - (525*beta*beta2*fz**3)/2. - (525*beta2**2*fz**3)/4.

  dAnmpdf(1,5,3) = (15*b1**2)/7. - (15*b1*bdeta)/7. - 3*b1*bK2 + 3*bdeta*bK2 +&
  & (11*b1*bK2P)/4. - (7*bdeta*bK2P)/4. + (beta*bK2P)/7. + (253*b1*bPi2P)/28. &
  &- (169*bdeta*bPi2P)/28. + (3*beta*bPi2P)/7. + (27*b1**2*fz)/2. - 21*b1*bdet&
  &a*fz + (15*bdeta**2*fz)/2. - (213*b1*beta*fz)/14. - (6*bdeta*beta*fz)/7. - &
  &(225*b1*beta2*fz)/14. + 12*beta*bK2*fz + 12*beta2*bK2*fz + (5*beta*bK2P*fz)&
  &/4. + (3*beta2*bK2P*fz)/2. - (21*beta*bPi2P*fz)/4. - (9*beta2*bPi2P*fz)/2. &
  &- (99*b1*beta*fz**2)/4. + (117*bdeta*beta*fz**2)/8. - (9*beta**2*fz**2)/8. &
  &- (189*b1*beta2*fz**2)/8. + (27*bdeta*beta2*fz**2)/2. - (9*beta*beta2*fz**2&
  &)/8.

  dAnmpdf(1,5,5) = 5*b1*bK2P - 5*bdeta*bK2P + 15*b1*bPi2P - 15*bdeta*bPi2P + 1&
  &5*b1**2*fz - 30*b1*bdeta*fz + 15*bdeta**2*fz + 15*b2*beta*fz + 15*b2*beta2*&
  &fz + 20*beta*bK2*fz + 20*beta2*bK2*fz + (175*beta*bK2P*fz)/12. + (175*beta2&
  &*bK2P*fz)/12. + (135*beta*bPi2P*fz)/4. + (135*beta2*bPi2P*fz)/4. + (405*b1*&
  &beta*fz**2)/8. - (405*bdeta*beta*fz**2)/8. + (405*b1*beta2*fz**2)/8. - (405&
  &*bdeta*beta2*fz**2)/8. - 105*beta**2*fz**3 - 210*beta*beta2*fz**3 - 105*bet&
  &a2**2*fz**3

  dAnmpdf(1,6,4) = (-5*b1*bK2P)/2. + (5*bdeta*bK2P)/2. - (15*b1*bPi2P)/2. + (1&
  &5*bdeta*bPi2P)/2. - (15*b1**2*fz)/2. + 15*b1*bdeta*fz - (15*bdeta**2*fz)/2.&
  & + (75*b1*beta*fz)/7. + (75*b1*beta2*fz)/7. - 15*beta*bK2*fz - 15*beta2*bK2&
  &*fz + (15*beta*bK2P*fz)/4. + (15*beta2*bK2P*fz)/4. + (150*beta*bPi2P*fz)/7.&
  & + (150*beta2*bPi2P*fz)/7. + (855*b1*beta*fz**2)/16. - (675*bdeta*beta*fz**&
  &2)/16. + (27*beta**2*fz**2)/28. + (855*b1*beta2*fz**2)/16. - (675*bdeta*bet&
  &a2*fz**2)/16. + (27*beta*beta2*fz**2)/28. - (35*beta**2*fz**3)/4. - (65*bet&
  &a*beta2*fz**3)/4. - (15*beta2**2*fz**3)/2.

  dAnmpdf(1,6,6) = (35*beta*bK2P*fz)/4. + (35*beta2*bK2P*fz)/4. + (105*beta*bP&
  &i2P*fz)/4. + (105*beta2*bPi2P*fz)/4. + (315*b1*beta*fz**2)/8. - (315*bdeta*&
  &beta*fz**2)/8. + (315*b1*beta2*fz**2)/8. - (315*bdeta*beta2*fz**2)/8. + (24&
  &5*beta**2*fz**3)/2. + 245*beta*beta2*fz**3 + (245*beta2**2*fz**3)/2.

  dAnmpdf(1,7,5) = (-35*beta*bK2P*fz)/4. - (35*beta2*bK2P*fz)/4. - (105*beta*b&
  &Pi2P*fz)/4. - (105*beta2*bPi2P*fz)/4. - (315*b1*beta*fz**2)/8. + (315*bdeta&
  &*beta*fz**2)/8. - (315*b1*beta2*fz**2)/8. + (315*bdeta*beta2*fz**2)/8. + 35&
  &*beta**2*fz**3 + 70*beta*beta2*fz**3 + 35*beta2**2*fz**3

  dAnmpdf(1,8,6) = (-315*beta**2*fz**3)/8. - (315*beta*beta2*fz**3)/4. - (315*&
  &beta2**2*fz**3)/8.

  dAnmpdf(2,0,0) = -(b1**2*fz)/4. + (3*b1*beta*fz**2)/4.

  dAnmpdf(2,0,2) = -(b1*bK2P)/8. + (bdeta*bK2P)/8. - (beta*bK2P)/42. - (6*b1*b&
  &Pi2P)/7. - (5*bdeta*bPi2P)/14. + (5*beta*bPi2P)/49. - (13*b1**2*fz)/16. + (&
  &9*b1*bdeta*fz)/8. + (3*bdeta**2*fz)/16. - (5*b1*beta*fz)/7. - (3*b2*beta*fz&
  &)/2. - (bdeta*beta*fz)/14. + (beta**2*fz)/98. - (3*b1*beta2*fz)/7. - b2*bet&
  &a2*fz + beta*bK2*fz + (2*beta2*bK2*fz)/3. + (beta*bK2P*fz)/2. + (beta2*bK2P&
  &*fz)/9. + (22*beta*bPi2P*fz)/7. + (10*beta2*bPi2P*fz)/7. - (51*b1*beta*fz**&
  &2)/8. - (3*bdeta*beta*fz**2)/8. + (9*beta**2*fz**2)/28. - (27*b1*beta2*fz**&
  &2)/4. + (3*bdeta*beta2*fz**2)/4. + (3*beta*beta2*fz**2)/14. + (51*beta**2*f&
  &z**3)/8. + (15*beta*beta2*fz**3)/2. + (3*beta2**2*fz**3)/2.

  dAnmpdf(2,0,4) = -(b1*bK2P)/8. + (7*bdeta*bK2P)/24. - (beta*bK2P)/42. - (75*&
  &b1*bPi2P)/56. + (103*bdeta*bPi2P)/56. - (beta*bPi2P)/14. - (b1**2*fz)/4. + &
  &(3*b1*bdeta*fz)/2. - (5*bdeta**2*fz)/4. + (5*b1*beta*fz)/28. - (3*b2*beta*f&
  &z)/4. + (bdeta*beta*fz)/7. + (9*b1*beta2*fz)/28. - (b2*beta2*fz)/4. - (5*be&
  &ta*bK2*fz)/2. - (11*beta2*bK2*fz)/6. - (47*beta*bK2P*fz)/12. - (119*beta2*b&
  &K2P*fz)/36. - (177*beta*bPi2P*fz)/14. - (78*beta2*bPi2P*fz)/7. - (207*b1*be&
  &ta*fz**2)/16. + (243*bdeta*beta*fz**2)/16. - (9*beta**2*fz**2)/28. - (171*b&
  &1*beta2*fz**2)/16. + (207*bdeta*beta2*fz**2)/16. - (9*beta*beta2*fz**2)/28.&
  & - (105*beta**2*fz**3)/4. - (195*beta*beta2*fz**3)/4. - (45*beta2**2*fz**3)&
  &/2.

  dAnmpdf(2,0,6) = (b1*bK2P)/4. - (bdeta*bK2P)/4. + (3*b1*bPi2P)/4. - (3*bdeta&
  &*bPi2P)/4. + (3*b1**2*fz)/4. - (3*b1*bdeta*fz)/2. + (3*bdeta**2*fz)/4. + (3&
  &*b2*beta*fz)/4. + (3*b2*beta2*fz)/4. + beta*bK2*fz + beta2*bK2*fz + (17*bet&
  &a*bK2P*fz)/12. + (17*beta2*bK2P*fz)/12. + (15*beta*bPi2P*fz)/4. + (15*beta2&
  &*bPi2P*fz)/4. + (45*b1*beta*fz**2)/8. - (45*bdeta*beta*fz**2)/8. + (45*b1*b&
  &eta2*fz**2)/8. - (45*bdeta*beta2*fz**2)/8. + (105*beta**2*fz**3)/16. + (105&
  &*beta*beta2*fz**3)/8. + (105*beta2**2*fz**3)/16.

  dAnmpdf(2,1,1) = (b1*bK2P)/3. + (beta*bK2P)/6. - (3*b1*bPi2P)/14. - (5*beta*&
  &bPi2P)/7. + (5*b1**2*fz)/2. - (b1*bdeta*fz)/2. - (11*b1*beta*fz)/7. - b2*be&
  &ta*fz + (bdeta*beta*fz)/2. - (beta**2*fz)/7. - b1*beta2*fz + (2*beta*bK2*fz&
  &)/3. - (2*beta*bK2P*fz)/9. - beta*bPi2P*fz - (21*b1*beta*fz**2)/2. + (9*bde&
  &ta*beta*fz**2)/4. - (9*beta**2*fz**2)/4. - (9*b1*beta2*fz**2)/2. - (3*beta*&
  &beta2*fz**2)/2.

  dAnmpdf(2,1,3) = (43*b1*bK2P)/24. - (19*bdeta*bK2P)/24. + (beta*bK2P)/7. + (&
  &219*b1*bPi2P)/56. - (51*bdeta*bPi2P)/56. + (3*beta*bPi2P)/7. + (29*b1**2*fz&
  &)/4. - (17*b1*bdeta*fz)/2. + (5*bdeta**2*fz)/4. + (117*b1*beta*fz)/28. + 8*&
  &b2*beta*fz - (6*bdeta*beta*fz)/7. + (93*b1*beta2*fz)/28. + 8*b2*beta2*fz - &
  &(4*beta*bK2*fz)/3. - (4*beta2*bK2*fz)/3. - (13*beta*bK2P*fz)/18. + (7*beta2&
  &*bK2P*fz)/9. - (141*beta*bPi2P*fz)/14. - (39*beta2*bPi2P*fz)/7. + (63*b1*be&
  &ta*fz**2)/2. - (9*bdeta*beta*fz**2)/4. + (27*beta**2*fz**2)/28. + (153*b1*b&
  &eta2*fz**2)/4. - 9*bdeta*beta2*fz**2 + (27*beta*beta2*fz**2)/28. - (105*bet&
  &a**2*fz**3)/2. - (165*beta*beta2*fz**3)/2. - 30*beta2**2*fz**3

  dAnmpdf(2,1,5) = (3*b1*bK2P)/2. - (3*bdeta*bK2P)/2. + (9*b1*bPi2P)/2. - (9*b&
  &deta*bPi2P)/2. + (9*b1**2*fz)/2. - 9*b1*bdeta*fz + (9*bdeta**2*fz)/2. + (9*&
  &b2*beta*fz)/2. + (9*b2*beta2*fz)/2. + 6*beta*bK2*fz + 6*beta2*bK2*fz + (59*&
  &beta*bK2P*fz)/4. + (59*beta2*bK2P*fz)/4. + (165*beta*bPi2P*fz)/4. + (165*be&
  &ta2*bPi2P*fz)/4. + (495*b1*beta*fz**2)/8. - (495*bdeta*beta*fz**2)/8. + (49&
  &5*b1*beta2*fz**2)/8. - (495*bdeta*beta2*fz**2)/8. + 105*beta**2*fz**3 + 210&
  &*beta*beta2*fz**3 + 105*beta2**2*fz**3

  dAnmpdf(2,2,0) = (3*b1**2*fz)/4. + (beta**2*fz)/2. - (3*b1*beta*fz**2)/4.

  dAnmpdf(2,2,2) = (-17*b1*bK2P)/12. + (bdeta*bK2P)/4. + (2*beta*bK2P)/21. + (&
  &5*b1*bPi2P)/14. - (3*bdeta*bPi2P)/14. + (113*beta*bPi2P)/98. - (47*b1**2*fz&
  &)/8. + (13*b1*bdeta*fz)/4. - (3*bdeta**2*fz)/8. + (78*b1*beta*fz)/7. + (13*&
  &b2*beta*fz)/2. - (23*bdeta*beta*fz)/14. + (6*beta**2*fz)/49. + (61*b1*beta2&
  &*fz)/7. + 3*b2*beta2*fz - (10*beta*bK2*fz)/3. - 4*beta2*bK2*fz + (28*beta*b&
  &K2P*fz)/9. + (4*beta2*bK2P*fz)/3. + (47*beta*bPi2P*fz)/7. + (22*beta2*bPi2P&
  &*fz)/7. + (255*b1*beta*fz**2)/4. - 21*bdeta*beta*fz**2 + (297*beta**2*fz**2&
  &)/28. + (99*b1*beta2*fz**2)/2. - 12*bdeta*beta2*fz**2 + (141*beta*beta2*fz*&
  &*2)/14. - (51*beta**2*fz**3)/4. - 15*beta*beta2*fz**3 - 3*beta2**2*fz**3

  dAnmpdf(2,2,4) = (-9*b1*bK2P)/2. + 3*bdeta*bK2P - (beta*bK2P)/14. - (64*b1*b&
  &Pi2P)/7. + (65*bdeta*bPi2P)/14. - (3*beta*bPi2P)/14. - (63*b1**2*fz)/4. + (&
  &45*b1*bdeta*fz)/2. - (27*bdeta**2*fz)/4. - (33*b1*beta*fz)/7. - 15*b2*beta*&
  &fz + (3*bdeta*beta*fz)/7. - (30*b1*beta2*fz)/7. - (33*b2*beta2*fz)/2. - (7*&
  &beta*bK2*fz)/2. - (11*beta2*bK2*fz)/2. + (65*beta*bK2P*fz)/12. + (25*beta2*&
  &bK2P*fz)/12. + 33*beta*bPi2P*fz + 24*beta2*bPi2P*fz - (243*b1*beta*fz**2)/1&
  &6. - (333*bdeta*beta*fz**2)/16. - (459*b1*beta2*fz**2)/16. - (117*bdeta*bet&
  &a2*fz**2)/16. + (945*beta**2*fz**3)/4. + (1755*beta*beta2*fz**3)/4. + (405*&
  &beta2**2*fz**3)/2.

  dAnmpdf(2,2,6) = (-5*b1*bK2P)/2. + (5*bdeta*bK2P)/2. - (15*b1*bPi2P)/2. + (1&
  &5*bdeta*bPi2P)/2. - (15*b1**2*fz)/2. + 15*b1*bdeta*fz - (15*bdeta**2*fz)/2.&
  & - (15*b2*beta*fz)/2. - (15*b2*beta2*fz)/2. - 10*beta*bK2*fz - 10*beta2*bK2&
  &*fz - (245*beta*bK2P*fz)/12. - (245*beta2*bK2P*fz)/12. - (225*beta*bPi2P*fz&
  &)/4. - (225*beta2*bPi2P*fz)/4. - (675*b1*beta*fz**2)/8. + (675*bdeta*beta*f&
  &z**2)/8. - (675*b1*beta2*fz**2)/8. + (675*bdeta*beta2*fz**2)/8. - (525*beta&
  &**2*fz**3)/4. - (525*beta*beta2*fz**3)/2. - (525*beta2**2*fz**3)/4.

  dAnmpdf(2,3,1) = (-2*b1*bK2P)/3. - (beta*bK2P)/2. - (11*b1*bPi2P)/14. - (2*b&
  &eta*bPi2P)/7. - (11*b1**2*fz)/2. + (5*b1*bdeta*fz)/2. + (11*b1*beta*fz)/7. &
  &+ (3*bdeta*beta*fz)/2. - (6*beta**2*fz)/7. + 3*b1*beta2*fz - 2*beta*bK2*fz &
  &+ beta*bPi2P*fz + (21*b1*beta*fz**2)/2. - (9*bdeta*beta*fz**2)/4. + (9*beta&
  &**2*fz**2)/4. + (9*b1*beta2*fz**2)/2. + (3*beta*beta2*fz**2)/2.

  dAnmpdf(2,3,3) = -(b1*bK2P)/4. - (bdeta*bK2P)/12. - (5*beta*bK2P)/7. - (75*b&
  &1*bPi2P)/28. + (47*bdeta*bPi2P)/28. - (15*beta*bPi2P)/7. - (5*b1**2*fz)/2. &
  &+ 3*b1*bdeta*fz - (bdeta**2*fz)/2. - (345*b1*beta*fz)/14. - 19*b2*beta*fz +&
  & (30*bdeta*beta*fz)/7. - (285*b1*beta2*fz)/14. - 16*b2*beta2*fz + (20*beta*&
  &bK2*fz)/3. + (32*beta2*bK2*fz)/3. - (143*beta*bK2P*fz)/9. - (128*beta2*bK2P&
  &*fz)/9. - (237*beta*bPi2P*fz)/7. - (216*beta2*bPi2P*fz)/7. - 189*b1*beta*fz&
  &**2 + (177*bdeta*beta*fz**2)/2. - (279*beta**2*fz**2)/14. - (369*b1*beta2*f&
  &z**2)/2. + 84*bdeta*beta2*fz**2 - (279*beta*beta2*fz**2)/14. + 105*beta**2*&
  &fz**3 + 165*beta*beta2*fz**3 + 60*beta2**2*fz**3

  dAnmpdf(2,3,5) = (5*b1*bK2P)/3. - (5*bdeta*bK2P)/3. + 5*b1*bPi2P - 5*bdeta*b&
  &Pi2P + 5*b1**2*fz - 10*b1*bdeta*fz + 5*bdeta**2*fz + 5*b2*beta*fz + 5*b2*be&
  &ta2*fz + (20*beta*bK2*fz)/3. + (20*beta2*bK2*fz)/3. - (1085*beta*bK2P*fz)/3&
  &6. - (1085*beta2*bK2P*fz)/36. - (375*beta*bPi2P*fz)/4. - (375*beta2*bPi2P*f&
  &z)/4. - (1125*b1*beta*fz**2)/8. + (1125*bdeta*beta*fz**2)/8. - (1125*b1*bet&
  &a2*fz**2)/8. + (1125*bdeta*beta2*fz**2)/8. - 525*beta**2*fz**3 - 1050*beta*&
  &beta2*fz**3 - 525*beta2**2*fz**3

  dAnmpdf(2,4,2) = (77*b1*bK2P)/24. - (25*bdeta*bK2P)/24. + (13*beta*bK2P)/14.&
  & + (11*b1*bPi2P)/2. - (10*bdeta*bPi2P)/7. + (171*beta*bPi2P)/98. + (243*b1*&
  &*2*fz)/16. - (115*b1*bdeta*fz)/8. + (35*bdeta**2*fz)/16. - (73*b1*beta*fz)/&
  &7. - (30*bdeta*beta*fz)/7. + (18*beta**2*fz)/49. - (100*b1*beta2*fz)/7. + 9&
  &*beta*bK2*fz + 6*beta2*bK2*fz - (5*beta*bK2P*fz)/2. - beta2*bK2P*fz - (69*b&
  &eta*bPi2P*fz)/7. - (32*beta2*bPi2P*fz)/7. - (459*b1*beta*fz**2)/8. + (171*b&
  &deta*beta*fz**2)/8. - (153*beta**2*fz**2)/14. - (171*b1*beta2*fz**2)/4. + (&
  &45*bdeta*beta2*fz**2)/4. - (72*beta*beta2*fz**2)/7. + (51*beta**2*fz**3)/8.&
  & + (15*beta*beta2*fz**3)/2. + (3*beta2**2*fz**3)/2.

  dAnmpdf(2,4,4) = (145*b1*bK2P)/24. - (35*bdeta*bK2P)/8. + (3*beta*bK2P)/7. +&
  & (825*b1*bPi2P)/56. - (545*bdeta*bPi2P)/56. + (9*beta*bPi2P)/7. + (85*b1**2&
  &*fz)/4. - (65*b1*bdeta*fz)/2. + (45*bdeta**2*fz)/4. + (477*b1*beta*fz)/28. &
  &+ (115*b2*beta*fz)/4. - (18*bdeta*beta*fz)/7. + (405*b1*beta2*fz)/28. + (11&
  &5*b2*beta2*fz)/4. + (35*beta*bK2*fz)/6. + (35*beta2*bK2*fz)/6. + (1205*beta&
  &*bK2P*fz)/36. + (1295*beta2*bK2P*fz)/36. + (1035*beta*bPi2P*fz)/14. + (570*&
  &beta2*bPi2P*fz)/7. + (3915*b1*beta*fz**2)/16. - (2535*bdeta*beta*fz**2)/16.&
  & + (279*beta**2*fz**2)/28. + (4095*b1*beta2*fz**2)/16. - (2715*bdeta*beta2*&
  &fz**2)/16. + (279*beta*beta2*fz**2)/28. - (1575*beta**2*fz**3)/4. - (2925*b&
  &eta*beta2*fz**3)/4. - (675*beta2**2*fz**3)/2.

  dAnmpdf(2,4,6) = (35*b1*bK2P)/12. - (35*bdeta*bK2P)/12. + (35*b1*bPi2P)/4. -&
  & (35*bdeta*bPi2P)/4. + (35*b1**2*fz)/4. - (35*b1*bdeta*fz)/2. + (35*bdeta**&
  &2*fz)/4. + (35*b2*beta*fz)/4. + (35*b2*beta2*fz)/4. + (35*beta*bK2*fz)/3. +&
  & (35*beta2*bK2*fz)/3. + (1645*beta*bK2P*fz)/36. + (1645*beta2*bK2P*fz)/36. &
  &+ (525*beta*bPi2P*fz)/4. + (525*beta2*bPi2P*fz)/4. + (1575*b1*beta*fz**2)/8&
  &. - (1575*bdeta*beta*fz**2)/8. + (1575*b1*beta2*fz**2)/8. - (1575*bdeta*bet&
  &a2*fz**2)/8. + (3675*beta**2*fz**3)/8. + (3675*beta*beta2*fz**3)/4. + (3675&
  &*beta2**2*fz**3)/8.

  dAnmpdf(2,5,3) = (-125*b1*bK2P)/24. + (85*bdeta*bK2P)/24. - (3*beta*bK2P)/7.&
  & - (685*b1*bPi2P)/56. + (405*bdeta*bPi2P)/56. - (9*beta*bPi2P)/7. - (75*b1*&
  &*2*fz)/4. + (55*b1*bdeta*fz)/2. - (35*bdeta**2*fz)/4. + (573*b1*beta*fz)/28&
  &. + (18*bdeta*beta*fz)/7. + (645*b1*beta2*fz)/28. - 20*beta*bK2*fz - 20*bet&
  &a2*bK2*fz + (85*beta*bK2P*fz)/6. + (35*beta2*bK2P*fz)/3. + (615*beta*bPi2P*&
  &fz)/14. + (255*beta2*bPi2P*fz)/7. + (315*b1*beta*fz**2)/2. - (345*bdeta*bet&
  &a*fz**2)/4. + (531*beta**2*fz**2)/28. + (585*b1*beta2*fz**2)/4. - 75*bdeta*&
  &beta2*fz**2 + (531*beta*beta2*fz**2)/28. - (105*beta**2*fz**3)/2. - (165*be&
  &ta*beta2*fz**3)/2. - 30*beta2**2*fz**3

  dAnmpdf(2,5,5) = (-35*b1*bK2P)/6. + (35*bdeta*bK2P)/6. - (35*b1*bPi2P)/2. + &
  &(35*bdeta*bPi2P)/2. - (35*b1**2*fz)/2. + 35*b1*bdeta*fz - (35*bdeta**2*fz)/&
  &2. - (35*b2*beta*fz)/2. - (35*b2*beta2*fz)/2. - (70*beta*bK2*fz)/3. - (70*b&
  &eta2*bK2*fz)/3. - (455*beta*bK2P*fz)/36. - (455*beta2*bK2P*fz)/36. - (105*b&
  &eta*bPi2P*fz)/4. - (105*beta2*bPi2P*fz)/4. - (315*b1*beta*fz**2)/8. + (315*&
  &bdeta*beta*fz**2)/8. - (315*b1*beta2*fz**2)/8. + (315*bdeta*beta2*fz**2)/8.&
  & + 735*beta**2*fz**3 + 1470*beta*beta2*fz**3 + 735*beta2**2*fz**3

  dAnmpdf(2,6,4) = (35*b1*bK2P)/12. - (35*bdeta*bK2P)/12. + (35*b1*bPi2P)/4. -&
  & (35*bdeta*bPi2P)/4. + (35*b1**2*fz)/4. - (35*b1*bdeta*fz)/2. + (35*bdeta**&
  &2*fz)/4. - (25*b1*beta*fz)/2. - (25*b1*beta2*fz)/2. + (35*beta*bK2*fz)/2. +&
  & (35*beta2*bK2*fz)/2. - (385*beta*bK2P*fz)/12. - (385*beta2*bK2P*fz)/12. - &
  &(660*beta*bPi2P*fz)/7. - (660*beta2*bPi2P*fz)/7. - (3465*b1*beta*fz**2)/16.&
  & + (2625*bdeta*beta*fz**2)/16. - (135*beta**2*fz**2)/14. - (3465*b1*beta2*f&
  &z**2)/16. + (2625*bdeta*beta2*fz**2)/16. - (135*beta*beta2*fz**2)/14. + (73&
  &5*beta**2*fz**3)/4. + (1365*beta*beta2*fz**3)/4. + (315*beta2**2*fz**3)/2.

  dAnmpdf(2,6,6) = (-105*beta*bK2P*fz)/4. - (105*beta2*bK2P*fz)/4. - (315*beta&
  &*bPi2P*fz)/4. - (315*beta2*bPi2P*fz)/4. - (945*b1*beta*fz**2)/8. + (945*bde&
  &ta*beta*fz**2)/8. - (945*b1*beta2*fz**2)/8. + (945*bdeta*beta2*fz**2)/8. - &
  &(2205*beta**2*fz**3)/4. - (2205*beta*beta2*fz**3)/2. - (2205*beta2**2*fz**3&
  &)/4.

  dAnmpdf(2,7,5) = (105*beta*bK2P*fz)/4. + (105*beta2*bK2P*fz)/4. + (315*beta*&
  &bPi2P*fz)/4. + (315*beta2*bPi2P*fz)/4. + (945*b1*beta*fz**2)/8. - (945*bdet&
  &a*beta*fz**2)/8. + (945*b1*beta2*fz**2)/8. - (945*bdeta*beta2*fz**2)/8. - 3&
  &15*beta**2*fz**3 - 630*beta*beta2*fz**3 - 315*beta2**2*fz**3

  dAnmpdf(2,8,6) = (3465*beta**2*fz**3)/16. + (3465*beta*beta2*fz**3)/8. + (34&
  &65*beta2**2*fz**3)/16.

  dAnmpdf(3,0,0) = (-3*b1*beta*fz**2)/4. + (beta**2*fz**3)/2.

  dAnmpdf(3,0,2) = -(beta*bK2P*fz)/12. + (beta2*bK2P*fz)/6. - (22*beta*bPi2P*f&
  &z)/7. - (10*beta2*bPi2P*fz)/7. + (39*b1*beta*fz**2)/16. + (33*bdeta*beta*fz&
  &**2)/16. - (9*beta**2*fz**2)/28. + (27*b1*beta2*fz**2)/8. + (3*bdeta*beta2*&
  &fz**2)/8. - (3*beta*beta2*fz**2)/14. - (51*beta**2*fz**3)/4. - 15*beta*beta&
  &2*fz**3 - 3*beta2**2*fz**3

  dAnmpdf(3,0,4) = (5*beta*bK2P*fz)/3. + (17*beta2*bK2P*fz)/12. + (177*beta*bP&
  &i2P*fz)/28. + (39*beta2*bPi2P*fz)/7. + (111*b1*beta*fz**2)/16. - (129*bdeta&
  &*beta*fz**2)/16. + (9*beta**2*fz**2)/56. + (93*b1*beta2*fz**2)/16. - (111*b&
  &deta*beta2*fz**2)/16. + (9*beta*beta2*fz**2)/56. + (105*beta**2*fz**3)/4. +&
  & (195*beta*beta2*fz**3)/4. + (45*beta2**2*fz**3)/2.

  dAnmpdf(3,0,6) = (-5*beta*bK2P*fz)/12. - (5*beta2*bK2P*fz)/12. - (5*beta*bPi&
  &2P*fz)/4. - (5*beta2*bPi2P*fz)/4. - (15*b1*beta*fz**2)/8. + (15*bdeta*beta*&
  &fz**2)/8. - (15*b1*beta2*fz**2)/8. + (15*bdeta*beta2*fz**2)/8. - (35*beta**&
  &2*fz**3)/8. - (35*beta*beta2*fz**3)/4. - (35*beta2**2*fz**3)/8.

  dAnmpdf(3,1,1) = (2*beta*bK2P*fz)/3. - (3*beta*bPi2P*fz)/7. + (21*b1*beta*fz&
  &**2)/2. - (3*bdeta*beta*fz**2)/4. + (57*beta**2*fz**2)/28. + (9*b1*beta2*fz&
  &**2)/2. + (3*beta*beta2*fz**2)/2. - 9*beta**2*fz**3 - 6*beta*beta2*fz**3

  dAnmpdf(3,1,3) = (5*beta*bK2P*fz)/4. - (beta2*bK2P*fz)/2. + (429*beta*bPi2P*&
  &fz)/28. + (141*beta2*bPi2P*fz)/14. - (27*b1*beta*fz**2)/4. - (99*bdeta*beta&
  &*fz**2)/8. + (9*beta**2*fz**2)/56. - (117*b1*beta2*fz**2)/8. - (9*bdeta*bet&
  &a2*fz**2)/2. + (9*beta*beta2*fz**2)/56. + 105*beta**2*fz**3 + 165*beta*beta&
  &2*fz**3 + 60*beta2**2*fz**3

  dAnmpdf(3,1,5) = (-85*beta*bK2P*fz)/12. - (85*beta2*bK2P*fz)/12. - (85*beta*&
  &bPi2P*fz)/4. - (85*beta2*bPi2P*fz)/4. - (255*b1*beta*fz**2)/8. + (255*bdeta&
  &*beta*fz**2)/8. - (255*b1*beta2*fz**2)/8. + (255*bdeta*beta2*fz**2)/8. - 10&
  &5*beta**2*fz**3 - 210*beta*beta2*fz**3 - 105*beta2**2*fz**3

  dAnmpdf(3,2,0) = (9*b1*beta*fz**2)/4. + (3*beta**2*fz**2)/2. - (beta**2*fz**&
  &3)/2.

  dAnmpdf(3,2,2) = (-9*beta*bK2P*fz)/2. - 3*beta2*bK2P*fz - (3*beta*bPi2P*fz)/&
  &7. - (2*beta2*bPi2P*fz)/7. - (423*b1*beta*fz**2)/8. + (81*bdeta*beta*fz**2)&
  &/8. - (279*beta**2*fz**2)/28. - (171*b1*beta2*fz**2)/4. + (27*bdeta*beta2*f&
  &z**2)/4. - (135*beta*beta2*fz**2)/14. + (153*beta**2*fz**3)/2. + 90*beta*be&
  &ta2*fz**3 + 18*beta2**2*fz**3

  dAnmpdf(3,2,4) = (-35*beta*bK2P*fz)/4. - (25*beta2*bK2P*fz)/4. - (585*beta*b&
  &Pi2P*fz)/14. - (240*beta2*bPi2P*fz)/7. - (405*b1*beta*fz**2)/16. + (765*bde&
  &ta*beta*fz**2)/16. - (9*beta**2*fz**2)/14. - (225*b1*beta2*fz**2)/16. + (58&
  &5*bdeta*beta2*fz**2)/16. - (9*beta*beta2*fz**2)/14. - (1575*beta**2*fz**3)/&
  &4. - (2925*beta*beta2*fz**3)/4. - (675*beta2**2*fz**3)/2.

  dAnmpdf(3,2,6) = (35*beta*bK2P*fz)/4. + (35*beta2*bK2P*fz)/4. + (105*beta*bP&
  &i2P*fz)/4. + (105*beta2*bPi2P*fz)/4. + (315*b1*beta*fz**2)/8. - (315*bdeta*&
  &beta*fz**2)/8. + (315*b1*beta2*fz**2)/8. - (315*bdeta*beta2*fz**2)/8. + (24&
  &5*beta**2*fz**3)/2. + 245*beta*beta2*fz**3 + (245*beta2**2*fz**3)/2.

  dAnmpdf(3,3,1) = (-4*beta*bK2P*fz)/3. - (11*beta*bPi2P*fz)/7. - (39*b1*beta*&
  &fz**2)/2. + (15*bdeta*beta*fz**2)/4. - (225*beta**2*fz**2)/28. - (15*b1*bet&
  &a2*fz**2)/2. - (9*beta*beta2*fz**2)/2. + 9*beta**2*fz**3 + 6*beta*beta2*fz*&
  &*3

  dAnmpdf(3,3,3) = (85*beta*bK2P*fz)/6. + 15*beta2*bK2P*fz + (135*beta*bPi2P*f&
  &z)/14. + (85*beta2*bPi2P*fz)/7. + (255*b1*beta*fz**2)/2. - (165*bdeta*beta*&
  &fz**2)/4. + (471*beta**2*fz**2)/28. + (525*b1*beta2*fz**2)/4. - 45*bdeta*be&
  &ta2*fz**2 + (471*beta*beta2*fz**2)/28. - 350*beta**2*fz**3 - 550*beta*beta2&
  &*fz**3 - 200*beta2**2*fz**3

  dAnmpdf(3,3,5) = (105*beta*bK2P*fz)/4. + (105*beta2*bK2P*fz)/4. + (315*beta*&
  &bPi2P*fz)/4. + (315*beta2*bPi2P*fz)/4. + (945*b1*beta*fz**2)/8. - (945*bdet&
  &a*beta*fz**2)/8. + (945*b1*beta2*fz**2)/8. - (945*bdeta*beta2*fz**2)/8. + 7&
  &35*beta**2*fz**3 + 1470*beta*beta2*fz**3 + 735*beta2**2*fz**3

  dAnmpdf(3,4,2) = (95*beta*bK2P*fz)/12. + (25*beta2*bK2P*fz)/6. + (95*beta*bP&
  &i2P*fz)/7. + (40*beta2*bPi2P*fz)/7. + (1215*b1*beta*fz**2)/16. - (435*bdeta&
  &*beta*fz**2)/16. + (291*beta**2*fz**2)/14. + (435*b1*beta2*fz**2)/8. - (105&
  &*bdeta*beta2*fz**2)/8. + (132*beta*beta2*fz**2)/7. - (255*beta**2*fz**3)/4.&
  & - 75*beta*beta2*fz**3 - 15*beta2**2*fz**3

  dAnmpdf(3,4,4) = (-35*beta*bK2P*fz)/2. - (245*beta2*bK2P*fz)/12. - (575*beta&
  &*bPi2P*fz)/28. - (205*beta2*bPi2P*fz)/7. - (1995*b1*beta*fz**2)/16. + (945*&
  &bdeta*beta*fz**2)/16. - (435*beta**2*fz**2)/56. - (2205*b1*beta2*fz**2)/16.&
  & + (1155*bdeta*beta2*fz**2)/16. - (435*beta*beta2*fz**2)/56. + (3675*beta**&
  &2*fz**3)/4. + (6825*beta*beta2*fz**3)/4. + (1575*beta2**2*fz**3)/2.

  dAnmpdf(3,4,6) = (-105*beta*bK2P*fz)/4. - (105*beta2*bK2P*fz)/4. - (315*beta&
  &*bPi2P*fz)/4. - (315*beta2*bPi2P*fz)/4. - (945*b1*beta*fz**2)/8. + (945*bde&
  &ta*beta*fz**2)/8. - (945*b1*beta2*fz**2)/8. + (945*bdeta*beta2*fz**2)/8. - &
  &(2205*beta**2*fz**3)/4. - (2205*beta*beta2*fz**3)/2. - (2205*beta2**2*fz**3&
  &)/4.

  dAnmpdf(3,5,3) = (-91*beta*bK2P*fz)/4. - (119*beta2*bK2P*fz)/6. - (1315*beta&
  &*bPi2P*fz)/28. - (535*beta2*bPi2P*fz)/14. - (651*b1*beta*fz**2)/4. + (693*b&
  &deta*beta*fz**2)/8. - (1455*beta**2*fz**2)/56. - (1197*b1*beta2*fz**2)/8. +&
  & (147*bdeta*beta2*fz**2)/2. - (1455*beta*beta2*fz**2)/56. + 245*beta**2*fz*&
  &*3 + 385*beta*beta2*fz**3 + 140*beta2**2*fz**3

  dAnmpdf(3,5,5) = (-21*beta*bK2P*fz)/4. - (21*beta2*bK2P*fz)/4. - (63*beta*bP&
  &i2P*fz)/4. - (63*beta2*bPi2P*fz)/4. - (189*b1*beta*fz**2)/8. + (189*bdeta*b&
  &eta*fz**2)/8. - (189*b1*beta2*fz**2)/8. + (189*bdeta*beta2*fz**2)/8. - 1323&
  &*beta**2*fz**3 - 2646*beta*beta2*fz**3 - 1323*beta2**2*fz**3

  dAnmpdf(3,6,4) = (133*beta*bK2P*fz)/4. + (133*beta2*bK2P*fz)/4. + 82*beta*bP&
  &i2P*fz + 82*beta2*bPi2P*fz + (2961*b1*beta*fz**2)/16. - (2205*bdeta*beta*fz&
  &**2)/16. + (45*beta**2*fz**2)/4. + (2961*b1*beta2*fz**2)/16. - (2205*bdeta*&
  &beta2*fz**2)/16. + (45*beta*beta2*fz**2)/4. - (2205*beta**2*fz**3)/4. - (40&
  &95*beta*beta2*fz**3)/4. - (945*beta2**2*fz**3)/2.

  dAnmpdf(3,6,6) = (77*beta*bK2P*fz)/4. + (77*beta2*bK2P*fz)/4. + (231*beta*bP&
  &i2P*fz)/4. + (231*beta2*bPi2P*fz)/4. + (693*b1*beta*fz**2)/8. - (693*bdeta*&
  &beta*fz**2)/8. + (693*b1*beta2*fz**2)/8. - (693*bdeta*beta2*fz**2)/8. + (16&
  &17*beta**2*fz**3)/2. + 1617*beta*beta2*fz**3 + (1617*beta2**2*fz**3)/2.

  dAnmpdf(3,7,5) = (-77*beta*bK2P*fz)/4. - (77*beta2*bK2P*fz)/4. - (231*beta*b&
  &Pi2P*fz)/4. - (231*beta2*bPi2P*fz)/4. - (693*b1*beta*fz**2)/8. + (693*bdeta&
  &*beta*fz**2)/8. - (693*b1*beta2*fz**2)/8. + (693*bdeta*beta2*fz**2)/8. + 69&
  &3*beta**2*fz**3 + 1386*beta*beta2*fz**3 + 693*beta2**2*fz**3

  dAnmpdf(3,8,6) = (-3003*beta**2*fz**3)/8. - (3003*beta*beta2*fz**3)/4. - (30&
  &03*beta2**2*fz**3)/8.

  dAnmpdf(4,0,0) = -(beta**2*fz**3)/2.

  dAnmpdf(4,0,2) = (51*beta**2*fz**3)/8. + (15*beta*beta2*fz**3)/2. + (3*beta2&
  &**2*fz**3)/2.

  dAnmpdf(4,0,4) = (-35*beta**2*fz**3)/4. - (65*beta*beta2*fz**3)/4. - (15*bet&
  &a2**2*fz**3)/2.

  dAnmpdf(4,0,6) = (35*beta**2*fz**3)/32. + (35*beta*beta2*fz**3)/16. + (35*be&
  &ta2**2*fz**3)/32.

  dAnmpdf(4,1,1) = 9*beta**2*fz**3 + 6*beta*beta2*fz**3

  dAnmpdf(4,1,3) = (-105*beta**2*fz**3)/2. - (165*beta*beta2*fz**3)/2. - 30*be&
  &ta2**2*fz**3

  dAnmpdf(4,1,5) = 35*beta**2*fz**3 + 70*beta*beta2*fz**3 + 35*beta2**2*fz**3

  dAnmpdf(4,2,0) = (3*beta**2*fz**3)/2.

  dAnmpdf(4,2,2) = (-255*beta**2*fz**3)/4. - 75*beta*beta2*fz**3 - 15*beta2**2&
  &*fz**3

  dAnmpdf(4,2,4) = (735*beta**2*fz**3)/4. + (1365*beta*beta2*fz**3)/4. + (315*&
  &beta2**2*fz**3)/2.

  dAnmpdf(4,2,6) = (-315*beta**2*fz**3)/8. - (315*beta*beta2*fz**3)/4. - (315*&
  &beta2**2*fz**3)/8.

  dAnmpdf(4,3,1) = -15*beta**2*fz**3 - 10*beta*beta2*fz**3

  dAnmpdf(4,3,3) = 245*beta**2*fz**3 + 385*beta*beta2*fz**3 + 140*beta2**2*fz*&
  &*3

  dAnmpdf(4,3,5) = -315*beta**2*fz**3 - 630*beta*beta2*fz**3 - 315*beta2**2*fz&
  &**3

  dAnmpdf(4,4,2) = (595*beta**2*fz**3)/8. + (175*beta*beta2*fz**3)/2. + (35*be&
  &ta2**2*fz**3)/2.

  dAnmpdf(4,4,4) = (-2205*beta**2*fz**3)/4. - (4095*beta*beta2*fz**3)/4. - (94&
  &5*beta2**2*fz**3)/2.

  dAnmpdf(4,4,6) = (3465*beta**2*fz**3)/16. + (3465*beta*beta2*fz**3)/8. + (34&
  &65*beta2**2*fz**3)/16.

  dAnmpdf(4,5,3) = (-441*beta**2*fz**3)/2. - (693*beta*beta2*fz**3)/2. - 126*b&
  &eta2**2*fz**3

  dAnmpdf(4,5,5) = 693*beta**2*fz**3 + 1386*beta*beta2*fz**3 + 693*beta2**2*fz&
  &**3

  dAnmpdf(4,6,4) = (1617*beta**2*fz**3)/4. + (3003*beta*beta2*fz**3)/4. + (693&
  &*beta2**2*fz**3)/2.

  dAnmpdf(4,6,6) = (-3003*beta**2*fz**3)/8. - (3003*beta*beta2*fz**3)/4. - (30&
  &03*beta2**2*fz**3)/8.

  dAnmpdf(4,7,5) = -429*beta**2*fz**3 - 858*beta*beta2*fz**3 - 429*beta2**2*fz&
  &**3

  dAnmpdf(4,8,6) = (6435*beta**2*fz**3)/32. + (6435*beta*beta2*fz**3)/16. + (6&
  &435*beta2**2*fz**3)/32.

  return
  
END SUBROUTINE calculate_dAnmpdf

!-----------------------------------------------------------------------------

SUBROUTINE calculate_dAnmpdb1(fz,b1,b2,bK2,beta,bPi2P,bK2P,bdeta,beta2,dAnmpdb1)

  IMPLICIT none

  DOUBLE PRECISION, intent(IN) :: fz,b1,beta,b2,bK2,bPi2P,bK2P,bdeta,beta2
  DOUBLE PRECISION, intent(OUT) :: dAnmpdb1(0:4,0:8,0:6)

  dAnmpdb1 = 0d0

  dAnmpdb1(0,0,2) = (9*b1)/98. + (3*b2)/14. - bK2/7. - bK2P/42. + (3*b1*fz)/14&
  &. + (b2*fz)/4. - (3*bdeta*fz)/28. - (bK2*fz)/6. - (5*bK2P*fz)/72. + (3*b1*f&
  &z**2)/16. - (3*bdeta*fz**2)/16.

  dAnmpdb1(0,0,4) = (3*b2)/14. + (2*bK2)/7. + (5*bK2P)/42. + (3*bPi2P)/14. + (&
  &3*b1*fz)/7. + (3*b2*fz)/4. - (3*bdeta*fz)/14. + (bK2P*fz)/24. + (3*bPi2P*fz&
  &)/8. + (3*b1*fz**2)/4. - (3*bdeta*fz**2)/4. + (9*beta*fz**2)/56. + (9*beta2&
  &*fz**2)/56. + (5*beta*fz**3)/16. + (5*beta2*fz**3)/16.

  dAnmpdb1(0,0,6) = (b2*fz)/2. + (2*bK2*fz)/3. + (13*bK2P*fz)/36. + (3*bPi2P*f&
  &z)/4. + (3*b1*fz**2)/4. - (3*bdeta*fz**2)/4. + (5*beta*fz**3)/8. + (5*beta2&
  &*fz**3)/8.

  dAnmpdb1(0,1,1) = (3*b1)/7. + b2/2. - bK2/3. - bK2P/18. + (b1*fz)/2. - (bdet&
  &a*fz)/4.

  dAnmpdb1(0,1,3) = b2/14. + (2*bK2)/21. + bK2P/9. + (2*bPi2P)/7. + (4*b1*fz)/&
  &7. - b2*fz - (2*bdeta*fz)/7. - (bK2*fz)/3. - (7*bK2P*fz)/72. - (3*bPi2P*fz)&
  &/8. - (3*b1*fz**2)/4. + (3*bdeta*fz**2)/4. + (3*beta*fz**2)/8. + (3*beta2*f&
  &z**2)/8.

  dAnmpdb1(0,1,5) = (-3*b2*fz)/2. - 2*bK2*fz - (5*bK2P*fz)/6. - (3*bPi2P*fz)/2&
  &. - (3*b1*fz**2)/2. + (3*bdeta*fz**2)/2. - (5*beta*fz**3)/8. - (5*beta2*fz*&
  &*3)/8.

  dAnmpdb1(0,2,0) = b1/2.

  dAnmpdb1(0,2,2) = (-30*b1)/49. - (12*b2)/7. - (3*bK2)/7. - (5*bK2P)/21. - bP&
  &i2P/2. - (27*b1*fz)/14. - (b2*fz)/4. + (27*bdeta*fz)/28. + (2*bK2*fz)/3. + &
  &(7*bK2P*fz)/36. - (3*b1*fz**2)/8. + (3*bdeta*fz**2)/8.

  dAnmpdb1(0,2,4) = (-5*b2)/7. - (20*bK2)/21. - (59*bK2P)/126. - (13*bPi2P)/14&
  &. - (13*b1*fz)/7. + (b2*fz)/4. + (13*bdeta*fz)/14. + (7*bK2*fz)/3. + (5*bK2&
  &P*fz)/9. - (3*b1*fz**2)/4. + (3*bdeta*fz**2)/4. - (6*beta*fz**2)/7. - (6*be&
  &ta2*fz**2)/7. - (15*beta*fz**3)/16. - (15*beta2*fz**3)/16.

  dAnmpdb1(0,2,6) = -(b2*fz)/2. - (2*bK2*fz)/3. - (11*bK2P*fz)/18. - (3*bPi2P*&
  &fz)/2. - (3*b1*fz**2)/2. + (3*bdeta*fz**2)/2. - (15*beta*fz**3)/8. - (15*be&
  &ta2*fz**3)/8.

  dAnmpdb1(0,3,1) = (-10*b1)/7. + bK2 + bK2P/6. - (b1*fz)/2. + (bdeta*fz)/4.

  dAnmpdb1(0,3,3) = (10*b2)/7. + (40*bK2)/21. + (29*bK2P)/63. + (3*bPi2P)/7. +&
  & (6*b1*fz)/7. + b2*fz - (3*bdeta*fz)/7. - (2*bK2*fz)/3. - (bK2P*fz)/36. + (&
  &3*bPi2P*fz)/4. + (3*b1*fz**2)/2. - (3*bdeta*fz**2)/2. - (3*beta*fz**2)/4. -&
  & (3*beta2*fz**2)/4.

  dAnmpdb1(0,3,5) = (3*b2*fz)/2. + 2*bK2*fz + (4*bK2P*fz)/3. + 3*bPi2P*fz + 3*&
  &b1*fz**2 - 3*bdeta*fz**2 + (15*beta*fz**3)/8. + (15*beta2*fz**3)/8.

  dAnmpdb1(0,4,2) = (50*b1)/49. - (10*bK2)/7. - bK2P/14. + bPi2P/2. + (12*b1*f&
  &z)/7. - (6*bdeta*fz)/7. - (bK2*fz)/2. - (bK2P*fz)/8. + (3*b1*fz**2)/16. - (&
  &3*bdeta*fz**2)/16.

  dAnmpdb1(0,4,4) = (5*bK2P)/21. + (5*bPi2P)/7. + (10*b1*fz)/7. - b2*fz - (5*b&
  &deta*fz)/7. - (7*bK2*fz)/3. - (61*bK2P*fz)/72. - (9*bPi2P*fz)/8. - (3*b1*fz&
  &**2)/4. + (3*bdeta*fz**2)/4. + (69*beta*fz**2)/56. + (69*beta2*fz**2)/56. +&
  & (15*beta*fz**3)/16. + (15*beta2*fz**3)/16.

  dAnmpdb1(0,4,6) = (bK2P*fz)/4. + (3*bPi2P*fz)/4. + (3*b1*fz**2)/4. - (3*bdet&
  &a*fz**2)/4. + (15*beta*fz**3)/8. + (15*beta2*fz**3)/8.

  dAnmpdb1(0,5,3) = (-5*bK2P)/21. - (5*bPi2P)/7. - (10*b1*fz)/7. + (5*bdeta*fz&
  &)/7. + bK2*fz + (bK2P*fz)/8. - (3*bPi2P*fz)/8. - (3*b1*fz**2)/4. + (3*bdeta&
  &*fz**2)/4. + (3*beta*fz**2)/8. + (3*beta2*fz**2)/8.

  dAnmpdb1(0,5,5) = -(bK2P*fz)/2. - (3*bPi2P*fz)/2. - (3*b1*fz**2)/2. + (3*bde&
  &ta*fz**2)/2. - (15*beta*fz**3)/8. - (15*beta2*fz**3)/8.

  dAnmpdb1(0,6,4) = (bK2P*fz)/4. + (3*bPi2P*fz)/4. + (3*b1*fz**2)/4. - (3*bdet&
  &a*fz**2)/4. - (15*beta*fz**2)/28. - (15*beta2*fz**2)/28. - (5*beta*fz**3)/1&
  &6. - (5*beta2*fz**3)/16.

  dAnmpdb1(0,6,6) = (-5*beta*fz**3)/8. - (5*beta2*fz**3)/8.

  dAnmpdb1(0,7,5) = (5*beta*fz**3)/8. + (5*beta2*fz**3)/8.

  dAnmpdb1(1,0,0) = (b1*fz**2)/4.

  dAnmpdb1(1,0,2) = -bK2P/14. + (15*bPi2P)/49. - (3*b1*fz)/14. - (b2*fz)/4. - &
  &(3*bdeta*fz)/28. + (3*beta*fz)/98. + (bK2*fz)/6. + (7*bK2P*fz)/36. + (6*bPi&
  &2P*fz)/7. + (5*b1*fz**2)/8. - (3*bdeta*fz**2)/8. + (5*beta*fz**2)/14. + (3*&
  &beta2*fz**2)/14. + (21*beta*fz**3)/16. + (9*beta2*fz**3)/8.

  dAnmpdb1(1,0,4) = -bK2P/14. - (3*bPi2P)/14. - (3*b1*fz)/7. - (3*b2*fz)/4. + &
  &(3*bdeta*fz)/14. + (bK2P*fz)/12. + (27*bPi2P*fz)/28. - (b1*fz**2)/2. - (bet&
  &a*fz**2)/4. - (9*beta2*fz**2)/28. + (27*beta*fz**3)/16. + (21*beta2*fz**3)/&
  &16.

  dAnmpdb1(1,0,6) = -(b2*fz)/2. - (2*bK2*fz)/3. - (11*bK2P*fz)/18. - (3*bPi2P*&
  &fz)/2. - (3*b1*fz**2)/2. + (3*bdeta*fz**2)/2. - (15*beta*fz**3)/8. - (15*be&
  &ta2*fz**3)/8.

  dAnmpdb1(1,1,1) = -bK2P/6. + (5*bPi2P)/7. - (13*b1*fz)/14. - (b2*fz)/2. - (b&
  &deta*fz)/4. - (beta*fz)/7. + (bK2*fz)/3. - (bK2P*fz)/9. - (bPi2P*fz)/2. - (&
  &5*b1*fz**2)/2. + (3*bdeta*fz**2)/4. + (beta*fz**2)/2. + (beta2*fz**2)/2.

  dAnmpdb1(1,1,3) = (-5*bK2P)/21. - (5*bPi2P)/7. - (10*b1*fz)/7. - (b2*fz)/2. &
  &+ (5*bdeta*fz)/7. + (bK2*fz)/3. - (49*bK2P*fz)/36. - (139*bPi2P*fz)/28. - (&
  &13*b1*fz**2)/2. + (9*bdeta*fz**2)/2. - (73*beta*fz**2)/28. - (57*beta2*fz**&
  &2)/28. - (33*beta*fz**3)/4. - (63*beta2*fz**3)/8.

  dAnmpdb1(1,1,5) = (b2*fz)/2. + (2*bK2*fz)/3. - (8*bK2P*fz)/9. - 3*bPi2P*fz -&
  & 3*b1*fz**2 + 3*bdeta*fz**2 - (75*beta*fz**3)/8. - (75*beta2*fz**3)/8.

  dAnmpdb1(1,2,0) = -(b1*fz) - (beta*fz)/2. - (b1*fz**2)/4.

  dAnmpdb1(1,2,2) = (2*bK2P)/7. - (137*bPi2P)/98. + (17*b1*fz)/14. + (11*b2*fz&
  &)/4. + (15*bdeta*fz)/28. + (4*beta*fz)/49. - (bK2*fz)/3. + (10*bK2P*fz)/9. &
  &+ (39*bPi2P*fz)/14. + (33*b1*fz**2)/4. - (9*bdeta*fz**2)/2. - (26*beta*fz**&
  &2)/7. - (55*beta2*fz**2)/14. - (21*beta*fz**3)/8. - (9*beta2*fz**3)/4.

  dAnmpdb1(1,2,4) = (19*bK2P)/42. + (19*bPi2P)/14. + (19*b1*fz)/7. + (17*b2*fz&
  &)/4. - (19*bdeta*fz)/14. + (5*bK2*fz)/3. + (89*bK2P*fz)/18. + (88*bPi2P*fz)&
  &/7. + (37*b1*fz**2)/2. - 15*bdeta*fz**2 + (47*beta*fz**2)/14. + 3*beta2*fz*&
  &*2 + (231*beta*fz**3)/16. + (243*beta2*fz**3)/16.

  dAnmpdb1(1,2,6) = (3*b2*fz)/2. + 2*bK2*fz + (10*bK2P*fz)/3. + 9*bPi2P*fz + 9&
  &*b1*fz**2 - 9*bdeta*fz**2 + (135*beta*fz**3)/8. + (135*beta2*fz**3)/8.

  dAnmpdb1(1,3,1) = bK2P/2. + (2*bPi2P)/7. + (69*b1*fz)/14. - (3*bdeta*fz)/4. &
  &+ (8*beta*fz)/7. - bK2*fz + (bPi2P*fz)/2. + (5*b1*fz**2)/2. - (3*bdeta*fz**&
  &2)/4. - (beta*fz**2)/2. - (beta2*fz**2)/2.

  dAnmpdb1(1,3,3) = (11*bK2P)/21. + (11*bPi2P)/7. + (22*b1*fz)/7. - 5*b2*fz - &
  &(11*bdeta*fz)/7. - (14*bK2*fz)/3. - (47*bK2P*fz)/18. - (57*bPi2P*fz)/14. - &
  &7*b1*fz**2 + 6*bdeta*fz**2 + (143*beta*fz**2)/14. + (141*beta2*fz**2)/14. +&
  & (33*beta*fz**3)/2. + (63*beta2*fz**3)/4.

  dAnmpdb1(1,3,5) = (-9*b2*fz)/2. - 6*bK2*fz - 5*bK2P*fz - 12*bPi2P*fz - 12*b1&
  &*fz**2 + 12*bdeta*fz**2 + (45*beta*fz**3)/8. + (45*beta2*fz**3)/8.

  dAnmpdb1(1,4,2) = (-17*bK2P)/14. - (187*bPi2P)/98. - 8*b1*fz + (18*bdeta*fz)&
  &/7. - (30*beta*fz)/49. + (7*bK2*fz)/2. - (3*bK2P*fz)/4. - (51*bPi2P*fz)/14.&
  & - (71*b1*fz**2)/8. + (39*bdeta*fz**2)/8. + (47*beta*fz**2)/14. + (26*beta2&
  &*fz**2)/7. + (21*beta*fz**3)/16. + (9*beta2*fz**3)/8.

  dAnmpdb1(1,4,4) = (-5*bK2P)/7. - (15*bPi2P)/7. - (30*b1*fz)/7. + 3*b2*fz + (&
  &15*bdeta*fz)/7. + 7*bK2*fz - (13*bK2P*fz)/12. - (169*bPi2P*fz)/28. - (21*b1&
  &*fz**2)/2. + (15*bdeta*fz**2)/2. - (237*beta*fz**2)/28. - (225*beta2*fz**2)&
  &/28. - (543*beta*fz**3)/16. - (549*beta2*fz**3)/16.

  dAnmpdb1(1,4,6) = (-5*bK2P*fz)/2. - (15*bPi2P*fz)/2. - (15*b1*fz**2)/2. + (1&
  &5*bdeta*fz**2)/2. - (225*beta*fz**3)/8. - (225*beta2*fz**3)/8.

  dAnmpdb1(1,5,3) = (5*bK2P)/7. + (15*bPi2P)/7. + (30*b1*fz)/7. - (15*bdeta*fz&
  &)/7. - 3*bK2*fz + (11*bK2P*fz)/4. + (253*bPi2P*fz)/28. + (27*b1*fz**2)/2. -&
  & (21*bdeta*fz**2)/2. - (213*beta*fz**2)/28. - (225*beta2*fz**2)/28. - (33*b&
  &eta*fz**3)/4. - (63*beta2*fz**3)/8.

  dAnmpdb1(1,5,5) = 5*bK2P*fz + 15*bPi2P*fz + 15*b1*fz**2 - 15*bdeta*fz**2 + (&
  &135*beta*fz**3)/8. + (135*beta2*fz**3)/8.

  dAnmpdb1(1,6,4) = (-5*bK2P*fz)/2. - (15*bPi2P*fz)/2. - (15*b1*fz**2)/2. + (1&
  &5*bdeta*fz**2)/2. + (75*beta*fz**2)/14. + (75*beta2*fz**2)/14. + (285*beta*&
  &fz**3)/16. + (285*beta2*fz**3)/16.

  dAnmpdb1(1,6,6) = (105*beta*fz**3)/8. + (105*beta2*fz**3)/8.

  dAnmpdb1(1,7,5) = (-105*beta*fz**3)/8. - (105*beta2*fz**3)/8.

  dAnmpdb1(2,0,0) = -(b1*fz**2)/4. + (beta*fz**3)/4.

  dAnmpdb1(2,0,2) = -(bK2P*fz)/8. - (6*bPi2P*fz)/7. - (13*b1*fz**2)/16. + (9*b&
  &deta*fz**2)/16. - (5*beta*fz**2)/14. - (3*beta2*fz**2)/14. - (17*beta*fz**3&
  &)/8. - (9*beta2*fz**3)/4.

  dAnmpdb1(2,0,4) = -(bK2P*fz)/8. - (75*bPi2P*fz)/56. - (b1*fz**2)/4. + (3*bde&
  &ta*fz**2)/4. + (5*beta*fz**2)/56. + (9*beta2*fz**2)/56. - (69*beta*fz**3)/1&
  &6. - (57*beta2*fz**3)/16.

  dAnmpdb1(2,0,6) = (bK2P*fz)/4. + (3*bPi2P*fz)/4. + (3*b1*fz**2)/4. - (3*bdet&
  &a*fz**2)/4. + (15*beta*fz**3)/8. + (15*beta2*fz**3)/8.

  dAnmpdb1(2,1,1) = (bK2P*fz)/3. - (3*bPi2P*fz)/14. + (5*b1*fz**2)/2. - (bdeta&
  &*fz**2)/4. - (11*beta*fz**2)/14. - (beta2*fz**2)/2. - (7*beta*fz**3)/2. - (&
  &3*beta2*fz**3)/2.

  dAnmpdb1(2,1,3) = (43*bK2P*fz)/24. + (219*bPi2P*fz)/56. + (29*b1*fz**2)/4. -&
  & (17*bdeta*fz**2)/4. + (117*beta*fz**2)/56. + (93*beta2*fz**2)/56. + (21*be&
  &ta*fz**3)/2. + (51*beta2*fz**3)/4.

  dAnmpdb1(2,1,5) = (3*bK2P*fz)/2. + (9*bPi2P*fz)/2. + (9*b1*fz**2)/2. - (9*bd&
  &eta*fz**2)/2. + (165*beta*fz**3)/8. + (165*beta2*fz**3)/8.

  dAnmpdb1(2,2,0) = (3*b1*fz**2)/4. - (beta*fz**3)/4.

  dAnmpdb1(2,2,2) = (-17*bK2P*fz)/12. + (5*bPi2P*fz)/14. - (47*b1*fz**2)/8. + &
  &(13*bdeta*fz**2)/8. + (39*beta*fz**2)/7. + (61*beta2*fz**2)/14. + (85*beta*&
  &fz**3)/4. + (33*beta2*fz**3)/2.

  dAnmpdb1(2,2,4) = (-9*bK2P*fz)/2. - (64*bPi2P*fz)/7. - (63*b1*fz**2)/4. + (4&
  &5*bdeta*fz**2)/4. - (33*beta*fz**2)/14. - (15*beta2*fz**2)/7. - (81*beta*fz&
  &**3)/16. - (153*beta2*fz**3)/16.

  dAnmpdb1(2,2,6) = (-5*bK2P*fz)/2. - (15*bPi2P*fz)/2. - (15*b1*fz**2)/2. + (1&
  &5*bdeta*fz**2)/2. - (225*beta*fz**3)/8. - (225*beta2*fz**3)/8.

  dAnmpdb1(2,3,1) = (-2*bK2P*fz)/3. - (11*bPi2P*fz)/14. - (11*b1*fz**2)/2. + (&
  &5*bdeta*fz**2)/4. + (11*beta*fz**2)/14. + (3*beta2*fz**2)/2. + (7*beta*fz**&
  &3)/2. + (3*beta2*fz**3)/2.

  dAnmpdb1(2,3,3) = -(bK2P*fz)/4. - (75*bPi2P*fz)/28. - (5*b1*fz**2)/2. + (3*b&
  &deta*fz**2)/2. - (345*beta*fz**2)/28. - (285*beta2*fz**2)/28. - 63*beta*fz*&
  &*3 - (123*beta2*fz**3)/2.

  dAnmpdb1(2,3,5) = (5*bK2P*fz)/3. + 5*bPi2P*fz + 5*b1*fz**2 - 5*bdeta*fz**2 -&
  & (375*beta*fz**3)/8. - (375*beta2*fz**3)/8.

  dAnmpdb1(2,4,2) = (77*bK2P*fz)/24. + (11*bPi2P*fz)/2. + (243*b1*fz**2)/16. -&
  & (115*bdeta*fz**2)/16. - (73*beta*fz**2)/14. - (50*beta2*fz**2)/7. - (153*b&
  &eta*fz**3)/8. - (57*beta2*fz**3)/4.

  dAnmpdb1(2,4,4) = (145*bK2P*fz)/24. + (825*bPi2P*fz)/56. + (85*b1*fz**2)/4. &
  &- (65*bdeta*fz**2)/4. + (477*beta*fz**2)/56. + (405*beta2*fz**2)/56. + (130&
  &5*beta*fz**3)/16. + (1365*beta2*fz**3)/16.

  dAnmpdb1(2,4,6) = (35*bK2P*fz)/12. + (35*bPi2P*fz)/4. + (35*b1*fz**2)/4. - (&
  &35*bdeta*fz**2)/4. + (525*beta*fz**3)/8. + (525*beta2*fz**3)/8.

  dAnmpdb1(2,5,3) = (-125*bK2P*fz)/24. - (685*bPi2P*fz)/56. - (75*b1*fz**2)/4.&
  & + (55*bdeta*fz**2)/4. + (573*beta*fz**2)/56. + (645*beta2*fz**2)/56. + (10&
  &5*beta*fz**3)/2. + (195*beta2*fz**3)/4.

  dAnmpdb1(2,5,5) = (-35*bK2P*fz)/6. - (35*bPi2P*fz)/2. - (35*b1*fz**2)/2. + (&
  &35*bdeta*fz**2)/2. - (105*beta*fz**3)/8. - (105*beta2*fz**3)/8.

  dAnmpdb1(2,6,4) = (35*bK2P*fz)/12. + (35*bPi2P*fz)/4. + (35*b1*fz**2)/4. - (&
  &35*bdeta*fz**2)/4. - (25*beta*fz**2)/4. - (25*beta2*fz**2)/4. - (1155*beta*&
  &fz**3)/16. - (1155*beta2*fz**3)/16.

  dAnmpdb1(2,6,6) = (-315*beta*fz**3)/8. - (315*beta2*fz**3)/8.

  dAnmpdb1(2,7,5) = (315*beta*fz**3)/8. + (315*beta2*fz**3)/8.

  dAnmpdb1(3,0,0) = -(beta*fz**3)/4.

  dAnmpdb1(3,0,2) = (13*beta*fz**3)/16. + (9*beta2*fz**3)/8.

  dAnmpdb1(3,0,4) = (37*beta*fz**3)/16. + (31*beta2*fz**3)/16.

  dAnmpdb1(3,0,6) = (-5*beta*fz**3)/8. - (5*beta2*fz**3)/8.

  dAnmpdb1(3,1,1) = (7*beta*fz**3)/2. + (3*beta2*fz**3)/2.

  dAnmpdb1(3,1,3) = (-9*beta*fz**3)/4. - (39*beta2*fz**3)/8.

  dAnmpdb1(3,1,5) = (-85*beta*fz**3)/8. - (85*beta2*fz**3)/8.

  dAnmpdb1(3,2,0) = (3*beta*fz**3)/4.

  dAnmpdb1(3,2,2) = (-141*beta*fz**3)/8. - (57*beta2*fz**3)/4.

  dAnmpdb1(3,2,4) = (-135*beta*fz**3)/16. - (75*beta2*fz**3)/16.

  dAnmpdb1(3,2,6) = (105*beta*fz**3)/8. + (105*beta2*fz**3)/8.

  dAnmpdb1(3,3,1) = (-13*beta*fz**3)/2. - (5*beta2*fz**3)/2.

  dAnmpdb1(3,3,3) = (85*beta*fz**3)/2. + (175*beta2*fz**3)/4.

  dAnmpdb1(3,3,5) = (315*beta*fz**3)/8. + (315*beta2*fz**3)/8.

  dAnmpdb1(3,4,2) = (405*beta*fz**3)/16. + (145*beta2*fz**3)/8.

  dAnmpdb1(3,4,4) = (-665*beta*fz**3)/16. - (735*beta2*fz**3)/16.

  dAnmpdb1(3,4,6) = (-315*beta*fz**3)/8. - (315*beta2*fz**3)/8.

  dAnmpdb1(3,5,3) = (-217*beta*fz**3)/4. - (399*beta2*fz**3)/8.

  dAnmpdb1(3,5,5) = (-63*beta*fz**3)/8. - (63*beta2*fz**3)/8.

  dAnmpdb1(3,6,4) = (987*beta*fz**3)/16. + (987*beta2*fz**3)/16.

  dAnmpdb1(3,6,6) = (231*beta*fz**3)/8. + (231*beta2*fz**3)/8.

  dAnmpdb1(3,7,5) = (-231*beta*fz**3)/8. - (231*beta2*fz**3)/8.

  return
  
END SUBROUTINE calculate_dAnmpdb1

!-----------------------------------------------------------------------------

SUBROUTINE calculate_dAnmpdb2(fz,b1,b2,bK2,beta,bPi2P,bK2P,bdeta,beta2,dAnmpdb2)

  IMPLICIT none

  DOUBLE PRECISION, intent(IN) :: fz,b1,beta,b2,bK2,bPi2P,bK2P,bdeta,beta2
  DOUBLE PRECISION, intent(OUT) :: dAnmpdb2(0:4,0:8,0:6)

  dAnmpdb2 = 0d0

  dAnmpdb2(0,0,2) = (3*b1)/14. + b2/2. - bK2/3. - bK2P/18. + (b1*fz)/4. - (bde&
  &ta*fz)/4.

  dAnmpdb2(0,0,4) = (3*b1)/14. + b2 + bK2/3. + (2*bK2P)/9. + bPi2P/2. + (3*b1*&
  &fz)/4. - (3*bdeta*fz)/4. + (3*beta*fz**2)/8. + (3*beta2*fz**2)/8.

  dAnmpdb2(0,0,6) = b2/2. + (2*bK2)/3. + (5*bK2P)/18. + bPi2P/2. + (b1*fz)/2. &
  &- (bdeta*fz)/2. + (3*beta*fz**2)/8. + (3*beta2*fz**2)/8.

  dAnmpdb2(0,1,1) = b1/2.

  dAnmpdb2(0,1,3) = b1/14. - 2*b2 - (2*bK2)/3. - (5*bK2P)/18. - bPi2P/2. - b1*&
  &fz + bdeta*fz

  dAnmpdb2(0,1,5) = -2*b2 - (8*bK2)/3. - (17*bK2P)/18. - (3*bPi2P)/2. - (3*b1*&
  &fz)/2. + (3*bdeta*fz)/2. - (3*beta*fz**2)/4. - (3*beta2*fz**2)/4.

  dAnmpdb2(0,2,2) = (-12*b1)/7. + bK2 + bK2P/6. - (b1*fz)/4. + (bdeta*fz)/4.

  dAnmpdb2(0,2,4) = (-5*b1)/7. + 2*b2 + (11*bK2)/3. + (7*bK2P)/9. + bPi2P/2. +&
  & (b1*fz)/4. - (bdeta*fz)/4. - (3*beta*fz**2)/4. - (3*beta2*fz**2)/4.

  dAnmpdb2(0,2,6) = -bK2P/6. - bPi2P/2. - (b1*fz)/2. + (bdeta*fz)/2. - (3*beta&
  &*fz**2)/4. - (3*beta2*fz**2)/4.

  dAnmpdb2(0,3,3) = (10*b1)/7. - 2*bK2 - bK2P/6. + bPi2P/2. + b1*fz - bdeta*fz

  dAnmpdb2(0,3,5) = bK2P/2. + (3*bPi2P)/2. + (3*b1*fz)/2. - (3*bdeta*fz)/2. + &
  &(3*beta*fz**2)/2. + (3*beta2*fz**2)/2.

  dAnmpdb2(0,4,4) = -bK2P/3. - bPi2P - b1*fz + bdeta*fz + (3*beta*fz**2)/8. + &
  &(3*beta2*fz**2)/8.

  dAnmpdb2(0,4,6) = (3*beta*fz**2)/8. + (3*beta2*fz**2)/8.

  dAnmpdb2(0,5,5) = (-3*beta*fz**2)/4. - (3*beta2*fz**2)/4.

  dAnmpdb2(1,0,2) = -bK2P/6. + (5*bPi2P)/7. - (b1*fz)/4. - (bdeta*fz)/4. + (be&
  &ta*fz)/14. + (3*beta*fz**2)/4. + (beta2*fz**2)/2.

  dAnmpdb2(1,0,4) = -bK2P/3. + (3*bPi2P)/14. - (3*b1*fz)/4. + (bdeta*fz)/4. + &
  &(beta*fz)/14. - (beta2*fz**2)/4.

  dAnmpdb2(1,0,6) = -bK2P/6. - bPi2P/2. - (b1*fz)/2. + (bdeta*fz)/2. - (3*beta&
  &*fz**2)/4. - (3*beta2*fz**2)/4.

  dAnmpdb2(1,1,1) = -(b1*fz)/2. - (beta*fz)/2.

  dAnmpdb2(1,1,3) = bK2P/6. - (27*bPi2P)/14. - (b1*fz)/2. + bdeta*fz - (9*beta&
  &*fz)/14. - (9*beta*fz**2)/2. - 4*beta2*fz**2

  dAnmpdb2(1,1,5) = bK2P/6. + bPi2P/2. + (b1*fz)/2. - (bdeta*fz)/2. - (3*beta*&
  &fz**2)/2. - (3*beta2*fz**2)/2.

  dAnmpdb2(1,2,2) = bK2P/2. + (2*bPi2P)/7. + (11*b1*fz)/4. - (3*bdeta*fz)/4. +&
  & (10*beta*fz)/7. - (3*beta*fz**2)/4. - (beta2*fz**2)/2.

  dAnmpdb2(1,2,4) = (4*bK2P)/3. + (39*bPi2P)/14. + (17*b1*fz)/4. - (13*bdeta*f&
  &z)/4. + (3*beta*fz)/7. + (39*beta*fz**2)/4. + 10*beta2*fz**2

  dAnmpdb2(1,2,6) = bK2P/2. + (3*bPi2P)/2. + (3*b1*fz)/2. - (3*bdeta*fz)/2. + &
  &(9*beta*fz**2)/2. + (9*beta2*fz**2)/2.

  dAnmpdb2(1,3,3) = (-3*bK2P)/2. - (29*bPi2P)/14. - 5*b1*fz + 3*bdeta*fz - (6*&
  &beta*fz)/7. + (9*beta*fz**2)/2. + 4*beta2*fz**2

  dAnmpdb2(1,3,5) = (-3*bK2P)/2. - (9*bPi2P)/2. - (9*b1*fz)/2. + (9*bdeta*fz)/&
  &2. - 6*beta*fz**2 - 6*beta2*fz**2

  dAnmpdb2(1,4,4) = bK2P + 3*bPi2P + 3*b1*fz - 3*bdeta*fz - (39*beta*fz**2)/4.&
  & - (39*beta2*fz**2)/4.

  dAnmpdb2(1,4,6) = (-15*beta*fz**2)/4. - (15*beta2*fz**2)/4.

  dAnmpdb2(1,5,5) = (15*beta*fz**2)/2. + (15*beta2*fz**2)/2.

  dAnmpdb2(2,0,2) = (-3*beta*fz**2)/4. - (beta2*fz**2)/2.

  dAnmpdb2(2,0,4) = (-3*beta*fz**2)/8. - (beta2*fz**2)/8.

  dAnmpdb2(2,0,6) = (3*beta*fz**2)/8. + (3*beta2*fz**2)/8.

  dAnmpdb2(2,1,1) = -(beta*fz**2)/2.

  dAnmpdb2(2,1,3) = 4*beta*fz**2 + 4*beta2*fz**2

  dAnmpdb2(2,1,5) = (9*beta*fz**2)/4. + (9*beta2*fz**2)/4.

  dAnmpdb2(2,2,2) = (13*beta*fz**2)/4. + (3*beta2*fz**2)/2.

  dAnmpdb2(2,2,4) = (-15*beta*fz**2)/2. - (33*beta2*fz**2)/4.

  dAnmpdb2(2,2,6) = (-15*beta*fz**2)/4. - (15*beta2*fz**2)/4.

  dAnmpdb2(2,3,3) = (-19*beta*fz**2)/2. - 8*beta2*fz**2

  dAnmpdb2(2,3,5) = (5*beta*fz**2)/2. + (5*beta2*fz**2)/2.

  dAnmpdb2(2,4,4) = (115*beta*fz**2)/8. + (115*beta2*fz**2)/8.

  dAnmpdb2(2,4,6) = (35*beta*fz**2)/8. + (35*beta2*fz**2)/8.

  dAnmpdb2(2,5,5) = (-35*beta*fz**2)/4. - (35*beta2*fz**2)/4.

  return
  
END SUBROUTINE calculate_dAnmpdb2
  
!-----------------------------------------------------------------------------

SUBROUTINE calculate_dAnmpdbK2(fz,b1,b2,bK2,beta,bPi2P,bK2P,bdeta,beta2,dAnmpdbK2)

  IMPLICIT none

  DOUBLE PRECISION, intent(IN) :: fz,b1,beta,b2,bK2,bPi2P,bK2P,bdeta,beta2
  DOUBLE PRECISION, intent(OUT) :: dAnmpdbK2(0:4,0:8,0:6)

  dAnmpdbK2 = 0d0

  dAnmpdbK2(0,0,2) = -b1/7. - b2/3. + (2*bK2)/9. + bK2P/27. - (b1*fz)/6. + (bd&
  &eta*fz)/6.

  dAnmpdbK2(0,0,4) = (2*b1)/7. + b2/3. - (8*bK2)/9. - (7*bK2P)/27. - bPi2P/3. &
  &- (beta*fz**2)/4. - (beta2*fz**2)/4.

  dAnmpdbK2(0,0,6) = (2*b2)/3. + (8*bK2)/9. + (10*bK2P)/27. + (2*bPi2P)/3. + (&
  &2*b1*fz)/3. - (2*bdeta*fz)/3. + (beta*fz**2)/2. + (beta2*fz**2)/2.

  dAnmpdbK2(0,1,1) = -b1/3.

  dAnmpdbK2(0,1,3) = (2*b1)/21. - (2*b2)/3. + (16*bK2)/9. + (11*bK2P)/27. + bP&
  &i2P/3. - (b1*fz)/3. + (bdeta*fz)/3.

  dAnmpdbK2(0,1,5) = (-8*b2)/3. - (32*bK2)/9. - (34*bK2P)/27. - 2*bPi2P - 2*b1&
  &*fz + 2*bdeta*fz - beta*fz**2 - beta2*fz**2

  dAnmpdbK2(0,2,2) = (-3*b1)/7. + b2 - (4*bK2)/3. - (2*bK2P)/9. + (2*b1*fz)/3.&
  & - (2*bdeta*fz)/3.

  dAnmpdbK2(0,2,4) = (-20*b1)/21. + (11*b2)/3. + (56*bK2)/9. + (52*bK2P)/27. +&
  & (8*bPi2P)/3. + (7*b1*fz)/3. - (7*bdeta*fz)/3. + (5*beta*fz**2)/4. + (5*bet&
  &a2*fz**2)/4.

  dAnmpdbK2(0,2,6) = (-2*bK2P)/9. - (2*bPi2P)/3. - (2*b1*fz)/3. + (2*bdeta*fz)&
  &/3. - beta*fz**2 - beta2*fz**2

  dAnmpdbK2(0,3,1) = b1

  dAnmpdbK2(0,3,3) = (40*b1)/21. - 2*b2 - (16*bK2)/3. - (4*bK2P)/3. - (4*bPi2P&
  &)/3. - (2*b1*fz)/3. + (2*bdeta*fz)/3.

  dAnmpdbK2(0,3,5) = (2*bK2P)/3. + 2*bPi2P + 2*b1*fz - 2*bdeta*fz + 2*beta*fz*&
  &*2 + 2*beta2*fz**2

  dAnmpdbK2(0,4,2) = (-10*b1)/7. + 2*bK2 + bK2P/3. - (b1*fz)/2. + (bdeta*fz)/2&
  &.

  dAnmpdbK2(0,4,4) = (-7*bK2P)/9. - (7*bPi2P)/3. - (7*b1*fz)/3. + (7*bdeta*fz)&
  &/3. - (7*beta*fz**2)/4. - (7*beta2*fz**2)/4.

  dAnmpdbK2(0,4,6) = (beta*fz**2)/2. + (beta2*fz**2)/2.

  dAnmpdbK2(0,5,3) = bK2P/3. + bPi2P + b1*fz - bdeta*fz

  dAnmpdbK2(0,5,5) = -(beta*fz**2) - beta2*fz**2

  dAnmpdbK2(0,6,4) = (3*beta*fz**2)/4. + (3*beta2*fz**2)/4.

  dAnmpdbK2(1,0,2) = bK2P/9. - (10*bPi2P)/21. + (b1*fz)/6. + (bdeta*fz)/6. - (&
  &beta*fz)/21. - (beta*fz**2)/2. - (beta2*fz**2)/3.

  dAnmpdbK2(1,0,4) = -bK2P/9. + (9*bPi2P)/7. - (2*bdeta*fz)/3. + (2*beta*fz)/2&
  &1. + (3*beta*fz**2)/2. + (7*beta2*fz**2)/6.

  dAnmpdbK2(1,0,6) = (-2*bK2P)/9. - (2*bPi2P)/3. - (2*b1*fz)/3. + (2*bdeta*fz)&
  &/3. - beta*fz**2 - beta2*fz**2

  dAnmpdbK2(1,1,1) = (b1*fz)/3. + (beta*fz)/3.

  dAnmpdbK2(1,1,3) = (5*bK2P)/9. - (11*bPi2P)/7. + (b1*fz)/3. + (bdeta*fz)/3. &
  &- (6*beta*fz)/7. + (2*beta2*fz**2)/3.

  dAnmpdbK2(1,1,5) = (2*bK2P)/9. + (2*bPi2P)/3. + (2*b1*fz)/3. - (2*bdeta*fz)/&
  &3. - 2*beta*fz**2 - 2*beta2*fz**2

  dAnmpdbK2(1,2,2) = (-2*bK2P)/3. + (26*bPi2P)/21. - (b1*fz)/3. + (25*beta*fz)&
  &/21. + 2*beta*fz**2 + (4*beta2*fz**2)/3.

  dAnmpdbK2(1,2,4) = (4*bK2P)/9. - (2*bPi2P)/7. + (5*b1*fz)/3. - (bdeta*fz)/3.&
  & + (4*beta*fz)/7. + (5*beta*fz**2)/2. + (17*beta2*fz**2)/6.

  dAnmpdbK2(1,2,6) = (2*bK2P)/3. + 2*bPi2P + 2*b1*fz - 2*bdeta*fz + 6*beta*fz*&
  &*2 + 6*beta2*fz**2

  dAnmpdbK2(1,3,1) = -(b1*fz) - beta*fz

  dAnmpdbK2(1,3,3) = (-4*bK2P)/3. - (16*bPi2P)/21. - (14*b1*fz)/3. + 2*bdeta*f&
  &z - (8*beta*fz)/7. - 6*beta*fz**2 - (20*beta2*fz**2)/3.

  dAnmpdbK2(1,3,5) = -2*bK2P - 6*bPi2P - 6*b1*fz + 6*bdeta*fz - 8*beta*fz**2 -&
  & 8*beta2*fz**2

  dAnmpdbK2(1,4,2) = bK2P + (4*bPi2P)/7. + (7*b1*fz)/2. - (3*bdeta*fz)/2. + (6&
  &*beta*fz)/7. - (3*beta*fz**2)/2. - beta2*fz**2

  dAnmpdbK2(1,4,4) = (7*bK2P)/3. + 7*bPi2P + 7*b1*fz - 7*bdeta*fz + (7*beta*fz&
  &**2)/2. + (7*beta2*fz**2)/2.

  dAnmpdbK2(1,4,6) = -5*beta*fz**2 - 5*beta2*fz**2

  dAnmpdbK2(1,5,3) = -bK2P - 3*bPi2P - 3*b1*fz + 3*bdeta*fz + 6*beta*fz**2 + 6&
  &*beta2*fz**2

  dAnmpdbK2(1,5,5) = 10*beta*fz**2 + 10*beta2*fz**2

  dAnmpdbK2(1,6,4) = (-15*beta*fz**2)/2. - (15*beta2*fz**2)/2.

  dAnmpdbK2(2,0,2) = (beta*fz**2)/2. + (beta2*fz**2)/3.

  dAnmpdbK2(2,0,4) = (-5*beta*fz**2)/4. - (11*beta2*fz**2)/12.

  dAnmpdbK2(2,0,6) = (beta*fz**2)/2. + (beta2*fz**2)/2.

  dAnmpdbK2(2,1,1) = (beta*fz**2)/3.

  dAnmpdbK2(2,1,3) = (-2*beta*fz**2)/3. - (2*beta2*fz**2)/3.

  dAnmpdbK2(2,1,5) = 3*beta*fz**2 + 3*beta2*fz**2

  dAnmpdbK2(2,2,2) = (-5*beta*fz**2)/3. - 2*beta2*fz**2

  dAnmpdbK2(2,2,4) = (-7*beta*fz**2)/4. - (11*beta2*fz**2)/4.

  dAnmpdbK2(2,2,6) = -5*beta*fz**2 - 5*beta2*fz**2

  dAnmpdbK2(2,3,1) = -(beta*fz**2)

  dAnmpdbK2(2,3,3) = (10*beta*fz**2)/3. + (16*beta2*fz**2)/3.

  dAnmpdbK2(2,3,5) = (10*beta*fz**2)/3. + (10*beta2*fz**2)/3.

  dAnmpdbK2(2,4,2) = (9*beta*fz**2)/2. + 3*beta2*fz**2

  dAnmpdbK2(2,4,4) = (35*beta*fz**2)/12. + (35*beta2*fz**2)/12.

  dAnmpdbK2(2,4,6) = (35*beta*fz**2)/6. + (35*beta2*fz**2)/6.

  dAnmpdbK2(2,5,3) = -10*beta*fz**2 - 10*beta2*fz**2

  dAnmpdbK2(2,5,5) = (-35*beta*fz**2)/3. - (35*beta2*fz**2)/3.

  dAnmpdbK2(2,6,4) = (35*beta*fz**2)/4. + (35*beta2*fz**2)/4.

  return
  
END SUBROUTINE calculate_dAnmpdbK2
  
!-----------------------------------------------------------------------------

SUBROUTINE calculate_dAnmpdbeta(fz,b1,b2,bK2,beta,bPi2P,bK2P,bdeta,beta2,dAnmpdbeta)

  IMPLICIT none

  DOUBLE PRECISION, intent(IN) :: fz,b1,beta,b2,bK2,bPi2P,bK2P,bdeta,beta2
  DOUBLE PRECISION, intent(OUT) :: dAnmpdbeta(0:4,0:8,0:6)

  dAnmpdbeta = 0d0

  dAnmpdbeta(0,0,4) = (9*b1*fz**2)/56. + (3*b2*fz**2)/8. - (bK2*fz**2)/4. - (b&
  &K2P*fz**2)/8. + (5*b1*fz**3)/16. - (5*bdeta*fz**3)/16.

  dAnmpdbeta(0,0,6) = (3*b2*fz**2)/8. + (bK2*fz**2)/2. + (7*bK2P*fz**2)/24. + &
  &(5*bPi2P*fz**2)/8. + (5*b1*fz**3)/8. - (5*bdeta*fz**3)/8. + (35*beta*fz**4)&
  &/64. + (35*beta2*fz**4)/64.

  dAnmpdbeta(0,1,3) = (3*b1*fz**2)/8.

  dAnmpdbeta(0,1,5) = (-3*b2*fz**2)/4. - bK2*fz**2 - (3*bK2P*fz**2)/8. - (5*bP&
  &i2P*fz**2)/8. - (5*b1*fz**3)/8. + (5*bdeta*fz**3)/8.

  dAnmpdbeta(0,2,4) = (-6*b1*fz**2)/7. - (3*b2*fz**2)/4. + (5*bK2*fz**2)/4. + &
  &(11*bK2P*fz**2)/24. - (15*b1*fz**3)/16. + (15*bdeta*fz**3)/16.

  dAnmpdbeta(0,2,6) = (-3*b2*fz**2)/4. - bK2*fz**2 - (19*bK2P*fz**2)/24. - (15&
  &*bPi2P*fz**2)/8. - (15*b1*fz**3)/8. + (15*bdeta*fz**3)/8. - (35*beta*fz**4)&
  &/16. - (35*beta2*fz**4)/16.

  dAnmpdbeta(0,3,3) = (-3*b1*fz**2)/4.

  dAnmpdbeta(0,3,5) = (3*b2*fz**2)/2. + 2*bK2*fz**2 + (23*bK2P*fz**2)/24. + (1&
  &5*bPi2P*fz**2)/8. + (15*b1*fz**3)/8. - (15*bdeta*fz**3)/8.

  dAnmpdbeta(0,4,4) = (69*b1*fz**2)/56. + (3*b2*fz**2)/8. - (7*bK2*fz**2)/4. -&
  & (13*bK2P*fz**2)/24. + (15*b1*fz**3)/16. - (15*bdeta*fz**3)/16.

  dAnmpdbeta(0,4,6) = (3*b2*fz**2)/8. + (bK2*fz**2)/2. + (17*bK2P*fz**2)/24. +&
  & (15*bPi2P*fz**2)/8. + (15*b1*fz**3)/8. - (15*bdeta*fz**3)/8. + (105*beta*f&
  &z**4)/32. + (105*beta2*fz**4)/32.

  dAnmpdbeta(0,5,3) = (3*b1*fz**2)/8.

  dAnmpdbeta(0,5,5) = (-3*b2*fz**2)/4. - bK2*fz**2 - (19*bK2P*fz**2)/24. - (15&
  &*bPi2P*fz**2)/8. - (15*b1*fz**3)/8. + (15*bdeta*fz**3)/8.

  dAnmpdbeta(0,6,4) = (-15*b1*fz**2)/28. + (3*bK2*fz**2)/4. + (5*bK2P*fz**2)/2&
  &4. - (5*b1*fz**3)/16. + (5*bdeta*fz**3)/16.

  dAnmpdbeta(0,6,6) = (-5*bK2P*fz**2)/24. - (5*bPi2P*fz**2)/8. - (5*b1*fz**3)/&
  &8. + (5*bdeta*fz**3)/8. - (35*beta*fz**4)/16. - (35*beta2*fz**4)/16.

  dAnmpdbeta(0,7,5) = (5*bK2P*fz**2)/24. + (5*bPi2P*fz**2)/8. + (5*b1*fz**3)/8&
  &. - (5*bdeta*fz**3)/8.

  dAnmpdbeta(0,8,6) = (35*beta*fz**4)/64. + (35*beta2*fz**4)/64.

  dAnmpdbeta(1,0,2) = (3*b1*fz)/98. + (b2*fz)/14. - (bK2*fz)/21. - (bK2P*fz)/1&
  &26. + (5*b1*fz**2)/14. + (3*b2*fz**2)/4. - (bdeta*fz**2)/28. - (bK2*fz**2)/&
  &2. - (5*bK2P*fz**2)/24. + (21*b1*fz**3)/16. - (9*bdeta*fz**3)/16.

  dAnmpdbeta(1,0,4) = (b2*fz)/14. + (2*bK2*fz)/21. + (5*bK2P*fz)/126. + (bPi2P&
  &*fz)/14. - (b1*fz**2)/4. - (bdeta*fz**2)/14. + (3*bK2*fz**2)/2. + (5*bK2P*f&
  &z**2)/4. + (177*bPi2P*fz**2)/56. + (27*b1*fz**3)/16. - (33*bdeta*fz**3)/16.&
  & + (3*beta*fz**3)/28. + (3*beta2*fz**3)/56. + (35*beta*fz**4)/8. + (65*beta&
  &2*fz**4)/16.

  dAnmpdbeta(1,0,6) = (-3*b2*fz**2)/4. - bK2*fz**2 - (19*bK2P*fz**2)/24. - (15&
  &*bPi2P*fz**2)/8. - (15*b1*fz**3)/8. + (15*bdeta*fz**3)/8. - (35*beta*fz**4)&
  &/16. - (35*beta2*fz**4)/16.

  dAnmpdbeta(1,1,1) = -(b1*fz)/7. - (b2*fz)/2. + (bK2*fz)/3. + (bK2P*fz)/18. +&
  & (b1*fz**2)/2. + (bdeta*fz**2)/4.

  dAnmpdbeta(1,1,3) = (-9*b2*fz)/14. - (6*bK2*fz)/7. - (bK2P*fz)/3. - (4*bPi2P&
  &*fz)/7. - (73*b1*fz**2)/28. - (9*b2*fz**2)/2. + (4*bdeta*fz**2)/7. - (3*bK2&
  &P*fz**2)/8. - (21*bPi2P*fz**2)/8. - (33*b1*fz**3)/4. + (39*bdeta*fz**3)/8. &
  &- (3*beta*fz**3)/4. - (3*beta2*fz**3)/8.

  dAnmpdbeta(1,1,5) = (-3*b2*fz**2)/2. - 2*bK2*fz**2 - (83*bK2P*fz**2)/24. - (&
  &75*bPi2P*fz**2)/8. - (75*b1*fz**3)/8. + (75*bdeta*fz**3)/8. - (35*beta*fz**&
  &4)/2. - (35*beta2*fz**4)/2.

  dAnmpdbeta(1,2,0) = -(b1*fz)/2.

  dAnmpdbeta(1,2,2) = (4*b1*fz)/49. + (10*b2*fz)/7. + (25*bK2*fz)/21. + (23*bK&
  &2P*fz)/63. + (bPi2P*fz)/2. - (26*b1*fz**2)/7. - (3*b2*fz**2)/4. - (19*bdeta&
  &*fz**2)/28. + 2*bK2*fz**2 + (7*bK2P*fz**2)/12. - (21*b1*fz**3)/8. + (9*bdet&
  &a*fz**3)/8.

  dAnmpdbeta(1,2,4) = (3*b2*fz)/7. + (4*bK2*fz)/7. + (3*bK2P*fz)/14. + (5*bPi2&
  &P*fz)/14. + (47*b1*fz**2)/14. + (39*b2*fz**2)/4. - (5*bdeta*fz**2)/14. + (5&
  &*bK2*fz**2)/2. + (37*bK2P*fz**2)/24. + (123*bPi2P*fz**2)/28. + (231*b1*fz**&
  &3)/16. - (159*bdeta*fz**3)/16. + (3*beta*fz**3)/7. + (3*beta2*fz**3)/14. - &
  &(105*beta*fz**4)/8. - (195*beta2*fz**4)/16.

  dAnmpdbeta(1,2,6) = (9*b2*fz**2)/2. + 6*bK2*fz**2 + (53*bK2P*fz**2)/8. + (13&
  &5*bPi2P*fz**2)/8. + (135*b1*fz**3)/8. - (135*bdeta*fz**3)/8. + (105*beta*fz&
  &**4)/4. + (105*beta2*fz**4)/4.

  dAnmpdbeta(1,3,1) = (8*b1*fz)/7. - bK2*fz - (bK2P*fz)/6. - (b1*fz**2)/2. - (&
  &bdeta*fz**2)/4.

  dAnmpdbeta(1,3,3) = (-6*b2*fz)/7. - (8*bK2*fz)/7. - (bK2P*fz)/7. + (bPi2P*fz&
  &)/7. + (143*b1*fz**2)/14. + (9*b2*fz**2)/2. - (bdeta*fz**2)/7. - 6*bK2*fz**&
  &2 - (bK2P*fz**2)/4. + (21*bPi2P*fz**2)/4. + (33*b1*fz**3)/2. - (39*bdeta*fz&
  &**3)/4. + (3*beta*fz**3)/2. + (3*beta2*fz**3)/4.

  dAnmpdbeta(1,3,5) = -6*b2*fz**2 - 8*bK2*fz**2 + (13*bK2P*fz**2)/24. + (45*bP&
  &i2P*fz**2)/8. + (45*b1*fz**3)/8. - (45*bdeta*fz**3)/8. + (105*beta*fz**4)/2&
  &. + (105*beta2*fz**4)/2.

  dAnmpdbeta(1,4,2) = (-30*b1*fz)/49. + (6*bK2*fz)/7. - (bK2P*fz)/42. - (bPi2P&
  &*fz)/2. + (47*b1*fz**2)/14. + (5*bdeta*fz**2)/7. - (3*bK2*fz**2)/2. - (3*bK&
  &2P*fz**2)/8. + (21*b1*fz**3)/16. - (9*bdeta*fz**3)/16.

  dAnmpdbeta(1,4,4) = -(bK2P*fz)/7. - (3*bPi2P*fz)/7. - (237*b1*fz**2)/28. - (&
  &39*b2*fz**2)/4. + (3*bdeta*fz**2)/7. + (7*bK2*fz**2)/2. - (14*bK2P*fz**2)/3&
  &. - (1023*bPi2P*fz**2)/56. - (543*b1*fz**3)/16. + (417*bdeta*fz**3)/16. - (&
  &33*beta*fz**3)/28. - (33*beta2*fz**3)/56. + (105*beta*fz**4)/8. + (195*beta&
  &2*fz**4)/16.

  dAnmpdbeta(1,4,6) = (-15*b2*fz**2)/4. - 5*bK2*fz**2 - (245*bK2P*fz**2)/24. -&
  & (225*bPi2P*fz**2)/8. - (225*b1*fz**3)/8. + (225*bdeta*fz**3)/8. - (525*bet&
  &a*fz**4)/8. - (525*beta2*fz**4)/8.

  dAnmpdbeta(1,5,3) = (bK2P*fz)/7. + (3*bPi2P*fz)/7. - (213*b1*fz**2)/28. - (3&
  &*bdeta*fz**2)/7. + 6*bK2*fz**2 + (5*bK2P*fz**2)/8. - (21*bPi2P*fz**2)/8. - &
  &(33*b1*fz**3)/4. + (39*bdeta*fz**3)/8. - (3*beta*fz**3)/4. - (3*beta2*fz**3&
  &)/8.

  dAnmpdbeta(1,5,5) = (15*b2*fz**2)/2. + 10*bK2*fz**2 + (175*bK2P*fz**2)/24. +&
  & (135*bPi2P*fz**2)/8. + (135*b1*fz**3)/8. - (135*bdeta*fz**3)/8. - (105*bet&
  &a*fz**4)/2. - (105*beta2*fz**4)/2.

  dAnmpdbeta(1,6,4) = (75*b1*fz**2)/14. - (15*bK2*fz**2)/2. + (15*bK2P*fz**2)/&
  &8. + (75*bPi2P*fz**2)/7. + (285*b1*fz**3)/16. - (225*bdeta*fz**3)/16. + (9*&
  &beta*fz**3)/14. + (9*beta2*fz**3)/28. - (35*beta*fz**4)/8. - (65*beta2*fz**&
  &4)/16.

  dAnmpdbeta(1,6,6) = (35*bK2P*fz**2)/8. + (105*bPi2P*fz**2)/8. + (105*b1*fz**&
  &3)/8. - (105*bdeta*fz**3)/8. + (245*beta*fz**4)/4. + (245*beta2*fz**4)/4.

  dAnmpdbeta(1,7,5) = (-35*bK2P*fz**2)/8. - (105*bPi2P*fz**2)/8. - (105*b1*fz*&
  &*3)/8. + (105*bdeta*fz**3)/8. + (35*beta*fz**4)/2. + (35*beta2*fz**4)/2.

  dAnmpdbeta(1,8,6) = (-315*beta*fz**4)/16. - (315*beta2*fz**4)/16.

  dAnmpdbeta(2,0,0) = (b1*fz**3)/4.

  dAnmpdbeta(2,0,2) = -(bK2P*fz)/42. + (5*bPi2P*fz)/49. - (5*b1*fz**2)/14. - (&
  &3*b2*fz**2)/4. - (bdeta*fz**2)/28. + (beta*fz**2)/98. + (bK2*fz**2)/2. + (b&
  &K2P*fz**2)/4. + (11*bPi2P*fz**2)/7. - (17*b1*fz**3)/8. - (bdeta*fz**3)/8. +&
  & (3*beta*fz**3)/14. + (beta2*fz**3)/14. + (51*beta*fz**4)/16. + (15*beta2*f&
  &z**4)/8.

  dAnmpdbeta(2,0,4) = -(bK2P*fz)/42. - (bPi2P*fz)/14. + (5*b1*fz**2)/56. - (3*&
  &b2*fz**2)/8. + (bdeta*fz**2)/14. - (5*bK2*fz**2)/4. - (47*bK2P*fz**2)/24. -&
  & (177*bPi2P*fz**2)/28. - (69*b1*fz**3)/16. + (81*bdeta*fz**3)/16. - (3*beta&
  &*fz**3)/14. - (3*beta2*fz**3)/28. - (105*beta*fz**4)/8. - (195*beta2*fz**4)&
  &/16.

  dAnmpdbeta(2,0,6) = (3*b2*fz**2)/8. + (bK2*fz**2)/2. + (17*bK2P*fz**2)/24. +&
  & (15*bPi2P*fz**2)/8. + (15*b1*fz**3)/8. - (15*bdeta*fz**3)/8. + (105*beta*f&
  &z**4)/32. + (105*beta2*fz**4)/32.

  dAnmpdbeta(2,1,1) = (bK2P*fz)/6. - (5*bPi2P*fz)/7. - (11*b1*fz**2)/14. - (b2&
  &*fz**2)/2. + (bdeta*fz**2)/4. - (beta*fz**2)/7. + (bK2*fz**2)/3. - (bK2P*fz&
  &**2)/9. - (bPi2P*fz**2)/2. - (7*b1*fz**3)/2. + (3*bdeta*fz**3)/4. - (3*beta&
  &*fz**3)/2. - (beta2*fz**3)/2.

  dAnmpdbeta(2,1,3) = (bK2P*fz)/7. + (3*bPi2P*fz)/7. + (117*b1*fz**2)/56. + 4*&
  &b2*fz**2 - (3*bdeta*fz**2)/7. - (2*bK2*fz**2)/3. - (13*bK2P*fz**2)/36. - (1&
  &41*bPi2P*fz**2)/28. + (21*b1*fz**3)/2. - (3*bdeta*fz**3)/4. + (9*beta*fz**3&
  &)/14. + (9*beta2*fz**3)/28. - (105*beta*fz**4)/4. - (165*beta2*fz**4)/8.

  dAnmpdbeta(2,1,5) = (9*b2*fz**2)/4. + 3*bK2*fz**2 + (59*bK2P*fz**2)/8. + (16&
  &5*bPi2P*fz**2)/8. + (165*b1*fz**3)/8. - (165*bdeta*fz**3)/8. + (105*beta*fz&
  &**4)/2. + (105*beta2*fz**4)/2.

  dAnmpdbeta(2,2,0) = (beta*fz**2)/2. - (b1*fz**3)/4.

  dAnmpdbeta(2,2,2) = (2*bK2P*fz)/21. + (113*bPi2P*fz)/98. + (39*b1*fz**2)/7. &
  &+ (13*b2*fz**2)/4. - (23*bdeta*fz**2)/28. + (6*beta*fz**2)/49. - (5*bK2*fz*&
  &*2)/3. + (14*bK2P*fz**2)/9. + (47*bPi2P*fz**2)/14. + (85*b1*fz**3)/4. - 7*b&
  &deta*fz**3 + (99*beta*fz**3)/14. + (47*beta2*fz**3)/14. - (51*beta*fz**4)/8&
  &. - (15*beta2*fz**4)/4.

  dAnmpdbeta(2,2,4) = -(bK2P*fz)/14. - (3*bPi2P*fz)/14. - (33*b1*fz**2)/14. - &
  &(15*b2*fz**2)/2. + (3*bdeta*fz**2)/14. - (7*bK2*fz**2)/4. + (65*bK2P*fz**2)&
  &/24. + (33*bPi2P*fz**2)/2. - (81*b1*fz**3)/16. - (111*bdeta*fz**3)/16. + (9&
  &45*beta*fz**4)/8. + (1755*beta2*fz**4)/16.

  dAnmpdbeta(2,2,6) = (-15*b2*fz**2)/4. - 5*bK2*fz**2 - (245*bK2P*fz**2)/24. -&
  & (225*bPi2P*fz**2)/8. - (225*b1*fz**3)/8. + (225*bdeta*fz**3)/8. - (525*bet&
  &a*fz**4)/8. - (525*beta2*fz**4)/8.

  dAnmpdbeta(2,3,1) = -(bK2P*fz)/2. - (2*bPi2P*fz)/7. + (11*b1*fz**2)/14. + (3&
  &*bdeta*fz**2)/4. - (6*beta*fz**2)/7. - bK2*fz**2 + (bPi2P*fz**2)/2. + (7*b1&
  &*fz**3)/2. - (3*bdeta*fz**3)/4. + (3*beta*fz**3)/2. + (beta2*fz**3)/2.

  dAnmpdbeta(2,3,3) = (-5*bK2P*fz)/7. - (15*bPi2P*fz)/7. - (345*b1*fz**2)/28. &
  &- (19*b2*fz**2)/2. + (15*bdeta*fz**2)/7. + (10*bK2*fz**2)/3. - (143*bK2P*fz&
  &**2)/18. - (237*bPi2P*fz**2)/14. - 63*b1*fz**3 + (59*bdeta*fz**3)/2. - (93*&
  &beta*fz**3)/7. - (93*beta2*fz**3)/14. + (105*beta*fz**4)/2. + (165*beta2*fz&
  &**4)/4.

  dAnmpdbeta(2,3,5) = (5*b2*fz**2)/2. + (10*bK2*fz**2)/3. - (1085*bK2P*fz**2)/&
  &72. - (375*bPi2P*fz**2)/8. - (375*b1*fz**3)/8. + (375*bdeta*fz**3)/8. - (52&
  &5*beta*fz**4)/2. - (525*beta2*fz**4)/2.

  dAnmpdbeta(2,4,2) = (13*bK2P*fz)/14. + (171*bPi2P*fz)/98. - (73*b1*fz**2)/14&
  &. - (15*bdeta*fz**2)/7. + (18*beta*fz**2)/49. + (9*bK2*fz**2)/2. - (5*bK2P*&
  &fz**2)/4. - (69*bPi2P*fz**2)/14. - (153*b1*fz**3)/8. + (57*bdeta*fz**3)/8. &
  &- (51*beta*fz**3)/7. - (24*beta2*fz**3)/7. + (51*beta*fz**4)/16. + (15*beta&
  &2*fz**4)/8.

  dAnmpdbeta(2,4,4) = (3*bK2P*fz)/7. + (9*bPi2P*fz)/7. + (477*b1*fz**2)/56. + &
  &(115*b2*fz**2)/8. - (9*bdeta*fz**2)/7. + (35*bK2*fz**2)/12. + (1205*bK2P*fz&
  &**2)/72. + (1035*bPi2P*fz**2)/28. + (1305*b1*fz**3)/16. - (845*bdeta*fz**3)&
  &/16. + (93*beta*fz**3)/14. + (93*beta2*fz**3)/28. - (1575*beta*fz**4)/8. - &
  &(2925*beta2*fz**4)/16.

  dAnmpdbeta(2,4,6) = (35*b2*fz**2)/8. + (35*bK2*fz**2)/6. + (1645*bK2P*fz**2)&
  &/72. + (525*bPi2P*fz**2)/8. + (525*b1*fz**3)/8. - (525*bdeta*fz**3)/8. + (3&
  &675*beta*fz**4)/16. + (3675*beta2*fz**4)/16.

  dAnmpdbeta(2,5,3) = (-3*bK2P*fz)/7. - (9*bPi2P*fz)/7. + (573*b1*fz**2)/56. +&
  & (9*bdeta*fz**2)/7. - 10*bK2*fz**2 + (85*bK2P*fz**2)/12. + (615*bPi2P*fz**2&
  &)/28. + (105*b1*fz**3)/2. - (115*bdeta*fz**3)/4. + (177*beta*fz**3)/14. + (&
  &177*beta2*fz**3)/28. - (105*beta*fz**4)/4. - (165*beta2*fz**4)/8.

  dAnmpdbeta(2,5,5) = (-35*b2*fz**2)/4. - (35*bK2*fz**2)/3. - (455*bK2P*fz**2)&
  &/72. - (105*bPi2P*fz**2)/8. - (105*b1*fz**3)/8. + (105*bdeta*fz**3)/8. + (7&
  &35*beta*fz**4)/2. + (735*beta2*fz**4)/2.

  dAnmpdbeta(2,6,4) = (-25*b1*fz**2)/4. + (35*bK2*fz**2)/4. - (385*bK2P*fz**2)&
  &/24. - (330*bPi2P*fz**2)/7. - (1155*b1*fz**3)/16. + (875*bdeta*fz**3)/16. -&
  & (45*beta*fz**3)/7. - (45*beta2*fz**3)/14. + (735*beta*fz**4)/8. + (1365*be&
  &ta2*fz**4)/16.

  dAnmpdbeta(2,6,6) = (-105*bK2P*fz**2)/8. - (315*bPi2P*fz**2)/8. - (315*b1*fz&
  &**3)/8. + (315*bdeta*fz**3)/8. - (2205*beta*fz**4)/8. - (2205*beta2*fz**4)/&
  &8.

  dAnmpdbeta(2,7,5) = (105*bK2P*fz**2)/8. + (315*bPi2P*fz**2)/8. + (315*b1*fz*&
  &*3)/8. - (315*bdeta*fz**3)/8. - (315*beta*fz**4)/2. - (315*beta2*fz**4)/2.

  dAnmpdbeta(2,8,6) = (3465*beta*fz**4)/32. + (3465*beta2*fz**4)/32.

  dAnmpdbeta(3,0,0) = -(b1*fz**3)/4. + (beta*fz**4)/4.

  dAnmpdbeta(3,0,2) = -(bK2P*fz**2)/24. - (11*bPi2P*fz**2)/7. + (13*b1*fz**3)/&
  &16. + (11*bdeta*fz**3)/16. - (3*beta*fz**3)/14. - (beta2*fz**3)/14. - (51*b&
  &eta*fz**4)/8. - (15*beta2*fz**4)/4.

  dAnmpdbeta(3,0,4) = (5*bK2P*fz**2)/6. + (177*bPi2P*fz**2)/56. + (37*b1*fz**3&
  &)/16. - (43*bdeta*fz**3)/16. + (3*beta*fz**3)/28. + (3*beta2*fz**3)/56. + (&
  &105*beta*fz**4)/8. + (195*beta2*fz**4)/16.

  dAnmpdbeta(3,0,6) = (-5*bK2P*fz**2)/24. - (5*bPi2P*fz**2)/8. - (5*b1*fz**3)/&
  &8. + (5*bdeta*fz**3)/8. - (35*beta*fz**4)/16. - (35*beta2*fz**4)/16.

  dAnmpdbeta(3,1,1) = (bK2P*fz**2)/3. - (3*bPi2P*fz**2)/14. + (7*b1*fz**3)/2. &
  &- (bdeta*fz**3)/4. + (19*beta*fz**3)/14. + (beta2*fz**3)/2. - (9*beta*fz**4&
  &)/2. - (3*beta2*fz**4)/2.

  dAnmpdbeta(3,1,3) = (5*bK2P*fz**2)/8. + (429*bPi2P*fz**2)/56. - (9*b1*fz**3)&
  &/4. - (33*bdeta*fz**3)/8. + (3*beta*fz**3)/28. + (3*beta2*fz**3)/56. + (105&
  &*beta*fz**4)/2. + (165*beta2*fz**4)/4.

  dAnmpdbeta(3,1,5) = (-85*bK2P*fz**2)/24. - (85*bPi2P*fz**2)/8. - (85*b1*fz**&
  &3)/8. + (85*bdeta*fz**3)/8. - (105*beta*fz**4)/2. - (105*beta2*fz**4)/2.

  dAnmpdbeta(3,2,0) = (3*b1*fz**3)/4. + beta*fz**3 - (beta*fz**4)/4.

  dAnmpdbeta(3,2,2) = (-9*bK2P*fz**2)/4. - (3*bPi2P*fz**2)/14. - (141*b1*fz**3&
  &)/8. + (27*bdeta*fz**3)/8. - (93*beta*fz**3)/14. - (45*beta2*fz**3)/14. + (&
  &153*beta*fz**4)/4. + (45*beta2*fz**4)/2.

  dAnmpdbeta(3,2,4) = (-35*bK2P*fz**2)/8. - (585*bPi2P*fz**2)/28. - (135*b1*fz&
  &**3)/16. + (255*bdeta*fz**3)/16. - (3*beta*fz**3)/7. - (3*beta2*fz**3)/14. &
  &- (1575*beta*fz**4)/8. - (2925*beta2*fz**4)/16.

  dAnmpdbeta(3,2,6) = (35*bK2P*fz**2)/8. + (105*bPi2P*fz**2)/8. + (105*b1*fz**&
  &3)/8. - (105*bdeta*fz**3)/8. + (245*beta*fz**4)/4. + (245*beta2*fz**4)/4.

  dAnmpdbeta(3,3,1) = (-2*bK2P*fz**2)/3. - (11*bPi2P*fz**2)/14. - (13*b1*fz**3&
  &)/2. + (5*bdeta*fz**3)/4. - (75*beta*fz**3)/14. - (3*beta2*fz**3)/2. + (9*b&
  &eta*fz**4)/2. + (3*beta2*fz**4)/2.

  dAnmpdbeta(3,3,3) = (85*bK2P*fz**2)/12. + (135*bPi2P*fz**2)/28. + (85*b1*fz*&
  &*3)/2. - (55*bdeta*fz**3)/4. + (157*beta*fz**3)/14. + (157*beta2*fz**3)/28.&
  & - 175*beta*fz**4 - (275*beta2*fz**4)/2.

  dAnmpdbeta(3,3,5) = (105*bK2P*fz**2)/8. + (315*bPi2P*fz**2)/8. + (315*b1*fz*&
  &*3)/8. - (315*bdeta*fz**3)/8. + (735*beta*fz**4)/2. + (735*beta2*fz**4)/2.

  dAnmpdbeta(3,4,2) = (95*bK2P*fz**2)/24. + (95*bPi2P*fz**2)/14. + (405*b1*fz*&
  &*3)/16. - (145*bdeta*fz**3)/16. + (97*beta*fz**3)/7. + (44*beta2*fz**3)/7. &
  &- (255*beta*fz**4)/8. - (75*beta2*fz**4)/4.

  dAnmpdbeta(3,4,4) = (-35*bK2P*fz**2)/4. - (575*bPi2P*fz**2)/56. - (665*b1*fz&
  &**3)/16. + (315*bdeta*fz**3)/16. - (145*beta*fz**3)/28. - (145*beta2*fz**3)&
  &/56. + (3675*beta*fz**4)/8. + (6825*beta2*fz**4)/16.

  dAnmpdbeta(3,4,6) = (-105*bK2P*fz**2)/8. - (315*bPi2P*fz**2)/8. - (315*b1*fz&
  &**3)/8. + (315*bdeta*fz**3)/8. - (2205*beta*fz**4)/8. - (2205*beta2*fz**4)/&
  &8.

  dAnmpdbeta(3,5,3) = (-91*bK2P*fz**2)/8. - (1315*bPi2P*fz**2)/56. - (217*b1*f&
  &z**3)/4. + (231*bdeta*fz**3)/8. - (485*beta*fz**3)/28. - (485*beta2*fz**3)/&
  &56. + (245*beta*fz**4)/2. + (385*beta2*fz**4)/4.

  dAnmpdbeta(3,5,5) = (-21*bK2P*fz**2)/8. - (63*bPi2P*fz**2)/8. - (63*b1*fz**3&
  &)/8. + (63*bdeta*fz**3)/8. - (1323*beta*fz**4)/2. - (1323*beta2*fz**4)/2.

  dAnmpdbeta(3,6,4) = (133*bK2P*fz**2)/8. + 41*bPi2P*fz**2 + (987*b1*fz**3)/16&
  &. - (735*bdeta*fz**3)/16. + (15*beta*fz**3)/2. + (15*beta2*fz**3)/4. - (220&
  &5*beta*fz**4)/8. - (4095*beta2*fz**4)/16.

  dAnmpdbeta(3,6,6) = (77*bK2P*fz**2)/8. + (231*bPi2P*fz**2)/8. + (231*b1*fz**&
  &3)/8. - (231*bdeta*fz**3)/8. + (1617*beta*fz**4)/4. + (1617*beta2*fz**4)/4.

  dAnmpdbeta(3,7,5) = (-77*bK2P*fz**2)/8. - (231*bPi2P*fz**2)/8. - (231*b1*fz*&
  &*3)/8. + (231*bdeta*fz**3)/8. + (693*beta*fz**4)/2. + (693*beta2*fz**4)/2.

  dAnmpdbeta(3,8,6) = (-3003*beta*fz**4)/16. - (3003*beta2*fz**4)/16.

  dAnmpdbeta(4,0,0) = -(beta*fz**4)/4.

  dAnmpdbeta(4,0,2) = (51*beta*fz**4)/16. + (15*beta2*fz**4)/8.

  dAnmpdbeta(4,0,4) = (-35*beta*fz**4)/8. - (65*beta2*fz**4)/16.

  dAnmpdbeta(4,0,6) = (35*beta*fz**4)/64. + (35*beta2*fz**4)/64.

  dAnmpdbeta(4,1,1) = (9*beta*fz**4)/2. + (3*beta2*fz**4)/2.

  dAnmpdbeta(4,1,3) = (-105*beta*fz**4)/4. - (165*beta2*fz**4)/8.

  dAnmpdbeta(4,1,5) = (35*beta*fz**4)/2. + (35*beta2*fz**4)/2.

  dAnmpdbeta(4,2,0) = (3*beta*fz**4)/4.

  dAnmpdbeta(4,2,2) = (-255*beta*fz**4)/8. - (75*beta2*fz**4)/4.

  dAnmpdbeta(4,2,4) = (735*beta*fz**4)/8. + (1365*beta2*fz**4)/16.

  dAnmpdbeta(4,2,6) = (-315*beta*fz**4)/16. - (315*beta2*fz**4)/16.

  dAnmpdbeta(4,3,1) = (-15*beta*fz**4)/2. - (5*beta2*fz**4)/2.

  dAnmpdbeta(4,3,3) = (245*beta*fz**4)/2. + (385*beta2*fz**4)/4.

  dAnmpdbeta(4,3,5) = (-315*beta*fz**4)/2. - (315*beta2*fz**4)/2.

  dAnmpdbeta(4,4,2) = (595*beta*fz**4)/16. + (175*beta2*fz**4)/8.

  dAnmpdbeta(4,4,4) = (-2205*beta*fz**4)/8. - (4095*beta2*fz**4)/16.

  dAnmpdbeta(4,4,6) = (3465*beta*fz**4)/32. + (3465*beta2*fz**4)/32.

  dAnmpdbeta(4,5,3) = (-441*beta*fz**4)/4. - (693*beta2*fz**4)/8.

  dAnmpdbeta(4,5,5) = (693*beta*fz**4)/2. + (693*beta2*fz**4)/2.

  dAnmpdbeta(4,6,4) = (1617*beta*fz**4)/8. + (3003*beta2*fz**4)/16.

  dAnmpdbeta(4,6,6) = (-3003*beta*fz**4)/16. - (3003*beta2*fz**4)/16.

  dAnmpdbeta(4,7,5) = (-429*beta*fz**4)/2. - (429*beta2*fz**4)/2.

  dAnmpdbeta(4,8,6) = (6435*beta*fz**4)/64. + (6435*beta2*fz**4)/64.

  return
  
END SUBROUTINE calculate_dAnmpdbeta

!-----------------------------------------------------------------------------

SUBROUTINE calculate_dAnmpdbPi2P(fz,b1,b2,bK2,beta,bPi2P,bK2P,bdeta,beta2,dAnmpdbPi2P)

  IMPLICIT none

  DOUBLE PRECISION, intent(IN) :: fz,b1,beta,b2,bK2,bPi2P,bK2P,bdeta,beta2
  DOUBLE PRECISION, intent(OUT) :: dAnmpdbPi2P(0:4,0:8,0:6)

  dAnmpdbPi2P = 0d0

  dAnmpdbPi2P(0,0,4) = (3*b1)/14. + b2/2. - bK2/3. - (5*bK2P)/36. + (3*b1*fz)/&
  &8. - (3*bdeta*fz)/8.

  dAnmpdbPi2P(0,0,6) = b2/2. + (2*bK2)/3. + (13*bK2P)/36. + (3*bPi2P)/4. + (3*&
  &b1*fz)/4. - (3*bdeta*fz)/4. + (5*beta*fz**2)/8. + (5*beta2*fz**2)/8.

  dAnmpdbPi2P(0,1,3) = (2*b1)/7. - b2/2. + bK2/3. + (5*bK2P)/36. - (3*b1*fz)/8&
  &. + (3*bdeta*fz)/8.

  dAnmpdbPi2P(0,1,5) = (-3*b2)/2. - 2*bK2 - (5*bK2P)/6. - (3*bPi2P)/2. - (3*b1&
  &*fz)/2. + (3*bdeta*fz)/2. - (5*beta*fz**2)/8. - (5*beta2*fz**2)/8.

  dAnmpdbPi2P(0,2,2) = -b1/2.

  dAnmpdbPi2P(0,2,4) = (-13*b1)/14. + b2/2. + (8*bK2)/3. + (31*bK2P)/36. + (3*&
  &bPi2P)/4.

  dAnmpdbPi2P(0,2,6) = -b2/2. - (2*bK2)/3. - (11*bK2P)/18. - (3*bPi2P)/2. - (3&
  &*b1*fz)/2. + (3*bdeta*fz)/2. - (15*beta*fz**2)/8. - (15*beta2*fz**2)/8.

  dAnmpdbPi2P(0,3,3) = (3*b1)/7. + b2/2. - (4*bK2)/3. - (7*bK2P)/18. + (3*b1*f&
  &z)/4. - (3*bdeta*fz)/4.

  dAnmpdbPi2P(0,3,5) = (3*b2)/2. + 2*bK2 + (4*bK2P)/3. + 3*bPi2P + 3*b1*fz - 3&
  &*bdeta*fz + (15*beta*fz**2)/8. + (15*beta2*fz**2)/8.

  dAnmpdbPi2P(0,4,2) = b1/2.

  dAnmpdbPi2P(0,4,4) = (5*b1)/7. - b2 - (7*bK2)/3. - (35*bK2P)/36. - (3*bPi2P)&
  &/2. - (9*b1*fz)/8. + (9*bdeta*fz)/8.

  dAnmpdbPi2P(0,4,6) = bK2P/4. + (3*bPi2P)/4. + (3*b1*fz)/4. - (3*bdeta*fz)/4.&
  & + (15*beta*fz**2)/8. + (15*beta2*fz**2)/8.

  dAnmpdbPi2P(0,5,3) = (-5*b1)/7. + bK2 + bK2P/4. - (3*b1*fz)/8. + (3*bdeta*fz&
  &)/8.

  dAnmpdbPi2P(0,5,5) = -bK2P/2. - (3*bPi2P)/2. - (3*b1*fz)/2. + (3*bdeta*fz)/2&
  &. - (15*beta*fz**2)/8. - (15*beta2*fz**2)/8.

  dAnmpdbPi2P(0,6,4) = bK2P/4. + (3*bPi2P)/4. + (3*b1*fz)/4. - (3*bdeta*fz)/4.

  dAnmpdbPi2P(0,6,6) = (-5*beta*fz**2)/8. - (5*beta2*fz**2)/8.

  dAnmpdbPi2P(0,7,5) = (5*beta*fz**2)/8. + (5*beta2*fz**2)/8.

  dAnmpdbPi2P(1,0,2) = (15*b1)/49. + (5*b2)/7. - (10*bK2)/21. - (5*bK2P)/63. +&
  & (6*b1*fz)/7. - (5*bdeta*fz)/14.

  dAnmpdbPi2P(1,0,4) = (-3*b1)/14. + (3*b2)/14. + (9*bK2)/7. + (11*bK2P)/14. +&
  & (17*bPi2P)/7. + (27*b1*fz)/28. - (41*bdeta*fz)/28. + (beta*fz)/14. + (177*&
  &beta*fz**2)/56. + (39*beta2*fz**2)/14.

  dAnmpdbPi2P(1,0,6) = -b2/2. - (2*bK2)/3. - (11*bK2P)/18. - (3*bPi2P)/2. - (3&
  &*b1*fz)/2. + (3*bdeta*fz)/2. - (15*beta*fz**2)/8. - (15*beta2*fz**2)/8.

  dAnmpdbPi2P(1,1,1) = (5*b1)/7. - (b1*fz)/2.

  dAnmpdbPi2P(1,1,3) = (-5*b1)/7. - (27*b2)/14. - (11*bK2)/7. - (7*bK2P)/6. - &
  &(24*bPi2P)/7. - (139*b1*fz)/28. + (83*bdeta*fz)/28. - (4*beta*fz)/7. - (21*&
  &beta*fz**2)/8. - (9*beta2*fz**2)/4.

  dAnmpdbPi2P(1,1,5) = b2/2. + (2*bK2)/3. - (8*bK2P)/9. - 3*bPi2P - 3*b1*fz + &
  &3*bdeta*fz - (75*beta*fz**2)/8. - (75*beta2*fz**2)/8.

  dAnmpdbPi2P(1,2,2) = (-137*b1)/98. + (2*b2)/7. + (26*bK2)/21. + (34*bK2P)/63&
  &. + bPi2P + (39*b1*fz)/14. - (11*bdeta*fz)/14. + (beta*fz)/2.

  dAnmpdbPi2P(1,2,4) = (19*b1)/14. + (39*b2)/14. - (2*bK2)/7. + (37*bK2P)/14. &
  &+ (93*bPi2P)/14. + (88*b1*fz)/7. - (127*bdeta*fz)/14. + (5*beta*fz)/14. + (&
  &123*beta*fz**2)/28. + (36*beta2*fz**2)/7.

  dAnmpdbPi2P(1,2,6) = (3*b2)/2. + 2*bK2 + (10*bK2P)/3. + 9*bPi2P + 9*b1*fz - &
  &9*bdeta*fz + (135*beta*fz**2)/8. + (135*beta2*fz**2)/8.

  dAnmpdbPi2P(1,3,1) = (2*b1)/7. + (b1*fz)/2.

  dAnmpdbPi2P(1,3,3) = (11*b1)/7. - (29*b2)/14. - (16*bK2)/21. - (62*bK2P)/63.&
  & - (8*bPi2P)/7. - (57*b1*fz)/14. + (43*bdeta*fz)/14. + (beta*fz)/7. + (21*b&
  &eta*fz**2)/4. + (9*beta2*fz**2)/2.

  dAnmpdbPi2P(1,3,5) = (-9*b2)/2. - 6*bK2 - 5*bK2P - 12*bPi2P - 12*b1*fz + 12*&
  &bdeta*fz + (45*beta*fz**2)/8. + (45*beta2*fz**2)/8.

  dAnmpdbPi2P(1,4,2) = (-187*b1)/98. + (4*bK2)/7. - (5*bK2P)/21. - bPi2P - (51&
  &*b1*fz)/14. + (8*bdeta*fz)/7. - (beta*fz)/2.

  dAnmpdbPi2P(1,4,4) = (-15*b1)/7. + 3*b2 + 7*bK2 + (17*bK2P)/42. - (11*bPi2P)&
  &/7. - (169*b1*fz)/28. + (85*bdeta*fz)/28. - (3*beta*fz)/7. - (1023*beta*fz*&
  &*2)/56. - (261*beta2*fz**2)/14.

  dAnmpdbPi2P(1,4,6) = (-5*bK2P)/2. - (15*bPi2P)/2. - (15*b1*fz)/2. + (15*bdet&
  &a*fz)/2. - (225*beta*fz**2)/8. - (225*beta2*fz**2)/8.

  dAnmpdbPi2P(1,5,3) = (15*b1)/7. - 3*bK2 + (53*bK2P)/42. + (32*bPi2P)/7. + (2&
  &53*b1*fz)/28. - (169*bdeta*fz)/28. + (3*beta*fz)/7. - (21*beta*fz**2)/8. - &
  &(9*beta2*fz**2)/4.

  dAnmpdbPi2P(1,5,5) = 5*bK2P + 15*bPi2P + 15*b1*fz - 15*bdeta*fz + (135*beta*&
  &fz**2)/8. + (135*beta2*fz**2)/8.

  dAnmpdbPi2P(1,6,4) = (-5*bK2P)/2. - (15*bPi2P)/2. - (15*b1*fz)/2. + (15*bdet&
  &a*fz)/2. + (75*beta*fz**2)/7. + (75*beta2*fz**2)/7.

  dAnmpdbPi2P(1,6,6) = (105*beta*fz**2)/8. + (105*beta2*fz**2)/8.

  dAnmpdbPi2P(1,7,5) = (-105*beta*fz**2)/8. - (105*beta2*fz**2)/8.

  dAnmpdbPi2P(2,0,2) = (-5*bK2P)/21. + (50*bPi2P)/49. - (6*b1*fz)/7. - (5*bdet&
  &a*fz)/14. + (5*beta*fz)/49. + (11*beta*fz**2)/7. + (5*beta2*fz**2)/7.

  dAnmpdbPi2P(2,0,4) = (-41*bK2P)/84. - (17*bPi2P)/7. - (75*b1*fz)/56. + (103*&
  &bdeta*fz)/56. - (beta*fz)/14. - (177*beta*fz**2)/28. - (39*beta2*fz**2)/7.

  dAnmpdbPi2P(2,0,6) = bK2P/4. + (3*bPi2P)/4. + (3*b1*fz)/4. - (3*bdeta*fz)/4.&
  & + (15*beta*fz**2)/8. + (15*beta2*fz**2)/8.

  dAnmpdbPi2P(2,1,1) = (-3*b1*fz)/14. - (5*beta*fz)/7. - (beta*fz**2)/2.

  dAnmpdbPi2P(2,1,3) = (19*bK2P)/28. + (4*bPi2P)/7. + (219*b1*fz)/56. - (51*bd&
  &eta*fz)/56. + (3*beta*fz)/7. - (141*beta*fz**2)/28. - (39*beta2*fz**2)/14.

  dAnmpdbPi2P(2,1,5) = (3*bK2P)/2. + (9*bPi2P)/2. + (9*b1*fz)/2. - (9*bdeta*fz&
  &)/2. + (165*beta*fz**2)/8. + (165*beta2*fz**2)/8.

  dAnmpdbPi2P(2,2,2) = (2*bK2P)/7. - (9*bPi2P)/49. + (5*b1*fz)/14. - (3*bdeta*&
  &fz)/14. + (113*beta*fz)/98. + (47*beta*fz**2)/14. + (11*beta2*fz**2)/7.

  dAnmpdbPi2P(2,2,4) = (-193*bK2P)/84. - (71*bPi2P)/28. - (64*b1*fz)/7. + (65*&
  &bdeta*fz)/14. - (3*beta*fz)/14. + (33*beta*fz**2)/2. + 12*beta2*fz**2

  dAnmpdbPi2P(2,2,6) = (-5*bK2P)/2. - (15*bPi2P)/2. - (15*b1*fz)/2. + (15*bdet&
  &a*fz)/2. - (225*beta*fz**2)/8. - (225*beta2*fz**2)/8.

  dAnmpdbPi2P(2,3,1) = (-11*b1*fz)/14. - (2*beta*fz)/7. + (beta*fz**2)/2.

  dAnmpdbPi2P(2,3,3) = (-13*bK2P)/42. - (20*bPi2P)/7. - (75*b1*fz)/28. + (47*b&
  &deta*fz)/28. - (15*beta*fz)/7. - (237*beta*fz**2)/14. - (108*beta2*fz**2)/7&
  &.

  dAnmpdbPi2P(2,3,5) = (5*bK2P)/3. + 5*bPi2P + 5*b1*fz - 5*bdeta*fz - (375*bet&
  &a*fz**2)/8. - (375*beta2*fz**2)/8.

  dAnmpdbPi2P(2,4,2) = (13*bK2P)/21. + (57*bPi2P)/49. + (11*b1*fz)/2. - (10*bd&
  &eta*fz)/7. + (171*beta*fz)/98. - (69*beta*fz**2)/14. - (16*beta2*fz**2)/7.

  dAnmpdbPi2P(2,4,4) = (325*bK2P)/84. + (115*bPi2P)/14. + (825*b1*fz)/56. - (5&
  &45*bdeta*fz)/56. + (9*beta*fz)/7. + (1035*beta*fz**2)/28. + (285*beta2*fz**&
  &2)/7.

  dAnmpdbPi2P(2,4,6) = (35*bK2P)/12. + (35*bPi2P)/4. + (35*b1*fz)/4. - (35*bde&
  &ta*fz)/4. + (525*beta*fz**2)/8. + (525*beta2*fz**2)/8.

  dAnmpdbPi2P(2,5,3) = (-85*bK2P)/28. - (40*bPi2P)/7. - (685*b1*fz)/56. + (405&
  &*bdeta*fz)/56. - (9*beta*fz)/7. + (615*beta*fz**2)/28. + (255*beta2*fz**2)/&
  &14.

  dAnmpdbPi2P(2,5,5) = (-35*bK2P)/6. - (35*bPi2P)/2. - (35*b1*fz)/2. + (35*bde&
  &ta*fz)/2. - (105*beta*fz**2)/8. - (105*beta2*fz**2)/8.

  dAnmpdbPi2P(2,6,4) = (35*bK2P)/12. + (35*bPi2P)/4. + (35*b1*fz)/4. - (35*bde&
  &ta*fz)/4. - (330*beta*fz**2)/7. - (330*beta2*fz**2)/7.

  dAnmpdbPi2P(2,6,6) = (-315*beta*fz**2)/8. - (315*beta2*fz**2)/8.

  dAnmpdbPi2P(2,7,5) = (315*beta*fz**2)/8. + (315*beta2*fz**2)/8.

  dAnmpdbPi2P(3,0,2) = (-11*beta*fz**2)/7. - (5*beta2*fz**2)/7.

  dAnmpdbPi2P(3,0,4) = (177*beta*fz**2)/56. + (39*beta2*fz**2)/14.

  dAnmpdbPi2P(3,0,6) = (-5*beta*fz**2)/8. - (5*beta2*fz**2)/8.

  dAnmpdbPi2P(3,1,1) = (-3*beta*fz**2)/14.

  dAnmpdbPi2P(3,1,3) = (429*beta*fz**2)/56. + (141*beta2*fz**2)/28.

  dAnmpdbPi2P(3,1,5) = (-85*beta*fz**2)/8. - (85*beta2*fz**2)/8.

  dAnmpdbPi2P(3,2,2) = (-3*beta*fz**2)/14. - (beta2*fz**2)/7.

  dAnmpdbPi2P(3,2,4) = (-585*beta*fz**2)/28. - (120*beta2*fz**2)/7.

  dAnmpdbPi2P(3,2,6) = (105*beta*fz**2)/8. + (105*beta2*fz**2)/8.

  dAnmpdbPi2P(3,3,1) = (-11*beta*fz**2)/14.

  dAnmpdbPi2P(3,3,3) = (135*beta*fz**2)/28. + (85*beta2*fz**2)/14.

  dAnmpdbPi2P(3,3,5) = (315*beta*fz**2)/8. + (315*beta2*fz**2)/8.

  dAnmpdbPi2P(3,4,2) = (95*beta*fz**2)/14. + (20*beta2*fz**2)/7.

  dAnmpdbPi2P(3,4,4) = (-575*beta*fz**2)/56. - (205*beta2*fz**2)/14.

  dAnmpdbPi2P(3,4,6) = (-315*beta*fz**2)/8. - (315*beta2*fz**2)/8.

  dAnmpdbPi2P(3,5,3) = (-1315*beta*fz**2)/56. - (535*beta2*fz**2)/28.

  dAnmpdbPi2P(3,5,5) = (-63*beta*fz**2)/8. - (63*beta2*fz**2)/8.

  dAnmpdbPi2P(3,6,4) = 41*beta*fz**2 + 41*beta2*fz**2

  dAnmpdbPi2P(3,6,6) = (231*beta*fz**2)/8. + (231*beta2*fz**2)/8.

  dAnmpdbPi2P(3,7,5) = (-231*beta*fz**2)/8. - (231*beta2*fz**2)/8.

  return
  
END SUBROUTINE calculate_dAnmpdbPi2P
  
!-----------------------------------------------------------------------------

SUBROUTINE calculate_dAnmpdbK2P(fz,b1,b2,bK2,beta,bPi2P,bK2P,bdeta,beta2,dAnmpdbK2P)

  IMPLICIT none

  DOUBLE PRECISION, intent(IN) :: fz,b1,beta,b2,bK2,bPi2P,bK2P,bdeta,beta2
  DOUBLE PRECISION, intent(OUT) :: dAnmpdbK2P(0:4,0:8,0:6)

  dAnmpdbK2P = 0d0

  dAnmpdbK2P(0,0,2) = -b1/42. - b2/18. + bK2/27. + (11*bK2P)/324. - (5*b1*fz)/&
  &72. + (5*bdeta*fz)/72.

  dAnmpdbK2P(0,0,4) = (5*b1)/42. + (2*b2)/9. - (7*bK2)/27. - (19*bK2P)/162. - &
  &(5*bPi2P)/36. + (b1*fz)/24. - (bdeta*fz)/24. - (beta*fz**2)/8. - (beta2*fz*&
  &*2)/8.

  dAnmpdbK2P(0,0,6) = (5*b2)/18. + (10*bK2)/27. + (59*bK2P)/324. + (13*bPi2P)/&
  &36. + (13*b1*fz)/36. - (13*bdeta*fz)/36. + (7*beta*fz**2)/24. + (7*beta2*fz&
  &**2)/24.

  dAnmpdbK2P(0,1,1) = -b1/18.

  dAnmpdbK2P(0,1,3) = b1/9. - (5*b2)/18. + (11*bK2)/27. + (23*bK2P)/162. + (5*&
  &bPi2P)/36. - (7*b1*fz)/72. + (7*bdeta*fz)/72.

  dAnmpdbK2P(0,1,5) = (-17*b2)/18. - (34*bK2)/27. - (79*bK2P)/162. - (5*bPi2P)&
  &/6. - (5*b1*fz)/6. + (5*bdeta*fz)/6. - (3*beta*fz**2)/8. - (3*beta2*fz**2)/&
  &8.

  dAnmpdbK2P(0,2,2) = (-5*b1)/21. + b2/6. - (2*bK2)/9. - (5*bK2P)/54. + (7*b1*&
  &fz)/36. - (7*bdeta*fz)/36.

  dAnmpdbK2P(0,2,4) = (-59*b1)/126. + (7*b2)/9. + (52*bK2)/27. + (215*bK2P)/32&
  &4. + (31*bPi2P)/36. + (5*b1*fz)/9. - (5*bdeta*fz)/9. + (11*beta*fz**2)/24. &
  &+ (11*beta2*fz**2)/24.

  dAnmpdbK2P(0,2,6) = -b2/6. - (2*bK2)/9. - (13*bK2P)/54. - (11*bPi2P)/18. - (&
  &11*b1*fz)/18. + (11*bdeta*fz)/18. - (19*beta*fz**2)/24. - (19*beta2*fz**2)/&
  &24.

  dAnmpdbK2P(0,3,1) = b1/6.

  dAnmpdbK2P(0,3,3) = (29*b1)/63. - b2/6. - (4*bK2)/3. - (11*bK2P)/27. - (7*bP&
  &i2P)/18. - (b1*fz)/36. + (bdeta*fz)/36.

  dAnmpdbK2P(0,3,5) = b2/2. + (2*bK2)/3. + (5*bK2P)/9. + (4*bPi2P)/3. + (4*b1*&
  &fz)/3. - (4*bdeta*fz)/3. + (23*beta*fz**2)/24. + (23*beta2*fz**2)/24.

  dAnmpdbK2P(0,4,2) = -b1/14. + bK2/3. + bK2P/12. - (b1*fz)/8. + (bdeta*fz)/8.

  dAnmpdbK2P(0,4,4) = (5*b1)/21. - b2/3. - (7*bK2)/9. - (13*bK2P)/27. - (35*bP&
  &i2P)/36. - (61*b1*fz)/72. + (61*bdeta*fz)/72. - (13*beta*fz**2)/24. - (13*b&
  &eta2*fz**2)/24.

  dAnmpdbK2P(0,4,6) = bK2P/12. + bPi2P/4. + (b1*fz)/4. - (bdeta*fz)/4. + (17*b&
  &eta*fz**2)/24. + (17*beta2*fz**2)/24.

  dAnmpdbK2P(0,5,3) = (-5*b1)/21. + bK2/3. + bK2P/6. + bPi2P/4. + (b1*fz)/8. -&
  & (bdeta*fz)/8.

  dAnmpdbK2P(0,5,5) = -bK2P/6. - bPi2P/2. - (b1*fz)/2. + (bdeta*fz)/2. - (19*b&
  &eta*fz**2)/24. - (19*beta2*fz**2)/24.

  dAnmpdbK2P(0,6,4) = bK2P/12. + bPi2P/4. + (b1*fz)/4. - (bdeta*fz)/4. + (5*be&
  &ta*fz**2)/24. + (5*beta2*fz**2)/24.

  dAnmpdbK2P(0,6,6) = (-5*beta*fz**2)/24. - (5*beta2*fz**2)/24.

  dAnmpdbK2P(0,7,5) = (5*beta*fz**2)/24. + (5*beta2*fz**2)/24.

  dAnmpdbK2P(1,0,2) = -b1/14. - b2/6. + bK2/9. - bK2P/54. - (5*bPi2P)/63. + (7&
  &*b1*fz)/36. + (bdeta*fz)/36. - (beta*fz)/126. - (5*beta*fz**2)/24. - (5*bet&
  &a2*fz**2)/36.

  dAnmpdbK2P(1,0,4) = -b1/14. - b2/3. - bK2/9. + (2*bK2P)/27. + (11*bPi2P)/14.&
  & + (b1*fz)/12. - (13*bdeta*fz)/36. + (5*beta*fz)/126. + (5*beta*fz**2)/4. +&
  & (77*beta2*fz**2)/72.

  dAnmpdbK2P(1,0,6) = -b2/6. - (2*bK2)/9. - (13*bK2P)/54. - (11*bPi2P)/18. - (&
  &11*b1*fz)/18. + (11*bdeta*fz)/18. - (19*beta*fz**2)/24. - (19*beta2*fz**2)/&
  &24.

  dAnmpdbK2P(1,1,1) = -b1/6. - (b1*fz)/9. + (beta*fz)/18.

  dAnmpdbK2P(1,1,3) = (-5*b1)/21. + b2/6. + (5*bK2)/9. - bK2P/27. - (7*bPi2P)/&
  &6. - (49*b1*fz)/36. + (29*bdeta*fz)/36. - (beta*fz)/3. - (3*beta*fz**2)/8. &
  &- (5*beta2*fz**2)/36.

  dAnmpdbK2P(1,1,5) = b2/6. + (2*bK2)/9. - (7*bK2P)/27. - (8*bPi2P)/9. - (8*b1&
  &*fz)/9. + (8*bdeta*fz)/9. - (83*beta*fz**2)/24. - (83*beta2*fz**2)/24.

  dAnmpdbK2P(1,2,2) = (2*b1)/7. + b2/2. - (2*bK2)/3. + (34*bPi2P)/63. + (10*b1&
  &*fz)/9. - (bdeta*fz)/2. + (23*beta*fz)/63. + (7*beta*fz**2)/12. + (7*beta2*&
  &fz**2)/18.

  dAnmpdbK2P(1,2,4) = (19*b1)/42. + (4*b2)/3. + (4*bK2)/9. + (65*bK2P)/54. + (&
  &37*bPi2P)/14. + (89*b1*fz)/18. - (32*bdeta*fz)/9. + (3*beta*fz)/14. + (37*b&
  &eta*fz**2)/24. + (133*beta2*fz**2)/72.

  dAnmpdbK2P(1,2,6) = b2/2. + (2*bK2)/3. + (11*bK2P)/9. + (10*bPi2P)/3. + (10*&
  &b1*fz)/3. - (10*bdeta*fz)/3. + (53*beta*fz**2)/8. + (53*beta2*fz**2)/8.

  dAnmpdbK2P(1,3,1) = b1/2. - (beta*fz)/6.

  dAnmpdbK2P(1,3,3) = (11*b1)/21. - (3*b2)/2. - (4*bK2)/3. - (8*bK2P)/9. - (62&
  &*bPi2P)/63. - (47*b1*fz)/18. + (11*bdeta*fz)/6. - (beta*fz)/7. - (beta*fz**&
  &2)/4. - (11*beta2*fz**2)/18.

  dAnmpdbK2P(1,3,5) = (-3*b2)/2. - 2*bK2 - 2*bK2P - 5*bPi2P - 5*b1*fz + 5*bdet&
  &a*fz + (13*beta*fz**2)/24. + (13*beta2*fz**2)/24.

  dAnmpdbK2P(1,4,2) = (-17*b1)/14. + bK2 + bK2P/6. - (5*bPi2P)/21. - (3*b1*fz)&
  &/4. + (bdeta*fz)/4. - (beta*fz)/42. - (3*beta*fz**2)/8. - (beta2*fz**2)/4.

  dAnmpdbK2P(1,4,4) = (-5*b1)/7. + b2 + (7*bK2)/3. + (4*bK2P)/9. + (17*bPi2P)/&
  &42. - (13*b1*fz)/12. + (bdeta*fz)/12. - (beta*fz)/7. - (14*beta*fz**2)/3. -&
  & (115*beta2*fz**2)/24.

  dAnmpdbK2P(1,4,6) = (-5*bK2P)/6. - (5*bPi2P)/2. - (5*b1*fz)/2. + (5*bdeta*fz&
  &)/2. - (245*beta*fz**2)/24. - (245*beta2*fz**2)/24.

  dAnmpdbK2P(1,5,3) = (5*b1)/7. - bK2 + bK2P/3. + (53*bPi2P)/42. + (11*b1*fz)/&
  &4. - (7*bdeta*fz)/4. + (beta*fz)/7. + (5*beta*fz**2)/8. + (3*beta2*fz**2)/4&
  &.

  dAnmpdbK2P(1,5,5) = (5*bK2P)/3. + 5*bPi2P + 5*b1*fz - 5*bdeta*fz + (175*beta&
  &*fz**2)/24. + (175*beta2*fz**2)/24.

  dAnmpdbK2P(1,6,4) = (-5*bK2P)/6. - (5*bPi2P)/2. - (5*b1*fz)/2. + (5*bdeta*fz&
  &)/2. + (15*beta*fz**2)/8. + (15*beta2*fz**2)/8.

  dAnmpdbK2P(1,6,6) = (35*beta*fz**2)/8. + (35*beta2*fz**2)/8.

  dAnmpdbK2P(1,7,5) = (-35*beta*fz**2)/8. - (35*beta2*fz**2)/8.

  dAnmpdbK2P(2,0,2) = bK2P/12. - (5*bPi2P)/21. - (b1*fz)/8. + (bdeta*fz)/8. - &
  &(beta*fz)/42. + (beta*fz**2)/4. + (beta2*fz**2)/18.

  dAnmpdbK2P(2,0,4) = -bK2P/18. - (41*bPi2P)/84. - (b1*fz)/8. + (7*bdeta*fz)/2&
  &4. - (beta*fz)/42. - (47*beta*fz**2)/24. - (119*beta2*fz**2)/72.

  dAnmpdbK2P(2,0,6) = bK2P/12. + bPi2P/4. + (b1*fz)/4. - (bdeta*fz)/4. + (17*b&
  &eta*fz**2)/24. + (17*beta2*fz**2)/24.

  dAnmpdbK2P(2,1,1) = (b1*fz)/3. + (beta*fz)/6. - (beta*fz**2)/9.

  dAnmpdbK2P(2,1,3) = (7*bK2P)/18. + (19*bPi2P)/28. + (43*b1*fz)/24. - (19*bde&
  &ta*fz)/24. + (beta*fz)/7. - (13*beta*fz**2)/36. + (7*beta2*fz**2)/18.

  dAnmpdbK2P(2,1,5) = bK2P/2. + (3*bPi2P)/2. + (3*b1*fz)/2. - (3*bdeta*fz)/2. &
  &+ (59*beta*fz**2)/8. + (59*beta2*fz**2)/8.

  dAnmpdbK2P(2,2,2) = -bK2P/2. + (2*bPi2P)/7. - (17*b1*fz)/12. + (bdeta*fz)/4.&
  & + (2*beta*fz)/21. + (14*beta*fz**2)/9. + (2*beta2*fz**2)/3.

  dAnmpdbK2P(2,2,4) = (-5*bK2P)/4. - (193*bPi2P)/84. - (9*b1*fz)/2. + 3*bdeta*&
  &fz - (beta*fz)/14. + (65*beta*fz**2)/24. + (25*beta2*fz**2)/24.

  dAnmpdbK2P(2,2,6) = (-5*bK2P)/6. - (5*bPi2P)/2. - (5*b1*fz)/2. + (5*bdeta*fz&
  &)/2. - (245*beta*fz**2)/24. - (245*beta2*fz**2)/24.

  dAnmpdbK2P(2,3,1) = (-2*b1*fz)/3. - (beta*fz)/2.

  dAnmpdbK2P(2,3,3) = bK2P/9. - (13*bPi2P)/42. - (b1*fz)/4. - (bdeta*fz)/12. -&
  & (5*beta*fz)/7. - (143*beta*fz**2)/18. - (64*beta2*fz**2)/9.

  dAnmpdbK2P(2,3,5) = (5*bK2P)/9. + (5*bPi2P)/3. + (5*b1*fz)/3. - (5*bdeta*fz)&
  &/3. - (1085*beta*fz**2)/72. - (1085*beta2*fz**2)/72.

  dAnmpdbK2P(2,4,2) = (23*bK2P)/36. + (13*bPi2P)/21. + (77*b1*fz)/24. - (25*bd&
  &eta*fz)/24. + (13*beta*fz)/14. - (5*beta*fz**2)/4. - (beta2*fz**2)/2.

  dAnmpdbK2P(2,4,4) = (5*bK2P)/3. + (325*bPi2P)/84. + (145*b1*fz)/24. - (35*bd&
  &eta*fz)/8. + (3*beta*fz)/7. + (1205*beta*fz**2)/72. + (1295*beta2*fz**2)/72&
  &.

  dAnmpdbK2P(2,4,6) = (35*bK2P)/36. + (35*bPi2P)/12. + (35*b1*fz)/12. - (35*bd&
  &eta*fz)/12. + (1645*beta*fz**2)/72. + (1645*beta2*fz**2)/72.

  dAnmpdbK2P(2,5,3) = (-25*bK2P)/18. - (85*bPi2P)/28. - (125*b1*fz)/24. + (85*&
  &bdeta*fz)/24. - (3*beta*fz)/7. + (85*beta*fz**2)/12. + (35*beta2*fz**2)/6.

  dAnmpdbK2P(2,5,5) = (-35*bK2P)/18. - (35*bPi2P)/6. - (35*b1*fz)/6. + (35*bde&
  &ta*fz)/6. - (455*beta*fz**2)/72. - (455*beta2*fz**2)/72.

  dAnmpdbK2P(2,6,4) = (35*bK2P)/36. + (35*bPi2P)/12. + (35*b1*fz)/12. - (35*bd&
  &eta*fz)/12. - (385*beta*fz**2)/24. - (385*beta2*fz**2)/24.

  dAnmpdbK2P(2,6,6) = (-105*beta*fz**2)/8. - (105*beta2*fz**2)/8.

  dAnmpdbK2P(2,7,5) = (105*beta*fz**2)/8. + (105*beta2*fz**2)/8.

  dAnmpdbK2P(3,0,2) = -(beta*fz**2)/24. + (beta2*fz**2)/12.

  dAnmpdbK2P(3,0,4) = (5*beta*fz**2)/6. + (17*beta2*fz**2)/24.

  dAnmpdbK2P(3,0,6) = (-5*beta*fz**2)/24. - (5*beta2*fz**2)/24.

  dAnmpdbK2P(3,1,1) = (beta*fz**2)/3.

  dAnmpdbK2P(3,1,3) = (5*beta*fz**2)/8. - (beta2*fz**2)/4.

  dAnmpdbK2P(3,1,5) = (-85*beta*fz**2)/24. - (85*beta2*fz**2)/24.

  dAnmpdbK2P(3,2,2) = (-9*beta*fz**2)/4. - (3*beta2*fz**2)/2.

  dAnmpdbK2P(3,2,4) = (-35*beta*fz**2)/8. - (25*beta2*fz**2)/8.

  dAnmpdbK2P(3,2,6) = (35*beta*fz**2)/8. + (35*beta2*fz**2)/8.

  dAnmpdbK2P(3,3,1) = (-2*beta*fz**2)/3.

  dAnmpdbK2P(3,3,3) = (85*beta*fz**2)/12. + (15*beta2*fz**2)/2.

  dAnmpdbK2P(3,3,5) = (105*beta*fz**2)/8. + (105*beta2*fz**2)/8.

  dAnmpdbK2P(3,4,2) = (95*beta*fz**2)/24. + (25*beta2*fz**2)/12.

  dAnmpdbK2P(3,4,4) = (-35*beta*fz**2)/4. - (245*beta2*fz**2)/24.

  dAnmpdbK2P(3,4,6) = (-105*beta*fz**2)/8. - (105*beta2*fz**2)/8.

  dAnmpdbK2P(3,5,3) = (-91*beta*fz**2)/8. - (119*beta2*fz**2)/12.

  dAnmpdbK2P(3,5,5) = (-21*beta*fz**2)/8. - (21*beta2*fz**2)/8.

  dAnmpdbK2P(3,6,4) = (133*beta*fz**2)/8. + (133*beta2*fz**2)/8.

  dAnmpdbK2P(3,6,6) = (77*beta*fz**2)/8. + (77*beta2*fz**2)/8.

  dAnmpdbK2P(3,7,5) = (-77*beta*fz**2)/8. - (77*beta2*fz**2)/8.

  return
  
END SUBROUTINE calculate_dAnmpdbK2P
  
!-----------------------------------------------------------------------------

SUBROUTINE calculate_dAnmpdbdeta(fz,b1,b2,bK2,beta,bPi2P,bK2P,bdeta,beta2,dAnmpdbdeta)

  IMPLICIT none

  DOUBLE PRECISION, intent(IN) :: fz,b1,beta,b2,bK2,bPi2P,bK2P,bdeta,beta2
  DOUBLE PRECISION, intent(OUT) :: dAnmpdbdeta(0:4,0:8,0:6)

  dAnmpdbdeta = 0d0

  dAnmpdbdeta(0,0,2) = (-3*b1*fz)/28. - (b2*fz)/4. + (bK2*fz)/6. + (5*bK2P*fz)&
  &/72. - (3*b1*fz**2)/16. + (3*bdeta*fz**2)/16.

  dAnmpdbdeta(0,0,4) = (-3*b1*fz)/14. - (3*b2*fz)/4. - (bK2P*fz)/24. - (3*bPi2&
  &P*fz)/8. - (3*b1*fz**2)/4. + (3*bdeta*fz**2)/4. - (5*beta*fz**3)/16. - (5*b&
  &eta2*fz**3)/16.

  dAnmpdbdeta(0,0,6) = -(b2*fz)/2. - (2*bK2*fz)/3. - (13*bK2P*fz)/36. - (3*bPi&
  &2P*fz)/4. - (3*b1*fz**2)/4. + (3*bdeta*fz**2)/4. - (5*beta*fz**3)/8. - (5*b&
  &eta2*fz**3)/8.

  dAnmpdbdeta(0,1,1) = -(b1*fz)/4.

  dAnmpdbdeta(0,1,3) = (-2*b1*fz)/7. + b2*fz + (bK2*fz)/3. + (7*bK2P*fz)/72. +&
  & (3*bPi2P*fz)/8. + (3*b1*fz**2)/4. - (3*bdeta*fz**2)/4.

  dAnmpdbdeta(0,1,5) = (3*b2*fz)/2. + 2*bK2*fz + (5*bK2P*fz)/6. + (3*bPi2P*fz)&
  &/2. + (3*b1*fz**2)/2. - (3*bdeta*fz**2)/2. + (5*beta*fz**3)/8. + (5*beta2*f&
  &z**3)/8.

  dAnmpdbdeta(0,2,2) = (27*b1*fz)/28. + (b2*fz)/4. - (2*bK2*fz)/3. - (7*bK2P*f&
  &z)/36. + (3*b1*fz**2)/8. - (3*bdeta*fz**2)/8.

  dAnmpdbdeta(0,2,4) = (13*b1*fz)/14. - (b2*fz)/4. - (7*bK2*fz)/3. - (5*bK2P*f&
  &z)/9. + (3*b1*fz**2)/4. - (3*bdeta*fz**2)/4. + (15*beta*fz**3)/16. + (15*be&
  &ta2*fz**3)/16.

  dAnmpdbdeta(0,2,6) = (b2*fz)/2. + (2*bK2*fz)/3. + (11*bK2P*fz)/18. + (3*bPi2&
  &P*fz)/2. + (3*b1*fz**2)/2. - (3*bdeta*fz**2)/2. + (15*beta*fz**3)/8. + (15*&
  &beta2*fz**3)/8.

  dAnmpdbdeta(0,3,1) = (b1*fz)/4.

  dAnmpdbdeta(0,3,3) = (-3*b1*fz)/7. - b2*fz + (2*bK2*fz)/3. + (bK2P*fz)/36. -&
  & (3*bPi2P*fz)/4. - (3*b1*fz**2)/2. + (3*bdeta*fz**2)/2.

  dAnmpdbdeta(0,3,5) = (-3*b2*fz)/2. - 2*bK2*fz - (4*bK2P*fz)/3. - 3*bPi2P*fz &
  &- 3*b1*fz**2 + 3*bdeta*fz**2 - (15*beta*fz**3)/8. - (15*beta2*fz**3)/8.

  dAnmpdbdeta(0,4,2) = (-6*b1*fz)/7. + (bK2*fz)/2. + (bK2P*fz)/8. - (3*b1*fz**&
  &2)/16. + (3*bdeta*fz**2)/16.

  dAnmpdbdeta(0,4,4) = (-5*b1*fz)/7. + b2*fz + (7*bK2*fz)/3. + (61*bK2P*fz)/72&
  &. + (9*bPi2P*fz)/8. + (3*b1*fz**2)/4. - (3*bdeta*fz**2)/4. - (15*beta*fz**3&
  &)/16. - (15*beta2*fz**3)/16.

  dAnmpdbdeta(0,4,6) = -(bK2P*fz)/4. - (3*bPi2P*fz)/4. - (3*b1*fz**2)/4. + (3*&
  &bdeta*fz**2)/4. - (15*beta*fz**3)/8. - (15*beta2*fz**3)/8.

  dAnmpdbdeta(0,5,3) = (5*b1*fz)/7. - bK2*fz - (bK2P*fz)/8. + (3*bPi2P*fz)/8. &
  &+ (3*b1*fz**2)/4. - (3*bdeta*fz**2)/4.

  dAnmpdbdeta(0,5,5) = (bK2P*fz)/2. + (3*bPi2P*fz)/2. + (3*b1*fz**2)/2. - (3*b&
  &deta*fz**2)/2. + (15*beta*fz**3)/8. + (15*beta2*fz**3)/8.

  dAnmpdbdeta(0,6,4) = -(bK2P*fz)/4. - (3*bPi2P*fz)/4. - (3*b1*fz**2)/4. + (3*&
  &bdeta*fz**2)/4. + (5*beta*fz**3)/16. + (5*beta2*fz**3)/16.

  dAnmpdbdeta(0,6,6) = (5*beta*fz**3)/8. + (5*beta2*fz**3)/8.

  dAnmpdbdeta(0,7,5) = (-5*beta*fz**3)/8. - (5*beta2*fz**3)/8.

  dAnmpdbdeta(1,0,2) = (-3*b1*fz)/28. - (b2*fz)/4. + (bK2*fz)/6. + (bK2P*fz)/3&
  &6. - (5*bPi2P*fz)/14. - (3*b1*fz**2)/8. + (bdeta*fz**2)/8. - (beta*fz**2)/2&
  &8. - (9*beta*fz**3)/16. - (3*beta2*fz**3)/8.

  dAnmpdbdeta(1,0,4) = (3*b1*fz)/14. + (b2*fz)/4. - (2*bK2*fz)/3. - (13*bK2P*f&
  &z)/36. - (41*bPi2P*fz)/28. + (bdeta*fz**2)/2. - (beta*fz**2)/14. - (33*beta&
  &*fz**3)/16. - (27*beta2*fz**3)/16.

  dAnmpdbdeta(1,0,6) = (b2*fz)/2. + (2*bK2*fz)/3. + (11*bK2P*fz)/18. + (3*bPi2&
  &P*fz)/2. + (3*b1*fz**2)/2. - (3*bdeta*fz**2)/2. + (15*beta*fz**3)/8. + (15*&
  &beta2*fz**3)/8.

  dAnmpdbdeta(1,1,1) = -(b1*fz)/4. + (3*b1*fz**2)/4. + (beta*fz**2)/4.

  dAnmpdbdeta(1,1,3) = (5*b1*fz)/7. + b2*fz + (bK2*fz)/3. + (29*bK2P*fz)/36. +&
  & (83*bPi2P*fz)/28. + (9*b1*fz**2)/2. - (5*bdeta*fz**2)/2. + (4*beta*fz**2)/&
  &7. + (39*beta*fz**3)/8. + (9*beta2*fz**3)/2.

  dAnmpdbdeta(1,1,5) = -(b2*fz)/2. - (2*bK2*fz)/3. + (8*bK2P*fz)/9. + 3*bPi2P*&
  &fz + 3*b1*fz**2 - 3*bdeta*fz**2 + (75*beta*fz**3)/8. + (75*beta2*fz**3)/8.

  dAnmpdbdeta(1,2,2) = (15*b1*fz)/28. - (3*b2*fz)/4. - (bK2P*fz)/2. - (11*bPi2&
  &P*fz)/14. - (9*b1*fz**2)/2. + (7*bdeta*fz**2)/4. - (19*beta*fz**2)/28. + (9&
  &*beta*fz**3)/8. + (3*beta2*fz**3)/4.

  dAnmpdbdeta(1,2,4) = (-19*b1*fz)/14. - (13*b2*fz)/4. - (bK2*fz)/3. - (32*bK2&
  &P*fz)/9. - (127*bPi2P*fz)/14. - 15*b1*fz**2 + (23*bdeta*fz**2)/2. - (5*beta&
  &*fz**2)/14. - (159*beta*fz**3)/16. - (171*beta2*fz**3)/16.

  dAnmpdbdeta(1,2,6) = (-3*b2*fz)/2. - 2*bK2*fz - (10*bK2P*fz)/3. - 9*bPi2P*fz&
  & - 9*b1*fz**2 + 9*bdeta*fz**2 - (135*beta*fz**3)/8. - (135*beta2*fz**3)/8.

  dAnmpdbdeta(1,3,1) = (-3*b1*fz)/4. - (3*b1*fz**2)/4. - (beta*fz**2)/4.

  dAnmpdbdeta(1,3,3) = (-11*b1*fz)/7. + 3*b2*fz + 2*bK2*fz + (11*bK2P*fz)/6. +&
  & (43*bPi2P*fz)/14. + 6*b1*fz**2 - 5*bdeta*fz**2 - (beta*fz**2)/7. - (39*bet&
  &a*fz**3)/4. - 9*beta2*fz**3

  dAnmpdbdeta(1,3,5) = (9*b2*fz)/2. + 6*bK2*fz + 5*bK2P*fz + 12*bPi2P*fz + 12*&
  &b1*fz**2 - 12*bdeta*fz**2 - (45*beta*fz**3)/8. - (45*beta2*fz**3)/8.

  dAnmpdbdeta(1,4,2) = (18*b1*fz)/7. - (3*bK2*fz)/2. + (bK2P*fz)/4. + (8*bPi2P&
  &*fz)/7. + (39*b1*fz**2)/8. - (15*bdeta*fz**2)/8. + (5*beta*fz**2)/7. - (9*b&
  &eta*fz**3)/16. - (3*beta2*fz**3)/8.

  dAnmpdbdeta(1,4,4) = (15*b1*fz)/7. - 3*b2*fz - 7*bK2*fz + (bK2P*fz)/12. + (8&
  &5*bPi2P*fz)/28. + (15*b1*fz**2)/2. - (9*bdeta*fz**2)/2. + (3*beta*fz**2)/7.&
  & + (417*beta*fz**3)/16. + (423*beta2*fz**3)/16.

  dAnmpdbdeta(1,4,6) = (5*bK2P*fz)/2. + (15*bPi2P*fz)/2. + (15*b1*fz**2)/2. - &
  &(15*bdeta*fz**2)/2. + (225*beta*fz**3)/8. + (225*beta2*fz**3)/8.

  dAnmpdbdeta(1,5,3) = (-15*b1*fz)/7. + 3*bK2*fz - (7*bK2P*fz)/4. - (169*bPi2P&
  &*fz)/28. - (21*b1*fz**2)/2. + (15*bdeta*fz**2)/2. - (3*beta*fz**2)/7. + (39&
  &*beta*fz**3)/8. + (9*beta2*fz**3)/2.

  dAnmpdbdeta(1,5,5) = -5*bK2P*fz - 15*bPi2P*fz - 15*b1*fz**2 + 15*bdeta*fz**2&
  & - (135*beta*fz**3)/8. - (135*beta2*fz**3)/8.

  dAnmpdbdeta(1,6,4) = (5*bK2P*fz)/2. + (15*bPi2P*fz)/2. + (15*b1*fz**2)/2. - &
  &(15*bdeta*fz**2)/2. - (225*beta*fz**3)/16. - (225*beta2*fz**3)/16.

  dAnmpdbdeta(1,6,6) = (-105*beta*fz**3)/8. - (105*beta2*fz**3)/8.

  dAnmpdbdeta(1,7,5) = (105*beta*fz**3)/8. + (105*beta2*fz**3)/8.

  dAnmpdbdeta(2,0,2) = (bK2P*fz)/8. - (5*bPi2P*fz)/14. + (9*b1*fz**2)/16. + (3&
  &*bdeta*fz**2)/16. - (beta*fz**2)/28. - (beta*fz**3)/8. + (beta2*fz**3)/4.

  dAnmpdbdeta(2,0,4) = (7*bK2P*fz)/24. + (103*bPi2P*fz)/56. + (3*b1*fz**2)/4. &
  &- (5*bdeta*fz**2)/4. + (beta*fz**2)/14. + (81*beta*fz**3)/16. + (69*beta2*f&
  &z**3)/16.

  dAnmpdbdeta(2,0,6) = -(bK2P*fz)/4. - (3*bPi2P*fz)/4. - (3*b1*fz**2)/4. + (3*&
  &bdeta*fz**2)/4. - (15*beta*fz**3)/8. - (15*beta2*fz**3)/8.

  dAnmpdbdeta(2,1,1) = -(b1*fz**2)/4. + (beta*fz**2)/4. + (3*beta*fz**3)/4.

  dAnmpdbdeta(2,1,3) = (-19*bK2P*fz)/24. - (51*bPi2P*fz)/56. - (17*b1*fz**2)/4&
  &. + (5*bdeta*fz**2)/4. - (3*beta*fz**2)/7. - (3*beta*fz**3)/4. - 3*beta2*fz&
  &**3

  dAnmpdbdeta(2,1,5) = (-3*bK2P*fz)/2. - (9*bPi2P*fz)/2. - (9*b1*fz**2)/2. + (&
  &9*bdeta*fz**2)/2. - (165*beta*fz**3)/8. - (165*beta2*fz**3)/8.

  dAnmpdbdeta(2,2,2) = (bK2P*fz)/4. - (3*bPi2P*fz)/14. + (13*b1*fz**2)/8. - (3&
  &*bdeta*fz**2)/8. - (23*beta*fz**2)/28. - 7*beta*fz**3 - 4*beta2*fz**3

  dAnmpdbdeta(2,2,4) = 3*bK2P*fz + (65*bPi2P*fz)/14. + (45*b1*fz**2)/4. - (27*&
  &bdeta*fz**2)/4. + (3*beta*fz**2)/14. - (111*beta*fz**3)/16. - (39*beta2*fz*&
  &*3)/16.

  dAnmpdbdeta(2,2,6) = (5*bK2P*fz)/2. + (15*bPi2P*fz)/2. + (15*b1*fz**2)/2. - &
  &(15*bdeta*fz**2)/2. + (225*beta*fz**3)/8. + (225*beta2*fz**3)/8.

  dAnmpdbdeta(2,3,1) = (5*b1*fz**2)/4. + (3*beta*fz**2)/4. - (3*beta*fz**3)/4.

  dAnmpdbdeta(2,3,3) = -(bK2P*fz)/12. + (47*bPi2P*fz)/28. + (3*b1*fz**2)/2. - &
  &(bdeta*fz**2)/2. + (15*beta*fz**2)/7. + (59*beta*fz**3)/2. + 28*beta2*fz**3

  dAnmpdbdeta(2,3,5) = (-5*bK2P*fz)/3. - 5*bPi2P*fz - 5*b1*fz**2 + 5*bdeta*fz*&
  &*2 + (375*beta*fz**3)/8. + (375*beta2*fz**3)/8.

  dAnmpdbdeta(2,4,2) = (-25*bK2P*fz)/24. - (10*bPi2P*fz)/7. - (115*b1*fz**2)/1&
  &6. + (35*bdeta*fz**2)/16. - (15*beta*fz**2)/7. + (57*beta*fz**3)/8. + (15*b&
  &eta2*fz**3)/4.

  dAnmpdbdeta(2,4,4) = (-35*bK2P*fz)/8. - (545*bPi2P*fz)/56. - (65*b1*fz**2)/4&
  &. + (45*bdeta*fz**2)/4. - (9*beta*fz**2)/7. - (845*beta*fz**3)/16. - (905*b&
  &eta2*fz**3)/16.

  dAnmpdbdeta(2,4,6) = (-35*bK2P*fz)/12. - (35*bPi2P*fz)/4. - (35*b1*fz**2)/4.&
  & + (35*bdeta*fz**2)/4. - (525*beta*fz**3)/8. - (525*beta2*fz**3)/8.

  dAnmpdbdeta(2,5,3) = (85*bK2P*fz)/24. + (405*bPi2P*fz)/56. + (55*b1*fz**2)/4&
  &. - (35*bdeta*fz**2)/4. + (9*beta*fz**2)/7. - (115*beta*fz**3)/4. - 25*beta&
  &2*fz**3

  dAnmpdbdeta(2,5,5) = (35*bK2P*fz)/6. + (35*bPi2P*fz)/2. + (35*b1*fz**2)/2. -&
  & (35*bdeta*fz**2)/2. + (105*beta*fz**3)/8. + (105*beta2*fz**3)/8.

  dAnmpdbdeta(2,6,4) = (-35*bK2P*fz)/12. - (35*bPi2P*fz)/4. - (35*b1*fz**2)/4.&
  & + (35*bdeta*fz**2)/4. + (875*beta*fz**3)/16. + (875*beta2*fz**3)/16.

  dAnmpdbdeta(2,6,6) = (315*beta*fz**3)/8. + (315*beta2*fz**3)/8.

  dAnmpdbdeta(2,7,5) = (-315*beta*fz**3)/8. - (315*beta2*fz**3)/8.

  dAnmpdbdeta(3,0,2) = (11*beta*fz**3)/16. + (beta2*fz**3)/8.

  dAnmpdbdeta(3,0,4) = (-43*beta*fz**3)/16. - (37*beta2*fz**3)/16.

  dAnmpdbdeta(3,0,6) = (5*beta*fz**3)/8. + (5*beta2*fz**3)/8.

  dAnmpdbdeta(3,1,1) = -(beta*fz**3)/4.

  dAnmpdbdeta(3,1,3) = (-33*beta*fz**3)/8. - (3*beta2*fz**3)/2.

  dAnmpdbdeta(3,1,5) = (85*beta*fz**3)/8. + (85*beta2*fz**3)/8.

  dAnmpdbdeta(3,2,2) = (27*beta*fz**3)/8. + (9*beta2*fz**3)/4.

  dAnmpdbdeta(3,2,4) = (255*beta*fz**3)/16. + (195*beta2*fz**3)/16.

  dAnmpdbdeta(3,2,6) = (-105*beta*fz**3)/8. - (105*beta2*fz**3)/8.

  dAnmpdbdeta(3,3,1) = (5*beta*fz**3)/4.

  dAnmpdbdeta(3,3,3) = (-55*beta*fz**3)/4. - 15*beta2*fz**3

  dAnmpdbdeta(3,3,5) = (-315*beta*fz**3)/8. - (315*beta2*fz**3)/8.

  dAnmpdbdeta(3,4,2) = (-145*beta*fz**3)/16. - (35*beta2*fz**3)/8.

  dAnmpdbdeta(3,4,4) = (315*beta*fz**3)/16. + (385*beta2*fz**3)/16.

  dAnmpdbdeta(3,4,6) = (315*beta*fz**3)/8. + (315*beta2*fz**3)/8.

  dAnmpdbdeta(3,5,3) = (231*beta*fz**3)/8. + (49*beta2*fz**3)/2.

  dAnmpdbdeta(3,5,5) = (63*beta*fz**3)/8. + (63*beta2*fz**3)/8.

  dAnmpdbdeta(3,6,4) = (-735*beta*fz**3)/16. - (735*beta2*fz**3)/16.

  dAnmpdbdeta(3,6,6) = (-231*beta*fz**3)/8. - (231*beta2*fz**3)/8.

  dAnmpdbdeta(3,7,5) = (231*beta*fz**3)/8. + (231*beta2*fz**3)/8.

  return
  
END SUBROUTINE calculate_dAnmpdbdeta
  
!-----------------------------------------------------------------------------

SUBROUTINE calculate_dAnmpdbeta2(fz,b1,b2,bK2,beta,bPi2P,bK2P,bdeta,beta2,dAnmpdbeta2)

  IMPLICIT none

  DOUBLE PRECISION, intent(IN) :: fz,b1,beta,b2,bK2,bPi2P,bK2P,bdeta,beta2
  DOUBLE PRECISION, intent(OUT) :: dAnmpdbeta2(0:4,0:8,0:6)

  dAnmpdbeta2 = 0d0

  dAnmpdbeta2(0,0,4) = (9*b1*fz**2)/56. + (3*b2*fz**2)/8. - (bK2*fz**2)/4. - (&
  &bK2P*fz**2)/8. + (5*b1*fz**3)/16. - (5*bdeta*fz**3)/16.

  dAnmpdbeta2(0,0,6) = (3*b2*fz**2)/8. + (bK2*fz**2)/2. + (7*bK2P*fz**2)/24. +&
  & (5*bPi2P*fz**2)/8. + (5*b1*fz**3)/8. - (5*bdeta*fz**3)/8. + (35*beta*fz**4&
  &)/64. + (35*beta2*fz**4)/64.

  dAnmpdbeta2(0,1,3) = (3*b1*fz**2)/8.

  dAnmpdbeta2(0,1,5) = (-3*b2*fz**2)/4. - bK2*fz**2 - (3*bK2P*fz**2)/8. - (5*b&
  &Pi2P*fz**2)/8. - (5*b1*fz**3)/8. + (5*bdeta*fz**3)/8.

  dAnmpdbeta2(0,2,4) = (-6*b1*fz**2)/7. - (3*b2*fz**2)/4. + (5*bK2*fz**2)/4. +&
  & (11*bK2P*fz**2)/24. - (15*b1*fz**3)/16. + (15*bdeta*fz**3)/16.

  dAnmpdbeta2(0,2,6) = (-3*b2*fz**2)/4. - bK2*fz**2 - (19*bK2P*fz**2)/24. - (1&
  &5*bPi2P*fz**2)/8. - (15*b1*fz**3)/8. + (15*bdeta*fz**3)/8. - (35*beta*fz**4&
  &)/16. - (35*beta2*fz**4)/16.

  dAnmpdbeta2(0,3,3) = (-3*b1*fz**2)/4.

  dAnmpdbeta2(0,3,5) = (3*b2*fz**2)/2. + 2*bK2*fz**2 + (23*bK2P*fz**2)/24. + (&
  &15*bPi2P*fz**2)/8. + (15*b1*fz**3)/8. - (15*bdeta*fz**3)/8.

  dAnmpdbeta2(0,4,4) = (69*b1*fz**2)/56. + (3*b2*fz**2)/8. - (7*bK2*fz**2)/4. &
  &- (13*bK2P*fz**2)/24. + (15*b1*fz**3)/16. - (15*bdeta*fz**3)/16.

  dAnmpdbeta2(0,4,6) = (3*b2*fz**2)/8. + (bK2*fz**2)/2. + (17*bK2P*fz**2)/24. &
  &+ (15*bPi2P*fz**2)/8. + (15*b1*fz**3)/8. - (15*bdeta*fz**3)/8. + (105*beta*&
  &fz**4)/32. + (105*beta2*fz**4)/32.

  dAnmpdbeta2(0,5,3) = (3*b1*fz**2)/8.

  dAnmpdbeta2(0,5,5) = (-3*b2*fz**2)/4. - bK2*fz**2 - (19*bK2P*fz**2)/24. - (1&
  &5*bPi2P*fz**2)/8. - (15*b1*fz**3)/8. + (15*bdeta*fz**3)/8.

  dAnmpdbeta2(0,6,4) = (-15*b1*fz**2)/28. + (3*bK2*fz**2)/4. + (5*bK2P*fz**2)/&
  &24. - (5*b1*fz**3)/16. + (5*bdeta*fz**3)/16.

  dAnmpdbeta2(0,6,6) = (-5*bK2P*fz**2)/24. - (5*bPi2P*fz**2)/8. - (5*b1*fz**3)&
  &/8. + (5*bdeta*fz**3)/8. - (35*beta*fz**4)/16. - (35*beta2*fz**4)/16.

  dAnmpdbeta2(0,7,5) = (5*bK2P*fz**2)/24. + (5*bPi2P*fz**2)/8. + (5*b1*fz**3)/&
  &8. - (5*bdeta*fz**3)/8.

  dAnmpdbeta2(0,8,6) = (35*beta*fz**4)/64. + (35*beta2*fz**4)/64.

  dAnmpdbeta2(1,0,2) = (3*b1*fz**2)/14. + (b2*fz**2)/2. - (bK2*fz**2)/3. - (5*&
  &bK2P*fz**2)/36. + (9*b1*fz**3)/8. - (3*bdeta*fz**3)/8.

  dAnmpdbeta2(1,0,4) = (-9*b1*fz**2)/28. - (b2*fz**2)/4. + (7*bK2*fz**2)/6. + &
  &(77*bK2P*fz**2)/72. + (39*bPi2P*fz**2)/14. + (21*b1*fz**3)/16. - (27*bdeta*&
  &fz**3)/16. + (3*beta*fz**3)/56. + (65*beta*fz**4)/16. + (15*beta2*fz**4)/4.

  dAnmpdbeta2(1,0,6) = (-3*b2*fz**2)/4. - bK2*fz**2 - (19*bK2P*fz**2)/24. - (1&
  &5*bPi2P*fz**2)/8. - (15*b1*fz**3)/8. + (15*bdeta*fz**3)/8. - (35*beta*fz**4&
  &)/16. - (35*beta2*fz**4)/16.

  dAnmpdbeta2(1,1,1) = (b1*fz**2)/2.

  dAnmpdbeta2(1,1,3) = (-57*b1*fz**2)/28. - 4*b2*fz**2 + (2*bK2*fz**2)/3. - (5&
  &*bK2P*fz**2)/36. - (9*bPi2P*fz**2)/4. - (63*b1*fz**3)/8. + (9*bdeta*fz**3)/&
  &2. - (3*beta*fz**3)/8.

  dAnmpdbeta2(1,1,5) = (-3*b2*fz**2)/2. - 2*bK2*fz**2 - (83*bK2P*fz**2)/24. - &
  &(75*bPi2P*fz**2)/8. - (75*b1*fz**3)/8. + (75*bdeta*fz**3)/8. - (35*beta*fz*&
  &*4)/2. - (35*beta2*fz**4)/2.

  dAnmpdbeta2(1,2,2) = (-55*b1*fz**2)/14. - (b2*fz**2)/2. + (4*bK2*fz**2)/3. +&
  & (7*bK2P*fz**2)/18. - (9*b1*fz**3)/4. + (3*bdeta*fz**3)/4.

  dAnmpdbeta2(1,2,4) = 3*b1*fz**2 + 10*b2*fz**2 + (17*bK2*fz**2)/6. + (133*bK2&
  &P*fz**2)/72. + (36*bPi2P*fz**2)/7. + (243*b1*fz**3)/16. - (171*bdeta*fz**3)&
  &/16. + (3*beta*fz**3)/14. - (195*beta*fz**4)/16. - (45*beta2*fz**4)/4.

  dAnmpdbeta2(1,2,6) = (9*b2*fz**2)/2. + 6*bK2*fz**2 + (53*bK2P*fz**2)/8. + (1&
  &35*bPi2P*fz**2)/8. + (135*b1*fz**3)/8. - (135*bdeta*fz**3)/8. + (105*beta*f&
  &z**4)/4. + (105*beta2*fz**4)/4.

  dAnmpdbeta2(1,3,1) = -(b1*fz**2)/2.

  dAnmpdbeta2(1,3,3) = (141*b1*fz**2)/14. + 4*b2*fz**2 - (20*bK2*fz**2)/3. - (&
  &11*bK2P*fz**2)/18. + (9*bPi2P*fz**2)/2. + (63*b1*fz**3)/4. - 9*bdeta*fz**3 &
  &+ (3*beta*fz**3)/4.

  dAnmpdbeta2(1,3,5) = -6*b2*fz**2 - 8*bK2*fz**2 + (13*bK2P*fz**2)/24. + (45*b&
  &Pi2P*fz**2)/8. + (45*b1*fz**3)/8. - (45*bdeta*fz**3)/8. + (105*beta*fz**4)/&
  &2. + (105*beta2*fz**4)/2.

  dAnmpdbeta2(1,4,2) = (26*b1*fz**2)/7. - bK2*fz**2 - (bK2P*fz**2)/4. + (9*b1*&
  &fz**3)/8. - (3*bdeta*fz**3)/8.

  dAnmpdbeta2(1,4,4) = (-225*b1*fz**2)/28. - (39*b2*fz**2)/4. + (7*bK2*fz**2)/&
  &2. - (115*bK2P*fz**2)/24. - (261*bPi2P*fz**2)/14. - (549*b1*fz**3)/16. + (4&
  &23*bdeta*fz**3)/16. - (33*beta*fz**3)/56. + (195*beta*fz**4)/16. + (45*beta&
  &2*fz**4)/4.

  dAnmpdbeta2(1,4,6) = (-15*b2*fz**2)/4. - 5*bK2*fz**2 - (245*bK2P*fz**2)/24. &
  &- (225*bPi2P*fz**2)/8. - (225*b1*fz**3)/8. + (225*bdeta*fz**3)/8. - (525*be&
  &ta*fz**4)/8. - (525*beta2*fz**4)/8.

  dAnmpdbeta2(1,5,3) = (-225*b1*fz**2)/28. + 6*bK2*fz**2 + (3*bK2P*fz**2)/4. -&
  & (9*bPi2P*fz**2)/4. - (63*b1*fz**3)/8. + (9*bdeta*fz**3)/2. - (3*beta*fz**3&
  &)/8.

  dAnmpdbeta2(1,5,5) = (15*b2*fz**2)/2. + 10*bK2*fz**2 + (175*bK2P*fz**2)/24. &
  &+ (135*bPi2P*fz**2)/8. + (135*b1*fz**3)/8. - (135*bdeta*fz**3)/8. - (105*be&
  &ta*fz**4)/2. - (105*beta2*fz**4)/2.

  dAnmpdbeta2(1,6,4) = (75*b1*fz**2)/14. - (15*bK2*fz**2)/2. + (15*bK2P*fz**2)&
  &/8. + (75*bPi2P*fz**2)/7. + (285*b1*fz**3)/16. - (225*bdeta*fz**3)/16. + (9&
  &*beta*fz**3)/28. - (65*beta*fz**4)/16. - (15*beta2*fz**4)/4.

  dAnmpdbeta2(1,6,6) = (35*bK2P*fz**2)/8. + (105*bPi2P*fz**2)/8. + (105*b1*fz*&
  &*3)/8. - (105*bdeta*fz**3)/8. + (245*beta*fz**4)/4. + (245*beta2*fz**4)/4.

  dAnmpdbeta2(1,7,5) = (-35*bK2P*fz**2)/8. - (105*bPi2P*fz**2)/8. - (105*b1*fz&
  &**3)/8. + (105*bdeta*fz**3)/8. + (35*beta*fz**4)/2. + (35*beta2*fz**4)/2.

  dAnmpdbeta2(1,8,6) = (-315*beta*fz**4)/16. - (315*beta2*fz**4)/16.

  dAnmpdbeta2(2,0,2) = (-3*b1*fz**2)/14. - (b2*fz**2)/2. + (bK2*fz**2)/3. + (b&
  &K2P*fz**2)/18. + (5*bPi2P*fz**2)/7. - (9*b1*fz**3)/4. + (bdeta*fz**3)/4. + &
  &(beta*fz**3)/14. + (15*beta*fz**4)/8. + (3*beta2*fz**4)/4.

  dAnmpdbeta2(2,0,4) = (9*b1*fz**2)/56. - (b2*fz**2)/8. - (11*bK2*fz**2)/12. -&
  & (119*bK2P*fz**2)/72. - (39*bPi2P*fz**2)/7. - (57*b1*fz**3)/16. + (69*bdeta&
  &*fz**3)/16. - (3*beta*fz**3)/28. - (195*beta*fz**4)/16. - (45*beta2*fz**4)/&
  &4.

  dAnmpdbeta2(2,0,6) = (3*b2*fz**2)/8. + (bK2*fz**2)/2. + (17*bK2P*fz**2)/24. &
  &+ (15*bPi2P*fz**2)/8. + (15*b1*fz**3)/8. - (15*bdeta*fz**3)/8. + (105*beta*&
  &fz**4)/32. + (105*beta2*fz**4)/32.

  dAnmpdbeta2(2,1,1) = -(b1*fz**2)/2. - (3*b1*fz**3)/2. - (beta*fz**3)/2.

  dAnmpdbeta2(2,1,3) = (93*b1*fz**2)/56. + 4*b2*fz**2 - (2*bK2*fz**2)/3. + (7*&
  &bK2P*fz**2)/18. - (39*bPi2P*fz**2)/14. + (51*b1*fz**3)/4. - 3*bdeta*fz**3 +&
  & (9*beta*fz**3)/28. - (165*beta*fz**4)/8. - 15*beta2*fz**4

  dAnmpdbeta2(2,1,5) = (9*b2*fz**2)/4. + 3*bK2*fz**2 + (59*bK2P*fz**2)/8. + (1&
  &65*bPi2P*fz**2)/8. + (165*b1*fz**3)/8. - (165*bdeta*fz**3)/8. + (105*beta*f&
  &z**4)/2. + (105*beta2*fz**4)/2.

  dAnmpdbeta2(2,2,2) = (61*b1*fz**2)/14. + (3*b2*fz**2)/2. - 2*bK2*fz**2 + (2*&
  &bK2P*fz**2)/3. + (11*bPi2P*fz**2)/7. + (33*b1*fz**3)/2. - 4*bdeta*fz**3 + (&
  &47*beta*fz**3)/14. - (15*beta*fz**4)/4. - (3*beta2*fz**4)/2.

  dAnmpdbeta2(2,2,4) = (-15*b1*fz**2)/7. - (33*b2*fz**2)/4. - (11*bK2*fz**2)/4&
  &. + (25*bK2P*fz**2)/24. + 12*bPi2P*fz**2 - (153*b1*fz**3)/16. - (39*bdeta*f&
  &z**3)/16. + (1755*beta*fz**4)/16. + (405*beta2*fz**4)/4.

  dAnmpdbeta2(2,2,6) = (-15*b2*fz**2)/4. - 5*bK2*fz**2 - (245*bK2P*fz**2)/24. &
  &- (225*bPi2P*fz**2)/8. - (225*b1*fz**3)/8. + (225*bdeta*fz**3)/8. - (525*be&
  &ta*fz**4)/8. - (525*beta2*fz**4)/8.

  dAnmpdbeta2(2,3,1) = (3*b1*fz**2)/2. + (3*b1*fz**3)/2. + (beta*fz**3)/2.

  dAnmpdbeta2(2,3,3) = (-285*b1*fz**2)/28. - 8*b2*fz**2 + (16*bK2*fz**2)/3. - &
  &(64*bK2P*fz**2)/9. - (108*bPi2P*fz**2)/7. - (123*b1*fz**3)/2. + 28*bdeta*fz&
  &**3 - (93*beta*fz**3)/14. + (165*beta*fz**4)/4. + 30*beta2*fz**4

  dAnmpdbeta2(2,3,5) = (5*b2*fz**2)/2. + (10*bK2*fz**2)/3. - (1085*bK2P*fz**2)&
  &/72. - (375*bPi2P*fz**2)/8. - (375*b1*fz**3)/8. + (375*bdeta*fz**3)/8. - (5&
  &25*beta*fz**4)/2. - (525*beta2*fz**4)/2.

  dAnmpdbeta2(2,4,2) = (-50*b1*fz**2)/7. + 3*bK2*fz**2 - (bK2P*fz**2)/2. - (16&
  &*bPi2P*fz**2)/7. - (57*b1*fz**3)/4. + (15*bdeta*fz**3)/4. - (24*beta*fz**3)&
  &/7. + (15*beta*fz**4)/8. + (3*beta2*fz**4)/4.

  dAnmpdbeta2(2,4,4) = (405*b1*fz**2)/56. + (115*b2*fz**2)/8. + (35*bK2*fz**2)&
  &/12. + (1295*bK2P*fz**2)/72. + (285*bPi2P*fz**2)/7. + (1365*b1*fz**3)/16. -&
  & (905*bdeta*fz**3)/16. + (93*beta*fz**3)/28. - (2925*beta*fz**4)/16. - (675&
  &*beta2*fz**4)/4.

  dAnmpdbeta2(2,4,6) = (35*b2*fz**2)/8. + (35*bK2*fz**2)/6. + (1645*bK2P*fz**2&
  &)/72. + (525*bPi2P*fz**2)/8. + (525*b1*fz**3)/8. - (525*bdeta*fz**3)/8. + (&
  &3675*beta*fz**4)/16. + (3675*beta2*fz**4)/16.

  dAnmpdbeta2(2,5,3) = (645*b1*fz**2)/56. - 10*bK2*fz**2 + (35*bK2P*fz**2)/6. &
  &+ (255*bPi2P*fz**2)/14. + (195*b1*fz**3)/4. - 25*bdeta*fz**3 + (177*beta*fz&
  &**3)/28. - (165*beta*fz**4)/8. - 15*beta2*fz**4

  dAnmpdbeta2(2,5,5) = (-35*b2*fz**2)/4. - (35*bK2*fz**2)/3. - (455*bK2P*fz**2&
  &)/72. - (105*bPi2P*fz**2)/8. - (105*b1*fz**3)/8. + (105*bdeta*fz**3)/8. + (&
  &735*beta*fz**4)/2. + (735*beta2*fz**4)/2.

  dAnmpdbeta2(2,6,4) = (-25*b1*fz**2)/4. + (35*bK2*fz**2)/4. - (385*bK2P*fz**2&
  &)/24. - (330*bPi2P*fz**2)/7. - (1155*b1*fz**3)/16. + (875*bdeta*fz**3)/16. &
  &- (45*beta*fz**3)/14. + (1365*beta*fz**4)/16. + (315*beta2*fz**4)/4.

  dAnmpdbeta2(2,6,6) = (-105*bK2P*fz**2)/8. - (315*bPi2P*fz**2)/8. - (315*b1*f&
  &z**3)/8. + (315*bdeta*fz**3)/8. - (2205*beta*fz**4)/8. - (2205*beta2*fz**4)&
  &/8.

  dAnmpdbeta2(2,7,5) = (105*bK2P*fz**2)/8. + (315*bPi2P*fz**2)/8. + (315*b1*fz&
  &**3)/8. - (315*bdeta*fz**3)/8. - (315*beta*fz**4)/2. - (315*beta2*fz**4)/2.

  dAnmpdbeta2(2,8,6) = (3465*beta*fz**4)/32. + (3465*beta2*fz**4)/32.

  dAnmpdbeta2(3,0,2) = (bK2P*fz**2)/12. - (5*bPi2P*fz**2)/7. + (9*b1*fz**3)/8.&
  & + (bdeta*fz**3)/8. - (beta*fz**3)/14. - (15*beta*fz**4)/4. - (3*beta2*fz**&
  &4)/2.

  dAnmpdbeta2(3,0,4) = (17*bK2P*fz**2)/24. + (39*bPi2P*fz**2)/14. + (31*b1*fz*&
  &*3)/16. - (37*bdeta*fz**3)/16. + (3*beta*fz**3)/56. + (195*beta*fz**4)/16. &
  &+ (45*beta2*fz**4)/4.

  dAnmpdbeta2(3,0,6) = (-5*bK2P*fz**2)/24. - (5*bPi2P*fz**2)/8. - (5*b1*fz**3)&
  &/8. + (5*bdeta*fz**3)/8. - (35*beta*fz**4)/16. - (35*beta2*fz**4)/16.

  dAnmpdbeta2(3,1,1) = (3*b1*fz**3)/2. + (beta*fz**3)/2. - (3*beta*fz**4)/2.

  dAnmpdbeta2(3,1,3) = -(bK2P*fz**2)/4. + (141*bPi2P*fz**2)/28. - (39*b1*fz**3&
  &)/8. - (3*bdeta*fz**3)/2. + (3*beta*fz**3)/56. + (165*beta*fz**4)/4. + 30*b&
  &eta2*fz**4

  dAnmpdbeta2(3,1,5) = (-85*bK2P*fz**2)/24. - (85*bPi2P*fz**2)/8. - (85*b1*fz*&
  &*3)/8. + (85*bdeta*fz**3)/8. - (105*beta*fz**4)/2. - (105*beta2*fz**4)/2.

  dAnmpdbeta2(3,2,2) = (-3*bK2P*fz**2)/2. - (bPi2P*fz**2)/7. - (57*b1*fz**3)/4&
  &. + (9*bdeta*fz**3)/4. - (45*beta*fz**3)/14. + (45*beta*fz**4)/2. + 9*beta2&
  &*fz**4

  dAnmpdbeta2(3,2,4) = (-25*bK2P*fz**2)/8. - (120*bPi2P*fz**2)/7. - (75*b1*fz*&
  &*3)/16. + (195*bdeta*fz**3)/16. - (3*beta*fz**3)/14. - (2925*beta*fz**4)/16&
  &. - (675*beta2*fz**4)/4.

  dAnmpdbeta2(3,2,6) = (35*bK2P*fz**2)/8. + (105*bPi2P*fz**2)/8. + (105*b1*fz*&
  &*3)/8. - (105*bdeta*fz**3)/8. + (245*beta*fz**4)/4. + (245*beta2*fz**4)/4.

  dAnmpdbeta2(3,3,1) = (-5*b1*fz**3)/2. - (3*beta*fz**3)/2. + (3*beta*fz**4)/2&
  &.

  dAnmpdbeta2(3,3,3) = (15*bK2P*fz**2)/2. + (85*bPi2P*fz**2)/14. + (175*b1*fz*&
  &*3)/4. - 15*bdeta*fz**3 + (157*beta*fz**3)/28. - (275*beta*fz**4)/2. - 100*&
  &beta2*fz**4

  dAnmpdbeta2(3,3,5) = (105*bK2P*fz**2)/8. + (315*bPi2P*fz**2)/8. + (315*b1*fz&
  &**3)/8. - (315*bdeta*fz**3)/8. + (735*beta*fz**4)/2. + (735*beta2*fz**4)/2.

  dAnmpdbeta2(3,4,2) = (25*bK2P*fz**2)/12. + (20*bPi2P*fz**2)/7. + (145*b1*fz*&
  &*3)/8. - (35*bdeta*fz**3)/8. + (44*beta*fz**3)/7. - (75*beta*fz**4)/4. - (1&
  &5*beta2*fz**4)/2.

  dAnmpdbeta2(3,4,4) = (-245*bK2P*fz**2)/24. - (205*bPi2P*fz**2)/14. - (735*b1&
  &*fz**3)/16. + (385*bdeta*fz**3)/16. - (145*beta*fz**3)/56. + (6825*beta*fz*&
  &*4)/16. + (1575*beta2*fz**4)/4.

  dAnmpdbeta2(3,4,6) = (-105*bK2P*fz**2)/8. - (315*bPi2P*fz**2)/8. - (315*b1*f&
  &z**3)/8. + (315*bdeta*fz**3)/8. - (2205*beta*fz**4)/8. - (2205*beta2*fz**4)&
  &/8.

  dAnmpdbeta2(3,5,3) = (-119*bK2P*fz**2)/12. - (535*bPi2P*fz**2)/28. - (399*b1&
  &*fz**3)/8. + (49*bdeta*fz**3)/2. - (485*beta*fz**3)/56. + (385*beta*fz**4)/&
  &4. + 70*beta2*fz**4

  dAnmpdbeta2(3,5,5) = (-21*bK2P*fz**2)/8. - (63*bPi2P*fz**2)/8. - (63*b1*fz**&
  &3)/8. + (63*bdeta*fz**3)/8. - (1323*beta*fz**4)/2. - (1323*beta2*fz**4)/2.

  dAnmpdbeta2(3,6,4) = (133*bK2P*fz**2)/8. + 41*bPi2P*fz**2 + (987*b1*fz**3)/1&
  &6. - (735*bdeta*fz**3)/16. + (15*beta*fz**3)/4. - (4095*beta*fz**4)/16. - (&
  &945*beta2*fz**4)/4.

  dAnmpdbeta2(3,6,6) = (77*bK2P*fz**2)/8. + (231*bPi2P*fz**2)/8. + (231*b1*fz*&
  &*3)/8. - (231*bdeta*fz**3)/8. + (1617*beta*fz**4)/4. + (1617*beta2*fz**4)/4&
  &.

  dAnmpdbeta2(3,7,5) = (-77*bK2P*fz**2)/8. - (231*bPi2P*fz**2)/8. - (231*b1*fz&
  &**3)/8. + (231*bdeta*fz**3)/8. + (693*beta*fz**4)/2. + (693*beta2*fz**4)/2.

  dAnmpdbeta2(3,8,6) = (-3003*beta*fz**4)/16. - (3003*beta2*fz**4)/16.

  dAnmpdbeta2(4,0,2) = (15*beta*fz**4)/8. + (3*beta2*fz**4)/4.

  dAnmpdbeta2(4,0,4) = (-65*beta*fz**4)/16. - (15*beta2*fz**4)/4.

  dAnmpdbeta2(4,0,6) = (35*beta*fz**4)/64. + (35*beta2*fz**4)/64.

  dAnmpdbeta2(4,1,1) = (3*beta*fz**4)/2.

  dAnmpdbeta2(4,1,3) = (-165*beta*fz**4)/8. - 15*beta2*fz**4

  dAnmpdbeta2(4,1,5) = (35*beta*fz**4)/2. + (35*beta2*fz**4)/2.

  dAnmpdbeta2(4,2,2) = (-75*beta*fz**4)/4. - (15*beta2*fz**4)/2.

  dAnmpdbeta2(4,2,4) = (1365*beta*fz**4)/16. + (315*beta2*fz**4)/4.

  dAnmpdbeta2(4,2,6) = (-315*beta*fz**4)/16. - (315*beta2*fz**4)/16.

  dAnmpdbeta2(4,3,1) = (-5*beta*fz**4)/2.

  dAnmpdbeta2(4,3,3) = (385*beta*fz**4)/4. + 70*beta2*fz**4

  dAnmpdbeta2(4,3,5) = (-315*beta*fz**4)/2. - (315*beta2*fz**4)/2.

  dAnmpdbeta2(4,4,2) = (175*beta*fz**4)/8. + (35*beta2*fz**4)/4.

  dAnmpdbeta2(4,4,4) = (-4095*beta*fz**4)/16. - (945*beta2*fz**4)/4.

  dAnmpdbeta2(4,4,6) = (3465*beta*fz**4)/32. + (3465*beta2*fz**4)/32.

  dAnmpdbeta2(4,5,3) = (-693*beta*fz**4)/8. - 63*beta2*fz**4

  dAnmpdbeta2(4,5,5) = (693*beta*fz**4)/2. + (693*beta2*fz**4)/2.

  dAnmpdbeta2(4,6,4) = (3003*beta*fz**4)/16. + (693*beta2*fz**4)/4.

  dAnmpdbeta2(4,6,6) = (-3003*beta*fz**4)/16. - (3003*beta2*fz**4)/16.

  dAnmpdbeta2(4,7,5) = (-429*beta*fz**4)/2. - (429*beta2*fz**4)/2.

  dAnmpdbeta2(4,8,6) = (6435*beta*fz**4)/64. + (6435*beta2*fz**4)/64.

  return
  
END SUBROUTINE calculate_dAnmpdbeta2
  
!-----------------------------------------------------------------------------

END MODULE pk22

!-----------------------------------------------------------------------------
! Module to read in, interpolate, and find the derivative with lnk, of the linear and nonlinear matter power spectrum

! The input file is in the following format (same as CAMB's):
! [1st column] k (h Mpc^-1) 
! [2nd column] P_L(k) (h^-3 Mpc^3)
! [3rd to 30th columns] P_NL(k)

!-----------------------------------------------------------------------------

MODULE pk_all
  
  IMPLICIT none
  
  DOUBLE PRECISION, dimension(:), allocatable :: xlin,ylin,y2lin
  DOUBLE PRECISION, dimension(:,:), allocatable :: ynl,ynlsign,y2nl
  INTEGER :: jlo,ndata
  
contains

!-----------------------------------------------------------------------------
  
SUBROUTINE open_pk(filename,n,D)

  IMPLICIT none

  DOUBLE PRECISION, intent(IN) :: D
  INTEGER, intent(IN) :: n
  DOUBLE PRECISION, dimension(:), allocatable :: klin,pklin
  DOUBLE PRECISION, dimension(:,:), allocatable :: pknl
  DOUBLE PRECISION, dimension(4) :: dummy
  INTEGER :: i,j
  CHARACTER(len=128) :: filename

  ndata = n
  ALLOCATE (xlin(ndata),ylin(ndata),y2lin(ndata))
  ALLOCATE (ynl(28,ndata),ynlsign(28,ndata),y2nl(28,ndata))
  ALLOCATE (klin(ndata),pklin(ndata),pknl(28,ndata))

  open(3,file=filename,status='old')
  print*, 'Read in '//trim(filename)
  do i = 1, ndata
     read(3,*) klin(i), pklin(i), pknl(1:28,i) ! k and P(k), linear and nonlinear
  enddo
  close(3)
  
  xlin = dlog(klin)
  ylin = dlog((D**2.d0)*pklin)
  do i = 1, ndata
     ynl(1:5,i) = dlog((D**2.d0)*abs(pknl(1:5,i)))
     ynl(6:28,i) = dlog((D**4.d0)*abs(pknl(6:28,i)))
     ! store the sign but do the interpolation in log of the absolute value
     ynlsign(1:28,i) = pknl(1:28,i)/(abs(pknl(1:28,i)))
     ! I4(k) has positive and negative values, so do linear interpolation
     ynl(4,i) = (D**2.d0)*pknl(4,i)
     ynlsign(4,i) = 1.d0
  enddo
  
  DEALLOCATE (klin,pklin,pknl)
  CALL spline(xlin,ylin,ndata,1.d30,1.d30,y2lin)
  do i = 1, 28
     CALL spline(xlin,ynl(i,:),ndata,1.d30,1.d30,y2nl(i,:))
  enddo
  return
  
END SUBROUTINE open_pk

!-----------------------------------------------------------------------------

SUBROUTINE close_pk

  DEALLOCATE (xlin,ylin,y2lin)
  DEALLOCATE (ynl,ynlsign,y2nl)
  return

END SUBROUTINE close_pk

!-----------------------------------------------------------------------------

DOUBLE PRECISION FUNCTION pklin(klin)
  IMPLICIT none
  DOUBLE PRECISION :: a,b,h,x,y,klin
  x = dlog(klin)
  CALL hunt(xlin,ndata,x,jlo)
  h=xlin(jlo+1)-xlin(jlo)
  a=(xlin(jlo+1)-x)/h
  b=(x-xlin(jlo))/h
  y=a*ylin(jlo)+b*ylin(jlo+1)+((a**3-a)*y2lin(jlo)+(b**3-b)*y2lin(jlo+1))*(h**2)/6.
  pklin = dexp(y)
  return
END FUNCTION pklin

!-----------------------------------------------------------------------------

DOUBLE PRECISION FUNCTION dlnpklindlnk(klin)
  IMPLICIT none
  DOUBLE PRECISION :: a,b,h,x,y,klin
  x = dlog(klin)
  CALL hunt(xlin,ndata,x,jlo)
  h=xlin(jlo+1)-xlin(jlo)
  a=(xlin(jlo+1)-x)/h
  b=(x-xlin(jlo))/h
  y=(ylin(jlo+1)-ylin(jlo))/h+(-(3.*a**2-1.)*y2lin(jlo)+(3.*b**2-1.)*y2lin(jlo+1))*h/6.
  dlnpklindlnk = y
  return
END FUNCTION dlnpklindlnk

!-----------------------------------------------------------------------------

DOUBLE PRECISION FUNCTION pknl(inl,klin)
  IMPLICIT none
  INTEGER :: inl
  DOUBLE PRECISION :: a,b,h,x,y,klin
  x = dlog(klin)
  CALL hunt(xlin,ndata,x,jlo)
  h=xlin(jlo+1)-xlin(jlo)
  a=(xlin(jlo+1)-x)/h
  b=(x-xlin(jlo))/h
  y=a*ynl(inl,jlo)+b*ynl(inl,jlo+1)+((a**3-a)*y2nl(inl,jlo)+(b**3-b)*y2nl(inl,jlo+1))*(h**2)/6.
  if (inl.eq.4) then
     pknl = ynlsign(inl,jlo)*y
  else
     pknl = ynlsign(inl,jlo)*dexp(y)
  endif
  return
END FUNCTION pknl

!-----------------------------------------------------------------------------

DOUBLE PRECISION FUNCTION dlnpknldlnk(inl,klin)
  IMPLICIT none
  INTEGER :: inl
  DOUBLE PRECISION :: a,b,h,x,y,klin
  x = dlog(klin)
  CALL hunt(xlin,ndata,x,jlo)
  h=xlin(jlo+1)-xlin(jlo)
  a=(xlin(jlo+1)-x)/h
  b=(x-xlin(jlo))/h
  y=(ynl(inl,jlo+1)-ynl(inl,jlo))/h+(-(3.*a**2-1.)*y2nl(inl,jlo)+(3.*b**2-1.)*y2nl(inl,jlo+1))*h/6.
  if (inl.eq.4) y=y/(pknl(inl,klin))
  dlnpknldlnk = y
  return
END FUNCTION dlnpknldlnk

!-----------------------------------------------------------------------------

END MODULE pk_all

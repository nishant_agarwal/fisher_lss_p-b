FC     = gfortran
FFLAGS =  -Ofast -fopenmp -ffast-math -funroll-loops
OBJS	= cosmo.o da.o spline.o hunt.o rombint.o dverk.o growth.o pklinear.o pk.o pk22.o pk13.o pkoc0.o pkns.o pknrun.o bk.o dvert.o

.SUFFIXES: .f90
.f90.o: 
	$(FC) $(FFLAGS) -c $<
default: fisher
fisher: $(OBJS) fisher.o
	$(FC) $(FFLAGS) -o $@ $(OBJS) $@.o $(LDFLAGS)
clean:
	-rm -f *.o *.mod
tidy: clean
	-rm -f fisher

!-----------------------------------------------------------------------------
! Module to calculate the bispectrum, bk, and its derivatives
!-----------------------------------------------------------------------------

MODULE bk

  USE pk_all
  IMPLICIT none
  
contains

!-----------------------------------------------------------------------------
  
double precision FUNCTION pkinbk(k,mu,&
     fz,b1,beta,bd2d,betav1,betav2,Pe1,Pe2,Pe3,Anmp,C13ln)

  IMPLICIT none

  double precision, intent(IN) :: k,mu,fz,b1,beta,bd2d,betav1,betav2,Pe1,Pe2,Pe3,&
       Anmp(0:4,0:8,0:6),C13ln(0:3,1:5)
  double precision :: pknlcomps(1:28),Imp(0:8,0:6),calIn(1:5),LegP(0:6)
  double precision :: mu2,pkL
  integer :: inl,il,in,im,ip

  pkL = pklin(k)
  do inl = 1, 28
     pknlcomps(inl) = pknl(inl,k)
  enddo

  Imp = 0d0
  Imp(0,0) = pknlcomps(6)
  Imp(0,2) = pknlcomps(7)
  Imp(0,4) = pknlcomps(8)
  Imp(0,6) = pknlcomps(9)
  Imp(1,1) = pknlcomps(10)
  Imp(1,3) = pknlcomps(11)
  Imp(1,5) = pknlcomps(12)
  Imp(2,0) = pknlcomps(13)
  Imp(2,2) = pknlcomps(14)
  Imp(2,4) = pknlcomps(15)
  Imp(2,6) = pknlcomps(16)
  Imp(3,1) = pknlcomps(17)
  Imp(3,3) = pknlcomps(18)
  Imp(3,5) = pknlcomps(19)
  Imp(4,2) = pknlcomps(20)
  Imp(4,4) = pknlcomps(21)
  Imp(4,6) = pknlcomps(22)
  Imp(5,3) = pknlcomps(23)
  Imp(5,5) = pknlcomps(24)
  Imp(6,4) = pknlcomps(25)
  Imp(6,6) = pknlcomps(26)
  Imp(7,5) = pknlcomps(27)
  Imp(8,6) = pknlcomps(28)

  calIn = 0d0
  calIn(1:5) = pknlcomps(1:5)
  
  mu2=mu*mu
  LegP = 0d0
  LegP(0) = 1d0
  LegP(2) = 0.5d0*(3d0*mu2-1d0)
  LegP(4) = (1d0/8d0)*(35d0*mu2**2d0-30d0*mu2+3d0)
  LegP(6) = (1d0/16d0)*(231d0*mu2**3d0-315d0*mu2**2d0+105d0*mu2-5d0)
  pkinbk = (b1-beta*mu2*fz)**2d0*pkL+Pe1 &
       -2d0*(b1*bd2d-beta*mu2*fz*(bd2d+b1*betav1+b1*betav2*mu2) &
       +(beta*mu2*fz)**2d0*(betav1+betav2*mu2))*k**2d0*pkL &
       +k**2d0*Pe2+beta*mu2*k**2d0*Pe3 &
       +sum((/(sum((/(sum((/(Anmp(in,im,ip)*Imp(im,ip)*mu2**in &
       ,ip=0,6)/)),im=0,8)/)),in=0,4)/)) &
       +2d0*sum((/(sum((/(C13ln(il,in)*calIn(in)*LegP(2*il) &
       ,in=1,5)/)),il=0,3)/))*pkL
  
  return
  
END FUNCTION pkinbk

!-----------------------------------------------------------------------------

double precision FUNCTION bktreeperm(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
     fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)

  IMPLICIT none

  double precision, intent(IN) :: k1,mu1,k2,mu2,k3,mu3,pkL1,pkL2,pkL3,&
       fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5
  double precision :: mu23

  mu23 = (k1**2d0 - k2**2d0 - k3**2d0)/(2d0*k2*k3)
  
  bktreeperm = Be/3. + 2*(b1 - beta*fz*mu1**2)*(Pe4 - fz*mu1**2*Pe5)*pkL1 + 2*&
  &(b1 - beta*fz*mu2**2)*(b1 - beta*fz*mu3**2)*(b2/2. + b1*(0.7142857142857143&
  & + ((k2/k3 + k3/k2)*mu23)/2. + (2*mu23**2)/7.) + bK2*(-0.3333333333333333 +&
  & mu23**2) + (b1*fz*(k2/k3 + k3/k2)*mu2*mu3)/2. + beta2*fz**2*mu2**2*mu3**2 &
  &- beta*fz*(0.42857142857142855 + ((k2/k3 + k3/k2)*mu23)/2. + (4*mu23**2)/7.&
  &)*(mu2 + mu3)**2 - (bdeta*fz*(mu2**2 + mu3**2))/2. - (beta*fz**2*mu2*mu3*((&
  &k2*mu2**2)/k3 + (k3*mu3**2)/k2))/2. + bPi2P*(mu2*mu23*mu3 + (5*(1 - mu23**2&
  &)*(mu2 + mu3)**2)/7.) + bK2P*(0.1111111111111111 + mu2*mu23*mu3 + (-mu2**2 &
  &- mu3**2)/3.))*pkL2*pkL3

  return

END FUNCTION bktreeperm

!-----------------------------------------------------------------------------

double precision FUNCTION dbktreepermdk1(k1,mu1,pkL1,nkL1,k2,mu2,pkL2,nkL2,&
     k3,mu3,pkL3,nkL3,fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)

  IMPLICIT none

  double precision, intent(IN) :: k1,mu1,k2,mu2,k3,mu3,pkL1,pkL2,pkL3,&
       nkL1,nkL2,nkL3,fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5
  
  dbktreepermdk1 = 2*(b1 - beta*fz*mu1**2)*nkL1*(Pe4 - fz*mu1**2*Pe5)*pkL1 + 2&
  &*(b1 - beta*fz*mu2**2)*(b1 - beta*fz*mu3**2)*((bK2*k1*(k1**2 - k2**2 - k3**&
  &2))/(k2**2*k3**2) + b1*((k1*(k2/k3 + k3/k2))/(2.*k2*k3) + (2*k1*(k1**2 - k2&
  &**2 - k3**2))/(7.*k2**2*k3**2)) + (bK2P*k1*mu2*mu3)/(k2*k3) - beta*fz*((k1*&
  &(k2/k3 + k3/k2))/(2.*k2*k3) + (4*k1*(k1**2 - k2**2 - k3**2))/(7.*k2**2*k3**&
  &2))*(mu2 + mu3)**2 + bPi2P*((k1*mu2*mu3)/(k2*k3) - (5*k1*(k1**2 - k2**2 - k&
  &3**2)*(mu2 + mu3)**2)/(7.*k2**2*k3**2)))*pkL2*pkL3

  return

END FUNCTION dbktreepermdk1

!-----------------------------------------------------------------------------

double precision FUNCTION dbktreepermdk2(k1,mu1,pkL1,nkL1,k2,mu2,pkL2,nkL2,&
     k3,mu3,pkL3,nkL3,fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)

  IMPLICIT none

  double precision, intent(IN) :: k1,mu1,k2,mu2,k3,mu3,pkL1,pkL2,pkL3,&
       nkL1,nkL2,nkL3,fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5
  
  dbktreepermdk2 = 2*(b1 - beta*fz*mu2**2)*(b1 - beta*fz*mu3**2)*(bK2*(-((k1**&
  &2 - k2**2 - k3**2)/(k2*k3**2)) - (k1**2 - k2**2 - k3**2)**2/(2.*k2**3*k3**2&
  &)) + b1*(-(k2/k3 + k3/k2)/(2.*k3) - (2*(k1**2 - k2**2 - k3**2))/(7.*k2*k3**&
  &2) + ((1/k3 - k3/k2**2)*(k1**2 - k2**2 - k3**2))/(4.*k2*k3) - ((k2/k3 + k3/&
  &k2)*(k1**2 - k2**2 - k3**2))/(4.*k2**2*k3) - (k1**2 - k2**2 - k3**2)**2/(7.&
  &*k2**3*k3**2)) + (b1*fz*(1/k3 - k3/k2**2)*mu2*mu3)/2. - beta*fz*(-(k2/k3 + &
  &k3/k2)/(2.*k3) - (4*(k1**2 - k2**2 - k3**2))/(7.*k2*k3**2) + ((1/k3 - k3/k2&
  &**2)*(k1**2 - k2**2 - k3**2))/(4.*k2*k3) - ((k2/k3 + k3/k2)*(k1**2 - k2**2 &
  &- k3**2))/(4.*k2**2*k3) - (2*(k1**2 - k2**2 - k3**2)**2)/(7.*k2**3*k3**2))*&
  &(mu2 + mu3)**2 + bK2P*(-((mu2*mu3)/k3) - ((k1**2 - k2**2 - k3**2)*mu2*mu3)/&
  &(2.*k2**2*k3)) - (beta*fz**2*mu2*mu3*(mu2**2/k3 - (k3*mu3**2)/k2**2))/2. + &
  &bPi2P*(-((mu2*mu3)/k3) - ((k1**2 - k2**2 - k3**2)*mu2*mu3)/(2.*k2**2*k3) + &
  &(5*((k1**2 - k2**2 - k3**2)/(k2*k3**2) + (k1**2 - k2**2 - k3**2)**2/(2.*k2*&
  &*3*k3**2))*(mu2 + mu3)**2)/7.))*pkL2*pkL3 + 2*(b1 - beta*fz*mu2**2)*(b1 - b&
  &eta*fz*mu3**2)*(b2/2. + b1*(0.7142857142857143 + ((k2/k3 + k3/k2)*(k1**2 - &
  &k2**2 - k3**2))/(4.*k2*k3) + (k1**2 - k2**2 - k3**2)**2/(14.*k2**2*k3**2)) &
  &+ bK2*(-0.3333333333333333 + (k1**2 - k2**2 - k3**2)**2/(4.*k2**2*k3**2)) +&
  & (b1*fz*(k2/k3 + k3/k2)*mu2*mu3)/2. + beta2*fz**2*mu2**2*mu3**2 - beta*fz*(&
  &0.42857142857142855 + ((k2/k3 + k3/k2)*(k1**2 - k2**2 - k3**2))/(4.*k2*k3) &
  &+ (k1**2 - k2**2 - k3**2)**2/(7.*k2**2*k3**2))*(mu2 + mu3)**2 - (bdeta*fz*(&
  &mu2**2 + mu3**2))/2. - (beta*fz**2*mu2*mu3*((k2*mu2**2)/k3 + (k3*mu3**2)/k2&
  &))/2. + bPi2P*(((k1**2 - k2**2 - k3**2)*mu2*mu3)/(2.*k2*k3) + (5*(1 - (k1**&
  &2 - k2**2 - k3**2)**2/(4.*k2**2*k3**2))*(mu2 + mu3)**2)/7.) + bK2P*(0.11111&
  &11111111111 + ((k1**2 - k2**2 - k3**2)*mu2*mu3)/(2.*k2*k3) + (-mu2**2 - mu3&
  &**2)/3.))*nkL2*pkL2*pkL3

  return

END FUNCTION dbktreepermdk2

!-----------------------------------------------------------------------------

double precision FUNCTION dbktreepermdk3(k1,mu1,pkL1,nkL1,k2,mu2,pkL2,nkL2,&
     k3,mu3,pkL3,nkL3,fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)

  IMPLICIT none

  double precision, intent(IN) :: k1,mu1,k2,mu2,k3,mu3,pkL1,pkL2,pkL3,&
       nkL1,nkL2,nkL3,fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5
  
  dbktreepermdk3 = 2*(b1 - beta*fz*mu2**2)*(b1 - beta*fz*mu3**2)*(bK2*(-((k1**&
  &2 - k2**2 - k3**2)/(k2**2*k3)) - (k1**2 - k2**2 - k3**2)**2/(2.*k2**2*k3**3&
  &)) + b1*(-(k2/k3 + k3/k2)/(2.*k2) - (2*(k1**2 - k2**2 - k3**2))/(7.*k2**2*k&
  &3) + ((1/k2 - k2/k3**2)*(k1**2 - k2**2 - k3**2))/(4.*k2*k3) - ((k2/k3 + k3/&
  &k2)*(k1**2 - k2**2 - k3**2))/(4.*k2*k3**2) - (k1**2 - k2**2 - k3**2)**2/(7.&
  &*k2**2*k3**3)) + (b1*fz*(1/k2 - k2/k3**2)*mu2*mu3)/2. - beta*fz*(-(k2/k3 + &
  &k3/k2)/(2.*k2) - (4*(k1**2 - k2**2 - k3**2))/(7.*k2**2*k3) + ((1/k2 - k2/k3&
  &**2)*(k1**2 - k2**2 - k3**2))/(4.*k2*k3) - ((k2/k3 + k3/k2)*(k1**2 - k2**2 &
  &- k3**2))/(4.*k2*k3**2) - (2*(k1**2 - k2**2 - k3**2)**2)/(7.*k2**2*k3**3))*&
  &(mu2 + mu3)**2 + bK2P*(-((mu2*mu3)/k2) - ((k1**2 - k2**2 - k3**2)*mu2*mu3)/&
  &(2.*k2*k3**2)) - (beta*fz**2*mu2*mu3*(-((k2*mu2**2)/k3**2) + mu3**2/k2))/2.&
  & + bPi2P*(-((mu2*mu3)/k2) - ((k1**2 - k2**2 - k3**2)*mu2*mu3)/(2.*k2*k3**2)&
  & + (5*((k1**2 - k2**2 - k3**2)/(k2**2*k3) + (k1**2 - k2**2 - k3**2)**2/(2.*&
  &k2**2*k3**3))*(mu2 + mu3)**2)/7.))*pkL2*pkL3 + 2*(b1 - beta*fz*mu2**2)*(b1 &
  &- beta*fz*mu3**2)*(b2/2. + b1*(0.7142857142857143 + ((k2/k3 + k3/k2)*(k1**2&
  & - k2**2 - k3**2))/(4.*k2*k3) + (k1**2 - k2**2 - k3**2)**2/(14.*k2**2*k3**2&
  &)) + bK2*(-0.3333333333333333 + (k1**2 - k2**2 - k3**2)**2/(4.*k2**2*k3**2)&
  &) + (b1*fz*(k2/k3 + k3/k2)*mu2*mu3)/2. + beta2*fz**2*mu2**2*mu3**2 - beta*f&
  &z*(0.42857142857142855 + ((k2/k3 + k3/k2)*(k1**2 - k2**2 - k3**2))/(4.*k2*k&
  &3) + (k1**2 - k2**2 - k3**2)**2/(7.*k2**2*k3**2))*(mu2 + mu3)**2 - (bdeta*f&
  &z*(mu2**2 + mu3**2))/2. - (beta*fz**2*mu2*mu3*((k2*mu2**2)/k3 + (k3*mu3**2)&
  &/k2))/2. + bPi2P*(((k1**2 - k2**2 - k3**2)*mu2*mu3)/(2.*k2*k3) + (5*(1 - (k&
  &1**2 - k2**2 - k3**2)**2/(4.*k2**2*k3**2))*(mu2 + mu3)**2)/7.) + bK2P*(0.11&
  &11111111111111 + ((k1**2 - k2**2 - k3**2)*mu2*mu3)/(2.*k2*k3) + (-mu2**2 - &
  &mu3**2)/3.))*nkL3*pkL2*pkL3

  return

END FUNCTION dbktreepermdk3

!-----------------------------------------------------------------------------

double precision FUNCTION dbktreepermdmu1(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
     fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)

  IMPLICIT none

  double precision, intent(IN) :: k1,mu1,k2,mu2,k3,mu3,pkL1,pkL2,pkL3,&
       fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5
  double precision :: mu23

  mu23 = (k1**2d0 - k2**2d0 - k3**2d0)/(2d0*k2*k3)

  dbktreepermdmu1 = -4*fz*mu1*(b1 - beta*fz*mu1**2)*Pe5*pkL1 - 4*beta*fz*mu1*(&
  &Pe4 - fz*mu1**2*Pe5)*pkL1    

  return

END FUNCTION dbktreepermdmu1

!-----------------------------------------------------------------------------

double precision FUNCTION dbktreepermdmu2(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
     fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)

  IMPLICIT none

  double precision, intent(IN) :: k1,mu1,k2,mu2,k3,mu3,pkL1,pkL2,pkL3,&
       fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5
  double precision :: mu23

  mu23 = (k1**2d0 - k2**2d0 - k3**2d0)/(2d0*k2*k3)

  dbktreepermdmu2 = 2*(b1 - beta*fz*mu2**2)*(b1 - beta*fz*mu3**2)*(-(bdeta*fz*&
  &mu2) + (b1*fz*(k2/k3 + k3/k2)*mu3)/2. - (beta*fz**2*k2*mu2**2*mu3)/k3 + 2*b&
  &eta2*fz**2*mu2*mu3**2 - 2*beta*fz*(0.42857142857142855 + ((k2/k3 + k3/k2)*m&
  &u23)/2. + (4*mu23**2)/7.)*(mu2 + mu3) + bK2P*((-2*mu2)/3. + mu23*mu3) - (be&
  &ta*fz**2*mu3*((k2*mu2**2)/k3 + (k3*mu3**2)/k2))/2. + bPi2P*(mu23*mu3 + (10*&
  &(1 - mu23**2)*(mu2 + mu3))/7.))*pkL2*pkL3 - 4*beta*fz*mu2*(b1 - beta*fz*mu3&
  &**2)*(b2/2. + b1*(0.7142857142857143 + ((k2/k3 + k3/k2)*mu23)/2. + (2*mu23*&
  &*2)/7.) + bK2*(-0.3333333333333333 + mu23**2) + (b1*fz*(k2/k3 + k3/k2)*mu2*&
  &mu3)/2. + beta2*fz**2*mu2**2*mu3**2 - beta*fz*(0.42857142857142855 + ((k2/k&
  &3 + k3/k2)*mu23)/2. + (4*mu23**2)/7.)*(mu2 + mu3)**2 - (bdeta*fz*(mu2**2 + &
  &mu3**2))/2. - (beta*fz**2*mu2*mu3*((k2*mu2**2)/k3 + (k3*mu3**2)/k2))/2. + b&
  &Pi2P*(mu2*mu23*mu3 + (5*(1 - mu23**2)*(mu2 + mu3)**2)/7.) + bK2P*(0.1111111&
  &111111111 + mu2*mu23*mu3 + (-mu2**2 - mu3**2)/3.))*pkL2*pkL3    

  return

END FUNCTION dbktreepermdmu2

!-----------------------------------------------------------------------------

double precision FUNCTION dbktreepermdmu3(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
     fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)

  IMPLICIT none

  double precision, intent(IN) :: k1,mu1,k2,mu2,k3,mu3,pkL1,pkL2,pkL3,&
       fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5
  double precision :: mu23

  mu23 = (k1**2d0 - k2**2d0 - k3**2d0)/(2d0*k2*k3)

  dbktreepermdmu3 = 2*(b1 - beta*fz*mu2**2)*(b1 - beta*fz*mu3**2)*((b1*fz*(k2/&
  &k3 + k3/k2)*mu2)/2. + bK2P*(mu2*mu23 - (2*mu3)/3.) - bdeta*fz*mu3 + 2*beta2&
  &*fz**2*mu2**2*mu3 - (beta*fz**2*k3*mu2*mu3**2)/k2 - 2*beta*fz*(0.4285714285&
  &7142855 + ((k2/k3 + k3/k2)*mu23)/2. + (4*mu23**2)/7.)*(mu2 + mu3) - (beta*f&
  &z**2*mu2*((k2*mu2**2)/k3 + (k3*mu3**2)/k2))/2. + bPi2P*(mu2*mu23 + (10*(1 -&
  & mu23**2)*(mu2 + mu3))/7.))*pkL2*pkL3 - 4*beta*fz*(b1 - beta*fz*mu2**2)*mu3&
  &*(b2/2. + b1*(0.7142857142857143 + ((k2/k3 + k3/k2)*mu23)/2. + (2*mu23**2)/&
  &7.) + bK2*(-0.3333333333333333 + mu23**2) + (b1*fz*(k2/k3 + k3/k2)*mu2*mu3)&
  &/2. + beta2*fz**2*mu2**2*mu3**2 - beta*fz*(0.42857142857142855 + ((k2/k3 + &
  &k3/k2)*mu23)/2. + (4*mu23**2)/7.)*(mu2 + mu3)**2 - (bdeta*fz*(mu2**2 + mu3*&
  &*2))/2. - (beta*fz**2*mu2*mu3*((k2*mu2**2)/k3 + (k3*mu3**2)/k2))/2. + bPi2P&
  &*(mu2*mu23*mu3 + (5*(1 - mu23**2)*(mu2 + mu3)**2)/7.) + bK2P*(0.11111111111&
  &11111 + mu2*mu23*mu3 + (-mu2**2 - mu3**2)/3.))*pkL2*pkL3   

  return

END FUNCTION dbktreepermdmu3

!-----------------------------------------------------------------------------

double precision FUNCTION dbktreepermdf(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
     fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)

  IMPLICIT none

  double precision, intent(IN) :: k1,mu1,k2,mu2,k3,mu3,pkL1,pkL2,pkL3,&
       fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5
  double precision :: mu23

  mu23 = (k1**2d0 - k2**2d0 - k3**2d0)/(2d0*k2*k3)
  
  dbktreepermdf = -2*beta*mu1**2*Pe4*pkL1 + 2*beta*fz*mu1**4*Pe5*pkL1 - 2*mu1*&
  &*2*(b1 - beta*fz*mu1**2)*Pe5*pkL1 + 2*(b1 - beta*fz*mu2**2)*(b1 - beta*fz*m&
  &u3**2)*((b1*(k2/k3 + k3/k2)*mu2*mu3)/2. + 2*beta2*fz*mu2**2*mu3**2 - beta*(&
  &0.42857142857142855 + ((k2/k3 + k3/k2)*mu23)/2. + (4*mu23**2)/7.)*(mu2 + mu&
  &3)**2 - (bdeta*(mu2**2 + mu3**2))/2. - beta*fz*mu2*mu3*((k2*mu2**2)/k3 + (k&
  &3*mu3**2)/k2))*pkL2*pkL3 - 2*beta*(b1 - beta*fz*mu2**2)*mu3**2*(b2/2. + b1*&
  &(0.7142857142857143 + ((k2/k3 + k3/k2)*mu23)/2. + (2*mu23**2)/7.) + bK2*(-0&
  &.3333333333333333 + mu23**2) + (b1*fz*(k2/k3 + k3/k2)*mu2*mu3)/2. + beta2*f&
  &z**2*mu2**2*mu3**2 - beta*fz*(0.42857142857142855 + ((k2/k3 + k3/k2)*mu23)/&
  &2. + (4*mu23**2)/7.)*(mu2 + mu3)**2 - (bdeta*fz*(mu2**2 + mu3**2))/2. - (be&
  &ta*fz**2*mu2*mu3*((k2*mu2**2)/k3 + (k3*mu3**2)/k2))/2. + bPi2P*(mu2*mu23*mu&
  &3 + (5*(1 - mu23**2)*(mu2 + mu3)**2)/7.) + bK2P*(0.1111111111111111 + mu2*m&
  &u23*mu3 + (-mu2**2 - mu3**2)/3.))*pkL2*pkL3 - 2*beta*mu2**2*(b1 - beta*fz*m&
  &u3**2)*(b2/2. + b1*(0.7142857142857143 + ((k2/k3 + k3/k2)*mu23)/2. + (2*mu2&
  &3**2)/7.) + bK2*(-0.3333333333333333 + mu23**2) + (b1*fz*(k2/k3 + k3/k2)*mu&
  &2*mu3)/2. + beta2*fz**2*mu2**2*mu3**2 - beta*fz*(0.42857142857142855 + ((k2&
  &/k3 + k3/k2)*mu23)/2. + (4*mu23**2)/7.)*(mu2 + mu3)**2 - (bdeta*fz*(mu2**2 &
  &+ mu3**2))/2. - (beta*fz**2*mu2*mu3*((k2*mu2**2)/k3 + (k3*mu3**2)/k2))/2. +&
  & bPi2P*(mu2*mu23*mu3 + (5*(1 - mu23**2)*(mu2 + mu3)**2)/7.) + bK2P*(0.11111&
  &11111111111 + mu2*mu23*mu3 + (-mu2**2 - mu3**2)/3.))*pkL2*pkL3

  return

END FUNCTION dbktreepermdf

!-----------------------------------------------------------------------------

double precision FUNCTION dbktreepermdb1(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
     fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)

  IMPLICIT none

  double precision, intent(IN) :: k1,mu1,k2,mu2,k3,mu3,pkL1,pkL2,pkL3,&
       fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5
  double precision :: mu23

  mu23 = (k1**2d0 - k2**2d0 - k3**2d0)/(2d0*k2*k3)
  
  dbktreepermdb1 = 2*Pe4*pkL1 - 2*fz*mu1**2*Pe5*pkL1 + 2*(b1 - beta*fz*mu2**2)&
  &*(0.7142857142857143 + ((k2/k3 + k3/k2)*mu23)/2. + (2*mu23**2)/7. + (fz*(k2&
  &/k3 + k3/k2)*mu2*mu3)/2.)*(b1 - beta*fz*mu3**2)*pkL2*pkL3 + 2*(b1 - beta*fz&
  &*mu2**2)*(b2/2. + b1*(0.7142857142857143 + ((k2/k3 + k3/k2)*mu23)/2. + (2*m&
  &u23**2)/7.) + bK2*(-0.3333333333333333 + mu23**2) + (b1*fz*(k2/k3 + k3/k2)*&
  &mu2*mu3)/2. + beta2*fz**2*mu2**2*mu3**2 - beta*fz*(0.42857142857142855 + ((&
  &k2/k3 + k3/k2)*mu23)/2. + (4*mu23**2)/7.)*(mu2 + mu3)**2 - (bdeta*fz*(mu2**&
  &2 + mu3**2))/2. - (beta*fz**2*mu2*mu3*((k2*mu2**2)/k3 + (k3*mu3**2)/k2))/2.&
  & + bPi2P*(mu2*mu23*mu3 + (5*(1 - mu23**2)*(mu2 + mu3)**2)/7.) + bK2P*(0.111&
  &1111111111111 + mu2*mu23*mu3 + (-mu2**2 - mu3**2)/3.))*pkL2*pkL3 + 2*(b1 - &
  &beta*fz*mu3**2)*(b2/2. + b1*(0.7142857142857143 + ((k2/k3 + k3/k2)*mu23)/2.&
  & + (2*mu23**2)/7.) + bK2*(-0.3333333333333333 + mu23**2) + (b1*fz*(k2/k3 + &
  &k3/k2)*mu2*mu3)/2. + beta2*fz**2*mu2**2*mu3**2 - beta*fz*(0.428571428571428&
  &55 + ((k2/k3 + k3/k2)*mu23)/2. + (4*mu23**2)/7.)*(mu2 + mu3)**2 - (bdeta*fz&
  &*(mu2**2 + mu3**2))/2. - (beta*fz**2*mu2*mu3*((k2*mu2**2)/k3 + (k3*mu3**2)/&
  &k2))/2. + bPi2P*(mu2*mu23*mu3 + (5*(1 - mu23**2)*(mu2 + mu3)**2)/7.) + bK2P&
  &*(0.1111111111111111 + mu2*mu23*mu3 + (-mu2**2 - mu3**2)/3.))*pkL2*pkL3
  
  return

END FUNCTION dbktreepermdb1

!-----------------------------------------------------------------------------

double precision FUNCTION dbktreepermdbeta(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
     fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)

  IMPLICIT none

  double precision, intent(IN) :: k1,mu1,k2,mu2,k3,mu3,pkL1,pkL2,pkL3,&
       fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5
  double precision :: mu23

  mu23 = (k1**2d0 - k2**2d0 - k3**2d0)/(2d0*k2*k3)
  
  dbktreepermdbeta = -2*fz*mu1**2*Pe4*pkL1 + 2*fz**2*mu1**4*Pe5*pkL1 + 2*(b1 -&
  & beta*fz*mu2**2)*(b1 - beta*fz*mu3**2)*(-(fz*(0.42857142857142855 + ((k2/k3&
  & + k3/k2)*mu23)/2. + (4*mu23**2)/7.)*(mu2 + mu3)**2) - (fz**2*mu2*mu3*((k2*&
  &mu2**2)/k3 + (k3*mu3**2)/k2))/2.)*pkL2*pkL3 - 2*fz*(b1 - beta*fz*mu2**2)*mu&
  &3**2*(b2/2. + b1*(0.7142857142857143 + ((k2/k3 + k3/k2)*mu23)/2. + (2*mu23*&
  &*2)/7.) + bK2*(-0.3333333333333333 + mu23**2) + (b1*fz*(k2/k3 + k3/k2)*mu2*&
  &mu3)/2. + beta2*fz**2*mu2**2*mu3**2 - beta*fz*(0.42857142857142855 + ((k2/k&
  &3 + k3/k2)*mu23)/2. + (4*mu23**2)/7.)*(mu2 + mu3)**2 - (bdeta*fz*(mu2**2 + &
  &mu3**2))/2. - (beta*fz**2*mu2*mu3*((k2*mu2**2)/k3 + (k3*mu3**2)/k2))/2. + b&
  &Pi2P*(mu2*mu23*mu3 + (5*(1 - mu23**2)*(mu2 + mu3)**2)/7.) + bK2P*(0.1111111&
  &111111111 + mu2*mu23*mu3 + (-mu2**2 - mu3**2)/3.))*pkL2*pkL3 - 2*fz*mu2**2*&
  &(b1 - beta*fz*mu3**2)*(b2/2. + b1*(0.7142857142857143 + ((k2/k3 + k3/k2)*mu&
  &23)/2. + (2*mu23**2)/7.) + bK2*(-0.3333333333333333 + mu23**2) + (b1*fz*(k2&
  &/k3 + k3/k2)*mu2*mu3)/2. + beta2*fz**2*mu2**2*mu3**2 - beta*fz*(0.428571428&
  &57142855 + ((k2/k3 + k3/k2)*mu23)/2. + (4*mu23**2)/7.)*(mu2 + mu3)**2 - (bd&
  &eta*fz*(mu2**2 + mu3**2))/2. - (beta*fz**2*mu2*mu3*((k2*mu2**2)/k3 + (k3*mu&
  &3**2)/k2))/2. + bPi2P*(mu2*mu23*mu3 + (5*(1 - mu23**2)*(mu2 + mu3)**2)/7.) &
  &+ bK2P*(0.1111111111111111 + mu2*mu23*mu3 + (-mu2**2 - mu3**2)/3.))*pkL2*pk&
  &L3
  
  return

END FUNCTION dbktreepermdbeta

!-----------------------------------------------------------------------------

double precision FUNCTION dbktreepermdb2(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
     fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)

  IMPLICIT none

  double precision, intent(IN) :: k1,mu1,k2,mu2,k3,mu3,pkL1,pkL2,pkL3,&
       fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5
  double precision :: mu23

  mu23 = (k1**2d0 - k2**2d0 - k3**2d0)/(2d0*k2*k3)
  
  dbktreepermdb2 = (b1 - beta*fz*mu2**2)*(b1 - beta*fz*mu3**2)*pkL2*pkL3
  
  return

END FUNCTION dbktreepermdb2

!-----------------------------------------------------------------------------

double precision FUNCTION dbktreepermdbK2(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
     fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)

  IMPLICIT none

  double precision, intent(IN) :: k1,mu1,k2,mu2,k3,mu3,pkL1,pkL2,pkL3,&
       fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5
  double precision :: mu23

  mu23 = (k1**2d0 - k2**2d0 - k3**2d0)/(2d0*k2*k3)
  
  dbktreepermdbK2 = 2*(b1 - beta*fz*mu2**2)*(-0.3333333333333333 + mu23**2)*(b&
  &1 - beta*fz*mu3**2)*pkL2*pkL3

  return

END FUNCTION dbktreepermdbK2

!-----------------------------------------------------------------------------

double precision FUNCTION dbktreepermdbdeta(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
     fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)

  IMPLICIT none

  double precision, intent(IN) :: k1,mu1,k2,mu2,k3,mu3,pkL1,pkL2,pkL3,&
       fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5
  double precision :: mu23

  mu23 = (k1**2d0 - k2**2d0 - k3**2d0)/(2d0*k2*k3)
  
  dbktreepermdbdeta = -(fz*(b1 - beta*fz*mu2**2)*(mu2**2 + mu3**2)*(b1 - beta*&
  &fz*mu3**2)*pkL2*pkL3)
  
  return

END FUNCTION dbktreepermdbdeta

!-----------------------------------------------------------------------------

double precision FUNCTION dbktreepermdbeta2(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
     fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)

  IMPLICIT none

  double precision, intent(IN) :: k1,mu1,k2,mu2,k3,mu3,pkL1,pkL2,pkL3,&
       fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5
  double precision :: mu23

  mu23 = (k1**2d0 - k2**2d0 - k3**2d0)/(2d0*k2*k3)
  
  dbktreepermdbeta2 = 2*fz**2*mu2**2*(b1 - beta*fz*mu2**2)*mu3**2*(b1 - beta*f&
  &z*mu3**2)*pkL2*pkL3
  
  return

END FUNCTION dbktreepermdbeta2

!-----------------------------------------------------------------------------

double precision FUNCTION dbktreepermdbK2P(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
     fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)

  IMPLICIT none

  double precision, intent(IN) :: k1,mu1,k2,mu2,k3,mu3,pkL1,pkL2,pkL3,&
       fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5
  double precision :: mu23

  mu23 = (k1**2d0 - k2**2d0 - k3**2d0)/(2d0*k2*k3)
  
  dbktreepermdbK2P = 2*(b1 - beta*fz*mu2**2)*(b1 - beta*fz*mu3**2)*(0.11111111&
  &11111111 + mu2*mu23*mu3 + (-mu2**2 - mu3**2)/3.)*pkL2*pkL3
  
  return

END FUNCTION dbktreepermdbK2P

!-----------------------------------------------------------------------------

double precision FUNCTION dbktreepermdbPi2P(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
     fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)

  IMPLICIT none

  double precision, intent(IN) :: k1,mu1,k2,mu2,k3,mu3,pkL1,pkL2,pkL3,&
       fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5
  double precision :: mu23

  mu23 = (k1**2d0 - k2**2d0 - k3**2d0)/(2d0*k2*k3)
  
  dbktreepermdbPi2P = 2*(b1 - beta*fz*mu2**2)*(b1 - beta*fz*mu3**2)*(mu2*mu23*&
  &mu3 + (5*(1 - mu23**2)*(mu2 + mu3)**2)/7.)*pkL2*pkL3
  
  return

END FUNCTION dbktreepermdbPi2P

!-----------------------------------------------------------------------------

double precision FUNCTION dbktreepermdBe(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
     fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)

  IMPLICIT none

  double precision, intent(IN) :: k1,mu1,k2,mu2,k3,mu3,pkL1,pkL2,pkL3,&
       fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5
  double precision :: mu23

  mu23 = (k1**2d0 - k2**2d0 - k3**2d0)/(2d0*k2*k3)
  
  dbktreepermdBe = 1/3.

  return

END FUNCTION dbktreepermdBe

!-----------------------------------------------------------------------------

double precision FUNCTION dbktreepermdPe4(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
     fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)

  IMPLICIT none

  double precision, intent(IN) :: k1,mu1,k2,mu2,k3,mu3,pkL1,pkL2,pkL3,&
       fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5
  double precision :: mu23

  mu23 = (k1**2d0 - k2**2d0 - k3**2d0)/(2d0*k2*k3)
  
  dbktreepermdPe4 = 2*(b1 - beta*fz*mu1**2)*pkL1
  
  return

END FUNCTION dbktreepermdPe4

!-----------------------------------------------------------------------------

double precision FUNCTION dbktreepermdPe5(k1,mu1,pkL1,k2,mu2,pkL2,k3,mu3,pkL3,&
     fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5)

  IMPLICIT none

  double precision, intent(IN) :: k1,mu1,k2,mu2,k3,mu3,pkL1,pkL2,pkL3,&
       fz,b1,beta,b2,bK2,bdeta,beta2,bK2P,bPi2P,Be,Pe4,Pe5
  double precision :: mu23

  mu23 = (k1**2d0 - k2**2d0 - k3**2d0)/(2d0*k2*k3)
  
  dbktreepermdPe5 = -2*fz*mu1**2*(b1 - beta*fz*mu1**2)*pkL1
  
  return

END FUNCTION dbktreepermdPe5

!-----------------------------------------------------------------------------

END MODULE bk

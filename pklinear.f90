MODULE pk_linear
  
  IMPLICIT none
  
  DOUBLE PRECISION, dimension(:), allocatable :: xlin,ylin,y2lin
  INTEGER :: jlo,ndata
  
contains

!-----------------------------------------------------------------------------
  
SUBROUTINE open_pk_linear(filename_linear,n_linear,D)

  IMPLICIT none

  DOUBLE PRECISION, intent(IN) :: D
  INTEGER, intent(IN) :: n_linear
  DOUBLE PRECISION, dimension(:), allocatable :: klin,pklin
  INTEGER :: i
  CHARACTER(len=128) :: filename_linear

  ndata = n_linear
  ALLOCATE (xlin(ndata),ylin(ndata),y2lin(ndata))
  ALLOCATE (klin(ndata),pklin(ndata))

  open(3,file=filename_linear,status='old')
  print*, 'Read in '//trim(filename_linear)
  do i = 1, ndata
     read(3,*) klin(i), pklin(i) ! k and linear P(k)
  enddo
  close(3)
  
  xlin = dlog(klin)
  ylin = dlog((D**2.d0)*pklin)
  
  DEALLOCATE (klin,pklin)
  CALL spline(xlin,ylin,ndata,1.d30,1.d30,y2lin)
  return
  
END SUBROUTINE open_pk_linear

!-----------------------------------------------------------------------------

SUBROUTINE close_pk_linear

  DEALLOCATE (xlin,ylin,y2lin)
  return

END SUBROUTINE close_pk_linear

!-----------------------------------------------------------------------------

DOUBLE PRECISION FUNCTION pklinp(klin)
  IMPLICIT none
  DOUBLE PRECISION :: a,b,h,x,y,klin
  x = dlog(klin)
  CALL hunt(xlin,ndata,x,jlo)
  h=xlin(jlo+1)-xlin(jlo)
  a=(xlin(jlo+1)-x)/h
  b=(x-xlin(jlo))/h
  y=a*ylin(jlo)+b*ylin(jlo+1)+((a**3-a)*y2lin(jlo)+(b**3-b)*y2lin(jlo+1))*(h**2)/6.
  pklinp = dexp(y)
  return
END FUNCTION pklinp

!-----------------------------------------------------------------------------

END MODULE pk_linear
